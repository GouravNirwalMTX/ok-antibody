trigger ContactTrigger on Contact (after insert, after update) {
    if(trigger.isInsert && trigger.isAfter){
        ContactTriggerHandler.createAppointmentOnInsert(trigger.new, trigger.oldMap);
        ContactTriggerHandler.sendWelcomeSMS(trigger.new, null);
        
    }
    if(trigger.isUpdate && trigger.isAfter){
        ContactTriggerHandler.updateUsers(trigger.new, trigger.oldMap);//Added by Ritvik on 06/26/2020
        ContactTriggerHandler.createAppointmentOnInsert(trigger.new, trigger.oldMap);
        ContactTriggerHandler.sendWelcomeSMS(trigger.new, trigger.oldMap);
    }
}