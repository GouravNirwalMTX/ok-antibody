trigger MessageTrigger on Messages__c (after insert, after update) {
if(trigger.isInsert && trigger.isAfter){
        MessageTriggerHandler.getMessagesAndChangeStatusOnContact(trigger.new, trigger.oldMap);
    }
    if(trigger.isUpdate && trigger.isAfter){
        MessageTriggerHandler.getMessagesAndChangeStatusOnContact(trigger.new, trigger.oldMap);
    }
}