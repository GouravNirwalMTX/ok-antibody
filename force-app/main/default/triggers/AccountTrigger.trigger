trigger AccountTrigger on Account (before insert, before update) {
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            AccountTriggerHandler.beforeInsert(trigger.new);
        }
        if(Trigger.isUpdate) {
            AccountTriggerHandler.beforeUpdate(trigger.new,trigger.oldMap);
        }
    }
}