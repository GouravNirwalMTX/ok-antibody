trigger AppointmentTrigger on Appointment__c (after insert, after update, after delete, after undelete, before insert, before update) {
    if(trigger.isAfter) {
        if(trigger.isInsert) {
            AppointmentTriggerHandler.afterInsert(trigger.new);
        }
        if(trigger.isUpdate) {
            AppointmentTriggerHandler.afterUpdate(trigger.new,trigger.oldMap);
        }
        if(trigger.isDelete) {
            AppointmentTriggerHandler.afterDelete(trigger.old);
        }
        if(trigger.isUndelete) {
            AppointmentTriggerHandler.afterUndelete(trigger.new);
        }
    }
    if(trigger.isBefore){
        if(trigger.isInsert){
            AppointmentTriggerHandler.beforeInsert(trigger.new);
        }
        if(trigger.isUpdate){
            AppointmentTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
        }
    }
}