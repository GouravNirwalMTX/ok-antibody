import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    registerListener,
    unregisterAllListeners,
    fireEvent
} from "c/pubsub";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getContactInformation from "@salesforce/apex/OkpcContactInformationController.getContactInformation";
import changeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.changeAppointmentStatus";
// import setCurrentStep from "@salesforce/apex/OkpcContactInformationController.setCurrentStep";

export default class OkpcIntakeForm extends LightningElement {
    @track currentStep = 1;
    @track originalStep = 1;
    @track showPersonalInformation = false;
    @track showConsent = true;
    @track showAppointmentPreference = false;
    @track showPriorExposure = false;
    @track showSymptoms = false;
    @track showSpinner = false;
    @track edit = false;
    @track showthankYou = false;
    @track source;
    @track hideStep1and5 = false;
    @track isAutoSchedule = false;
    @track hideStep3 = false; //ad
    @track showDashboard;
    @track testingType = 'Antibody Testing';

    connectedCallback() {
        registerListener("handleSideBarClick", this.handleSideBarClick, this);
        console.log('inside conn');
        this.getStep();
    }

    // setStep(step){
    //     setCurrentStep({currentStep: step})
    //     .then(result =>{
    //         console.log('step saved');
    //     })
    //     .catch(error =>{
    //         console.log('error');
    //     })
    // }

    getStep() {
        getContactInformation()
            .then(result => {
                console.log(JSON.stringify(result));
                if (result.Source__c == 'MMS') {
                    this.hideStep3 = true;
                } else {
                    this.hideStep3 = false;
                }
                if(result.Auto_Scheduled__c == true){
                    console.log('AutoTesing>>' + result.Auto_Scheduled__c)
                    this.hideStep1and5 = true;
                   this.isAutoSchedule = true;

                }else{
                    this.hideStep1and5 = false;
                    this.isAutoSchedule = true;
                }
                if (result.Current_Step__c) {
                    console.log('CurrentStepOnLoad>>'+ result.Current_Step__c)
                    this.originalStep = result.Current_Step__c;
                    this.handleSideBarClick(this.originalStep);
                }
                if (result.Form_Submitted__c == true) {
                    this.showDashboard = true;
                } else {
                    this.showDashboard = false;
                }

            })
            .catch(error => {
                console.log('error');
            })
    }

    clearAllSteps() {
        this.showConsent = false;
        this.showPersonalInformation = false;
        this.showAppointmentPreference = false;
        this.showPriorExposure = false;
        this.showSymptoms = false;
        this.showthankYou = false;
    }
    
    navigateToPersonalInfo(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okpc-consent-page').saveConsent();
        } else {
            if (this.hideStep1and5 == true) {
                this.clearAllSteps();
                this.showPersonalInformation = true;
                this.currentStep = 1;
                this.determineOriginalStep();
            }else{
                this.clearAllSteps();
                this.showPersonalInformation = true;
                this.currentStep = 2;
                this.determineOriginalStep();
            }
           
        }

    }

    navigateToAppointment(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okpc-prior-symptoms').saveContacts();
        }else if(event.target.name == 'savepriorsymptoms'){
            this.template.querySelector('c-okpc-prior-symptoms').saveContacts();
          //  this.showDashboard = true;
        } else {
            if (this.hideStep3 == true) {
                this.clearAllSteps();
                this.showAppointmentPreference = true;
                this.currentStep = 4;
                this.determineOriginalStep();
            } else {
                this.clearAllSteps();
                this.showAppointmentPreference = true;
                this.currentStep = 5;
                this.determineOriginalStep();
            }

        }


    }
    navigateToShowSymptoms(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okpc-contact-info').saveContacts();
        } else {
            
            if (this.hideStep1and5 == true) {
                if (event.target.name == 'next') {
                    this.clearAllSteps();
                    this.currentStep = 2;
                    this.determineOriginalStep();
                    this.showSymptoms = true;
                } else {
                    this.clearAllSteps();
                    this.currentStep = 2;
                    this.determineOriginalStep();
                    this.showSymptoms = true;
                }

            }  else {
                this.clearAllSteps();
                this.currentStep = 3;
                this.determineOriginalStep();
                this.showSymptoms = true;
            }
        }

    }

    navigateToPriorExposure(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okpc-symptoms-page').saveContacts();
        } else {
            if (this.hideStep1and5 == true) {
                this.clearAllSteps();
                this.showPriorExposure = true;
                this.currentStep = 3;
                this.determineOriginalStep();
            } else {
                this.clearAllSteps();
                this.showPriorExposure = true;
                this.currentStep = 4;
                this.determineOriginalStep();
            }

        }
    }

    navigateToConsent() {
        this.clearAllSteps();
        this.currentStep = 1;
        this.determineOriginalStep();
        this.showConsent = true;
    }

    handleEdit() {
        this.edit = true;
        this.template.querySelector('c-okpc-contact-info').editContacts();
    }

    handleCancel() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').handleCancelButton();
    }
    handleSave() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').saveContacts();
    }

    determineOriginalStep() {
        if (this.originalStep >= this.currentStep) {
            this.originalStep = this.originalStep;
        } else {
            this.originalStep = this.currentStep;
        }
    }

    handleSideBarClick(currentStep) {
        console.log('isxode cnj>>' +  currentStep);
        this.currentStep = currentStep;
        this.clearAllSteps();
        if (this.hideStep1and5 == true) {
            switch (currentStep) {
                case 1: {
                    this.showPersonalInformation = true;
                    break;
                }
                case 2: {
                    this.showSymptoms = true;
                    break;
                }
                case 3: {
                    this.showPriorExposure = true;
                    break;
                }
                case 4: {
                    this.showAppointmentPreference = true
                    break;
                }
            }
        } else {
            switch (currentStep) {
                case 1: {
                    this.showConsent = true;
                    break;
                }
                case 2: {
                    this.showPersonalInformation = true;
                    break;
                }
                case 3: {
                    this.showSymptoms = true;
                    break;
                }
                case 4: {
                    this.showPriorExposure = true
                    break;
                }
                case 5: {
                    this.showAppointmentPreference = true
                    break;
                }
            }
        }

    }

    showDashboardDetails(event){
       
        console.log(this.currentStep, this.originalStep);
            this.showthankYou = false;
            this.showDashboard = true;
     
    }
    
    handleSubmit(event) {
        if (event.target.name == 'submit') {
            this.template.querySelector('c-okpc-preference-component').setValues();
            
        }
       
        this.clearAllSteps();
        if (this.hideStep3 == true) {
            this.currentStep = 4;
        } else {
            this.currentStep = 5;
        }
        this.determineOriginalStep();
        
        // this.showthankYou = true;
        
       /* changeAppointmentStatus({
                currentstep: this.currentStep
            })
            .then(result => {
                this.showthankYou = false;
                // window.open('/s/dashboard', '_self');
                this.showDashboard = true;
            })
            .catch(error => {
                console.log('error status change');
            })
            */
    }
    
    handleThankYouBack() {
        // this.navigateToConsent();
        this.showthankYou = false;
        window.open('/s/dashboard', '_self');
    }

    consentSave(event) {
        console.log('Consent=> ', event.detail.status);
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.clearAllSteps();
            this.showPersonalInformation = true;
            this.currentStep = 2;
            this.determineOriginalStep();
        }

    }

    saveSymptoms(event) {
        console.log('Consent=> ', event.detail.status);
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {

            if (this.hideStep1and5 == true) {

                this.clearAllSteps();
                this.currentStep = 3;
                this.determineOriginalStep();
                this.showPriorExposure = true;


            }else{
                this.clearAllSteps();
                this.showPriorExposure = true;
                this.currentStep = 4;
                this.determineOriginalStep();
            }

           
        }
    }

    chekcError(event){
        console.log('Consent=> ', event.detail.status);
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
    }
}

    priorSymptomsSave(event) {
        console.log('Consent=> ', event.detail.status);
        console.log('event name>>' , this.hideStep1and5)
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            if(this.hideStep1and5){
                this.showDashboard = true;
            }else{
                if (this.hideStep3 == true) {
                    this.clearAllSteps();
                    this.showAppointmentPreference = true;
                    this.currentStep = 4;
                    this.determineOriginalStep();
                } else {
                    this.clearAllSteps();
                    this.showAppointmentPreference = true;
                    this.currentStep = 5;
                    this.determineOriginalStep();
                }
            }
          
        }
    }

    saveContacts(event) {
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            if (this.hideStep1and5 == true) {

                this.clearAllSteps();
                this.currentStep = 2;
                this.determineOriginalStep();
                this.showSymptoms = true;


            } else {
                this.clearAllSteps();
                this.currentStep = 3;
                this.determineOriginalStep();
                this.showSymptoms = true;
            }
        }
    }

    editProfile(event){
        console.log('Event>>>' + event);
        console.log('EventDetail>>>' + event.detail);
        console.log('Innnnn');
        if(event.detail == 'editProfile'){
            this.hideStep1and5 = true;
            console.log('Innnnn>>>>>>>>.',this.hideStep1and5);
            if(this.hideStep1and5){
            this.clearAllSteps();
            this.showDashboard = false;
            this.showConsent = false;
            this.currentStep = 1;
            this.originalStep = 1;
            this.determineOriginalStep();
            this.showPersonalInformation = true;
            }else{
            this.clearAllSteps();
            this.hideStep1and5 = true;
            this.showDashboard = false;
            this.currentStep = 1;
            this.originalStep = 1;
            this.determineOriginalStep();
            this.showConsent = true;
            }
            
           
        }
    }

}