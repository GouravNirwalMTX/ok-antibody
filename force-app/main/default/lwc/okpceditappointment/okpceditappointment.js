import { LightningElement, api, track } from 'lwc';
import setPreferredValues from '@salesforce/apex/OkpcContactInformationController.setPreferredValuesAndUpdate';
import getPreferredValues from '@salesforce/apex/OkpcContactInformationController.getPreferredValues';
import {ShowToastEvent} from 'lightning/platformShowToastEvent'

export default class Okpceditappointment extends LightningElement {
    @api siteId;
    @api appointmentId;
    @track todayDate;
    @track preferredDate;
    @track preferredTime;
    @track preferredTimeMin = null;
    @track preferredTimeMax = null;

    connectedCallback() {
        this.todayDate = this.getTodayDate();
        this.getValuesPreferred();
    }

    handleInputChange(event) {
        if (event.target.name == 'preferredDate') {
            this.preferredDate = event.target.value;
        } else if (event.target.name == 'preferredTime') {
            this.preferredTime = event.target.value;
            console.log('tiem=> ', event.target.value);
        }
    }
    updateAppointment() {
        setPreferredValues({
            preferredDate: this.preferredDate,
            preferredTime: this.preferredTime,
            preferredSite: this.siteId,
            appointmentId: this.appointmentId
        })
        .then(result => {
            console.log('saved success');
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'success',
                message: 'Saved successfully',
            }));
            this.handleCloseModal();
        })
        .catch(error => {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Error',
            }));
            console.log('error ', JSON.stringify(error));
        })
    }

    getValuesPreferred() {
        getPreferredValues()
            .then(result => {
                console.log('preferred values: ', JSON.stringify(result));
                this.preferredDate = result.Preferred_Date__c;
                if (result.Preferred_Time__c) {
                    this.preferredTime = this.msToTime(result.Preferred_Time__c);
                }
                if(result.Testing_Site__r.Business_Start_Time__c){
                    this.preferredTimeMin = this.msToTime(result.Testing_Site__r.Business_Start_Time__c);
                }
                if(result.Testing_Site__r.Business_End_Time__c){
                    this.preferredTimeMax = this.msToTime(result.Testing_Site__r.Business_End_Time__c);
                }
            })
            .catch(error => {
                console.log('error in data ', JSON.stringify(error));
            })
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }
}