import {
    LightningElement,
    track,
    api
} from 'lwc';
//import getAntibody from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getAntibodyTestingSite';
import getAppointmentBulk from '@salesforce/apex/okvriusBulkDownload.getAppointmentBulk';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetails';
import getLabId from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getLabId';
//import getBatch from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.retrieveBatch';
import getBatchforTesting from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.retrieveBatchforTestingSite';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkvriuDowloadCmpHolder extends LightningElement {
    @track accountId;
    @track showPaginator = false;
    @track data = [];
    @track labId = '';
    batchId = null;
    @track loadTable = false;
    @track batchoptions;
    @track whereClausetemp = ' id = null';
    @track whereClausedate = ' id = null';
    @track fromDate;
    @track toDate;
    @track todaysDate = '';

    connectedCallback() {
        this.getlabIdFromContact();
      //  this.getTestingSitedata();
        this.getTodaysDate();
        console.log('connectd', this.labId);

    }


    getTodaysDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        this.todaysDate = today;
        console.log('today' + today)
        console.log('today----' + this.todaysDate)
    }

    async handleInputChange(event) {
        await this.handleInputChangeDate(event);
        this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
            element.retrieveRecords();
        });
    }

    handleInputChangeDate(event) {
        if (event.target.name == 'fromDate') {
            this.fromDate = event.target.value;
            console.log(this.fromDate);
        } else if (event.target.name == 'toDate') {
            this.toDate = event.target.value;
            console.log(this.toDate);
        }
        if (this.fromDate && this.toDate) {
            this.whereClausedate = 'Testing_Site__c = \'' + this.labId + '\'AND Results__c = null AND Date_of_Lab_Test_Completion__c >=' + this.fromDate + ' AND Date_of_Lab_Test_Completion__c <= ' + this.toDate;
        }

    }

    getTestingSitedata() {
        getTestingSite({})
            .then(data => {
                if (data.ContactId)
                    this.accountId = data.Contact.AccountId;
                this.showPaginator = true;
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
    }

  
    getlabIdFromContact() {
        getLabId()
            .then(result => {
                this.labId = result.AccountId;
                //this.getAntibodyList();
              
                console.log(this.labId);
               
            })
            .catch(error => {
                console.error(error);
            })
    }



    getAntibodyList() {
        if (!this.isValid()) {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter correct dates',
            }));
            return;
        }
        this.data = [];
        console.log('labId: ', this.labId);

        var sessionKey = sessionStorage.getItem('accId');
            if(sessionKey)
                this.labId = sessionKey;

        getAppointmentBulk({
                labId: this.labId,
                toDate: this.toDate,
                fromDate: this.fromDate
            })
            .then(result => {
                console.log(JSON.stringify(result));
                if (result == null) {

                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'warning',
                        message: 'No record present for the provided range of dates.Please select the correct range',
                    }));
                    return;

                }
                // this.data = result;
                result.forEach(el => {
                    let temparr = {};
                    temparr.Id = el.patientId;
                    temparr.firstName = el.firstName;
                    temparr.lastName = el.lastName;
                    temparr.email = el.email;
                    temparr.mobileNumber = el.mobileNumber;
                    temparr.appointmentStatus = el.appointmentStatus;
                    temparr.appointmentDate = el.appointmentDate;
                    temparr.appointmentTime = el.appointmentTime;
                    temparr.cancellationReason = el.cancellationReason;
                    temparr.appointmentCompleteDate = el.appointmentCompleteDate;
                    temparr.appointmentScheduleGenerationDate = el.appointmentScheduleGenerationDate;
                    temparr.isSpanishTranslator = el.isSpanishTranslator;
                    temparr.consultReason = el.consultReason;
                    temparr.consultReasonRecordtype = el.consultReasonRecordtype;
                    this.data.push(temparr);
                })
                if (this.data) {
                    console.log('this.data: ', JSON.stringify(this.data));
                    this.downloadCSVFile();
                }
            })
            .catch(error => {
                this.error = error;
                this.data = undefined;
            });
    }

    // create csv file
    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        });

        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);

        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;

        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }

       // Creating anchor element to download
       let downloadElement = document.createElement('a');
       var universalBOM = "\uFEFF";
       // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
       downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM+csvString);
       downloadElement.target = '_self';
       // CSV File Name
       downloadElement.download = 'ATData.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
    }
}