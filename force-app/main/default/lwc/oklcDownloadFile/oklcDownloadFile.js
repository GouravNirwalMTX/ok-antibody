import { LightningElement,track } from 'lwc';
import getAntibodyBulk from '@salesforce/apex/oklcAntibodyBulkDownload.getAntibodyBulk';
import getLabId from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getLabId';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OklcUploadFiletoCreateAntibody extends LightningElement {
    filesUploaded = [] ;
    @track fromDate;
    @track toDate;
    @track labId ='';
    @track todaysDate ='';
    @track showSpinner = false;
    @track showFileName = false;

     
    connectedCallback(){
        this.getlabIdFromContact();
        console.log('connectd',this.labId);
       this.getTodaysDate();
    }

    getTodaysDate(){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        this.todaysDate = today;
        console.log('today' + today)
        console.log('today----' + this.todaysDate)
    }

    getlabIdFromContact(){
        getLabId()
        .then(result =>{
            this.labId = result.AccountId;
            console.log('^^^^ ', this.labId);
        })
        .catch(error =>{
            console.error(error);
        })
    }

    handleInputChange(event){
        if (event.target.name == 'fromDate'){
            this.fromDate = event.target.value;
            console.log(this.fromDate);
        }else if(event.target.name == 'toDate'){
            this.toDate = event.target.value;
            console.log(this.toDate);
        }
       
    }
    
    getAntibodyList() {
        if(!this.isValid()){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter correct dates',
            }));
            return;
        }
        this.data = [];
        console.log('labId: ***** ', this.labId);
       
        if(this.labId == '' || typeof this.labId == 'undefined'){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'No Lab is associated to the account.',
            }));
            return;
        }
      
        getAntibodyBulk({labId: this.labId,
                         toDate: this.toDate,
                         fromDate: this.fromDate
                         })
            .then(result => {
                console.log(JSON.stringify(result));
                if(result == null){
                    
                        this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: 'warning',
                            message: 'No record present for the provided range of dates.Please select the correct range',
                        }));
                        return;
                    
                }
                result.forEach(el =>{
                    let temparr = {};
                    //temparr.ReportingFacility = el.testingSiteName;
                    temparr.patientId = el.patientId;
                    temparr.Name = el.firstName +' '+ el.lastname;
                    temparr.dateOfBirth = el.dateOfBirth;
                    //temparr.PatientAddress = el.patientAddress + ' ' + el.patientAddress2;
                    //temparr.PatientState = el.patientState;
                    //temparr.PatientPhone = el.patientPhone;
                    temparr.TestNumber = el.TestNumber;
                    //temparr.OrderProvider = el.orderingFacilty;
                    //temparr.dateOfSpecimenCollection = el.dateOfSpecimenCollectionFormula;
                    //temparr.sourceOfSpecimen = el.sourceOfSpecimen;
                    //temparr.testPerformedDescription = el.testPerformedDescription;
                   // temparr.testResultDescription = el.testResultDescription;
                    temparr.result = el.result;
                    temparr.dateOfLabTestCompletion = el.dateOfLabTestCompletion;
                    temparr.TestingLabName = el.testingLabName;
                  //  temparr.gender = el.gender;
                  //  temparr.organizationName = el.organizationName;
                 //   temparr.barCoderNumber = el.barCoderNumber;
                 //   temparr.batchId = el.batchId;
                 //   temparr.containerBarCode = el.containerBarCode;
                    
                    this.data.push(temparr);
                })
                if(this.data){
                    console.log('this.data: ', JSON.stringify(this.data));
                    this.downloadCSVFile();
                }
            })
            .catch(error => {
                console.log('erro>>', JSON.stringify(error));
                this.error = error;
                this.data = undefined;
            });
    }

    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        });

        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);
        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;

        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }

        // Creating anchor element to download
        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'Patient Sample Results.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
    }
}