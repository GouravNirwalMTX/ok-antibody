import { LightningElement, track, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { createRecord } from 'lightning/uiRecordApi';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import createAntibodyContact from '@salesforce/apex/OK_SelfRegistration.createContactFromTestingSite';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import RACE_FIELD from '@salesforce/schema/Contact.Race__c';
import GENDER_FIELD from '@salesforce/schema/Contact.Gender__c';


export default class OkTestingCreateAntibodyContact extends NavigationMixin(LightningElement) {

    // label = {
    //     contactType
    // }

    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track race = '';
    @track gender = '';
    @track emailOpt = true;
    @track smsOpt = false;
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '.vrcpuser'
    @track portal = [{ label: 'Citizen (COVID)', value: "COVID" }, { label: 'Citizen (Antibody)', value: 'Antibody' }];

    @api account;
    @track contactType;
    @track raceOptions;
    @track genderOptions;
    @track scheduled = true;
    @track todayDate;
    @track dob;
    @track phoneRequired = false;
    @track isCovid = false;
    @track isAntibody = false;


    get contactTypeOptions() {
        return [
            { label: '--None--', value: '' },
            { label: 'COVID Citizen', value: 'Citizen_COVID' },
            { label: 'Antibody Citizen', value: 'Citizen' },
        ];
    }
    @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: RACE_FIELD})
     setRacePicklistOptions({error, data}) {
        if (data) {
        // Apparently combobox doesn't like it if you dont supply any options at all.
        // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
        this.raceOptions = data.values;
        } else if (error) {
        console.log('777 ', error);
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: GENDER_FIELD})
        setGenderPicklistOptions({error, data}) {
            if (data) {
            // Apparently combobox doesn't like it if you dont supply any options at all.
            // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
            this.genderOptions = data.values;
            } else if (error) {
            console.log(error);
            }
        }
    
    handleChange(event){
        var name = event.target.name;
        console.log(' name   ' + name);
       // alert(' value   ' + event.target.value);
        if(name == 'firstName')
         this.firstName = event.target.value;
        else if(name == 'lastname')
         this.lastName = event.target.value;
        else if(name == 'phone')
         this.phone = event.target.value;
        else if(name == 'email')
         this.email = event.target.value;
        else if(name == 'contactType'){
            this.contactType = event.target.value;
            if(this.contactType == 'Citizen'){
                this.isAntibody = true;
                this.isCovid = false;
            }else{
                this.isCovid = true;
                this.isAntibody = false;
            }
        }else if(name == 'race')
            this.race = event.target.value;
         else if(name == 'gender')
         this.gender = event.target.value;
         else if(name == 'smsOpt'){
            this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
            if(this.smsOpt)
            this.phoneRequired = true;
            else
            this.phoneRequired = false;
         }
         
         else if(name == 'emailOpt')
         this.emailOpt = this.template.querySelector('[data-field="email"]').checked;
         else if(name == 'scheduled')
         this.scheduled = this.template.querySelector('[data-field="scheduled"]').checked;  
         else if (name == 'dob')
            this.dob = event.target.value;       

    }

    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    connectedCallback() {
        this.todayDate = this.getTodayDate();
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    @api
    createContact() {
        if(!this.isValid()){
            return;
        }
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        this.firstName = this.firstName.trim();
        this.lastName = this.lastName.trim();
       
        if (this.firstName == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
        }
        if(!this.validateName(this.firstName)){
            this.errorMessage = 'First Name cannot contain any numbers or special characters';
            this.showError = true;
        }
        
        if (this.lastName == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
        }
        if(!this.validateName(this.lastName)){
            this.errorMessage = 'Last Name cannot contain any numbers or special characters';
            this.showError = true;
        }
        if (this.email.trim() == '' || typeof this.email == 'undefined') {
            this.errorMessage = 'Please provide Email';
            this.showError = true;
        }
        if (!this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
        }
        if(this.smsOpt){
            if(this.phone == '' || typeof this.phone == 'undefined'){
                this.errorMessage = 'Please provide Mobile Number';
                this.showError = true;
            }
        }
        if ((this.phone != '' && typeof this.phone != 'undefined') && !this.validatePhone(this.phone)) {
            this.errorMessage = 'Phone number cannot contain any special characters';
            this.showError = true;
        }

        if(this.showError){
            console.log('hiii');
            let data = {
                errorMsg : this.errorMessage,
                showError : true
            };
            this.dispatchEvent(new CustomEvent('showerror', { detail : data }));
        }
    
        console.log('bjfj')
        let cont = { 'sobjectType': 'Contact' };
        cont.FirstName = this.firstName;
        cont.LastName = this.lastName;
        cont.MobilePhone = this.phone;
        cont.Email = this.email;
        cont.Auto_Scheduled__c = this.scheduled;
        cont.Opt_out_for_SMS_Updates__c = !(this.smsOpt);
        cont.HasOptedOutOfEmail = !(this.emailOpt);
        cont.Birthdate = this.dob;
        cont.Gender__c = this.gender;
        cont.Race__c =  this.race;
        cont.Testing_Site__c = this.account.Id;
        createAntibodyContact({c : cont, recordType : 'Citizen'})
            .then(result => {
                if (result.type != 'success') {
                    //alert('fh')
                    this.showError = true;
                    this.errorMessage = result.message;
                    let data = {
                        errorMsg : this.errorMessage,
                        showError : true
                    };
                    this.dispatchEvent(new CustomEvent('showError', { detail : data }));
                } else if (result.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = result.message;
                    console.log('navigateToForgotPassword ' + result.message);
                    let data = {
                        successMsg : this.thankYouMessage
                    };
                    this.dispatchEvent(new CustomEvent('success', { detail : data }));
                }
                
            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                var msg = JSON.stringify(error.body.message);
                // this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }



    validateEmail(email) {
        var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
        //var regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validateName(name){
        var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
        var isValid = name.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validatePhone(mobile){
        var regExpEmail = /^[0-9]*$/;
        var isValid = mobile.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    isValid() {
        let valid = true;
       let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        let isAllValid2 = [
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        if(isAllValid == true && isAllValid2 == true){
            valid = true;
        }else{
            valid = false;
        }
        return valid;
    }
}