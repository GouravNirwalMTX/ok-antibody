import {
    LightningElement,
    track,
    api
} from 'lwc';
import getSymptoms from "@salesforce/apex/OkpcContactInformationController.getSymptoms";
import saveSymptoms from "@salesforce/apex/OkpcContactInformationController.saveSymptoms";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcSymptomsPage extends LightningElement {
    @track dataObj = {};
    @api contactId;
    @track showSpinner = false; // defaults to false
    @track showDateFields = false; //defaults to false
    @api currentStep;
    @track testingType;
    @track showhighRisk;

    connectedCallback() {
        console.log('inside conn symptoms');
        console.log('Contact Id in Symptoms>>',this.contactId)
        this.dataObj.contactId = this.contactId;
       // this.getContact();
    }

    

    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
        ];
    }

    get YesNoUnKnwOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
            {
                label: "Unknown",
                value: "Unknown"
            },
        ];
    }

    handleInputChange(event) {
        this.dataObj[event.target.name] = event.target.value;
        console.log('Object data>>>' + JSON.stringify(this.dataObj));
     /*   if (this.dataObj.whereYouLive == 'Yes') {
            this.showhighRisk = true;
        } else {
            this.showhighRisk = false;
            this.dataObj.highRisk = null;
        } */
    }

    @api
    sendSymptomsDetailsToDashboard(){
        if (this.dataObj.ifSymptomatic == '' || typeof this.dataObj.ifSymptomatic == 'undefined' || this.dataObj.whereYouLive == '' || typeof this.dataObj.whereYouLive == 'undefined'  || this.dataObj.inContactWithpositiveCase == '' || typeof this.dataObj.inContactWithpositiveCase == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
        
        if(this.dataObj.ifSymptomatic == 'Yes' || this.dataObj.whereYouLive == 'Yes' || this.dataObj.inContactWithpositiveCase == 'Yes'){
            // this.testingType = 'Rapid Testing';
            this.dataObj.testingType = 'Rapid Testing';
     }else{
        // this.testingType = 'PCR Testing';
        this.dataObj.testingType = 'PCR Testing';
     }
        let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.appointmentDetails = this.dataObj;
                this.sendEventToParent(tempObj);
    }

 /*   getContact() {
        this.showSpinner = true;
        getSymptoms({
            testingType: 'Covid'
                     })
            .then(result => {
                console.log('symptoms=>', JSON.stringify(result));
                this.dataObj.ifSymptomatic = result.ifSymptomatic;
                this.dataObj.whereYouLive = result.whereYouLive;
               // this.dataObj.highRisk = result.highRisk;
                this.dataObj.inContactWithpositiveCase = result.inContactWithpositiveCase;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                // if (this.dataObj.whereYouLive == 'Yes') {
                //     this.showhighRisk = true;
                // } else {
                //     this.showhighRisk = false;
                //     this.dataObj.highRisk = null;
                // } 
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @api
    saveContacts() {
        this.dataObj.contactId = this.contactId;
        if (this.dataObj.ifSymptomatic == '' || typeof this.dataObj.ifSymptomatic == 'undefined' || this.dataObj.whereYouLive == '' || typeof this.dataObj.whereYouLive == 'undefined'  || this.dataObj.inContactWithpositiveCase == '' || typeof this.dataObj.inContactWithpositiveCase == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
        if(this.dataObj.ifSymptomatic == 'Yes' || this.dataObj.whereYouLive == 'Yes' || this.dataObj.inContactWithpositiveCase == 'Yes'){
                this.testingType = 'Rapid Testing';
        }else{
            this.testingType = 'PCR Testing';
        }
        this.showSpinner = true;
        saveSymptoms({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep,
                testingType: this.testingType
            })
            .then(result => {
                console.log('Successfully Saved');
                // this.extraWrapper = {...this.dataObj};
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "3";
                this.sendEventToParent(tempObj);
                
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                this.showSpinner = false;
            })
    }
      */
    sendEventToParent(obj) {

        const selectedEvent = new CustomEvent("symptomssave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }
}