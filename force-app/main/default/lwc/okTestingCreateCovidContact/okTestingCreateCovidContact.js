import { LightningElement, track, wire, api } from 'lwc';
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.savePatientDetails";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";
import createAntibodyContact from '@salesforce/apex/OK_SelfRegistration.createContactFromTestingSite';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import RACE_FIELD from '@salesforce/schema/Contact.Race__c';
import GENDER_FIELD from '@salesforce/schema/Contact.Gender__c';


export default class OkTestingCreateCovidContact extends LightningElement {
    @track dataObj = {};
    @track appointmentData = {};
    @track extraWrapper = {};
    @api contactId;
    @track showSpinner = false;
    @track disableInputs = true;
    @track testingType;
    @track diseaseSymptoms = [];
    @track EmployedinhighRisk= [];
    @track PatientResidencyType = [];
    @track pregnantOptions = [];
    @track selectedValues = [];
    @track age;
    @track isSymptomatic = false;
    @api currentStep;
    @track todayDate;
    @track showHighRisk = false;
    @track secondRowRequired = false;
    @track thirdRowRequired = false;
    @track dynamicCSS2ndRow = 'col-sm-8';
    @track dynamicCSS3nrdRow = 'col-sm-4';
    @api   isDependent = false;
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track race = '';
    @track gender = '';
    @track emailOpt = true;
    @track smsOpt = false;
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '.vrcpuser'
    @track portal = [{ label: 'Citizen (COVID)', value: "COVID" }, { label: 'Citizen (Antibody)', value: 'Antibody' }];

    @api account;
    @track contactType;
    @track raceOptions;
    @track genderOptions;
    @track scheduled = true;
    @track todayDate;
    @track dob;
    @track phoneRequired = false;
    @track isCovid = false;
    @track isAntibody = false;


    get contactTypeOptions() {
        return [
            { label: '--None--', value: '' },
            { label: 'COVID Citizen', value: 'Citizen_COVID' },
            { label: 'Antibody Citizen', value: 'Citizen' },
        ];
    }


    @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: RACE_FIELD})
     setRacePicklistOptions({error, data}) {
        if (data) {
        // Apparently combobox doesn't like it if you dont supply any options at all.
        // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
        this.raceOptions = data.values;
        } else if (error) {
        console.log('777 ', error);
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: GENDER_FIELD})
        setGenderPicklistOptions({error, data}) {
            if (data) {
            // Apparently combobox doesn't like it if you dont supply any options at all.
            // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
            this.genderOptions = data.values;
            } else if (error) {
            console.log(error);
            }
        }

    customImage = nysdohResource;

    
    handleChange(event){
        var name = event.target.name;
        console.log(' name   ' + name);
       // alert(' value   ' + event.target.value);
        if(name == 'firstName')
         this.firstName = event.target.value;
        else if(name == 'lastname')
         this.lastName = event.target.value;
        else if(name == 'phone')
         this.phone = event.target.value;
        else if(name == 'email')
         this.email = event.target.value;
        else if(name == 'contactType'){
            this.contactType = event.target.value;
            if(this.contactType == 'Citizen'){
                this.isAntibody = true;
                this.isCovid = false;
            }else{
                this.isCovid = true;
                this.isAntibody = false;
            }
        }else if(name == 'race')
            this.race = event.target.value;
         else if(name == 'gender')
         this.gender = event.target.value;
         else if(name == 'smsOpt'){
            this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
            if(this.smsOpt)
            this.phoneRequired = true;
            else
            this.phoneRequired = false;
         }
         
         else if(name == 'emailOpt')
         this.emailOpt = this.template.querySelector('[data-field="email"]').checked;
         else if(name == 'scheduled')
         this.scheduled = this.template.querySelector('[data-field="scheduled"]').checked;  
         else if (name == 'dob')
            this.dob = event.target.value;       

    }

    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    @api
    createContact() {
        alert('fbjf');
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        this.dataObj.firstName = this.dataObj.firstName.trim();
        this.dataObj.lastName = this.dataObj.lastName.trim();
       
        if (this.dataObj.firstName == '' || typeof this.dataObj.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
        }
        if(!this.validateName(this.dataObj.firstName)){
            this.errorMessage = 'First Name cannot contain any numbers or special characters';
            this.showError = true;
        }
        
        if (this.dataObj.lastName == '' || typeof this.dataObj.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
        }
        if(!this.validateName(this.dataObj.lastName)){
            this.errorMessage = 'Last Name cannot contain any numbers or special characters';
            this.showError = true;
        }
        if (this.dataObj.email.trim() == '' || typeof this.dataObj.email == 'undefined') {
            this.errorMessage = 'Please provide Email';
            this.showError = true;
        }
        if (!this.validateEmail(this.dataObj.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
        }
        if(this.dataObj.smsOpt){
            if(this.dataObj.phone == '' || typeof this.dataObj.phone == 'undefined'){
                this.errorMessage = 'Please provide Mobile Number';
                this.showError = true;
            }
        }
        if ((this.dataObj.phone != '' && typeof this.phone != 'undefined') && !this.validatePhone(this.dataObj.phone)) {
            this.errorMessage = 'Phone number cannot contain any special characters';
            this.showError = true;
        }

        if(this.showError){
            console.log('hiii');
            let data = {
                errorMsg : this.errorMessage,
                showError : true
            };
            this.dispatchEvent(new CustomEvent('showerror', { detail : data }));
        }
    
        console.log('bjfj')
        if (!this.isValid()) {
            let data = {
                errorMsg : "Please complete the required fields.",
                showError : true
            };
            this.dispatchEvent(new CustomEvent('showerror', { detail : data }));
            return;
        }
       if(this.dataObj.ifSymptomatic == 'Yes' || this.dataObj.whereYouLive == 'Yes' || this.dataObj.inContactWithpositiveCase == 'Yes'){
               // this.testingType = 'Rapid Testing';
               this.dataObj.testingType = 'Rapid Testing';
        }else{
           // this.testingType = 'PCR Testing';
           this.dataObj.testingType = 'PCR Testing';
        }
        console.log('final app data>>>' , JSON.stringify(this.dataObj));
        
       /* let cont = { 'sobjectType': 'Contact' };
        cont.FirstName = this.firstName;
        cont.LastName = this.lastName;
        cont.MobilePhone = this.phone;
        cont.Email = this.email;
        cont.Auto_Scheduled__c = this.scheduled;
        cont.Opt_out_for_SMS_Updates__c = !(this.smsOpt);
        cont.HasOptedOutOfEmail = !(this.emailOpt);
        cont.Birthdate = this.dob;
        cont.Gender__c = this.gender;
        cont.Race__c =  this.race;
        cont.Testing_Site__c = this.account.Id;*/ 

        createAntibodyContact({jsonContact : this.dataObj, recordType : 'Citizen_Covid'})
            .then(result => {
                if (result.type != 'success') {
                    //alert('fh')
                    this.showError = true;
                    this.errorMessage = result.message;
                    let data = {
                        errorMsg : this.errorMessage,
                        showError : true
                    };
                    this.dispatchEvent(new CustomEvent('showError', { detail : data }));
                } else if (result.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = result.message;
                    console.log('navigateToForgotPassword ' + result.message);
                    let data = {
                        successMsg : this.thankYouMessage
                    };
                    this.dispatchEvent(new CustomEvent('success', { detail : data }));
                }
                
            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                var msg = JSON.stringify(error.body.message);
                // this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }

    customImage = nysdohResource;


    validateEmail(email) {
        var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
        //var regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validateName(name){
        var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
        var isValid = name.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validatePhone(mobile){
        var regExpEmail = /^[0-9]*$/;
        var isValid = mobile.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }
    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
        ];
    }

    get YesNoUnKnwOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
            {
                label: "Unknown",
                value: "Unknown"
            },
        ];
    }
    connectedCallback() {
       
        fetchPicklist({
                objectName: "Contact",
                fieldName: "Disease_symptoms__c"
            })
            .then((result) => {
                this.diseaseSymptoms = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Employed_in_high_risk_setting__c"
            })
            .then((result) => {
                this.EmployedinhighRisk = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Patient_Residency_Type__c"
            })
            .then((result) => {
                this.PatientResidencyType = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Pregnant__c"
            })
            .then((result) => {
                this.pregnantOptions = result;
            })
            .catch((error) => {
                let message = error.message || error.body.message;
                console.log(message);
            });

           this.todayDate = this.getTodayDate();
           console.log('ToDayDate>>' , this.todayDate)
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    handleInputChange(event) {
        this.dataObj[event.target.name] = event.target.value;
        if(event.target.name == 'diseaseSymptoms'){
            var tempArray = this.dataObj.diseaseSymptoms;
            console.log('>>>>>temparray>>' + JSON.stringify(tempArray));
           var x = tempArray.join(';');
            console.log('<<<<>>>',JSON.stringify(x));
            this.dataObj.diseaseSymptoms = x;
        }
        console.log(JSON.stringify(this.dataObj));
    }

    handleAppointmentInputChange(event) {
        this.appointmentData[event.target.name] = event.target.value;
        if (this.appointmentData.ifSymptomatic == 'Yes') {
            this.thirdRowRequired = true;
            this.isSymptomatic = true;
            this.dynamicCSS3nrdRow = 'col-sm-4';
        } else {
            this.dataObj.diseaseSymptoms = "";
            this.selectedValues = [];
            this.thirdRowRequired = false;
            this.isSymptomatic = false;
            this.dataObj.illnessDate = null;
            this.dynamicCSS3nrdRow = 'col-sm-4';
        }
        if(this.appointmentData.whereYouLive == 'Yes'){
            this.showHighRisk = true;
            this.secondRowRequired = true;
            this.dynamicCSS2ndRow = 'col-sm-8'
        }else{
            this.showHighRisk = false;
            this.secondRowRequired = false;
            this.dataObj.empHighRisk = null;
            this.dynamicCSS2ndRow = 'col-sm-12'
        }
        console.log(JSON.stringify(this.appointmentData));
    }

    @api
    saveContacts() {
        console.log('data=>', JSON.stringify(this.dataObj));
       // this.dataObj.contactId = this.contactId;
       
        if (!this.isValid()) {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
            if(this.appointmentData.ifSymptomatic == 'Yes' || this.appointmentData.whereYouLive == 'Yes' || this.appointmentData.inContactWithpositiveCase == 'Yes'){
               // this.testingType = 'Rapid Testing';
               this.appointmentData.testingType = 'Rapid Testing';
        }else{
           // this.testingType = 'PCR Testing';
           this.appointmentData.testingType = 'PCR Testing';
        }
        console.log('final app data>>>' , JSON.stringify(this.appointmentData));
        console.log('final app data>>>patient History b4 save' , JSON.stringify(this.dataObj));
        saveContactDetails({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep,
                testingType: this.testingType,
                stepName : 'patientHistory',
                isDependent : this.isDependent
            })
            .then(result => {
                console.log('Successfully Saved');
                this.disableInputs = true;
                // this.extraWrapper = {...this.dataObj};
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
            })
            
    }

    @api
    handleCancelButton() {
        this.disableInputs = true;
        // this.dataObj = {...this.extraWrapper};
    }

    sendEventToParent(obj) {

        const selectedEvent = new CustomEvent("contactsave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    validateDate(date) {
        var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var isValid = date.match(regExpPhone);
        console.log("isValid", isValid);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a valid format for date";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
        }
        return isValid;
    }

    isValid() {
        let valid = true;
       let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        
        let isAllValid2 = [
            ...this.template.querySelectorAll("lightning-radio-group")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        let isAllValid3 = [
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        let isAllValid4 = [
            ...this.template.querySelectorAll("lightning-dual-listbox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true); 
       if(isAllValid && isAllValid2 && isAllValid3 && isAllValid4){
            valid = true;
        }else{
            valid = false;
        }
       // valid = isAllValid2
        return valid;
    }

    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }
    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;

    }
    

}