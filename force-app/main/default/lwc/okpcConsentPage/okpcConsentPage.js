import {
    LightningElement,
    track,
    api
} from 'lwc';
import getConsent from "@salesforce/apex/OkpcContactInformationController.getConsent";
import setConsent from "@salesforce/apex/OkpcContactInformationController.setConsent";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcConsentPage extends LightningElement {
    @track consentvalue = false;
    @track showSpinner = false;
    @api currentStep;
    

    connectedCallback() {
        this.getContact();
        console.log('current step: ', this.currentStep);

    }

    // handleChange(e) {
    //     this.value = e.detail.value;
    //     this.sms = this.value.includes("SMS");
    //     this.email = this.value.includes("Email");
    // }

    getContact() {
        this.showSpinner = true;
        getConsent()
            .then(result => {
                this.consentvalue = result.Consent__c;
                // this.sms = result.Opt_in_for_SMS_Updates__c;
                // this.email = !result.HasOptedOutOfEmail;
                // console.log("consnet=>", JSON.stringify(result));
                // if(this.sms == true){
                //     this.value.push('SMS');
                // }
                // if(this.email == true){
                //     this.value.push('Email');
                // }
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @api
    saveConsent() {
        if (this.consentvalue != true) {
            console.log("Please check the consent");
            let tempObj = {};
            tempObj.message = "Please select consent to move forward";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }


        this.showSpinner = true;
        setConsent({
                consent: this.consentvalue,
                currentstep : this.currentStep
            })
            .then(result => {
                console.log('success saving consnt');
                this.showSpinner = false;
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "1";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log("error =>", JSON.stringify(error));
                this.showSpinner = false;
                
            })
    }


    handleConsentChange(event) {
        console.log(event.target.checked);
        this.consentvalue = event.target.checked;
    }

    sendEventToParent(obj) {
        
        const selectedEvent = new CustomEvent("consentsave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    clickOfLink(){
        window.open("https://www.ok.gov/health/Organization/Health_Insurance_Portability_and_Accountability_Act_(HIPAA)/Privacy_Notice.html", "_blank");
    }
}