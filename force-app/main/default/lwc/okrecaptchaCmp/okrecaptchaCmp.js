import { LightningElement, track, api } from 'lwc';
import reCAPTCHA from '@salesforce/label/c.ok_reCAPTCHA_VF_URL';

export default class OkrecaptchaCmp extends LightningElement {
    @track _url= reCAPTCHA;
}