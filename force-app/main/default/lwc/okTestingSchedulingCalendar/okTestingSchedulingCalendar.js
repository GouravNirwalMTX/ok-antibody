import { LightningElement, track ,api} from 'lwc';
import getAppintments from '@salesforce/apex/DC_ScheduleAppointment.getAccountSlots';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class OkTestingSchedulingCalendar extends LightningElement {
    @api preferredDate;
    @api preferredTime;
    @track recid;
    @track showSpinner;
    @track startTime;
    @track endTime;
    @track data = {};
    @track appointmentFrequency = 30;
    @api hidePrefferedTime = false;
    timeSlots = [];
    @track systemTime;
    @track todaysDate;
    @track todaysDateWithTime;
    @track currentYear;
    @api testingType = '';
    @api isAntibody;
   
    currentSelRecordId = '';

    connectedCallback() {
        var now = new Date();
        now.setHours(now.getHours());
        var isPM = now.getHours() >= 12;
        var hr = now.getHours() ;
        var min = now.getMinutes() ;
        var isMidday = now.getHours() == 12;
        var time = [now.getHours() - (isPM && !isMidday ? 12 : 0),
                (now.getMinutes() < 10 ? '0'+ now.getMinutes() : now.getMinutes())
            ].join(':') +
            (isPM ? ' PM' : ' AM');
            this.systemTime = time;
            var dd = String(now.getDate()).padStart(2, '0');
            var mm = String(now.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = now.getFullYear();
           this.currentYear = yyyy;
          
            this.todaysDate = new Date(yyyy,mm,dd).getTime();
            this.todaysDateWithTime = new Date(yyyy,mm,dd,hr,min).getTime();
          
    }

    @api
    get accountid(){
        return this.recid;
    }
    set accountid(val){
       this.recid = val;
       this.callApex();
    }

    callApex(){
        getBusinessTimeOnAccount({selectedAccountId : this.recid})
        .then(result =>{
            if(result != null){
                this.startTime = this.msToTime(result.Business_Start_Time__c);
                this.endTime = this.msToTime(result.Business_End_Time__c);
                if(result.Appointment_Frequency__c){
                    this.appointmentFrequency = result.Appointment_Frequency__c;
                }
            }
            this.retrieveRecords();
        })
        .catch(result =>{
            //
        })
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    
    retrieveRecords() {
        this.showSpinner = true;
        getAppintments({ accountId: this.recid, testingType : this.testingType}).then(result => {
            this.createCalendar(result);
        })
        .catch(error => {
            this.error = error;
        });
    }

    // createBulkSlot() {
    //     let startTime = this.startTime;
    //     let endTime = this.endTime;

    //     let startObj = this.getFormattedTime(startTime);
    //     let endObj = this.getFormattedTime(endTime);

    //     let displayTime = parseInt(JSON.stringify(startObj.hourT));

    //     while(startObj.hourT <= endObj.hourT ) {
            
    //         if(startObj.hourT <= 11) {
    //             this.bulkSlot(displayTime, 'AM');
    //         } else {
    //             this.bulkSlot(displayTime, 'PM');
    //         }
            
    //         displayTime = displayTime < 12 ? displayTime + 1 : 1;
    //         startObj.hourT += 1;
    //     }
    // }

    createTimeSlots() {
        let minhr = 0;
        let maxhr = 23;
        let minmnt = 0;
        let maxmnt = 60;
        if(this.startTime) {
            let timeObj = this.getFormattedTime(this.startTime);
            minhr = timeObj.hourT;
            minmnt = timeObj.minT;
        }
        if(this.endTime) {
            let timeObj = this.getFormattedTime(this.endTime);
            maxmnt = timeObj.minT;
            maxhr = timeObj.hourT;
        }

        let timeInterval = this.appointmentFrequency;
        for(let hr = parseInt(minhr); hr <= parseInt(maxhr); hr++) {
            for(let mnt = 0; mnt < 60; mnt = mnt + parseInt(timeInterval)) {
                if(((parseInt(minmnt) <= parseInt(mnt) || parseInt(hr) > parseInt(minhr)) && parseInt(hr) != parseInt(maxhr)) || 
                    (parseInt(hr) == parseInt(maxhr) && parseInt(maxmnt) > parseInt(mnt))) {
                    let timeStr = this.generateTimeOption(hr,mnt);
                    this.timeSlots.push(timeStr);    
                    this.addSlots(timeStr);
                }
            }
        }
    }

    generateTimeOption(hr,mnt) {
        let radian = hr < 12 ? 'AM' : 'PM';
        let timehr = hr <= 12 ? (hr == 0 ? 12 : hr) : hr - 12;
        let minute = mnt < 10 ? '0' + mnt : '' +  mnt;
        return (timehr < 10 ? '0' + timehr : timehr) + ':' + minute + ' ' + radian;
    }

    getFormattedTime(timeInstance) {
        let hourT = parseInt(timeInstance.substr(0,2));
        let minT = parseInt(timeInstance.substr(3,2));
        let amPM = hourT <= 11 ? 'AM' : 'PM';
        return {hourT: hourT, minT: minT, amPM: amPM};
    }

    createCalendar(result) {

        this.data = {};
        this.data.rows = [];
        this.data.dates = result.dateHeaderList;
        this.data.weeks = result.weekHeaderList;
        
        this.createTimeSlots();
        
        let columnCount = 0;
        for (var m in result.records){
            let recds = result.records[m];
            let rowCount = 0;
            this.timeSlots.forEach(element => {
                if(typeof recds[element] != 'undefined' && recds[element] != null && recds[element].length > 0) {
                    this.data.rows[rowCount].cols[columnCount].isAvailable = true;
                    this.data.rows[rowCount].cols[columnCount].availableSlots = recds[element].length;
                    this.data.rows[rowCount].cols[columnCount].slotId = recds[element][0].Id;
                }
                rowCount++;
            });
            columnCount++;
        }
      //  console.log('final calender >>>' , JSON.stringify(this.data));
        this.showSpinner = false;
    }

    bulkSlot(time, amPM){
        for( let i = 0 ; i< 2 ; i++){
            let timeStr = time+':'+ (i*30)+ (i==0?'0':'') +' '+amPM;
            if(time < 10) 
                timeStr = '0' + timeStr;
            this.timeSlots.push(timeStr);
        for(let i = 0 ; i < this.data.dates.length;i++)    
            this.addSlots( timeStr);
        }
    }

    addSlots( timeStr ){
        let cols = [];
        var numOfSlots = this.data.dates.length;

        for(var i= 0; i< numOfSlots ; i++){
            cols.push({
                availableSlots : 0,
                bookedSlots: 0,
                recId : '',
                slotDate: this.data.dates[i],
                newRecord: true,
                isAvailable : false,
                isSelected : false, 
                slotId : this.makeid(18)
            });
        }
        /*
        let cols = [];
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false, 
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });
        cols.push({
            availableSlots : 0,
            isAvailable : false,
            isSelected : false,
            slotId : this.makeid(18)
        });*/

        this.data.rows.push({
            timeSt : timeStr,
            cols : cols
        });
    }

    handleSelectedSlots(event){
        this.currentSelRecordId = event.target.dataset.recid;
        let tempSlotid = event.target.dataset.slotid;
        let slotTime = event.target.dataset.slottime;
        let todaysDate = '';
        let month = '';
        let year = this.currentYear;
        let day = '';
        let hours;
        let min = '';
        
        if(slotTime){
           
            min = slotTime.substring(slotTime.indexOf(':') + 1, 5);
            hours = parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            if(slotTime.substring(6,8) == 'PM' && hours < 12){
                hours = 12 + parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            }else if(slotTime.substring(6,8) == 'PM' && hours >= 12){
                hours = 12;
            }else if(slotTime.substring(6,8) == 'AM' && hours == 12){
                hours = parseInt(slotTime.substring(0, slotTime.indexOf(':'))) - 12;
            }else {
                hours = parseInt(slotTime.substring(0, slotTime.indexOf(':')));
            }
        }

        // if(this.systemTime > slotTime){
        //     this.dispatchEvent(new ShowToastEvent({
        //         title: '',
        //         variant: 'error',
        //         message: 'Please select time greater than current time'+ this.systemTime ,
        //     }));
        //     return;
        // }
        // this.dispatchEvent(new CustomEvent('select', {
        //     detail: this.currentSelRecordId
        // }));

        for( let i = 0 ; i < this.data.rows.length ; i++ ){

            for( let j = 0 ; j < this.data.rows[i].cols.length ; j++ ){
                if( this.data.rows[i].cols[j].slotId == tempSlotid ){
                    this.data.rows[i].cols[j].isSelected = true;
                    todaysDate = this.data.dates[j];
                    month = todaysDate.substring(0, todaysDate.indexOf('/'));
                    day = todaysDate.substring(todaysDate.indexOf('/') + 1);
                    
                }
                else{
                    if( this.data.rows[i].cols[j].isAvailable ){
                        this.data.rows[i].cols[j].isSelected = false;
                    }
                }
            }
        }
      
        if(this.todaysDate == new Date(year,month,day).getTime()){
                if(this.todaysDateWithTime >  new Date(year,month,day,hours,min).getTime()){
                  
                    this.dispatchEvent(new ShowToastEvent({
                               title: '',
                               variant: 'error',
                                message: 'Please select time greater than current time',
                            }));
                            this.dispatchEvent(new CustomEvent('disableschedule', {
                                detail: this.currentSelRecordId
                            }));
                             return;
                         }else {
                          
                            this.dispatchEvent(new CustomEvent('select', {
                                detail: this.currentSelRecordId
                            }));
                         }
                }else {
                    this.dispatchEvent(new CustomEvent('select', {
                            detail: this.currentSelRecordId
                        }));
                }
        
    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}