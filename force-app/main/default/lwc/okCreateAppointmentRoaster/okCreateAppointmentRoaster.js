import { LightningElement, track ,api} from 'lwc';
import getAppintments from '@salesforce/apex/DC_ScheduleAppointment.getTemplateAppointmentSlots';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createSlots from '@salesforce/apex/OKTC_ScheduleAppointment.createTemplateSlots';

export default class OktcCalendar extends LightningElement {

    @track recid;
    @track data = {};
    @track currentSelRecordId = '';
    @track allSelectedSlots = new Set();
    @track columnSelectionMap = new Map();
    @track rowSelectionMap = new Map();
    @track allSlots = new Map();
    @track appointmentFrequencyLabel = 'Number of Available Appointment Slots (Per [Duration])';
    slotData = [];
    @track loaded = false;
    @track numOfSlot = 1;
    @track startTime;
    @track endTime;
    @track appointmentFrequency = 30;
    
    timeSlots = [];

    @api
    get accountid(){
        return this.recid;
    }
    set accountid(val){
       this.recid = val;

       getBusinessTimeOnAccount()
        .then(result =>{
            if(result != null){
                this.startTime = this.msToTime(result.Business_Start_Time__c);
                this.endTime = this.msToTime(result.Business_End_Time__c);
                if(result.Appointment_Frequency__c){
                    this.appointmentFrequency = result.Appointment_Frequency__c;
                }
                this.callApex();
            }
        })
        .catch(result =>{
            //
        })
    }

    @api
    getAppointmentFrequency() {
        return this.appointmentFrequency;
    }

    @api
    refreshCalendarView() {
        console.log('Refresh ');
        this.currentSelRecordId = '';
        this.allSelectedSlots = new Set();
        this.allSlots = new Map();
        console.log('all Slots after ', this.allSlots);
        this.callApex();
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    callApex() {
        if(this.recid) {
            this.loaded = false;
            console.log('accnt ' + this.recid);
            this.getAppointments();
        }
    }


    getAppointments(){
        getAppintments({ accountId: this.recid }).then(result => {
           this.createCalendar(result);
            this.loaded = true;
        })
        .catch(error => {
            this.error = error;
            this.loaded = true;
        });
    }

    getFormattedTime(timeInstance) {
        let hourT = parseInt(timeInstance.substr(0,2));
        let minT = parseInt(timeInstance.substr(3,2));
        let amPM = hourT <= 11 ? 'AM' : 'PM';
        return {hourT: hourT, minT: minT, amPM: amPM};
    }

    createTimeSlots() {
        let minhr = 0;
        let maxhr = 23;
        let minmnt = 0;
        let maxmnt = 60;
        if(this.startTime) {
            let timeObj = this.getFormattedTime(this.startTime);
            minhr = timeObj.hourT;
            minmnt = timeObj.minT;
        }
        if(this.endTime) {
            let timeObj = this.getFormattedTime(this.endTime);
            maxmnt = timeObj.minT;
            maxhr = timeObj.hourT;
        }

        let timeInterval = this.appointmentFrequency;
        for(let hr = parseInt(minhr); hr <= parseInt(maxhr); hr++) {
            for(let mnt = 0; mnt < 60; mnt = mnt + parseInt(timeInterval)) {
                if(((parseInt(minmnt) <= parseInt(mnt) || parseInt(hr) > parseInt(minhr)) && parseInt(hr) != parseInt(maxhr)) || 
                    (parseInt(hr) == parseInt(maxhr) && parseInt(maxmnt) > parseInt(mnt))) {
                    let timeStr = this.generateTimeOption(hr,mnt);
                    this.timeSlots.push(timeStr);    
                    this.addSlots(timeStr);
                }
            }
        }
    }

    generateTimeOption(hr,mnt) {
        let radian = hr < 12 ? 'AM' : 'PM';
        let timehr = hr <= 12 ? (hr == 0 ? 12 : hr) : hr - 12;
        let minute = mnt < 10 ? '0' + mnt : '' +  mnt;
        return (timehr < 10 ? '0' + timehr : timehr) + ':' + minute + ' ' + radian;
    }

    createCalendar(result) {
        this.columnSelectionMap = new Map();
        this.rowSelectionMap = new Map();
        
        this.data = {};
        let weekDays = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
        
       // this.data.dates = result.dateHeaderList;
        this.data.weeks = weekDays;
        
        this.data.rows = [];
        this.timeSlots = [];
        
        this.createTimeSlots();

        let columnCount = 0;

        for (var m in result.records){
            let recds = result.records[m];
            let rowCount = 0;
            this.columnSelectionMap.set(columnCount, false);
            console.log('this.timeSlots ',this.timeSlots);
            this.timeSlots.forEach(element => {
                this.rowSelectionMap.set(rowCount, false);
                if(typeof recds[element] != 'undefined' && recds[element] != null && recds[element].length > 0) {
                    this.data.rows[rowCount].cols[columnCount].isAvailable = true;
                    this.data.rows[rowCount].cols[columnCount].class = recds[element][0].Number_of_Available_Slots__c > 0 ? 'available' : 'newRecord' ;
                    this.data.rows[rowCount].cols[columnCount].availableSlots = recds[element][0].Number_of_Available_Slots__c;
                    this.data.rows[rowCount].cols[columnCount].slotId = recds[element][0].Id;
                    this.data.rows[rowCount].cols[columnCount].recId = recds[element][0].Id;
                    this.data.rows[rowCount].cols[columnCount].newRecord = false;
                }
                rowCount++;
            });
            columnCount++;
        } 
        columnCount = 0;
       

    }

    addSlots( timeStr ){
        let cols = [];
        var weekDays = ['SUN','MON','TUE','WED','THU','FRI','SAT'];

        for(var i= 0; i< weekDays.length ; i++){
            cols.push({
                availableSlots : 0,
                bookedSlots: 0,
                recId : '',
                slotDate: null,
                day : weekDays[i],
                newRecord: true,
                isAvailable : false,
                isSelected : false, 
                class : '',
                slotId : this.makeid(18)
            });
        }
        this.data.rows.push({
            timeSt : timeStr,
            cols : cols
        });
    }

    handleSelectedSlots(event){
        this.currentSelRecordId = event.target.dataset.recid;
        let tempSlotid = event.target.dataset.slotid;
        let rowindex = event.target.dataset.rowindex;
        let colindex = event.target.dataset.colindex;
        if(this.data.rows[rowindex].cols[colindex].slotId == tempSlotid) {
            if(this.allSelectedSlots.has(tempSlotid)) {
                this.data.rows[rowindex].cols[colindex].isSelected = false;
                this.allSelectedSlots.delete(tempSlotid);
                this.allSlots.delete(tempSlotid);
                console.log('& ** ' + JSON.stringify(event.target.dataset));
            } else {
                this.allSelectedSlots.add(tempSlotid);
                this.data.rows[rowindex].cols[colindex].isSelected = true;
                this.allSlots.set(tempSlotid, JSON.parse(JSON.stringify(event.target.dataset)));
                console.log(' ** ' + JSON.stringify(event.target.dataset));
            }
           
        }
        console.log('allSlots',this.allSlots);
        console.log('allSlots** ',JSON.stringify(this.allSlots));
    }

    manageRowclickAction(event) {
        let rowIndex = event.target.dataset.rowindex;
        let currentAttr = this.rowSelectionMap.get(rowIndex);
        let row = this.data.rows[rowIndex];
        console.log('rowIndex',rowIndex);
        console.log('row',row);
        row.cols.forEach((col) => {
            col.isSelected = !currentAttr;
            if(!currentAttr) {
                this.allSelectedSlots.add(col.slotId);
                let slot = {
                    recid: col.recid,
                    slotid: col.slotId,
                    slotdate: col.slotDate,
                    slottime: row.timeSt,
                    available : col.availableSlots,
                    day : col.day
                };
                this.allSlots.set(col.slotId, slot);
            } else {
                this.allSelectedSlots.delete(col.slotId);
                this.allSlots.delete(col.slotId);
            }
        });
        this.rowSelectionMap.set(rowIndex, !currentAttr);
        console.log('allSlots',this.allSlots);
    }

    manageColumnSelection(event){
        let colIndex = event.target.dataset.colindex;
        let currentAttr = this.columnSelectionMap.get(colIndex);
        let rowIndex = 0;
        this.data.rows.forEach((row) => {
            row.cols[colIndex].isSelected = !currentAttr;
            if(!currentAttr) {
                this.allSelectedSlots.add(row.cols[colIndex].slotId);
                let slot = {
                    recid: row.cols[colIndex].recid,
                    rowindex: rowIndex,
                    colindex: colIndex,
                    slotid: row.cols[colIndex].slotId,
                    slotdate: row.cols[colIndex].slotDate,
                    slottime: row.timeSt,
                    available : row.cols[colIndex].availableSlots,
                    day : row.cols[colIndex].day
                };
                this.allSlots.set(row.cols[colIndex].slotId, slot);
                rowIndex++;
            } else {
                this.allSelectedSlots.delete(row.cols[colIndex].slotId);
                this.allSlots.delete(row.cols[colIndex].slotId);
            }
        });
        this.columnSelectionMap.set(colIndex, !currentAttr);
        console.log('allSlots',this.allSlots);
    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
	
	handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleSlotChange(event) {
        this.numOfSlot = event.target.value;
    }

    createSlots(){
        console.log('ths.slot--');
        this.showSpinner = true;
        this.slotData = [];

            this.slotData = this.getSlots();
            console.log('ths.slot' + this.slotData.length);
            if(this.slotData.length > 0) {
                if(!this.numOfSlot) {
                    this.toastErrorAndReturn('Please provide Number of Available Appointment Slots.');
                    return;
                }
                else if(this.numOfSlot < 0) {
                    this.toastErrorAndReturn('Number of Available Appointment Slots must be greater or equals to 0.');
                    return;
                } else if(this.numOfSlot > 100) {
                    this.toastErrorAndReturn('Number of Available Appointment Slots must be less or equals to 100.');
                    return;
                } else {
                    let totalSlots = 0
                    let creatingSlots = 0
                    let deletingSlots = 0
                    let slots = this.numOfSlot;
                    this.slotData.forEach(function(slot) {
                        if(slot.available > slots) {
                            deletingSlots += Math.abs(slot.available - slots);
                        } else if(slot.available < slots) {
                            creatingSlots += Math.abs(slot.available - slots);
                        }
                        totalSlots += Math.abs(slot.available - slots);// == -1 ? -1 * (slot.available - this.numOfSlot) : (slotData.available - this.numOfSlot);
                    });
                    if((totalSlots) > 10000) {
                        this.toastErrorAndReturn('Processing (' + totalSlots +') slots, creating(' + creatingSlots + '), deleting(' + deletingSlots + ') . Please unselect some of the slots as we can only process up to 10000 Appointment Slots in a single transaction.');
                        return;
                    }
                }
                createSlots({
                    accId: this.recid,
                    slotWrapperString: JSON.stringify(this.slotData),
                    numOfSlot: this.numOfSlot
                }).then(result => {
                    const evt = new ShowToastEvent({
                        title: 'Success',
                        message: 'Succesfully updated the Appointment Slots.',
                        variant: 'success',
                    });
                    this.dispatchEvent(evt);
                   this.showSpinner = false;
                   this.refreshCalendarView()
                   //this.handleCloseModal();
                })
                .catch(error => {
                    console.log('fhh2222');
                    this.error = error;
                    this.toastErrorAndReturn('Unable to crate the Slots, please contact your Administrator.');
                });
            } else {
                this.toastErrorAndReturn('Please select a slot to be created.');
            }
    }

    getSlots() {
        console.log('all Slots ', this.allSlots);
        let slots = this.allSlots.size > 0 ? Array.from(this.allSlots.values()) : [];
        return slots;
    }
    toastErrorAndReturn(error) {
        const evt = new ShowToastEvent({
            title: 'Error',
            message: error,
            variant: 'error',
        });
        this.showSpinner = false;
        this.dispatchEvent(evt);
    }

}