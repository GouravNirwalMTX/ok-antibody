import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/OK_SelfRegistration.registerUser';
import contactType from '@salesforce/label/c.OK_Registration_Type_VR_Citizen_Lobby';
//import reCAPTCHA from '@salesforce/label/c.ok_reCAPTCHA_VF_URL';
import pageUrl from '@salesforce/resourceUrl/recaptcha';

export default class OkvrcpSelfRegistration extends NavigationMixin(LightningElement) {

    label = {
        contactType
    }
    @track enableSubmit = true;
    @track navigateTo = pageUrl;
  //  @track _url = reCAPTCHA;
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '.vrlpuser'
    @track portal = [{
        label: 'Citizen (COVID)',
        value: "COVID"
    }, {
        label: 'Citizen (Antibody)',
        value: 'Antibody'
    }];
    @track showSpinner = false;
    @api showCaptcha;
    @track captchaValues = '';
    @api portalName = 'Vital Records Appointment Portal';
    customImage = nysdohResource;

    firstNameChange(event) {
        this.firstName = event.target.value;
        console.log('firstName=> ', this.firstName);
    }
    lastNameChange(event) {
        this.lastName = event.target.value;
        console.log('lastName=> ', this.lastName);
    }
    phoneChange(event) {
        this.phone = event.target.value;
        console.log('phone=> ', this.phone);
    }
    emailChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }
    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    @api
    getValue() {
       return window.data;
    }
    
    constructor() {
        super();

        //     this.navigateTo = pageUrl;

        //     window.addEventListener("message", this.listenForMessage);
    }

    captchaLoaded(evt) {
        var e = evt;
        console.log(e.target.getAttribute('src') + ' loaded');
        if (e.target.getAttribute('src') == pageUrl) {
            // You know that your captcha is loaded
        }
    }

    listenForMessage(message) {
        console.log('message', message.data)
        window.data=message.data;
        this.captchaValues =  message.data;
        console.log('valueCaptcha>>' + this.captchaValues)
        //this.data=message.data;
        // if (message.data == 'Unlock') { //code does all the processing when it gets unlock message from visual force page
        //   console.log('Inside');
        //     this.enableButton();
        //   console.log('Ourside')
        //    // window.address = message.data
        // }

    }
    enableButton() {
        console.log('called')
        this.enableSubmit = false;
    }
    connectedCallback() {
        //this.enableSubmit = true;
        console.log('isTrue>>' + this.showCaptcha)
        this.navigateTo = pageUrl;

        window.addEventListener("message", this.listenForMessage, false);
        /* getAccount()
         .then(result => {
             console.log('result'+JSON.stringify(result));
             this.accounts = result;
             console.log('acc'+this.accounts )
         })
         .catch((error) => {
             let message = error.message || error.body.message;
             console.log(message);
         }); */
    }

    doRegisteration() {
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        let fName = this.firstName.replace(/\s/g, "");
        let lName = this.lastName.replace(/\s/g, "");
        console.log('firsName ** ', fName);
        console.log('lastName ** ', lName);
        console.log('valueCaptcha>>inRegister' +  this.getValue())

        if (this.firstName.trim() == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if (!this.validateName(fName)) {
            this.errorMessage = 'First Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }
        if (this.lastName.trim() == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        if (this.getValue() != 'Unlock' || this.getValue() == 'undefined') {
            this.errorMessage = 'Please select the reCAPTCHA';
            this.showError = true;
            return;
        }
        if (!this.validateName(lName)) {
            this.errorMessage = 'Last Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }

        if ((this.phone == '') && (this.email == '')) {
            this.errorMessage = 'Please provide Mobile Number or an Email';
            this.showError = true;
            return;
        }
        if ((this.phone != '') && !this.validatePhone(this.phone)) {
            this.errorMessage = 'Phone number can only contains digits';
            this.showError = true;
            return;
        }
        if ((this.email != '') && !this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }

        this.showSpinner = true;
        register({
                firstName: this.firstName,
                lastName: this.lastName,
                phone: this.phone,
                email: this.email,
                type: this.label.contactType,
                delimiter: this.delimiter
            }) //Added delimiter by Sajal
            .then(data => {
                console.log('Data-->' + JSON.stringify(data));
                if (data.type == 'error') {
                    this.showError = true;
                    this.errorMessage = data.message;
                } else if (data.type == 'success') {
                    //this.showThankYou = true;
                    // this.thankYouMessage = data.message;
                    console.log('navigateToForgotPassword ' + data.redirectURL);
                    location.href = data.redirectURL;
                }
                this.showSpinner = false;

            })
            .catch(error => {
                this.showSpinner = false;
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                // var msg = JSON.stringify(error.body.fieldErrors.Username);
                var msgArray = error.body.fieldErrors.Username;
                let result = msgArray.map(({
                    statusCode
                }) => statusCode);
                let resultmessage = msgArray.map(({
                    message
                }) => message);
                // console.log('result >>>' +  JSON.stringify(result));
                // console.log('error message>>'+ msg);
                // console.log('Error >>>>-->' + JSON.stringify(msg.substring(1, msg.length - 1)));
                // this.errorMessage = msg.substring(1, msg.length - 1);
                if (result == 'DUPLICATE_USERNAME') {
                    this.errorMessage = 'User already exists';
                } else {
                    this.errorMessage = resultmessage;
                }

            });
    }



    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }
    validateName(name) {
        var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
        var isValid = name.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validatePhone(mobile) {
        var regExpEmail = /^[0-9]*$/;
        var isValid = mobile.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

}