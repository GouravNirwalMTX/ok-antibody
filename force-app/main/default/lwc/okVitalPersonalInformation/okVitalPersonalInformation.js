import {
    LightningElement,
    track,
    api
} from 'lwc';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.saveContactDetails";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";


export default class OkVitalPersonalInformation extends LightningElement {
    @track dataObj = {};
    @track extraWrapper = {};
    @track contactId;
    @track showSpinner = false;
    @track disableInputs = true;
    @track stateOptions = [];
    @track salutations = [];
    @track LangOptions = [];
    @track raceOptions = [];
    @track maritialStatusOptions = [];
    @track age;
    @api currentStep;
    @track todayDate;

    fields = ["Name", "Birthdate", "Age__c", "Gender__c", "Street_Address1__c", "Street_Address_2__c", "City__c", "State__c", "ZIP__c", "Email", "MobilePhone"];

    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "true"
            },
            {
                label: "No",
                value: "false"
            },
        ];
    }
  
    connectedCallback() {
        this.getContact();
        this.todayDate = this.getTodayDate();
        fetchPicklist({
                objectName: "Contact",
                fieldName: "State__c"
            })
            .then((result) => {
                this.stateOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Salutation"
            })
            .then((result) => {
                this.salutations = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Primary_Language__c"
            })
            .then((result) => {
                this.LangOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Race__c"
            })
            .then((result) => {
                this.raceOptions = result;
            })
            fetchPicklist({
                objectName: "Contact",
                fieldName: "Marital_Status__c"
            })
            .then((result) => {
                this.maritialStatusOptions = result;
            })
            .catch((error) => {
                let message = error.message || error.body.message;
                console.log(message);
            });
    }

    @api
    editContacts() {
        this.disableInputs = false;
    }

    getContact() {
        this.showSpinner = true;
        getContactDetails()
            .then(result => {

                // console.log('data=>', JSON.stringify(result));
                this.dataObj.firstName = result.firstName;
                this.dataObj.middleName = result.middleName;
                this.dataObj.lastName = result.lastName;
                this.dataObj.email = result.email;
                this.dataObj.phone = result.phone;
                if(result.phone){
                    this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
                }
                this.dataObj.patientId = result.patientId;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                this.dataObj.smsCheckbox = result.smsCheckbox;
                this.dataObj.emailCheckbox = result.emailCheckbox;
                this.dataObj.needSpanishInterpretor = result.needSpanishInterpretor;
                // this.extraWrapper = {...this.dataObj};
                console.log(JSON.stringify(this.dataObj));
                this.showSpinner = false;

            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    handleInputChange(event) {
        if (event.target.name == 'firstName') {
            this.dataObj.firstName = event.target.value;
        } else if (event.target.name == 'middleName') {
            this.dataObj.middleName = event.target.value;
        } else if (event.target.name == 'lastName') {
            this.dataObj.lastName = event.target.value;
        } else if (event.target.name == 'email') {
            this.dataObj.email = event.target.value;
        }else if (event.target.name == 'phone') {
            this.dataObj.phone = event.target.value;
            this.formatPhone(event.target);
        }else if (event.target.name == 'emailCheckbox') {
            this.dataObj.emailCheckbox = event.target.checked;
        } else if (event.target.name == 'smsCheckbox') {
            this.dataObj.smsCheckbox = event.target.checked;
        } else if (event.target.name == 'patientId') {
            this.dataObj.patientId = event.target.value;
        }else if (event.target.name == 'prescriptionMed') {
            this.dataObj.needSpanishInterpretor = event.target.value;
        }
        console.log(JSON.stringify(this.dataObj));
    }



    @api
    saveContacts() {
        console.log('data=>', JSON.stringify(this.dataObj));
        this.dataObj.contactId = this.contactId;
        if (this.dataObj.zip == "" || typeof this.dataObj.zip == 'undefined') {
            this.dataObj.zip = null;
        }
        if ((typeof this.dataObj.phone == 'undefined' || this.dataObj.phone == '') && (typeof this.dataObj.email == 'undefined' || this.dataObj.email == '')) {
            let tempObj = {};
            tempObj.message = "Please provide Mobile Number or an Email";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
        if (typeof this.dataObj.email != 'undefined' && (this.dataObj.email != '') && !this.validateEmail(this.dataObj.email)) {
            return;
        }
        // if (this.dataObj.emailCheckbox == true && (this.dataObj.email == '' || typeof this.dataObj.email == 'undefined')) {
        //     let tempObj = {};
        //     tempObj.message = "Please Complete the email field to recieve email notifications";
        //     tempObj.status = "error";
        //     this.sendEventToParent(tempObj);
        //     return;
        // }

        // if (this.dataObj.smsCheckbox == true && (this.dataObj.phone == '' || typeof this.dataObj.phone == 'undefined')) {
        //     let tempObj = {};
        //     tempObj.message = "Please Complete the phone field to recieve SMS notifications";
        //     tempObj.status = "error";
        //     this.sendEventToParent(tempObj);
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.lastName)) {
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.phone)) {
        //     return;
        // }

        // if (!this.validateTextField(this.dataObj.email)) {
        //     return;
        // }

        // if(!this.validateEmail(this.dataObj.email)){
        //     return;
        // }

        // if(!this.validatePhone(this.dataObj.phone)){
        //     return;
        // }
        // if(this.dataObj.zip){
        //     if(!this.validateZip(this.dataObj.zip)){
        //         return;
        //     }
        // }

        if (!this.isValid()) {
            return;
        }

        saveContactDetails({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep
            })
            .then(result => {
                console.log('Successfully Saved');
                this.disableInputs = true;
                // this.extraWrapper = {...this.dataObj};
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                this.sendEventToParent(tempObj);
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
            })
    }

    @api
    handleCancelButton() {
        this.disableInputs = true;
        // this.dataObj = {...this.extraWrapper};
    }

    calculateAge(date) {
        if (!this.validateDate(date)) {
            return;
        }
        var Bday = +new Date(date);
        this.age = ~~((Date.now() - Bday) / (31557600000));
    }

    sendEventToParent(obj) {

        const selectedEvent = new CustomEvent("contactsave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    validateTextField(field) {
        if (field == '' || typeof field == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return false;
        } else {
            return true;
        }
    }

    // formatPhone(obj) {
    //     let phone = obj.value;
    //     let formatedPhone='('+phone.substring(0,3)+')'+phone.substring(3,6)+'-'+phone.substring(6,10);
    // }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = 'Please provide a valid email address. example: "abc@xyz.com"';
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
        }
        return isValid;
    }

    // validatePhone(phone) {
    //     var regExpPhone = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
    //     var isValid = phone.match(regExpPhone);
    //     console.log("isValid", isValid);
    //     if(!isValid){
    //         let tempObj = {};
    //         tempObj.message = "Please provide a correct format for phone.";
    //         tempObj.status = "error";
    //         this.sendEventToParent(tempObj);
    //     }
    //     return isValid;
    // }

    validateDate(date) {
        var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var isValid = date.match(regExpPhone);
        console.log("isValid", isValid);
        if (!isValid) {
            let tempObj = {};
            tempObj.message = "Please provide a valid format for date";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
        }
        return isValid;
    }


    // validateZip(zip) {
    //     var regExpZip = /^[0-9]*$/;
    //     var isValid = (zip.match(regExpZip) && zip.length == 5);
    //     console.log("isValid", isValid);
    //     if(!isValid){
    //         let tempObj = {};
    //         tempObj.message = 'Please enter a valid format ("XXXXX") for Zip code.';
    //         tempObj.status = "error";
    //         this.sendEventToParent(tempObj);
    //     }
    //     return isValid;
    // }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        valid = isAllValid;
        return valid;
    }

    handlePhoneFocus(event) {
        event.target.value = this.dataObj.phone;
    }

    formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }
    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;

    }
    

}