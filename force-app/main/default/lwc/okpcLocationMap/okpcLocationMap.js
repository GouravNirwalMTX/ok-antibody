import { LightningElement, track, api } from "lwc";
import appTitle from '@salesforce/label/c.OKPC_Location_Map_Title';
import getTestingSites from "@salesforce/apex/OKPC_LocationMapController.getTestingSites";
import getLobbys from "@salesforce/apex/OKPC_LocationMapController.getLobbys";

export default class OkpcLocationMap extends LightningElement {

    label = {
        appTitle
    }

    @track mapMarkers = [];
    @track isLoaded = false;
    @track center = {
        location: {
            State: 'OK',
        },
    };
    @api zoomLevel = 9;
    @track showFooter = true;
    @track listView = 'auto';
    @track error;
    @api islobby = false;
    @track selectedMarkerValue = '';
    @track mapIdToAccount = {};
    @api locationTitle = 'Pick Testing Site';
    @api hideLocationLabel =false;
    @api testingType;

    connectedCallback() {
        console.log('lobby--'+ this.islobby)
        console.log('TestingTyep',this.testingType)
        this.handleLoad();
    }

    handleLoad() {
        if(this.islobby){
            getLobbys( {})
            .then(result => {
                // Markers
                this.mapMarkers = JSON.parse(JSON.stringify(result));
                this.mapMarkers.forEach((marker) => {
                    this.mapIdToAccount[marker.value] = marker.title;
                });

                // All done, load the page
                this.isLoaded = true;
            })
            .catch(error => {
                this.error = error;
                this.isLoaded = false;
            });
        }else{
            getTestingSites( {
                testingType: this.testingType
            })
            .then(result => {
                // Markers
                this.mapMarkers = JSON.parse(JSON.stringify(result));
                this.mapMarkers.forEach((marker) => {
                    this.mapIdToAccount[marker.value] = marker.title;
                });

                // All done, load the page
                this.isLoaded = true;
            })
            .catch(error => {
                this.error = error;
                this.isLoaded = false;
            });
        }
       
    }

    @api
    get selectedSite(){
        return this.selectedMarkerValue ? this.mapIdToAccount[this.selectedMarkerValue] : '';
    }

    handleMarkerSelect(event) {
        this.selectedMarkerValue = event.target.selectedMarkerValue;
        console.log('This.selectedValue >?>' , this.selectedMarkerValue)
    }

    handleSaveModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: this.selectedMarkerValue
        }));
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
}