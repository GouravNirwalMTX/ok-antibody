import { LightningElement, api } from 'lwc';

export default class OkpcHelpText extends LightningElement {
    @api
    helpTextForModelWindow=false;
    @api helpText;
    @api pageTitle;
}