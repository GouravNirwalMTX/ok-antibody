import { LightningElement,track } from 'lwc';
import retrieveResult from '@salesforce/apex/OKPCDashboardController.retrieveResult';
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';

export default class OkpcDashboard extends LightningElement {
    @track contactName; 
    @track contactId; 
    @track streetAddress; 
    @track appointment = {}; 
    @track isAutoSchedule = false;
    @track showMap = false;
    @track showConfirmation = false;
    @track showAppointmentModal = false;
    @track address;
    @track mapMarkers = [];
    @track results = {};
    @track hasResults = false; 
    @track isCancelledWindow = false;
    @track testingType = 'Antibody Testing';
    @track isBulk = false;
    connectedCallback(){
        this.callApex();
    }
    callApex() {
       // this.appointment = {};
        retrieveResult()
        .then(result =>{
            if(result != null){
                console.log('### ',JSON.stringify(result));
                if(result.contact) {
                    this.contactId = result.contact.Id;
                    let contact = result.contact;
                    this.contactName = contact.Name;
                    this.streetAddress = contact.MailingStreet;
                    this.isAutoSchedule = contact.Auto_Scheduled__c;
                    let addressString = '';
                    if(contact.MailingCity != null && typeof contact.MailingCity != 'undefined')
                        addressString += contact.MailingCity;
                    if(contact.MailingState != null && typeof contact.MailingState != 'undefined')
                        addressString += addressString == '' ? contact.MailingState : ', ' + contact.MailingState;
                    if(contact.MailingPostalCode != null && typeof contact.MailingPostalCode != 'undefined')
                        addressString += ' ' + contact.MailingPostalCode;
                    this.address = addressString;
                }
                if(result.appointment) {
                    console.log('## ',JSON.stringify(result.appointment));
                    let app = result.appointment[0];
                    let isScheduled = app.Status__c == 'Scheduled';
                    let isToBeScheduled = app.Status__c == 'To Be Scheduled';
                    let isCancelled = app.Status__c == 'Cancelled';
                    let isCompleted = false;
                    if((app.Status__c == 'Completed') || (app.Status__c == 'Closed')){
                        isCompleted = true;
                    }
                    this.appointment.id = app.Id;
                    //commented by sameer 07/22/2020 as we have changed the field reference.
                   // this.appointment.startTime = isScheduled ? this.msToTimeAMPM(app.Appointment_Slot__r.Start_Time__c) : this.msToTimeAMPM(app.Patient__r.Preferred_Time__c) ;
                   // this.appointment.startDate = isScheduled ? app.Appointment_Slot__r.Date__c : app.Patient__r.Preferred_Date__c ;
                  
                   this.appointment.startTime = isScheduled ? this.msToTimeAMPM(app.Appointment_Start_Time_v1__c) : this.msToTimeAMPM(app.Patient__r.Preferred_Time__c) ;
                   this.appointment.startDate = isScheduled ? app.Appointment_Start_Date_v1__c : app.Patient__r.Preferred_Date__c ;
                  
                    if(isCancelled){
                        this.appointment.startTime = this.msToTimeAMPM(app.Appointment_Start_Time_v1__c);
                        this.appointment.startDate = app.Appointment_Start_Date_v1__c;
                    }
                    
                    let addressString = '';
                    if(app.Patient__r.Testing_Site__r.BillingStreet != null && typeof app.Patient__r.Testing_Site__r.BillingStreet != 'undefined')
                        addressString += app.Patient__r.Testing_Site__r.BillingStreet;
                    if(app.Patient__r.Testing_Site__r.BillingCity != null && typeof app.Patient__r.Testing_Site__r.BillingCity != 'undefined')
                        addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingCity : ', ' + app.Patient__r.Testing_Site__r.BillingCity;
                    if(app.Patient__r.Testing_Site__r.BillingState != null && typeof app.Patient__r.Testing_Site__r.BillingState != 'undefined')
                        addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingState : ', ' + app.Patient__r.Testing_Site__r.BillingState;
                    if(app.Patient__r.Testing_Site__r.BillingPostalCode != null && typeof app.Patient__r.Testing_Site__r.BillingPostalCode != 'undefined')
                        addressString += ' ' + app.Patient__r.Testing_Site__r.BillingPostalCode;
                        
                    this.appointment.address = addressString;
                    this.appointment.isScheduled = isScheduled;
                    this.appointment.isToBeScheduled = isToBeScheduled;
                    this.appointment.isCancelled = isCancelled;
                    this.appointment.siteId = app.Patient__r.Testing_Site__c;
                    this.appointment.siteName = app.Patient__r.Testing_Site__r.Name;
                    this.appointment.isCompleted = isCompleted;
                    console.log('## this.appointment ', this.appointment);
                    this.retrieveMarker();
                }
                if(result.appointment[0].Antibodies_Testing__r[0]){
                    this.results = result.appointment[0].Antibodies_Testing__r[0];
                    this.appointment.labName = this.results.Laboratory__r.Name;
                    console.log('##@ ', JSON.stringify(this.results));
                    if(this.results.Results__c){
                        console.log('changing >>> ');
                        this.hasResults = true;
                        this.appointment.isCompleted = false; 
                    }
                }
                console.log('this.appointment.isCompleted;  ',this.appointment.isCompleted);
            }
        })
        .catch(result =>{
            //
        })
        // this.initColSelection();
        // this.callApex();
    }

    retrieveMarker() {
        if(this.mapMarkers.length <= 0){
            getAccountdetails({
                accountId: this.appointment.siteId
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                if(this.accountDetails){
                    this.mapMarkers.push(this.accountDetails);
                }
            })
            .catch(error => {
                console.log('error',error);
            })
        }
        
    }
    viewCertificate(){
        console.log('testing...');
        var url = window.location.href;
        url = url.substring( 0, url.indexOf( "/s/" ) );
        url += '/apex/ResultCertificate?id='+this.contactId;
        console.log('#$ ',url);
        window.open(url,'_blank');
    }

    viewLocationOnMap() {
        this.showMap = true;
    }

    editAppointment() {
        this.showAppointmentModal = true;
    }
   
    openCancel(){
        this.isCancelledWindow = true;
    }

    cancelAppointment() {
        this.showConfirmation = true;
    }

    editProfile(){
        const editprofile = new CustomEvent("editprofile", {
            detail: 'editProfile'
        });
        this.dispatchEvent(editprofile);
    }

    hideModal(event) {
        this.callApex();
        this.showConfirmation = false;
        this.showAppointmentModal = false;
        this.showMap = false;
    }
    msToTimeAMPM(s) {
        if(s == '' || s == null || typeof s == 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if(hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' '  + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    handleSubmitData(event) {
        console.log('Milla Event' + event)
        console.log('Milla Event' + JSON.stringify(event))
        if (event.detail.name == 'submit'  ) {
            this.callApex();
            this.isCancelledWindow = false;
        }
    }
}