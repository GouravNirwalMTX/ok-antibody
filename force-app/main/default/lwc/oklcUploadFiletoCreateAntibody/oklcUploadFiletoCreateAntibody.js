import { LightningElement,track } from 'lwc';
import insertData from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.insertData';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OklcUploadFiletoCreateAntibody extends LightningElement {
    filesUploaded = [] ;
    @track fileName;
    @track showSpinner = false;
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;
    result;
    @track showFileName = false;

    handleFileChange(event){
        console.log('uploading');
        this.filesUploaded = event.target.files;
        this.fileName = event.target.files[0].name;
        console.log('uploading ',this.fileName );
        if(this.filesUploaded.length > 0) {
            this.showFileName = true;
            this.handleUpload();
        }
    }

    handleFileDelete(){
        this.showFileName = false;
        this.filesUploaded.length = 0;
        this.fileName ='';
    }

    handleUpload() {
        if(this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload!!';
        }
    }

    uploadHelper(){
        this.file = this.filesUploaded[0];
        if (this.file.size > this.MAX_FILE_SIZE) {
            window.console.log('File Size is to long');
            return ;
        }
        this.showSpinner = true;
        console.log('spinner true');
        this.fileReader = new FileReader();
        this.fileReader.readAsText(this.file, "UTF-8");

        this.fileReader.onload = (() => {
            this.fileContents = this.fileReader.result;
            
           this.result = this.csvToJson(this.fileContents);
        //    this.insertResults(result);
            console.log('loading file');
        });
        this.fileReader.onloadend = (() =>{
            this.showSpinner = false;
            console.log('spinner false');
        });
        this.fileReader.onerror = function (evt) {
            //console.log("error reading file");
            this.showSpinner = false;
        }
    }

    csvToJson(csv){
        var arr = []; 
        
        arr =  csv.split('\n');
        //console.log('Array  = '+array);
        // console.log('arr = '+arr);
        // arr.pop();
        // console.log('arr = '+arr);
        var jsonObj = [];
        var headers = arr[0].split(',');
        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
            var obj = {};
            for(var j = 0; j < data.length; j++) {
                obj[headers[j].trim()] = data[j].trim();
                //console.log('obj headers = ' + obj[headers[j].trim()]);
            }
            jsonObj.push(obj);
        }
        var json = JSON.stringify(jsonObj);
        console.log('json = '+ json);
        return json;
    }

    insertResults(jsonstr){
        this.showSpinner = true;
        // console.log('JSON:: ', jsonstr);
        insertData({strfromle : jsonstr})
        .then(result =>{
            console.log('success ', JSON.stringify(result));
            // this.handleFileDelete();
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: result.type,
                message: result.message,
            }));
            this.showFileName = false;
            this.result = null;
        })
        .catch(error =>{
            console.log('error ', JSON.stringify(error));
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }


    handleSave(){
        if(this.result){
            this.insertResults(this.result);
        }else{
            console.log('No file selected');
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a CSV file',
            }));
        }
    }
    
}