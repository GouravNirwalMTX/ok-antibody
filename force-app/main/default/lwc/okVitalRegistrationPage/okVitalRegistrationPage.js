import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/okpcRegisterController.selfRegister';
import getContactInformation from '@salesforce/apex/okpcRegisterController.getContactInformation';
import communityURL from '@salesforce/label/c.OKPC_Community_url';
import loginURL from '@salesforce/label/c.OKPC_Login_URL';

export default class OkpcRegistrationpage extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track patientId = '';
    @track username = '';
    @track password = '';
    @track email = '';
    @track confirmPassword = '';
    @track contactId;

    customImage = nysdohResource;
    navigateToLogin() {
        console.log('In Login');
        location.href = loginURL;
    }
    navigateToHome() {
        console.log('goHome');
        location.href = communityURL;
    }
    redirectToUrl(urlValue) {
        console.log('redirectToUrl-->', urlValue);
        location.href = urlValue;
    }

    patientIdChange(event) {
        this.patientId = event.target.value;
        console.log('patientId=> ', this.patientId);
    }
    emailChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }
    usernameChange(event) {
        this.username = event.target.value;
        console.log('username=> ', this.username);
    }
    passwordChange(event) {
        this.password = event.target.value;
        console.log('password=> ', this.password);
    }
    confirmPasswordChange(event) {
        this.confirmPassword = event.target.value;
        console.log('confirmPassword=> ', this.confirmPassword);
    }

    connectedCallback() {
        this.getContactInfo();
    }

    doRegisteration() {
        this.showError = false;
        if (this.patientId == '' || typeof this.patientId == 'undefined') {
            this.errorMessage = 'Please provide Patient Id.';
            this.showError = true;
            return;
        }
        // if (this.username == '' || typeof this.username == 'undefined') {
        //     this.errorMessage = 'Please provide a username.';
        //     this.showError = true;
        //     return;
        // }
        if (this.email == '' || typeof this.email == 'undefined') {
            this.errorMessage = 'Please provide an email address.';
            this.showError = true;
            return;
        }
        if (this.password == '' || typeof this.password == 'undefined') {
            this.errorMessage = 'Please provide a password';
            this.showError = true;
            return;
        }
        if (this.confirmPassword == '' || typeof this.confirmPassword == 'undefined') {
            this.errorMessage = 'Please enter confirm password.';
            this.showError = true;
            return;
        }
        if (this.confirmPassword != this.password) {
            this.errorMessage = 'Password and Confirm password do not match.';
            this.showError = true;
            return;
        }
         if (!this.validatePassword(this.password)) {
            this.errorMessage = 'Your password must include characters, numbers and at least one special character and length above 10 character.';
            this.showError = true;
            return;
       }
        if (!this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }
        // if (!this.validateEmail(this.username)) {
        //     this.errorMessage = 'Please provide a valid username. example: "abc@xyz.com"';
        //     this.showError = true;
        //     return;
        // }


        register({ patientid: 'vitalRecordscitizenLobby', email: this.email, username: this.email, password: this.password, confirmPassword: this.confirmPassword, delimiter: '.COVID' })//Added delimiter by Sajal
            .then(data => {
                console.log('Data-->' + JSON.stringify(data));
                if (data.type == 'error') {
                    this.showError = true;
                    this.errorMessage = data.message;
                } else if (data.type == 'success') {
                    this.redirectToUrl(data.redirectURL);
                }

            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                var msg = JSON.stringify(error.body.message);
                this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }

    getContactInfo() {
        console.log('getContactInfo-->');

        let newURL = new URL(window.location.href).searchParams;
        this.contactId = newURL.get('contactid');
        console.log('contactId ===> ' + this.contactId);

        getContactInformation({ contactId: this.contactId })
            .then(data => {
                console.log('Data-->' + data);
                this.username = data.email;
                this.patientId = data.patientId;
                this.email = data.email;
            })
            .catch(error => {
                console.log('Error-->' + JSON.stringify(error));
            });
    }

    validatePassword(password) {
        var regExpPhone = /^(?=.*[a-z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{10,30}$/;
        var isValid = password.match(regExpPhone);
        console.log("isValid", isValid);
        return isValid;
    }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

}