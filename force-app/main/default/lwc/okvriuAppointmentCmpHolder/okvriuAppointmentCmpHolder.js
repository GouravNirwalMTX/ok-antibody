import { LightningElement, api, track } from 'lwc';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetails';
import markInProgress from '@salesforce/apex/DC_ScheduleAppointment.inProgressVitalAppointment';
import formFactorPropertyName from '@salesforce/client/formFactor';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class OkvriuAppointmentCmpHolder extends LightningElement {
    get isMobile() {
        return formFactorPropertyName != 'Large';
    }
    @track columns = [ 
        {label:'Citizen Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'Spanish Translator',name:'Patient__r.Need_Spanish_Interpreter__c',type:'text',action:''},
        {label:'Appointment Date',name:'Formatted_Appointment_Start_Date__c',type:'text',action:''},
        {label:'Time',name:'Start_Time__c',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Re-Schedule',class:'custom-hide-comp paginator-button'},{label:'Complete',class:'custom-hide-comp paginator-button'},{label:'In Progress',class:'custom-hide-comp paginator-button'},{label:'Cancel',class:'custom-hide-comp paginator-button'}]}
    ];
    @track whereClause;
    @track showPaginator = false; 

    @track columns2 = [ 
        {label:'Citizen Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'Citizen Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Mobile Number',name:'Patient__r.MobilePhone',type:'text',action:''},
        {label:'Email',name:'Patient__r.Email',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Schedule',class:'custom-hide-comp paginator-button'},{label:'Cancel',class:'custom-hide-comp paginator-button'}]}
    ];
    @track whereClause2;
    @track showPaginator2 = false;

    @track columns3 = [ 
        {label:'Citizen Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'Citizen Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Mobile Number',name:'Patient__r.MobilePhone',type:'text',action:''},
        {label:'Email',name:'Patient__r.Email',type:'text',action:''},
        {label:'Reason',name:'Additional_Notes__c',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Complete',varient:'brand',buttons:[{label:'Re-Schedule',class:'custom-hide-comp paginator-button'},{label:'Complete',class:'custom-hide-comp paginator-button'},{label:'Cancel',class:'custom-hide-comp paginator-button'}]}
    ];
    @track columns4 = [ 
        {label:'Citizen Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'Citizen Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Completed Appointment Date',name:'Formatted_Complete_Date__c',type:'text',action:''},
        {label:'Appointment Start Time',name:'Start_Time__c',type:'text',action:''},
        {label:'Completed Appointment Time',name:'Formatted_Complete_Time__c',type:'text',action:''}
    ];

    @track columns5 = [ 
        {label:'Citizen Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'Citizen Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Appointment Date',name:'Formatted_Appointment_Start_Date__c',type:'text',action:''},
        {label:'Time',name:'Formatted_Appointment_Start_Time__c',type:'text',action:''},
        {label:'cancelation Reason',name:'Reason_for_cancellation__c',type:'text',action:''}     
    ];

    @track whereClause4;
    @track whereClause5;
    @track whereClause3;
    @track showPaginator3 = false;
    
    @track resetTable = false;
    @track reloadTable = false;
    @track showCalendar = false;
    @track showConfirmation = false;
    @track appointmentId;
    @track accountId;
    @track modalType = '';
    @track isBulk = false;
    @track selectedAppointmentIds = [];
    @track showBatchModal =false;
    
    get type() {
        return this.modalType;
    }

    connectedCallback() {
       getTestingSite({})
        .then(data => {
            var sessionKey = sessionStorage.getItem('accId');
            if(sessionKey)
                this.accountId = sessionKey;
            else if(data.ContactId) 
                this.accountId = data.Contact.AccountId;
            if(data.ContactId && data.Contact.AccountId) {
                this.whereClause = ' Status__c = \'Scheduled\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause2 = ' Status__c = \'To Be Scheduled\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause3 = ' Status__c = \'In Progress\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause4 = ' Status__c = \'Completed\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause5 = ' Status__c = \'Cancelled\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
            } else {
                this.whereClause = ' Id = null ';
                this.whereClause2 = ' Id = null ';
                this.whereClause3 = ' Id = null ';
                this.whereClause4 = ' Id = null ';
                this.whereClause5 = ' Id = null ';
            }
            this.showPaginator = true;
        })
        .catch(error => {
            console.log('Error-->' + error);
        });
    }

    handlePaginatorAction(event) {
        let data = event.detail;
        console.log(data);
        if(data.actionName == 'Re-Schedule' || data.actionName == 'Schedule') {
            this.appointmentId = data.recordId;
            this.showCalendar = true;
        } else if(data.actionName == 'Complete') {
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'complete';
        } else if(data.actionName == 'Cancel') {
            this.isBulk = false;
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'cancel';
        }
        else if(data.actionName == 'In Progress') {
            this.appointmentId = data.recordId;
            this.modalType = 'inProgress';
            this.showConfirmation = true;
            //this.modalType = 'cancel';
           // this.inprogressAppointment();
        }
    }
    hideModal(event) {
        this.showCalendar = false;
        this.showConfirmation = false;
        
        if(event.detail == 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        } else if(event.detail == 'complete' || event.detail == 'cancel') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        }
    }

    resetFlag(event) {
        // this.resetTable = event.detail;
    }

    getSelectedIds(event){
        let temparr= [];
        this.selectedAppointmentIds = [];
        temparr = [...event.detail.selectedIds]
        this.selectedAppointmentIds = [...this.selectedAppointmentIds, ...temparr];
        console.log('selectedIds>>>'+ this.selectedAppointmentIds)
        if(event.detail.buttonLabel == 'Cancel Appointments'){
            if(this.selectedAppointmentIds < 1){
                this.showError();
                return;
            }
            this.isBulk = true;
            this.showConfirmation = true;
            this.modalType = 'cancel';
            
        }
    }
    showError(){
      
        this.dispatchEvent(new ShowToastEvent({
            title: '',
            variant: 'error',
            message: 'Please select atleast one appointment.',
        }));
        
    }

    hideBatchModal(event){
        this.showBatchModal =false;
        if(event.detail == 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        }
    }

    inprogressAppointment() {
      
        markInProgress({appointmentId: this.appointmentId})
        .then(result =>{
            if(result == 'success'){
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                    element.retrieveRecords();
                });
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: result,
                }));
            }
        })
        .catch(error =>{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }
}