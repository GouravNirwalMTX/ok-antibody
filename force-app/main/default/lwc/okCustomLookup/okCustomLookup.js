import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getRecord } from 'lightning/uiRecordApi';
import { api, LightningElement, track, wire } from 'lwc';
import findRecords from "@salesforce/apex/okLookUpController.searchDB";

let FIELDS = ['Contact.Name'];

export default class LookupLwc extends LightningElement {

    @api valueId;
    @api objName;
    @api iconName;
    @api labelName;
    @api readOnly = false;
    @api filter = '';
    @api showLabel = false;
    @api uniqueKey;
    objLabelName;
    @api contractId;
    @api contractConfigId ;
    @api isMedicare = false;
    @api isMedicaid = false;
    /*Create Record Start*/
    @api createRecord;
    @track recordTypeOptions;
    @track createRecordOpen;
    @track recordTypeSelector;
    @track mainRecord;
    @track isLoaded = false;
    @track selectedRecord;
    @track nameLable;
    @api showRemoveButton = false;
    
    //stencil
    @track cols = [1,2];
    @track opacs = ['opacity: 1', 'opacity: 0.9', 'opacity: 0.8', 'opacity: 0.7', 'opacity: 0.6', 'opacity: 0.5', 'opacity: 0.4', 'opacity: 0.3', 'opacity: 0.2', 'opacity: 0.1'];
    @track double = true;

    //For Stencil
    @track stencilClass = '';
    @track stencilReplacement = 'slds-hide';  
    //css
    @track myPadding = 'slds-modal__content';
    /*Create Record End*/

    @track searchTerm;
    @track valueObj;
   // @api recordName ;
    @api 
    set recordName(val) {
        if(val != undefined ){
            this.selectedRecord = JSON.parse(JSON.stringify(val));
            console.log('IOn LOokup compny name >>' , JSON.parse(JSON.stringify(val)))
           // this.nameLable = this.selectedRecord.name;
        }else{
           
            this.selectedRecord = undefined;
            this.nameLable = '';
            this.options = undefined;
            this.error = undefined;
            this.isValue = false;
            this.searchTerm = '';
         //   this.nameLable = undefined;
        }
       
      //  console.log('after InLookup Company>>', JSON.stringify(this.selectedRecord));
       
       // this.setVaule();
    }
    get recordName() {
        return this.selectedRecord;
    }
    href;
    @track options; //lookup values
    @track isValue;
    @track blurTimeout;
    @api isSubContractorUser;

    blurTimeout;

    //css
    @track boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    @track inputClass = '';

    setVaule(){
        this.selectedRecord = this.recordName;
        console.log('setValue>>' + JSON.stringify(this.recordName))
    }
    connectedCallback() {
       // this.isValue = true;
       console.log('recordLOOKUP>' , JSON.stringify(this.recordName))
        if(this.recordName != undefined ){
            this.selectedRecord = this.recordName;
         //   this.nameLable = this.selectedRecord.name;
            this.isValue = true;
        }
    }
    renderedCallback() {
        this.selectedRecord = this.recordName;
        if(this.objName) {
            let temp = this.objName;
            if(temp.includes('__c')){
                let newObjName = temp.replace(/__c/g,"");
                if(newObjName.includes('_')) {
                    let vNewObjName = newObjName.replace(/_/g," ");
                    this.objLabelName = vNewObjName;
                }else {
                    this.objLabelName = newObjName;
                }
                
            }else {
                this.objLabelName = this.objName;
            }
        }
    
    }
    onSelect1(event) {
        let ele = event.currentTarget;
        let selectedId = ele.dataset.id;
        //As a best practise sending selected value to parent and inreturn parent sends the value to @api valueId
        let key = this.uniqueKey;
       
        
        var name = '';
        var record ;
        this.options.forEach(function(rec){
            if(rec.Id == selectedId){
                name = rec.Name;   
                record = rec;             
            }
        });
       
        this.valueObj = name;
        this.isValue = true;
        const valueSelectedEvent = new CustomEvent('valueselect', {
            detail: { selectedId, key, record },
        });
        this.dispatchEvent(valueSelectedEvent);

        if(this.blurTimeout) {
            clearTimeout(this.blurTimeout);
        }
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    }

    handleClick() {
        this.handleOnchange();
        //this.searchTerm = '';
        this.inputClass = 'slds-has-focus';
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open';
        //let combobox = this.template.querySelector('#box');
        //combobox.classList.add("slds-is-open"); 
    }

    inblur() {
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.blurTimeout = setTimeout(() =>  {this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus'}, 300);
    }

   

    onChange(event) {
        this.searchTerm = event.target.value;
        this.handleOnchange();
    }

    handleRemovePill1() {
        this.isValue = false;
        let selectedId = '';
        let key = this.uniqueKey;
        this.searchTerm = '';
        const valueSelectedEvent = new CustomEvent('valueselect', {
            detail: { selectedId, key },
        });
        this.dispatchEvent(valueSelectedEvent);
    }


    handleRecTypeChange(event) {
        this.recordTypeId = event.target.value;
    }

    createRecordMain() {
        this.recordTypeSelector = false;
        this.mainRecord = true;
        //stencil before getting data
        this.stencilClass = '';
        this.stencilReplacement = 'slds-hide';
    }

    handleLoad(event) {
        let details = event.detail;

        if(details) {
            setTimeout(() => {
                this.stencilClass = 'slds-hide';
                this.stencilReplacement = '';
                this.myPadding = 'slds-p-around_medium slds-modal__content';
            }, 1000);
        }

    }

    handleSubmit() {
        this.template.querySelector('lightning-record-form').submit();
    }

    handleError() {

        this.dispatchEvent(
            new ShowToastEvent({
                title : 'Error',
                message : 'Error saving the record',
                variant : 'error',
            }),
        )
    }

    closeModal() {
        this.stencilClass = '';
        this.stencilReplacement = 'slds-hide';
        this.createRecordOpen = false;
        this.recordTypeSelector = false;
        this.mainRecord = false;
    }

    onSelect(event){
        var index = event.currentTarget.dataset.number;      
        this.selectedRecord = this.options[index];
        this.nameLable = this.selectedRecord.name;
        console.log('Selected Recdor>>' , JSON.stringify(this.selectedRecord));
        this.isValue = true;
        const valueChangeEvent = new CustomEvent("select", {
        detail: this.selectedRecord
        });
        // Fire the custom event
        this.dispatchEvent(valueChangeEvent);
    }

    handleRemovePill(event){
        event.preventDefault();
        this.selectedRecord = undefined;
        this.nameLable = '';
        this.options = undefined;
        this.error = undefined;
        this.isValue = false;
        this.searchTerm = '';
        /* fire the event with the value of undefined for the Selected RecordId */
        const selectedRecordEvent = new CustomEvent(
            "removeactivity",
            {
                detail : 'remove'
            }
        );
        this.dispatchEvent(selectedRecordEvent);
    }

    handleOnchange(){
        //event.preventDefault();
       console.log('MEDICAID <MEDOCare >>' , this.isMedicare +' ' + this.isMedicaid)
        findRecords({
            searchText : this.searchTerm, 
            isMedicaid : this.isMedicaid,
            isMedicare : this.isMedicare
        })
        .then(result => {
           // console.log(' records>>> ', JSON.stringify(result));
            this.options = JSON.parse(JSON.stringify(result));
           
            // for(let i=0; i < this.records.length; i++){
            //     const rec = this.records[i];
            //     this.records[i].activityLabel = rec.activityLabel;
            // }
            this.error = undefined;
            //console.log(' records ', this.records);
        })
        .catch(error => {
            this.error = error;
           // this.records = undefined;
        });
    }
}