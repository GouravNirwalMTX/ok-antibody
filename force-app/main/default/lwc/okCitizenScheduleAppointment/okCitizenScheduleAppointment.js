import { LightningElement, api, track } from 'lwc';
import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateCovidAppointment';
import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import createAppointment from '@salesforce/apex/DC_ScheduleAppointment.createAppointment';
import addAppointment from '@salesforce/apex/DC_ScheduleAppointment.addNewAppointment';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class OkCitizenScheduleAppointment extends LightningElement {
   
	@track options = [];
	@track selectedOption;
	@track isAttributeRequired = false;
	@track contactid;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track preferredTime;
    @track preferredDate;
    @track buttonLabelValue;
    @track slotId;
    @track showSchedule = false;
    @track showMapModal = false;
    @api objectName;
    @api fieldName;
    @api selectvalue;
    @api hidePrefferedTime = false;
    @api testingType = '';
    @api showConfirmationOnReschedule = false;
    @api isAntibody;
    @api newAppointment = false;
    @api recordid;
    @api ifcovid = false;
    @api appointmentDataIntake = {};
    @api isDependent = false;

	connectedCallback() {
        if(this.ifcovid){
            this.buttonLabelValue = 'Schedule & Submit';
        }else {
            this.buttonLabelValue = 'Schedule Appointment';
        }

        if(!this.newAppointment && !this.isDependent){
            getAppointmentDetailrec({appointmentId: this.recordid})
            .then(data => {
                let appointment = data.appointment;
                this.contactid = appointment.Patient__c;
                this.preferredTime = this.msToTimeAMPM(appointment.Patient__r.Preferred_Time__c);
                this.preferredDate = appointment.Patient__r.Preferred_Date__c;
                if(appointment.Status__c == 'Cancelled' || appointment.Status__c == 'Scheduled' || appointment.Status__c == 'To Be Scheduled' || appointment.Status__c == 'Eligible' || appointment.Status__c == null || typeof appointment.Status__c == 'undefined') {
                    // this.retrieveTestingSites();
                    this.isSiteSelected = true;
                } else {
                    this.dispatchEvent(new CustomEvent('close', {}));
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: 'You are not allowed to update this appointment.',
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
        }
        
    }
    
    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectvalue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }
    disableButton(event) {
        this.showSchedule = false;
        //this.slotId = event.detail;
    }
    createAppointmentRecord() {
        if(this.newAppointment){
            addAppointment({ accountId : this.selectvalue, appointmentId : this.recordid, slotId : this.slotId, testingType : this.testingType}).then(result => {
                if (result.type == 'error') {
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                } else if (result.type == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                    // this.createCalendar(result);
                    this.dispatchEvent(new CustomEvent('create', {
                        detail: 'save'
                    }));
                }
                
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: error.message,
                });
                this.dispatchEvent(event);
            });

        }else if(this.showConfirmationOnReschedule){
            let data = {
                recordId : this.recordid,
                actionName : 'Re-Schedule',
                slotId : this.slotId,
                accountId : this.selectvalue,
                testingType : this.testingType,
                showConfirmation : true
            };
            this.dispatchEvent(new CustomEvent('paginatoraction', { detail : data }));
        }else if(this.isDependent){
            createAppointment({ testingType : this.testingType, appointmentJSON : JSON.stringify(this.appointmentDataIntake), slotId : this.slotId, accountId : this.selectvalue})
            .then(result => {
                if (result.type == 'error') {
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                } else if (result.type == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                    // this.createCalendar(result);
                    this.dispatchEvent(new CustomEvent('close', {
                        detail: 'save'
                    }));
                }
                
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: error.message,
                });
                this.dispatchEvent(event);
            });

        }else{
            updateAppointmentrec({ accountId : this.selectvalue, appointmentId : this.recordid, slotId : this.slotId, testingType : this.testingType}).then(result => {
                if (result.type == 'error') {
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                } else if (result.type == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: result.message,
                    });
                    this.dispatchEvent(event);
                    // this.createCalendar(result);
                    this.dispatchEvent(new CustomEvent('close', {
                        detail: 'save'
                    }));
                }
                
            })
            .catch(error => {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: error.message,
                });
                this.dispatchEvent(event);
            });
        }
       
    }
    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
    justCloseModal(){
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }

    handleMapModalClose(event) {
        this.showMapModal = false;
        
        if(event.detail != 'close') {
            this.selectvalue = event.detail
        }
        
    }

    msToTimeAMPM(s) {
        if(s == '' || s == null || typeof s == 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if(hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' '  + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    
}