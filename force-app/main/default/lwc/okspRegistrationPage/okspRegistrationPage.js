import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/OK_SelfRegistration.selfRegisterContact';
import contactType from '@salesforce/label/c.OKSF_Registration_Covid_Citizen';

export default class OkpcRegistrationpage extends NavigationMixin(LightningElement) {

    label = {
        contactType
    }
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track portal = [{label:'Citizen (COVID)',value:"COVID"},{label:'Citizen (Antibody)',value:'Antibody'}];

    customImage = nysdohResource;

    firstNameChange(event) {
        this.firstName = event.target.value;
        console.log('firstName=> ', this.firstName);
    }
    lastNameChange(event) {
        this.lastName = event.target.value;
        console.log('lastName=> ', this.lastName);
    }
    phoneChange(event) {
        this.phone = event.target.value;
        console.log('phone=> ', this.phone);
    }
    emailChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }
    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    connectedCallback() {
       /* getAccount()
        .then(result => {
            console.log('result'+JSON.stringify(result));
            this.accounts = result;
            console.log('acc'+this.accounts )
        })
        .catch((error) => {
            let message = error.message || error.body.message;
            console.log(message);
        }); */
    }

    doRegisteration() {
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        if (this.firstName == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if (this.lastName == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        if (this.phone == '' || typeof this.phone == 'undefined') {
            this.errorMessage = 'Please provide Mobile Number';
            this.showError = true;
            return;
        }
        if (!this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }
        

        register({ firstName: this.firstName, lastName: this.lastName, phone: this.phone, email: this.email, type : this.label.contactType})//Added delimiter by Sajal
            .then(data => {
                console.log('Data-->' + JSON.stringify(data));
                if (data.type == 'error') {
                    this.showError = true;
                    this.errorMessage = data.message;
                } else if (data.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = data.message;
                }

            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                var msg = JSON.stringify(error.body.message);
               // this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }



    validateEmail(email) {
        var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

}