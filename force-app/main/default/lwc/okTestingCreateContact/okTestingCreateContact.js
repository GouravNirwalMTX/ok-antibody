import {
  LightningElement,
  track,
  api,
  wire
} from "lwc";
import {
  NavigationMixin
} from "lightning/navigation";
import {
  createRecord
} from "lightning/uiRecordApi";
import CONTACT_OBJECT from "@salesforce/schema/Contact";
import register from "@salesforce/apex/OK_SelfRegistration.saveUser";
import createContact from "@salesforce/apex/OK_SelfRegistration.createContactFromTestingSite";
//import contactType from '@salesforce/label/c.OK_Registration_Type_VR_Citizen_Consultation';
import getBusinessTimeOnAccount from "@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount";
// import { data } from 'okpcContactInfo/data';
import {
  getPicklistValues
} from "lightning/uiObjectInfoApi";
import {
  getObjectInfo
} from "lightning/uiObjectInfoApi";
import RACE_FIELD from "@salesforce/schema/Contact.Race__c";
import GENDER_FIELD from "@salesforce/schema/Contact.Gender__c";

export default class OkTestingCreateContact extends NavigationMixin(
  LightningElement
) {
  // label = {
  //     contactType
  // }

  @track showError = false;
  @track showSuccess = false;
  @track errorMessage = "";
  @track thankYouMessage = "";
  @track firstName = "";
  @track lastName = "";
  @track email = "";
  @track phone = "";
  @track race = "";
  @track gender = "";
  @track emailOpt = true;
  @track smsOpt = false;
  @track accountId;
  @track portalName;
  @track showThankYou = false;
  @track accounts = [];
  @track delimiter = ".vrcpuser";
  @track portal = [{
      label: "Citizen (COVID)",
      value: "COVID"
    },
    {
      label: "Citizen (Antibody)",
      value: "Antibody"
    }
  ];

  @track account;
  @track contactType;
  @track raceOptions;
  @track genderOptions;
  @track scheduled = true;
  @track todayDate;
  @track dob;
  @track phoneRequired = false;
  @track isCovid = false;
  @track isAntibody = false;
  @track disableSubmitButton = true;

  get contactTypeOptions() {
    return [{
        label: "--None--",
        value: ""
      },
      {
        label: "COVID Test",
        value: "Citizen_COVID"
      },
      {
        label: "Antibody Test",
        value: "Citizen"
      }
    ];
  }
  @wire(getObjectInfo, {
    objectApiName: CONTACT_OBJECT
  })
  objectInfo;

  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: RACE_FIELD
  })
  setRacePicklistOptions({
    error,
    data
  }) {
    if (data) {
      // Apparently combobox doesn't like it if you dont supply any options at all.
      // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
      this.raceOptions = data.values;
    } else if (error) {}
  }

  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: GENDER_FIELD
  })
  setGenderPicklistOptions({
    error,
    data
  }) {
    if (data) {
      // Apparently combobox doesn't like it if you dont supply any options at all.
      // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
      this.genderOptions = data.values;
    } else if (error) {}
  }

  handleChange(event) {
    var name = event.target.name;
    // alert(' value   ' + event.target.value);
    if (name == "firstName") this.firstName = event.target.value;
    else if (name == "lastname") this.lastName = event.target.value;
    else if (name == "phone") this.phone = event.target.value;
    else if (name == "email") this.email = event.target.value;
    else if (name == "contactType") {
      this.contactType = event.target.value;
      if (this.contactType == "Citizen") {
        this.isAntibody = true;
        this.isCovid = false;
        this.disableSubmitButton = false;
      } else if (this.contactType == "Citizen_COVID") {
        this.isCovid = true;
        this.isAntibody = false;
        this.disableSubmitButton = false;
      } else {
        this.isAntibody = false;
        this.isCovid = false;
        this.disableSubmitButton = true;
      }
    } else if (name == "race") this.race = event.target.value;
    else if (name == "gender") this.gender = event.target.value;
    else if (name == "smsOpt") {
      this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
      if (this.smsOpt) this.phoneRequired = true;
      else this.phoneRequired = false;
    } else if (name == "emailOpt")
      this.emailOpt = this.template.querySelector(
        '[data-field="email"]'
      ).checked;
    else if (name == "scheduled")
      this.scheduled = this.template.querySelector(
        '[data-field="scheduled"]'
      ).checked;
    else if (name == "dob") this.dob = event.target.value;
  }

  accountChange(event) {
    this.accountId = event.target.value;
  }
  portalChange(event) {
    this.portalName = event.target.value;
  }

  connectedCallback() {
    //  this.todayDate = this.getTodayDate();
    getBusinessTimeOnAccount()
      .then((result) => {
        this.account = result;
        if (result.Type_of_Contact__c === "Consultation") {
          this.contactType = "Citizen_Consultation_Vital_Records";
        } else if (result.Type_of_Contact__c === "Lobby") {
          this.contactType = "Citizen_Lobby_Vital_Records";
        }
      })
      .catch((error) => {
        console.log(
          "error while fetching self account: ",
          JSON.stringify(error)
        );
      });
  }

  /*   doRegisteration() {
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        this.firstName = this.firstName.trim();
        this.lastName = this.lastName.trim();
        if (this.contactType == '' || typeof this.contactType == 'undefined') {
            this.errorMessage = 'Please Select a contact type';
            this.showError = true;
            return;
        }

        if (this.firstName == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if(!this.validateName(this.firstName)){
            this.errorMessage = 'First Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }
        
        if (this.lastName == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        if(!this.validateName(this.lastName)){
            this.errorMessage = 'Last Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }
        if (this.email.trim() == '' || typeof this.email == 'undefined') {
            this.errorMessage = 'Please provide Email';
            this.showError = true;
            return;
        }
        if (!this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }

        if(this.smsOpt){
            if(this.phone == '' || typeof this.phone == 'undefined'){
                this.errorMessage = 'Please provide Mobile Number';
                this.showError = true;
                return;
            }
        }

        if ((this.phone != '' && typeof this.phone != 'undefined') && !this.validatePhone(this.phone)) {
            this.errorMessage = 'Phone number cannot contain any special characters';
            this.showError = true;
            return;
        }
    
        let cont = { 'sobjectType': 'Contact' };
        cont.FirstName = this.firstName;
        cont.LastName = this.lastName;
        cont.MobilePhone = this.phone;
        cont.Email = this.email;
        cont.Auto_Scheduled__c = this.scheduled;
        cont.Opt_out_for_SMS_Updates__c = !(this.smsOpt);
        cont.HasOptedOutOfEmail = !(this.emailOpt);
        cont.Birthdate = this.dob;
        cont.Gender__c = this.gender;
        cont.Race__c =  this.race;
        cont.Testing_Site__c = this.account.Id;
    
        createContact({c : cont, recordType : this.contactType})
            .then(result => {
                if (result.type != 'success') {
                    //alert('fh')
                    this.showError = true;
                    this.errorMessage = result.message;
                } else if (result.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = result.message;
                    console.log('navigateToForgotPassword ' + result.redirectURL);
                    //location.href = data.redirectURL;
                }
                
            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
                var msg = JSON.stringify(error.body.message);
                // this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }*/

  doRegistration() {
    if (this.isAntibody) {
      this.template
        .querySelector("c-ok-testing-create-antibody-contact")
        .createContact();
    } else if (this.isCovid) {
      this.template
        .querySelector("c-ok-testing-create-covid-contct")
        .validateZipAndSave();
    }
  }

  validateEmail(email) {
    var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
    //var regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var isValid = email.match(regExpEmail);
    return isValid;
  }

  validateName(name) {
    var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
    var isValid = name.match(regExpEmail);
    return isValid;
  }

  validatePhone(mobile) {
    var regExpEmail = /^[0-9]*$/;
    var isValid = mobile.match(regExpEmail);
    return isValid;
  }

  handleError(event) {
    let data = event.detail;
    console.log("HIi error ", JSON.stringify(data));
    this.showError = true;
    this.errorMessage = data.errorMsg;
  }

  handleSuccess(event) {
    let data = event.detail;
    console.log("HIi  ", data);
    this.showThankYou = true;
    this.thankYouMessage = data.successMsg;
  }
}