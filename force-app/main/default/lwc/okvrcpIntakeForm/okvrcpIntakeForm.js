import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    registerListener,
    unregisterAllListeners,
    fireEvent
} from "c/pubsub";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getContactInformation from "@salesforce/apex/OkpcContactInformationController.getContactInformation";
import changeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.changeAppointmentStatus";
import notChangeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.notChangeAppointmentStatus";
// import setCurrentStep from "@salesforce/apex/OkpcContactInformationController.setCurrentStep";

export default class OkVitalIntakeForm extends LightningElement {
    @track currentStep = 1;
    @track originalStep = 1;
    @track showPersonalInformation = true;
    @track showConsent = true;
    @track showAppointmentPreference = false;
    @track showPriorExposure = false;
    @track showSymptoms = false;
    @track showSpinner = false;
    @track edit = false;
    @track showthankYou = false;
    @track source;
    @track hideStep3 = false; //ad
    @track showDashboard;
    @track editProfileMode = false;

    connectedCallback() {
        registerListener("handleSideBarClick", this.handleSideBarClick, this);
        console.log('inside conn');
        this.getStep();
    }

    // setStep(step){
    //     setCurrentStep({currentStep: step})
    //     .then(result =>{
    //         console.log('step saved');
    //     })
    //     .catch(error =>{
    //         console.log('error');
    //     })
    // }

    getStep() {
        getContactInformation()
            .then(result => {
                console.log(JSON.stringify(result));
                if (result.Source__c == 'MMS') {
                    this.hideStep3 = true;
                } else {
                    this.hideStep3 = false;
                }
                if (result.Current_Step__c) {
                    this.originalStep = result.Current_Step__c;
                    this.handleSideBarClick(this.originalStep);
                }
                if (result.Form_Submitted__c == true) {
                    this.showDashboard = true;
                } else {
                    this.showDashboard = false;
                }

            })
            .catch(error => {
                console.log('error');
            })
    }

    clearAllSteps() {
        this.showConsent = false;
        this.showPersonalInformation = false;
        this.showAppointmentPreference = false;
        this.showPriorExposure = false;
        this.showSymptoms = false;
        this.showthankYou = false;
      //  this.editProfileMode = false;
    }

    navigateToAppointment(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okvrcp-personal-information').saveContacts();
        } else if(event.target.name == 'cancel'){
            this.clearAllSteps();
            this.showDashboard = true;
        }else {
                this.clearAllSteps();
                this.showAppointmentPreference = true;
                this.currentStep = 2;
                this.determineOriginalStep();
        }
    }
   
    navigateToConsent() {
        this.clearAllSteps();
        this.currentStep = 1;
        this.determineOriginalStep();
        this.showConsent = true;
    }

    handleEdit() {
        this.edit = true;
        this.template.querySelector('c-okvrcp-personal-information').editContacts();
    }

    handleCancel() {
        this.edit = false;
        this.template.querySelector('c-okvrcp-personal-information').handleCancelButton();
    }
    handleSave() {
        this.edit = false;
        this.template.querySelector('c-okvrcp-personal-information').saveContacts();
    }

    determineOriginalStep() {
        if (this.originalStep >= this.currentStep) {
            this.originalStep = this.originalStep;
        } else {
            this.originalStep = this.currentStep;
        }
    }

    handleSideBarClick(currentStep) {
        console.log('isxode cnj');
        this.currentStep = currentStep;
        this.clearAllSteps();
       
            switch (currentStep) {
                case 1: {
                    this.showPersonalInformation = true;
                    break;
                }
                case 2: {
                    this.showAppointmentPreference = true;
                    break;
                }
               
            }
        

    }

    handleSubmitData(event){
        if(event.detail.name == 'submit'){
          this.handleSubmit(event);
         }
    }

    handleSubmit(event) {
        if (event.target.name == 'submit' || event.detail.name == 'submit'  ) {
            this.template.querySelector('c-okvrcp-preference-component').setValues();
        }
        this.clearAllSteps();
            this.currentStep = 2;
        this.determineOriginalStep();
        // this.showthankYou = true;
        console.log(this.currentStep, this.originalStep);
        if(event.target.name == 'submit'){
            changeAppointmentStatus({
                currentstep: this.currentStep
            })
            .then(result => {
                this.showthankYou = false;
                // window.open('/s/dashboard', '_self');
                this.showDashboard = true;
            })
        }
        notChangeAppointmentStatus({
                currentstep: this.currentStep
            })
            .then(result => {
                this.showthankYou = false;
                // window.open('/s/dashboard', '_self');
                this.showDashboard = true;
            })
            .catch(error => {
                console.log('error status change');
            })

    }
    
    handleThankYouBack() {
        // this.navigateToConsent();
        this.showthankYou = false;
        window.open('/s/dashboard', '_self');
    }

  

    saveContacts(event) {
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.clearAllSteps();
            console.log('saveee   1' + this.editProfileMode);
            if(this.editProfileMode == true){
                console.log('saveee1');
                this.showDashboard = true;
            }else{
                this.currentStep = 2;
                this.determineOriginalStep();
                this.showAppointmentPreference = true;
            }
        }
        
    }

    navigateToPersonalInfo(event) {
        if (event.target.name == 'back') {
            this.clearAllSteps();
            this.showPersonalInformation = true;
            this.currentStep = 1;
            this.determineOriginalStep();
        }
    }

    editProfile(event){
        console.log('Event>>>' + event);
        this.clearAllSteps();
        this.showDashboard = false;
        this.showPersonalInformation = true;
        this.currentStep = 1;
        this.originalStep = 1;
        this.editProfileMode = true;
        this.determineOriginalStep();
    }


}