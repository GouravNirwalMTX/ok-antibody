import {
    LightningElement,
    api,
    track
} from 'lwc';
import getSiteAccounts from '@salesforce/apex/DC_ScheduleAppointment.getSiteAccounts';
// import getAccountSlots from '@salesforce/apex/DC_ScheduleAppointment.getAccountSlots';
// import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateAppointment';
// import getSelectedTestingSiterec from '@salesforce/apex/DC_ScheduleAppointment.getSelectedTestingSite';
// import createAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.createAppointment';
// import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import getPreferredValues from '@salesforce/apex/OkpcContactInformationController.getPreferredValues';
import setPreferredValues from '@salesforce/apex/OkpcContactInformationController.setPreferredValues';
// import getAccountdetails from '@salesforce/apex/OkpcContactInformationController.getAccountdetails';
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import getAppointmentDetail from '@salesforce/apex/OKPC_IntakeRequestController.getAppointmentData';
import getAppointmenttestingTypeDetail from '@salesforce/apex/OKPC_IntakeRequestController.getAppointmentDataForCovidReschedule';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'

export default class OkpcPreferenceComponent extends LightningElement {
    // map = new map();
    
    @track options = [];
    @track selectedOption;
    @track isAttributeRequired = false;
    @track contactid;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track selectValue = null;
    @track appointmentId = null;
    @track selectSiteName;
    @track slotId;
    @track showSchedule = false;
    @track canSchedule = false;
    @track showMapModal = false;
    @track showCalendar = false;
    @track preferredDate = null;
    @track preferredTime = null;
    @track testingSite;
    @track showtestingSite = false;
    @track accountDetails= {};
    @track showLocationDetails = false;
    @track showSpinner = false;
    @track showSingleMap = false;
    @track mapMarkers = [];
    @track location={};
    @track todayDate;
    @track preferredTimeMin = null;
    @track preferredTimeMax = null;
    @track testingSiteType = [];
    @track contactTestingType;
    @track appointmentDetail = {};
    @track finaltestingTypeReturned;
    @api appointmentIdDashboard;
    @api multipleAppnt = false;
    @api isReschedule = false;
    @api recordid;
    @api accountIdReschedule;
    @api appointmentDataIntake = {};
    @api ifcovid = false;
    @api showpickfromdashboard = false;
    @api testingType;
    @api objectName;
    @api fieldName;
    @api istesting;
    @api isDependent = false;

    map = new Map();
    connectedCallback() {
        console.log('in preference appointment details>>>>', JSON.parse(JSON.stringify(this.appointmentDataIntake)))
        console.log('in preference appointment details testing type>>>>', JSON.parse(JSON.stringify(this.appointmentDataIntake)).testingType)
        console.log('isDependent in preference compoenent', this.isDependent);
        console.log('is Reschedule>>>', this.isReschedule);
        if(!this.isReschedule){
            this.isDependent = true;
        }
        this.appointmentDetail = JSON.parse(JSON.stringify(this.appointmentDataIntake));
        this.contactTestingType = this.appointmentDetail.testingType;
        console.log('testting type >>>>' , this.contactTestingType)
        this.ifcovid = true;
        this.todayDate = this.getTodayDate();
        this.retrieveTestingSites();
        if(this.appointmentIdDashboard){
            this.getAccountDetailsAndAppiontmentDetailsIfReschedule();
        }
       // this.getValuesPreferred();
     //   this.getAppointment();
     //   this.getContact();
       
    }

    getAccountDetailsAndAppiontmentDetailsIfReschedule(){
        console.log('In prefrecence to get acc')
        if(this.isReschedule){
            this.appointmentId = this.appointmentIdDashboard;
            this.selectValue = this.accountIdReschedule;
            this.getAppointMentTestingType();
            this.getAccount(this.accountIdReschedule);
        }
       
    } 
  
     getTestingTypeBySelectedAccount(){
       console.log('>>>>In testing>??' , this.contactTestingType)
        if(this.testingSiteType.includes(this.contactTestingType)){
            this.finaltestingTypeReturned = this.contactTestingType;
            return this.finaltestingTypeReturned;
        }else if(this.testingSiteType.length >= 1){
            this.testingSiteType.forEach(element => {
                if(element != 'Antibody Testing'){
                   this.finaltestingTypeReturned = element;
                    return this.finaltestingTypeReturned;
                }
            })
        }else{
            this.finaltestingTypeReturned = 'Anitbody Returned';
            return this.finaltestingTypeReturned;
        }
       
    }
    

    getAppointMentTestingType(){
        getAppointmenttestingTypeDetail({
            appId: this.appointmentId
        })
        .then(result => {
            console.log('getAppointmenttestingTypeDetail>>' + JSON.stringify(result));
            this.contactTestingType = result.appointmentObj.appTestingType;
            console.log('RescheduleType>>'+ this.contactTestingType)
            this.getTestingTypeBySelectedAccount();
        })
        .catch(error => {
            console.log('error in data ', JSON.stringify(error));
            
        })
    }

    retrieveTestingSites() {
        getSiteAccounts({})
            .then(data => {
                data.forEach((account) => {
                    var optionValue = new Object();
                    optionValue.label = account.Name;
                    optionValue.value = account.Id;
                    this.accountList.push(optionValue);
                    this.map.set(account.Id, account.Name);
                });
                this.options = this.accountList;
            })
            .catch(error => {
                console.log('Error-->', error);
            });
    }

    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectValue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleCloseModalonSave() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    justCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }
    showAppointments(){
        console.log('app id ', this.appointmentId);
        console.log('accId id ', this.accountDetails);
        this.showCalendar = true;
    }
    hideModal(event){
        this.showCalendar = false;
        if(event.detail === 'save'){
            let tempObj = {};
                tempObj.name = "submit";
            const selectedEvent = new CustomEvent("submit", {
                detail: tempObj
            });
            this.dispatchEvent(selectedEvent);
        }
    }

    handleMapModalClose(event) {
        this.showMapModal = false;

        if (event.detail != 'close') {
            this.selectValue = event.detail
            this.canSchedule = true;
            this.getAccount(this.selectValue);
            
        }
        this.selectSiteName = this.map.get(this.selectValue);

    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    getAccount(Id) {
        this.showSpinner = true;
        getAccountdetails({
                accountId: Id
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                this.location = result.location;
                console.log('account details', JSON.stringify(result));
                if(this.accountDetails){
                    this.showLocationDetails = true;
                    if(this.accountDetails.testingType.includes(";")){
                        this.testingSiteType = this.accountDetails.testingType.split(';');
                    }else{
                        this.testingSiteType = [this.accountDetails.testingType];
                    }
                    
                    this.mapMarkers.length = 0;
                    this.mapMarkers.push(this.accountDetails);
                    this.getTestingTypeBySelectedAccount();
                }
                console.log('---result.businessStartTime---'+result.businessStartTime);
                console.log('---result.businessEndTime---'+result.businessEndTime);
                console.log('---testtingSiteType---'+this.testingSiteType);
                
                if(result.businessStartTime){
                    this.preferredTimeMin = this.msToTime(result.businessStartTime);
                }
                else{
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if(result.businessEndTime){
                    this.preferredTimeMax = this.msToTime(result.businessEndTime);
                }
                else{
                    this.preferredTimeMax = '19:00:00.000Z';
                }
                this.showSpinner = false;
                //this.getTestingTypeBySelectedAccount();
            })
            .catch(error => {
                console.log('error'+ error);
                this.showSpinner = false;
            })
    }

    handleSingleMap(){
        this.showSingleMap = true;
    }

    handlesingleMapClose(){
        this.showSingleMap = false;
    }

    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

    cancelAppointments(){
        console.log('app id ', this.appointmentId);
        let tempObj = {};  
        if(!this.isReschedule){
            console.log('In cancel when no rescheduling')
           
                tempObj.name = "cancel";
                tempObj.appId = this.appointmentId;
        }else{
            console.log('In cancel when is rescheduling')
           
                tempObj.name = "submit";
                tempObj.appId = this.appointmentId;
        }
        
        const selectedEvent = new CustomEvent("submit", {
            detail: tempObj
        });
        this.dispatchEvent(selectedEvent);
    }

    // All not used code pushed to end of the page

    /*
    getAppointment(){
        this.showSpinner = true;
        getAppointmentDetail()
        .then(result => {
            console.log('app detail ', result.appointmentId);
            if(!this.appointmentIdDashboard){
                this.appointmentId = result.appointmentId;            
                console.log('cc ** ' + this.appointmentId);
            }else{
                this.appointmentId = this.appointmentIdDashboard;
                console.log('Is Reschedule>>' + this.isReschedule);
                if(this.isReschedule){
                    this.getAppointMentTestingType();
                }
                
              //  this.contactTestingType = result.appointmentObj.appTestingType;
             //   console.log('cc else ** ' + this.appointmentId);
             //   console.log('cc else From DashBoard ** ' + this.contactTestingType);
            //    getTestingTypeBySelectedAccount();
            }
            
        })
        .catch(error => {
            console.log('error in data ', JSON.stringify(error));
            
        })
        this.showSpinner = false;

    }
*/


   /* 
    getContact() {
        this.showSpinner = true;
        getContactDetails()
            .then(result => {
               // console.log('data=>', JSON.stringify(result));
                this.canSchedule = result.canSchedule;
                this.contactTestingType = result.testingType;
                // this.extraWrapper = {...this.dataObj};
                console.log(JSON.stringify(this.contactTestingType));
                this.showSpinner = false;

            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }
    */

  /*  getValuesPreferred() {
        console.log('valieOfIstesting'+ this.istesting)
        this.showSpinner = true;
        getPreferredValues()
            .then(result => {
                console.log('preferred values: ', JSON.stringify(result));
                this.preferredDate = result.Preferred_Date__c;
               
                if (result.Preferred_Time__c) {
                    this.preferredTime = this.msToTime(result.Preferred_Time__c);
                }
                if (result.Testing_Site__c) {
                    this.selectValue = result.Testing_Site__c;
                        console.log('Prefrecne cmp Accid no dashboard>' + this.selectValue)
                        this.getAccount(this.selectValue);
                    this.showtestingSite = true;
                    if(this.istesting){
                        this.showtestingSite = false;
                    }
                        this.showLocationDetails = true;
                    
                    if(this.showpickfromdashboard){
                        this.showtestingSite = false;
                    }else{
                        this.showtestingSite = true;
                    }
                   
                }else if(this.appointmentIdDashboard){
                        if(this.isReschedule){
                            this.selectValue = this.accountIdReschedule;
                            this.getAccount(this.accountIdReschedule);
                        }
                       
                } */
                /*if(result.Testing_Site__r.Business_Start_Time__c){
                    this.preferredTimeMin = this.msToTime(result.Testing_Site__r.Business_Start_Time__c);
                }
                else{
                    this.preferredTimeMin = '07:00:00.000Z';
                }
                if(result.Testing_Site__r.Business_End_Time__c){
                    this.preferredTimeMax = this.msToTime(result.Testing_Site__r.Business_End_Time__c);
                }
                else{
                    this.preferredTimeMax = '19:00:00.000Z';
                }*/
            /*    this.showSpinner = false;

            })
            .catch(error => {
                console.log('error in data ', JSON.stringify(error));
                this.showSpinner = false;
            })
    }
      */

  /*  @api
    setValues() {
        setPreferredValues({
                preferredDate: this.preferredDate,
                preferredTime: this.preferredTime
                //preferredSite: this.selectValue
            })
            .then(result => {
                console.log('saved success');
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                this.handleCloseModalonSave();
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                console.log('error ', JSON.stringify(error));
            })
    } */

    /*
    handleInputChange(event) {
        if (event.target.name == 'preferredDate') {
            this.preferredDate = event.target.value;
        } else if (event.target.name == 'preferredTime') {
            this.preferredTime = event.target.value;
            console.log('tiem=> ', event.target.value);
        }
    } */

}