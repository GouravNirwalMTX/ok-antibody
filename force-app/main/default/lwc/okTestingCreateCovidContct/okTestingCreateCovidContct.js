import { LightningElement, track, wire, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";
import createAntibodyContact from "@salesforce/apex/OK_SelfRegistration.createContactFromTestingSite";
import validZip from "@salesforce/apex/OkpcContactInformationController.validZipCode";
import searchDB from "@salesforce/apex/okLookUpController.searchDB";
import setDefaultPrimaryInsuranceCompany from "@salesforce/apex/okLookUpController.setDefaultPrimaryInsuranceCompany";
import { getPicklistValues } from "lightning/uiObjectInfoApi";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import RACE_FIELD from "@salesforce/schema/Contact.Race__c";
import GENDER_FIELD from "@salesforce/schema/Contact.Gender__c";
import CONTACT_OBJECT from "@salesforce/schema/Contact";
import Id from "@salesforce/user/Id";
import USER_CONTACT_ACCOUNT_ID from "@salesforce/schema/User.Contact.AccountId";

export default class OkTestingCreateCovidContct extends LightningElement {
  @track dataObj = {};
  @track appointmentData = {};
  @track extraWrapper = {};
  @api contactId;
  @track showSpinner = false;
  @track disableInputs = true;
  @track testingType;
  @track diseaseSymptoms = [];
  @track EmployedinhighRisk = [];
  @track PatientResidencyType = [];
  @track pregnantOptions = [];
  @track selectedValues = [];
  @track stateOptions = [];
  @track countyOptions = [];
  @track salutations = [];
  @track LangOptions = [];
  @track maritialStatusOptions = [];
  @track ethnicityOptions = [];

  @track age;
  @track isSymptomatic = false;
  @api currentStep;
  @track todayDate;
  @track showHighRisk = false;
  @track secondRowRequired = false;
  @track thirdRowRequired = false;
  @track dynamicCSS2ndRow = "col-sm-8";
  @track dynamicCSS3nrdRow = "col-sm-4";
  @api isDependent = false;
  @track showError = false;
  @track showSuccess = false;
  @track errorMessage = "";
  @track thankYouMessage = "";
  @track firstName = "";
  @track lastName = "";
  @track email = "";
  @track phone = "";
  @track race = "";
  @track gender = "";
  @track emailOpt = true;
  @track smsOpt = false;
  @track accountId;
  @track portalName;
  @track showThankYou = false;
  @track accounts = [];
  @track delimiter = ".vrcpuser";
  @track portal = [
    { label: "Citizen (COVID)", value: "COVID" },
    { label: "Citizen (Antibody)", value: "Antibody" }
  ];
  @track dynamicCSS1stdRow = "col-sm-6";
  @track showResidencyType = false;

  @api account;
  @track contactType;
  @track raceOptions;
  @track genderOptions;
  @track scheduled = true;
  @track todayDate;
  @track dob;
  @track phoneRequired = false;
  @track isCovid = false;
  @track isAntibody = false;
  currentUserDetail = {};

  //@track isSSNRequired = true;
  @track isDLNRequired = false;
  @track showMedicareType = false;
  @track showMedicareSupplementPlan = false;
  //@track isMedicareTypeRequired = false;
  @track showRemainingInfo = false;
  //@track showMedicareNumber = false;
  //@track showMemberId = false;
  @track medicareOrMemberLabel = "Medicare Number";
  @track askReasonForTest = false;
  @track showCompanyNameField = false;
  @track insuranceCompanyRecords;
  //@track searchfield = "name";
  @track selectedRecord;
  @track insuranceIcon = "standard:contact";
  @track isGroupNumberRequired = false;
  @track memberIdValidationRegex = "^[a-zA-Z0-9]+$";
  @track memberIdLength = 30;
  @track primaryInsuranceCompanyCSS = "col-sm-8";
  @track defaultInsuranceCompanyRecords;
  @track showRemoveButton = false;

  get contactTypeOptions() {
    return [
      { label: "--None--", value: "" },
      { label: "COVID Citizen", value: "Citizen_COVID" },
      { label: "Antibody Citizen", value: "Citizen" }
    ];
  }

  @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
  objectInfo;

  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: RACE_FIELD
  })
  setRacePicklistOptions({ error, data }) {
    if (data) {
      // Apparently combobox doesn't like it if you dont supply any options at all.
      // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
      this.raceOptions = data.values;
    } else if (error) {
      console.log("777 ", error);
    }
  }

  @wire(getPicklistValues, {
    recordTypeId: "$objectInfo.data.defaultRecordTypeId",
    fieldApiName: GENDER_FIELD
  })
  setGenderPicklistOptions({ error, data }) {
    if (data) {
      // Apparently combobox doesn't like it if you dont supply any options at all.
      // Even though selectedOption was assigned in step 1, it wont "select" it unless it also has options
      this.genderOptions = data.values;
    } else if (error) {
      console.log(error);
    }
  }

  @wire(getRecord, { recordId: Id, fields: [USER_CONTACT_ACCOUNT_ID] })
  getCurrentUserDetail({ error, data }) {
    if (data) {
      //record.data.fields.fieldName.value.
      this.accountId = data.fields.Contact.value.fields.AccountId.value;
      // console.log('user data>>>' , this.accountId)
      /* console.log('user data>>>' , this.currentUserDetail)
                this.accountId = getFieldValue(this.currentUserDetail, USER_CONTACT_ACCOUNT_ID);
                console.log('Get Acc Id >>' , this.accountId)*/
    } else if (error) {
      console.log(error);
    }
  }

  // handleChange(event) {
  //   var name = event.target.name;
  //   console.log(" name   " + name);
  //   // alert(' value   ' + event.target.value);
  //   if (name == "firstName") this.firstName = event.target.value;
  //   else if (name == "lastname") this.lastName = event.target.value;
  //   else if (name == "phone") this.phone = event.target.value;
  //   else if (name == "email") this.email = event.target.value;
  //   else if (name == "contactType") {
  //     this.contactType = event.target.value;
  //     if (this.contactType == "Citizen") {
  //       this.isAntibody = true;
  //       this.isCovid = false;
  //     } else {
  //       this.isCovid = true;
  //       this.isAntibody = false;
  //     }
  //   } else if (name == "race") this.race = event.target.value;
  //   else if (name == "gender") this.gender = event.target.value;
  //   else if (name == "smsOpt") {
  //     this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
  //     if (this.smsOpt) this.phoneRequired = true;
  //     else this.phoneRequired = false;
  //   } else if (name == "emailOpt")
  //     this.emailOpt = this.template.querySelector(
  //       '[data-field="email"]'
  //     ).checked;
  //   else if (name == "scheduled")
  //     this.scheduled = this.template.querySelector(
  //       '[data-field="scheduled"]'
  //     ).checked;
  //   else if (name == "dob") this.dob = event.target.value;
  // }

  accountChange(event) {
    this.accountId = event.target.value;
    console.log("accountId=> ", this.accountId);
  }
  portalChange(event) {
    this.portalName = event.target.value;
    console.log("porstalName=> ", this.portalName);
  }
  @api
  validateZipAndSave() {
    this.showError = false;
    this.showSuccess = false;
    this.showThankYou = false;
    validZip({
      zipcode: this.dataObj.zip
    })
      .then((result) => {
        console.log("Vaild zip code>>", result);
        if (result == false) {
          this.isInValidZip = true;

          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "Error!",
              message: "Please provide a valid Zip."
            })
          );
        } else {
          var flag = 1;
          if (
            this.dataObj.socialSecurityNumber !== "" &&
            this.dataObj.socialSecurityNumber !== undefined
          ) {
            if (
              /^(\d)\1\1-\1\1-\1\1\1\1$/.test(this.dataObj.socialSecurityNumber)
            ) {
              this.dispatchEvent(
                new ShowToastEvent({
                  title: "",
                  variant: "Error!",
                  message: "Please provide a valid Social Security Number"
                })
              );
            } else {
              flag = 1;
            }
          }

          if (
            this.dataObj.drivingLicenseNumber !== "" &&
            this.dataObj.drivingLicenseNumber !== undefined
          ) {
            flag = 0;
            var drivingLicenseNumberArr = this.dataObj.drivingLicenseNumber.split(
              ""
            );
            const allEqual = (arr) => arr.every((val) => val === arr[0]);
            if (allEqual(drivingLicenseNumberArr) === true) {
              this.dispatchEvent(
                new ShowToastEvent({
                  title: "",
                  variant: "Error!",
                  message: "Please provide a valid Driving License Number"
                })
              );
            } else {
              flag = 1;
            }
          }
          if (flag === 1) {
            this.createCovidContact();
          }
        }
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }
  @api
  createContact() {
    this.showError = false;
    this.showSuccess = false;
    this.showThankYou = false;

    console.log("final app data>>>", JSON.stringify(this.dataObj));
    this.createCovidContact();
  }

  createCovidContact() {
    if (this.showError) {
      console.log("accID>>", this.accountId);
      let data = {
        errorMsg: this.errorMessage,
        showError: true
      };
      this.dispatchEvent(new CustomEvent("showerror", { detail: data }));
    }

    if (!this.isValid()) {
      let data = {
        errorMsg: "Please complete the required fields.",
        showError: true
      };
      this.dispatchEvent(new CustomEvent("showerror", { detail: data }));
      return;
    }
    if (
      this.dataObj.ifSymptomatic == "Yes" ||
      this.dataObj.whereYouLive == "Yes" ||
      this.dataObj.inContactWithpositiveCase == "Yes"
    ) {
      // this.testingType = 'Rapid Testing';
      this.dataObj.testingType = "Rapid Testing";
    } else {
      // this.testingType = 'PCR Testing';
      this.dataObj.testingType = "PCR Testing";
    }
    createAntibodyContact({
      jsonContact: JSON.stringify(this.dataObj),
      recordType: "Citizen_COVID",
      testingSite: this.accountId
    })
      .then((result) => {
        console.log("in reuslt>>>", JSON.stringify(result));
        if (result.type != "success") {
          this.showError = true;
          this.errorMessage = result.message;
          let data = {
            errorMsg: this.errorMessage,
            showError: true
          };
          this.dispatchEvent(new CustomEvent("showerror", { detail: data }));
          return;
        } else if (result.type == "success") {
          this.showThankYou = true;
          this.thankYouMessage = result.message;
          console.log("navigateToForgotPassword " + result.message);
          let data = {
            successMsg: this.thankYouMessage
          };
          this.dispatchEvent(new CustomEvent("success", { detail: data }));
        }
      })
      .catch((error) => {
        this.showError = true;
        console.log("Error-->" + JSON.stringify(error));
        var msg = JSON.stringify(error.body.message);
        // this.errorMessage = msg.substring(1, msg.length - 1);
      });
  }

  validateEmail(email) {
    var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
    //var regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var isValid = email.match(regExpEmail);
    console.log("isValid", isValid);
    return isValid;
  }

  validateName(name) {
    var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
    var isValid = name.match(regExpEmail);
    console.log("isValid--> ", isValid);
    return isValid;
  }

  validatePhone(mobile) {
    var regExpEmail = /^[0-9]*$/;
    var isValid = mobile.match(regExpEmail);
    console.log("isValid -->>>>> ", isValid);
    return isValid;
  }
  get onlyYesNoOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  get YesNoUnKnwOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      },
      {
        label: "Unknown",
        value: "Unknown"
      }
    ];
  }
  connectedCallback() {
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Disease_symptoms__c"
    }).then((result) => {
      this.diseaseSymptoms = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Employed_in_high_risk_setting__c"
    }).then((result) => {
      this.EmployedinhighRisk = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Salutation"
    }).then((result) => {
      this.salutations = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "County__c"
    }).then((result) => {
      this.countyOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Primary_Language__c"
    }).then((result) => {
      this.LangOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Marital_Status__c"
    }).then((result) => {
      this.maritialStatusOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "State__c"
    }).then((result) => {
      this.stateOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Ethnicity_Hispanic_Origin__c"
    }).then((result) => {
      this.ethnicityOptions = result;
    });

    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Insurance_Type__c"
    }).then((result) => {
      this.insuranceTypeOptions = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Select_the_Medicare_type__c"
    }).then((result) => {
      this.medicareTypeOptions = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Do_you_have_a_Medicare_Supplement_plan__c"
    }).then((result) => {
      this.medicareSupplementPlanOptions = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Relation_to_PolicyHolder__c"
    }).then((result) => {
      this.relationToPolicyHolderOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "What_is_the_reason_for_your_test_today__c"
    }).then((result) => {
      this.testReasonsOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Patient_Residency_Type__c"
    }).then((result) => {
      this.PatientResidencyType = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Pregnant__c"
    })
      .then((result) => {
        this.pregnantOptions = result;
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });

    this.dataObj.scheduled = true;
    this.todayDate = this.getTodayDate();
    console.log("ToDayDate>>", this.todayDate);
    setDefaultPrimaryInsuranceCompany()
      .then((result) => {
        this.defaultInsuranceCompanyRecords = result;
        // if (result) {
        //   for (let key in result) {
        //     if (result.hasOwnProperty(key)) {
        //       this.insuranceCompanyMap.push({ value: result[key], key: key });
        //     }
        //   }
        // }
        console.log("insuranceCompanyMap:" + JSON.stringify(result));
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });
  }

  getTodayDate() {
    let today = new Date(),
      day = today.getDate(),
      month = today.getMonth() + 1, //January is 0
      year = today.getFullYear();
    if (day < 10) {
      day = "0" + day;
    }
    if (month < 10) {
      month = "0" + month;
    }
    today = year + "-" + month + "-" + day;
    return today;
  }

  handleInputChange(event) {
    console.log(JSON.stringify(this.dataObj));
    this.dataObj[event.target.name] = event.target.value;
    if (event.target.name == "diseaseSymptoms") {
      var tempArray = this.dataObj.diseaseSymptoms;
      var x = tempArray.join(";");
      this.dataObj.diseaseSymptoms = x;
    }
    if (event.target.name == "dob") {
      this.dataObj.dob = event.target.value;
      this.calculateAge(event.target.value);
      if (!this.validateDate(event.target.value)) {
        this.dataObj.dob = "";
      }
    }
    if (this.dataObj.residentSetting == "Yes") {
      this.dynamicCSS1stdRow = "col-sm-4";
      this.showResidencyType = true;
    } else {
      this.dynamicCSS1stdRow = "col-sm-6";
      this.showResidencyType = false;
      this.dataObj.residencyType = null;
    }
    if (event.target.name == "smsCheckbox") {
      this.dataObj.smsCheckbox = event.target.checked;
    } else if (event.target.name == "emailCheckbox")
      this.dataObj.emailCheckbox = event.target.checked;
    else if (event.target.name == "scheduled")
      this.dataObj.scheduled = event.target.checked;

    if (event.target.name === "socialSecurityNumber") {
      this.isDLNRequired = false;
      //this.isSSNRequired = true;
    } else if (event.target.name === "drivingLicenseNumber") {
      this.isDLNRequired = true;
      //this.isSSNRequired = false;
    }
    if (event.target.name === "insuranceType") {
      this.memberIdLength = 30;
    }
    if (
      event.target.name === "insuranceType" ||
      event.target.name === "medicareType" ||
      event.target.name === "medicareSupplementPlan"
    ) {
      this.selectedRecord = undefined;
      this.showRemoveButton = false;
      this.insuranceCompanyRecords = undefined;
      this.dataObj.primaryInsuranceCompanyName = null;
      this.isGroupNumberRequired = false;
      this.memberIdValidationRegex = "^[a-zA-Z0-9]+$";
      this.showCompanyNameField = false;
      this.primaryInsuranceCompanyCSS = "col-sm-8";
      this.dataObj.otherPrimaryInsuranceCompanyName = "";
    }

    if (this.dataObj.insuranceType === "Medicare") {
      this.showMedicareType = true;
      this.showMedicareSupplementPlan = true;
      this.showRemainingInfo = true;
      this.medicareOrMemberLabel = "Medicare Number";
    } else if (
      this.dataObj.insuranceType === "Medicaid" ||
      this.dataObj.insuranceType === "Private Insurance"
    ) {
      this.showMedicareType = false;
      this.showMedicareSupplementPlan = false;
      this.showRemainingInfo = true;
      this.medicareOrMemberLabel = "Member Id";
      this.dataObj.medicareType = null;
      this.dataObj.medicareSupplementPlan = null;
    } else if (this.dataObj.insuranceType === "No Insurance") {
      this.showMedicareType = false;
      this.showMedicareSupplementPlan = false;
      this.showRemainingInfo = false;
      this.dataObj.medicareType = null;
    }
    if (this.dataObj.insuranceType === "Medicaid") {
      this.memberIdLength = 9;
      this.showRemoveButton = true;
      this.selectedRecord = this.defaultInsuranceCompanyRecords.find(
        (record) => record.isMedicaid === true
      );
      this.dataObj["primaryInsuranceCompanyName"] = this.selectedRecord.id;
      if (this.selectedRecord.memberIdValidationRegex !== undefined)
        this.memberIdValidationRegex = this.selectedRecord.memberIdValidationRegex;
      this.isGroupNumberRequired = this.selectedRecord.isGroupNumberRequired;
    }
    if (this.dataObj.medicareType === "Part A") {
      this.showMedicareSupplementPlan = false;
      this.showRemainingInfo = false;
    } else if (this.dataObj.medicareType === "Medicare Advantage Plan") {
      this.showMedicareSupplementPlan = false;
      this.showRemainingInfo = true;
      if (this.dataObj.medicareSupplementPlan === "Yes") {
        this.dataObj.memberId = "";
      }
      this.dataObj.medicareSupplementPlan = null;
    }

    if (event.target.name === "medicareSupplementPlan") {
      this.dataObj.memberId = "";
    }
    if (this.dataObj.medicareSupplementPlan === "Yes") {
      this.medicareOrMemberLabel = "Member Id";
    } else if (this.dataObj.medicareSupplementPlan === "No") {
      this.medicareOrMemberLabel = "Medicare Number";
      this.showRemoveButton = true;
      if (event.target.name === "medicareSupplementPlan") {
        this.selectedRecord = this.defaultInsuranceCompanyRecords.find(
          (record) => record.isMedicare === true
        );
        this.dataObj["primaryInsuranceCompanyName"] = this.selectedRecord.id;
        if (this.selectedRecord.memberIdValidationRegex !== undefined)
          this.memberIdValidationRegex = this.selectedRecord.memberIdValidationRegex;
        this.isGroupNumberRequired = this.selectedRecord.isGroupNumberRequired;
      }
    }

    if (
      this.dataObj.insuranceType === "No Insurance" ||
      this.dataObj.medicareType === "Part A"
    ) {
      this.selectedRecord = undefined;
      this.showRemoveButton = false;
      this.insuranceCompanyRecords = undefined;
      this.dataObj.primaryInsuranceCompanyName = null;
      this.dataObj.otherPrimaryInsuranceCompanyName = "";
      this.dataObj.memberId = "";
      this.dataObj.groupNumber = "";
      this.dataObj.groupName = "";
      this.dataObj.relationToPolicyHolder = null;
      this.dataObj.policyholderLastName = "";
      this.dataObj.policyholderFirstName = "";
      this.dataObj.policyholderMiddleName = "";
    }
    if (event.target.name === "relationToPolicyHolder") {
      if (this.dataObj.relationToPolicyHolder === "Self = 1") {
        this.dataObj.policyholderLastName = this.dataObj.lastName;
        this.dataObj.policyholderFirstName = this.dataObj.firstName;
        this.dataObj.policyholderMiddleName = this.dataObj.middleName;
      } else {
        this.dataObj.policyholderLastName = "";
        this.dataObj.policyholderFirstName = "";
        this.dataObj.policyholderMiddleName = "";
      }
    }
  }

  calculateAge(date) {
    if (!this.validateDate(date)) {
      return;
    }
    var Bday = +new Date(date);
    this.age = ~~((Date.now() - Bday) / 31557600000);
  }

  @api
  saveContacts() {
    //console.log("data=>", JSON.stringify(this.dataObj));
    // this.dataObj.contactId = this.contactId;

    if (!this.isValid()) {
      let tempObj = {};
      tempObj.message = "Please complete the required fields.";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
      return;
    }
    /* if(this.appointmentData.ifSymptomatic == 'Yes' || this.appointmentData.whereYouLive == 'Yes' || this.appointmentData.inContactWithpositiveCase == 'Yes'){
               // this.testingType = 'Rapid Testing';
               this.appointmentData.testingType = 'Rapid Testing';
        }else{
           // this.testingType = 'PCR Testing';
           this.appointmentData.testingType = 'PCR Testing';
        } */
    console.log("final app data>>>", JSON.stringify(this.appointmentData));
    console.log(
      "final app data>>>patient History b4 save",
      JSON.stringify(this.dataObj)
    );
    saveContactDetails({
      jsonData: JSON.stringify(this.dataObj),
      currentstep: this.currentStep,
      testingType: this.testingType,
      stepName: "patientHistory",
      isDependent: this.isDependent
    })
      .then((result) => {
        console.log("Successfully Saved");
        this.disableInputs = true;
        // this.extraWrapper = {...this.dataObj};
        let tempObj = {};
        tempObj.message = "saved successfully";
        tempObj.status = "success";
        this.sendEventToParent(tempObj);
      })
      .catch((error) => {
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }

  validateDate(date) {
    var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    var isValid = date.match(regExpPhone);
    console.log("isValid", isValid);
    if (!isValid) {
      let tempObj = {};
      tempObj.message = "Please provide a valid format for date";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
    }
    return isValid;
  }

  isValid() {
    let valid = true;
    let isAllValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid2 = [
      ...this.template.querySelectorAll("lightning-radio-group")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid3 = [
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid4 = [
      ...this.template.querySelectorAll("lightning-dual-listbox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    if (isAllValid && isAllValid2 && isAllValid3 && isAllValid4) {
      valid = true;
    } else {
      valid = false;
    }
    // valid = isAllValid2
    return valid;
  }

  /* formatPhone(obj) {
        var numbers = obj.value.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj.value = "";
        for (var i = 0; i < numbers.length; i++) {
            obj.value += (char[i] || "") + numbers[i];
        }
    }
    formatPhoneInput(obj) {
        var numbers = obj.replace(/\D/g, ""),
            char = {
                0: "(",
                3: ") ",
                6: "-"
            };
        obj = "";
        for (var i = 0; i < numbers.length; i++) {
            obj += (char[i] || "") + numbers[i];
        }
        return obj;

    }*/

  handleAppointmentInputChange(event) {
    this.dataObj[event.target.name] = event.target.value;
    if (this.dataObj.ifSymptomatic == "Yes") {
      this.thirdRowRequired = true;
      this.isSymptomatic = true;
      this.dynamicCSS3nrdRow = "col-sm-4";
    } else {
      this.dataObj.diseaseSymptoms = "";
      this.selectedValues = [];
      this.thirdRowRequired = false;
      this.isSymptomatic = false;
      this.dataObj.illnessDate = null;
      this.dynamicCSS3nrdRow = "col-sm-4";
    }
    if (this.dataObj.whereYouLive == "Yes") {
      this.showHighRisk = true;
      this.secondRowRequired = true;
      this.dynamicCSS2ndRow = "col-sm-8";
    } else {
      this.showHighRisk = false;
      this.secondRowRequired = false;
      this.dataObj.empHighRisk = null;
      this.dynamicCSS2ndRow = "col-sm-12";
    }

    if (
      this.dataObj.ifSymptomatic === "No" &&
      this.dataObj.inContactWithpositiveCase === "No" &&
      this.dataObj.isTestRequiredByEmp === "No"
    ) {
      this.askReasonForTest = true;
    } else {
      this.askReasonForTest = false;
    }
  }

  handleInsuranceCompanyChange(event) {
    console.log("In handle change >>", event.target.value);
    const searchKey = event.target.value;
    let isMedicare = this.dataObj.insuranceType === "Medicare";
    let isMedicaid = this.dataObj.insuranceType === "Medicaid";
    if (this.dataObj.insuranceType === "Private Insurance") {
      isMedicare = false;
      isMedicaid = false;
    }

    if (
      this.dataObj.insuranceType === "Medicare" &&
      ((this.dataObj.medicareType === "Part A and B" &&
        this.dataObj.medicareSupplementPlan === "Yes") ||
        this.dataObj.medicareType === "Medicare Advantage Plan")
    ) {
      isMedicaid = false;
      isMedicare = false;
    }

    searchDB({
      searchText: searchKey,
      isMedicare: isMedicare,
      isMedicaid: isMedicaid
    })
      .then((result) => {
        console.log("fectch result>>", JSON.parse(JSON.stringify(result)));
        const res = JSON.parse(JSON.stringify(result));
        this.insuranceCompanyRecords = [...res];
        this.insuranceIcon = res[0].iconName;
      })
      .catch((error) => {
        console.log("Error>>>", error);
      });
  }

  handleInsuranceCompanySelect(event) {
    const selectedRecordId = event.detail.id;
    const selectedRecordName = event.detail.name;

    this.selectedRecord = this.insuranceCompanyRecords.find(
      (record) => record.id === selectedRecordId
    );
    this.dataObj["primaryInsuranceCompanyName"] = selectedRecordId;
    if (event.detail.memberIdValidationRegex !== undefined)
      this.memberIdValidationRegex = event.detail.memberIdValidationRegex;
    this.isGroupNumberRequired = event.detail.isGroupNumberRequired;

    if (selectedRecordName === "OTHER - NOT LISTED  ()") {
      this.showCompanyNameField = true;
      this.primaryInsuranceCompanyCSS = "col-sm-4";
      this.memberIdValidationRegex = "^[a-zA-Z0-9]+$";
      this.isGroupNumberRequired = false;
    } else if (selectedRecordName !== "OTHER - NOT LISTED  ()") {
      this.showCompanyNameField = false;
      this.primaryInsuranceCompanyCSS = "col-sm-8";
      this.dataObj.otherPrimaryInsuranceCompanyName = "";
    }
  }

  handleRemove(event) {
    event.preventDefault();
    this.selectedRecord = undefined;
    this.showRemoveButton = false;
    this.insuranceCompanyRecords = undefined;
    this.dataObj.primaryInsuranceCompanyName = null;
    this.showCompanyNameField = false;
    this.primaryInsuranceCompanyCSS = "col-sm-8";
    this.isGroupNumberRequired = false;
    this.memberIdValidationRegex = "^[a-zA-Z0-9]+$";
  }
  formatSocialSecurityNumber(event) {
    const valToReplace = event.target.value;
    var val = valToReplace.replace(/\D/g, "");
    val = val.replace(/^(\d{3})/, "$1-");
    val = val.replace(/-(\d{2})/, "-$1-");
    val = val.replace(/(\d)-(\d{4}).*/, "$1-$2");
    this.dataObj.socialSecurityNumber = val;
    if (this.dataObj.socialSecurityNumber.length === 11) {
      if (/^(\d)\1\1-\1\1-\1\1\1\1$/.test(this.dataObj.socialSecurityNumber)) {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "Error!",
            message: "Please provide a valid Social Security Number"
          })
        );
      }
    }
  }
  changeMemberIdToUppercase(event) {
    const valToChange = event.target.value;
    this.dataObj.memberId = valToChange.toUpperCase();
  }
}