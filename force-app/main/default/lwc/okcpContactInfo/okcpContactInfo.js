import {
  LightningElement,
  track,
  api
} from "lwc";
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.savePatientDetails";
import validZip from "@salesforce/apex/OkpcContactInformationController.validZipCode";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";

export default class OkpcContactInfo extends LightningElement {
  @track dataObj = {};
  @track extraWrapper = {};
  @api contactId;
  @track showSpinner = false;
  @track disableInputs = true;
  @track isInValidZip = false;
  @track stateOptions = [];
  @track salutations = [];
  @track LangOptions = [];
  @track raceOptions = [];
  @track countyOptions = [];
  @track genderOptions = [];
  @track ethnicityOptions = [];
  @track maritialStatusOptions = [];
  @track age;
  @api currentStep;
  @track todayDate;
  @api isDependent = false;
  // @track isSSNNumner = true;
  @track isDrivingLicense = false;
  @track dynamicRegex = "^[a-zA-Z0-9]+$";

  fields = [
    "Name",
    "Birthdate",
    "Age__c",
    "Gender__c",
    "Street_Address1__c",
    "Street_Address_2__c",
    "City__c",
    "State__c",
    "ZIP__c",
    "Email",
    "MobilePhone"
  ];

  /*get genderOptions() {
        return [{
                label: "Male",
                value: "Male"
            },
            {
                label: "Female",
                value: "Female"
            },
            {
                label: "Other",
                value: "Other"
            }
        ];
    } */

  get onlyYesNoOptions() {
    return [{
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  connectedCallback() {
    this.getContact();

    this.todayDate = this.getTodayDate();
    fetchPicklist({
      objectName: "Contact",
      fieldName: "State__c"
    }).then((result) => {
      this.stateOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Ethnicity_Hispanic_Origin__c"
    }).then((result) => {
      this.ethnicityOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Gender__c"
    }).then((result) => {
      this.genderOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Salutation"
    }).then((result) => {
      this.salutations = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Primary_Language__c"
    }).then((result) => {
      this.LangOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Race__c"
    }).then((result) => {
      this.raceOptions = result;
    });
    fetchPicklist({
        objectName: "Contact",
        fieldName: "Marital_Status__c"
      })
      .then((result) => {
        this.maritialStatusOptions = result;
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });
    fetchPicklist({
        objectName: "Contact",
        fieldName: "County__c"
      })
      .then((result) => {
        this.countyOptions = result;
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });
  }

  @api
  editContacts() {
    this.disableInputs = false;
  }

  getContact() {
   // console.log("In contactinfor>>", this.contactId);
  //  console.log("In contactinfor>> is dependent", this.isDependent);
    this.showSpinner = true;
    getContactDetails({
        contactId: this.contactId
      })
      .then((result) => {
        // console.log('data=>', JSON.stringify(result));
        this.dataObj.firstName = result.firstName;
        this.dataObj.middleName = result.middleName;
        this.dataObj.lastName = result.lastName;
        this.dataObj.email = result.email;
        this.dataObj.phone = result.phone;
        if (result.phone) {
          this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
        }
        this.dataObj.patientId = result.patientId;
        // this.contactId = result.contactId;
        this.dataObj.contactId = result.contactId;
        this.dataObj.address2 = result.address2;
        this.dataObj.address1 = result.address1;
        this.dataObj.dob = result.dob;
        this.dataObj.city = result.city;
        this.dataObj.state = result.state;
        this.dataObj.zip = result.zip;
        this.dataObj.gender = result.gender;
        this.dataObj.smsCheckbox = result.smsCheckbox;
        this.dataObj.emailCheckbox = result.emailCheckbox;
        this.dataObj.suffix = result.suffix;
        this.dataObj.stateofBirth = result.stateofBirth;
        this.dataObj.countryofBirth = result.countryofBirth;
        this.dataObj.race = result.race;
        this.dataObj.ethnicity = result.ethnicity;
        this.dataObj.county = result.county;
        this.dataObj.primaryLanguage = result.primaryLanguage;
        this.dataObj.maritialStatus = result.maritialStatus;
        this.dataObj.drivingLicenseNumber = result.drivingLicenseNumber;
        this.dataObj.socialSecurityNumber = result.socialSecurityNumber;
        // if (
        //   (this.dataObj.socialSecurityNumber == undefined ||
        //     this.dataObj.socialSecurityNumber == null) &&
        //   this.dataObj.drivingLicenseNumber != undefined
        // ) {
        //   this.isSSNNumner = false;
        // } else {
        //   this.isSSNNumner = true;
        // }
        if (
          this.dataObj.drivingLicenseNumber == undefined ||
          this.dataObj.drivingLicenseNumber == null
        ) {
          this.isDrivingLicense = false;
        }
        if (!this.dataObj.state) {
          this.dataObj.state = "OK";
        }
        if (this.dataObj.dob) {
          this.calculateAge(this.dataObj.dob);
        }
        // this.extraWrapper = {...this.dataObj};

        if (
          this.isDependent &&
          (typeof this.contactId == "undefined" || this.contactId == null)
        ) {
          this.dataObj = {};
          this.age = null;
          this.dataObj.smsCheckbox = false;
          this.dataObj.emailCheckbox = false;
          this.dataObj.email = result.email;
          this.dataObj.address2 = result.address2;
          this.dataObj.address1 = result.address1;
          this.dataObj.city = result.city;
          this.dataObj.state = result.state;
          this.dataObj.zip = result.zip;
          this.dataObj.county = result.county;
          this.dataObj.phone = result.phone;
          if (result.phone) {
            this.dataObj.phone = this.formatPhoneInput(this.dataObj.phone);
          }
          //this.contactId = null;
          //this.dataObj.contactId = null;
        }
      //  console.log(JSON.stringify(this.dataObj));
        this.showSpinner = false;
      })
      .catch((error) => {
        console.log("error=>", JSON.stringify(error));
        this.showSpinner = false;
      });
  }

  handleInputChange(event) {
    if (event.target.name == "firstName") {
      this.dataObj.firstName = event.target.value;
    } else if (event.target.name == "middleName") {
      this.dataObj.middleName = event.target.value;
    } else if (event.target.name == "lastName") {
      this.dataObj.lastName = event.target.value;
    } else if (event.target.name == "email") {
      this.dataObj.email = event.target.value;
    } else if (event.target.name == "Suffix") {
      this.dataObj.suffix = event.target.value;
    } else if (event.target.name == "StateofBirth") {
      this.dataObj.stateofBirth = event.target.value;
    } else if (event.target.name == "CountryofBirth") {
      this.dataObj.countryofBirth = event.target.value;
    } else if (event.target.name == "Ethnicity") {
      this.dataObj.ethnicity = event.target.value;
    } else if (event.target.name == "PrimaryLanguage") {
      this.dataObj.primaryLanguage = event.target.value;
    } else if (event.target.name == "Race") {
      this.dataObj.race = event.target.value;
    } else if (event.target.name == "MaritialStatus") {
      this.dataObj.maritialStatus = event.target.value;
    } else if (event.target.name == "phone") {
      this.dataObj.phone = event.target.value;
      this.formatPhone(event.target);
    } else if (event.target.name == "emailCheckbox") {
      this.dataObj.emailCheckbox = event.target.checked;
    } else if (event.target.name == "smsCheckbox") {
      this.dataObj.smsCheckbox = event.target.checked;
    } else if (event.target.name == "patientId") {
      this.dataObj.patientId = event.target.value;
    } else if (event.target.name == "gender") {
      this.dataObj.gender = event.target.value;
    } else if (event.target.name == "county") {
      this.dataObj.county = event.target.value;
    } else if (event.target.name == "zip") {
      this.dataObj.zip = event.target.value;
    } else if (event.target.name == "state") {
      this.dataObj.state = event.target.value;
    } else if (event.target.name == "city") {
      this.dataObj.city = event.target.value;
    } else if (event.target.name == "address1") {
      this.dataObj.address1 = event.target.value;
    } else if (event.target.name == "address2") {
      this.dataObj.address2 = event.target.value;
    } else if (event.target.name == "socialSecurityNumber") {
      //this.isSSNNumner = true;
      this.isDrivingLicense = false;
      this.dataObj.socialSecurityNumber = event.target.value;
    } else if (event.target.name == "drivingLicenseNumber") {
      //this.isSSNNumner = false;
      this.isDrivingLicense = true;
      this.dataObj.drivingLicenseNumber = event.target.value;
    } else if (event.target.name == "dob") {
      this.dataObj.dob = event.target.value;
      this.calculateAge(event.target.value);
      if (!this.validateDate(event.target.value)) {
        this.dataObj.dob = "";
      }
    }
    if (
      this.dataObj.drivingLicenseNumber == undefined ||
      this.dataObj.drivingLicenseNumber == ""
    ) {
      //this.isSSNNumner = true;
      this.isDrivingLicense = false;
    } else if (
      this.dataObj.socialSecurityNumber == undefined ||
      this.dataObj.socialSecurityNumber == ""
    ) {
      //this.isSSNNumner = false;
      this.isDrivingLicense = true;
    }
  //  console.log(JSON.stringify(this.dataObj));
  }

  @api
  saveContacts() {
    /*  console.log('data=>', JSON.stringify(this.dataObj));
         if(!this.isDependent){
          this.dataObj.contactId = this.contactId;
         }else{
          this.dataObj.contactId = null;
         } */

    if (this.dataObj.zip == "" || typeof this.dataObj.zip == "undefined") {
      this.dataObj.zip = null;
    }
    // if (this.dataObj.emailCheckbox == true && (this.dataObj.email == '' || typeof this.dataObj.email == 'undefined')) {
    //     let tempObj = {};
    //     tempObj.message = "Please Complete the email field to recieve email notifications";
    //     tempObj.status = "error";
    //     this.sendEventToParent(tempObj);
    //     return;
    // }

    // if (this.dataObj.smsCheckbox == true && (this.dataObj.phone == '' || typeof this.dataObj.phone == 'undefined')) {
    //     let tempObj = {};
    //     tempObj.message = "Please Complete the phone field to recieve SMS notifications";
    //     tempObj.status = "error";
    //     this.sendEventToParent(tempObj);
    //     return;
    // }

    // if (!this.validateTextField(this.dataObj.lastName)) {
    //     return;
    // }

    // if (!this.validateTextField(this.dataObj.phone)) {
    //     return;
    // }

    // if (!this.validateTextField(this.dataObj.email)) {
    //     return;
    // }

    // if(!this.validateEmail(this.dataObj.email)){
    //     return;
    // }

    // if(!this.validatePhone(this.dataObj.phone)){
    //     return;
    // }
    // if(this.dataObj.zip){
    //     if(!this.validateZip(this.dataObj.zip)){
    //         return;
    //     }
    // }

    if (!this.isValid()) {
      return;
    }
    if (
      this.dataObj.socialSecurityNumber !== "" &&
      this.dataObj.socialSecurityNumber !== undefined
    ) {
      if (/^(\d)\1\1-\1\1-\1\1\1\1$/.test(this.dataObj.socialSecurityNumber)) {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "Error!",
            message: "Please provide a valid Social Security Number"
          })
        );
        return;
      }
    }
 //   console.log("value of zip", this.dataObj.zip);

    validZip({
        zipcode: this.dataObj.zip
      })
      .then((result) => {
      //  console.log("Vaild zip code>>", result);
        if (result == false) {
       //   console.log("In >>>>>", result);
          this.isInValidZip = true;
          let tempObj = {};
          tempObj.message = "Please Provide a Valid Zip";
          tempObj.status = "error";
          this.sendEventToParent(tempObj);
          return;
        } else {
          this.callSaveContact();
        }
      })
      .catch((error) => {
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }

  callSaveContact() {
   // console.log("this is vslids" + this.isInValidZip);
    saveContactDetails({
        jsonData: JSON.stringify(this.dataObj),
        currentstep: this.currentStep,
        stepName: "personalInfo",
        isDependent: this.isDependent
      })
      .then((result) => {
    //    console.log("Successfully Saved", result);
        this.contactId = result;
    //    console.log("ContactId>>", this.contactId);
        this.disableInputs = true;
        // this.extraWrapper = {...this.dataObj};
        let tempObj = {};
        tempObj.message = "saved successfully";
        tempObj.status = "success";
        tempObj.contactId = this.contactId;
        this.sendEventToParent(tempObj);
      })
      .catch((error) => {
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }

  @api
  handleCancelButton() {
    this.disableInputs = true;
    // this.dataObj = {...this.extraWrapper};
  }

  calculateAge(date) {
    if (!this.validateDate(date)) {
      return;
    }
    var Bday = +new Date(date);
    this.age = ~~((Date.now() - Bday) / 31557600000);
  }

  sendEventToParent(obj) {
    const selectedEvent = new CustomEvent("contactsave", {
      detail: obj
    });
    this.dispatchEvent(selectedEvent);
  }

  getTodayDate() {
    let today = new Date(),
      day = today.getDate(),
      month = today.getMonth() + 1, //January is 0
      year = today.getFullYear();
    if (day < 10) {
      day = "0" + day;
    }
    if (month < 10) {
      month = "0" + month;
    }
    today = year + "-" + month + "-" + day;
    return today;
  }

  validateTextField(field) {
    if (field == "" || typeof field == "undefined") {
      let tempObj = {};
      tempObj.message = "Please complete the required fields.";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
      return false;
    } else {
      return true;
    }
  }

  // formatPhone(obj) {
  //     let phone = obj.value;
  //     let formatedPhone='('+phone.substring(0,3)+')'+phone.substring(3,6)+'-'+phone.substring(6,10);
  // }

  validateEmail(email) {
    var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
    var isValid = email.match(regExpEmail);
  //  console.log("isValid", isValid);
    if (!isValid) {
      let tempObj = {};
      tempObj.message = "Please provide a correct format for email.";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
    }
    return isValid;
  }

  // validatePhone(phone) {
  //     var regExpPhone = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
  //     var isValid = phone.match(regExpPhone);
  //     console.log("isValid", isValid);
  //     if(!isValid){
  //         let tempObj = {};
  //         tempObj.message = "Please provide a correct format for phone.";
  //         tempObj.status = "error";
  //         this.sendEventToParent(tempObj);
  //     }
  //     return isValid;
  // }

  validateDate(date) {
    var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    var isValid = date.match(regExpPhone);
 //   console.log("isValid", isValid);
    if (!isValid) {
      let tempObj = {};
      tempObj.message = "Please provide a valid format for date";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
    }
    return isValid;
  }

  // validateZip(zip) {
  //     var regExpZip = /^[0-9]*$/;
  //     var isValid = (zip.match(regExpZip) && zip.length == 5);
  //     console.log("isValid", isValid);
  //     if(!isValid){
  //         let tempObj = {};
  //         tempObj.message = 'Please enter a valid format ("XXXXX") for Zip code.';
  //         tempObj.status = "error";
  //         this.sendEventToParent(tempObj);
  //     }
  //     return isValid;
  // }

  isValid() {
    let valid = true;
    let isAllValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    let isAllValid1 = [
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    if (isAllValid && isAllValid1) {
      valid = true;
    } else {
      valid = false;
    }
    // valid = isAllValid;
    return valid;
  }

  handlePhoneFocus(event) {
    event.target.value = this.dataObj.phone;
  }

  formatPhone(obj) {
    var numbers = obj.value.replace(/\D/g, ""),
      char = {
        0: "(",
        3: ") ",
        6: "-"
      };
    obj.value = "";
    for (var i = 0; i < numbers.length; i++) {
      obj.value += (char[i] || "") + numbers[i];
    }
  }
  formatPhoneInput(obj) {
    var numbers = obj.replace(/\D/g, ""),
      char = {
        0: "(",
        3: ") ",
        6: "-"
      };
    obj = "";
    for (var i = 0; i < numbers.length; i++) {
      obj += (char[i] || "") + numbers[i];
    }
    return obj;
  }

  formalSocialSecurityNumber(event) {
    const valToReplace = event.target.value;
    var val = valToReplace.replace(/\D/g, "");
    val = val.replace(/^(\d{3})/, "$1-");
    val = val.replace(/-(\d{2})/, "-$1-");
    val = val.replace(/(\d)-(\d{4}).*/, "$1-$2");
    if (this.dataObj.socialSecurityNumber.length === 11) {
      if (/^(\d)\1\1-\1\1-\1\1\1\1$/.test(this.dataObj.socialSecurityNumber)) {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "Error!",
            message: "Please provide a valid Social Security Number"
          })
        );
      }
    }
    this.dataObj.socialSecurityNumber = val;
  }
  getlength(event) {
    var licenseLength = event.target.value.length;
    var finalLength = licenseLength - 1;
    this.dynamicRegex =
      "^(?!.*([A-Za-z0-9])\\1{" + finalLength + "})[A-Za-z0-9]*";
  }
}