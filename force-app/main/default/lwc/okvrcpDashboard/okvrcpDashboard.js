import {
    LightningElement,
    track,
    api
} from 'lwc';
import retrieveResult from '@salesforce/apex/OKPCDashboardController.retrieveResult';
import deleteAppointment from '@salesforce/apex/OKPCDashboardController.deleteAppointment';
import createAppointment from '@salesforce/apex/OKPCDashboardController.createAppointment';
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcDashboard extends LightningElement {
    @track contactName;
    @track contactId;
    @track MobileNumber;
    appointmentList = [];
    @track appointment = {};
    @track showMap = false;
    @track showConfirmation = false;
    @track showAppointmentModal = false;
    @track email;
    @track selectedAccountId;
    @track mapMarkers = [];
    @track results = {};
    @track isCancelledWindow = false;
    @track hasResults = false;
    @api istestingsite = false;
    @track appointmentID = '';
    @track multipleAppt = false;
    // @track showAppointmentPreference =false;

    connectedCallback() {
        this.istestingsite = true;
        this.callApex();
    }
    callApex() {
        this.appointment = {};
        retrieveResult()
            .then(result => {
                    if (result != null) {
                        console.log('### ', result);
                        if (result.contact) {
                            this.contactId = result.contact.Id;
                            let contact = result.contact;
                            this.contactName = contact.Name;
                            this.MobileNumber = contact.MobilePhone;
                            this.email = contact.Email;
                        }
                        if (result.appointment) {
                            console.log('## ', result.appointment);
                            console.log('>>>ListApp>>' + JSON.stringify(result.vitalAppointment))
                            const resultListForComplete = [...result.vitalAppointment.filter(word => word.Status__c != 'Completed')];
                            console.log('FinalCompleteLsit>>>', JSON.stringify(resultListForComplete));
                            let tmpAppointments = [];
                            resultListForComplete.forEach(apt => {
                                    let aptRecord = {};
                                    let isScheduled = apt.Status__c == 'Scheduled';
                                    let isToBeScheduled = apt.Status__c == 'To Be Scheduled';
                                    let isCancelled = apt.Status__c == 'Cancelled';
                                    let isCompleted = false;
                                    if ((apt.Status__c == 'Completed') || (apt.Status__c == 'Closed')) {
                                        isCompleted = true;
                                    }
                                    aptRecord.id = apt.Id;
                                    aptRecord.startTime = isScheduled ? this.msToTimeAMPM(apt.Appointment_Slot__r.Start_Time__c) : this.msToTimeAMPM(apt.Patient__r.Preferred_Time__c);
                                    aptRecord.startDate = isScheduled ? apt.Appointment_Slot__r.Date__c : apt.Patient__r.Preferred_Date__c;

                                    if (isCancelled) {
                                        aptRecord.startTime = this.msToTimeAMPM(apt.Appointment_Start_Time_v1__c);
                                        aptRecord.startDate = apt.Appointment_Start_Date_v1__c;
                                    }

                                    let addressString = '';
                                    if (apt.Patient__r.Testing_Site__r.BillingStreet != null && typeof apt.Patient__r.Testing_Site__r.BillingStreet != 'undefined')
                                        addressString += apt.Patient__r.Testing_Site__r.BillingStreet;
                                    if (apt.Patient__r.Testing_Site__r.BillingCity != null && typeof apt.Patient__r.Testing_Site__r.BillingCity != 'undefined')
                                        addressString += addressString == '' ? apt.Patient__r.Testing_Site__r.BillingCity : ', ' + apt.Patient__r.Testing_Site__r.BillingCity;
                                    if (apt.Patient__r.Testing_Site__r.BillingState != null && typeof apt.Patient__r.Testing_Site__r.BillingState != 'undefined')
                                        addressString += addressString == '' ? apt.Patient__r.Testing_Site__r.BillingState : ', ' + apt.Patient__r.Testing_Site__r.BillingState;
                                    if (apt.Patient__r.Testing_Site__r.BillingPostalCode != null && typeof apt.Patient__r.Testing_Site__r.BillingPostalCode != 'undefined')
                                        addressString += ' ' + apt.Patient__r.Testing_Site__r.BillingPostalCode;
                                    aptRecord.address = addressString;
                                    console.log('>>>>>>>>' + addressString);

                                    aptRecord.isScheduled = isScheduled;
                                    console.log('>>>>>>>>' + aptRecord.isScheduled);
                                    aptRecord.isCancelled = isCancelled;
                                    aptRecord.isToBeScheduled = isToBeScheduled;
                                    aptRecord.siteId = apt.Patient__r.Testing_Site__c;
                                    console.log('Site' + aptRecord.siteId);
                                    aptRecord.siteName = apt.Patient__r.Testing_Site__r.Name;
                                    aptRecord.isCompleted = isCompleted;
                                   // console.log('## this.appointment ', this.appointment);
                                    // this.retrieveMarker();
                                    tmpAppointments.push(aptRecord);
                                    console.log('LoopList>> ' , tmpAppointments)
                                
                                });
                              
                                this.appointmentList = tmpAppointments;
                                console.log('llastList>> ', this.appointmentList)
                            /*
                                
                                let app = result.appointment;
                                let isScheduled = app.Status__c == 'Scheduled';
                                let isToBeScheduled = app.Status__c == 'To Be Scheduled';
                                let isCancelled = app.Status__c == 'Cancelled';
                                let isCompleted = false;
                                if ((app.Status__c == 'Completed') || (app.Status__c == 'Closed')) {
                                    isCompleted = true;
                                }
                                this.appointment.id = app.Id; this.appointment.startTime = isScheduled ? this.msToTimeAMPM(app.Appointment_Slot__r.Start_Time__c) : this.msToTimeAMPM(app.Patient__r.Preferred_Time__c); this.appointment.startDate = isScheduled ? app.Appointment_Slot__r.Date__c : app.Patient__r.Preferred_Date__c;
                                // this.appointment.startTime = isCancelled ? app.Appointment_Start_Time_v1__c : app.Appointment_Start_Time_v1__c  ;
                                //  this.appointment.startDate = isCancelled ? app.Appointment_Start_Date_v1__c : app.Appointment_Start_Date_v1__c ;
                                if (isCancelled) {
                                    this.appointment.startTime = this.msToTimeAMPM(app.Appointment_Start_Time_v1__c);
                                    this.appointment.startDate = app.Appointment_Start_Date_v1__c;
                                }
                                let addressString = '';
                                if (app.Patient__r.Testing_Site__r.BillingStreet != null && typeof app.Patient__r.Testing_Site__r.BillingStreet != 'undefined')
                                    addressString += app.Patient__r.Testing_Site__r.BillingStreet;
                                if (app.Patient__r.Testing_Site__r.BillingCity != null && typeof app.Patient__r.Testing_Site__r.BillingCity != 'undefined')
                                    addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingCity : ', ' + app.Patient__r.Testing_Site__r.BillingCity;
                                if (app.Patient__r.Testing_Site__r.BillingState != null && typeof app.Patient__r.Testing_Site__r.BillingState != 'undefined')
                                    addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingState : ', ' + app.Patient__r.Testing_Site__r.BillingState;
                                if (app.Patient__r.Testing_Site__r.BillingPostalCode != null && typeof app.Patient__r.Testing_Site__r.BillingPostalCode != 'undefined')
                                    addressString += ' ' + app.Patient__r.Testing_Site__r.BillingPostalCode; 
                                    this.appointment.address = addressString; 
                                    console.log('>>>>>>>>' + addressString); 
                                    this.appointment.isScheduled = isScheduled; 
                                    console.log('>>>>>>>>' + this.appointment.isScheduled);
                                     this.appointment.isCancelled = isCancelled; 
                                     this.appointment.isToBeScheduled = isToBeScheduled; 
                                     this.appointment.siteId = app.Patient__r.Testing_Site__c; 
                                     console.log('Site' + this.appointment.siteId); 
                                     this.appointment.siteName = app.Patient__r.Testing_Site__r.Name;
                                      this.appointment.isCompleted = isCompleted; 
                                      console.log('## this.appointment ', this.appointment);
                                       this.retrieveMarker();
                            }
                            if (result.results) {
                                this.results = result.results;
                                console.log('##@ ', JSON.stringify(this.results));
                                if (result.results.result) {
                                    console.log('changing >>> ');
                                    this.hasResults = true;
                                    this.appointment.isCompleted = false;
                                }
                            }  */
                            //console.log('this.appointment.isCompleted;  ',this.appointment.isCompleted);
                        }
                    }
                 }) .catch(error => {
                    console.log('error>>', { ...error });
                    this.showSpinner = false;
                });
                // this.initColSelection();
                // this.callApex();
            }

        retrieveMarker(event) {
            this.accountId = event.target.dataset.id;
            if (this.mapMarkers.length <= 0) {
                getAccountdetails({
                        accountId: this.accountId
                    })
                    .then(result => {
                        this.accountDetails = JSON.parse(JSON.stringify(result));
                        if (this.accountDetails) {
                            this.mapMarkers.push(this.accountDetails);
                            this.showMap = true;
                        }
                    })
                    .catch(error => {
                        console.log('error', error);
                    })
            }

        }
        viewCertificate() {
            console.log('testing...');
            var url = window.location.href;
            url = url.substring(0, url.indexOf("/s/"));
            url += '/apex/ResultCertificate?id=' + this.contactId;
            console.log('#$ ', url);
            window.open(url, '_blank');
        }

        viewLocationOnMap() {
            this.showMap = true;
        }

        editAppointment() {
            this.showAppointmentModal = true;
        }
        openCancel(event) {
            this.appointmentID = event.target.dataset.id;
            console.log('openId>>' , this.appointmentID );
            this.isCancelledWindow = true;
        }

        cancelAppointment(event) {
            this.appointmentID = event.target.dataset.id
            console.log('Cancel Id',  this.appointmentID);
            this.showConfirmation = true;
            // this.showAppointmentPreference = true;
        }

        hideModal(event) {
            this.callApex();
            this.showConfirmation = false;
            this.showAppointmentModal = false;
            this.showMap = false;
        }
        msToTimeAMPM(s) {
            if (s == '' || s == null || typeof s == 'undefined')
                return '';
            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;
            let radian = 'AM';
            if (hrs >= 12) {
                radian = 'PM';
                if (hrs > 12) {
                    hrs = hrs - 12;
                }

            }
            return this.pad(hrs) + ':' + this.pad(mins) + ' ' + radian;
        }

        pad(n, z) {
            z = z || 2;
            return ('00' + n).slice(-z);
        }
        /*  handleSubmit(){
                  this.callApex();
                  this.isCancelledWindow = false;
              // window.open('/s/dashboard', '_self');
          } */
        handleSubmitData(event) {
            console.log('Milla Event' + event)
            if (event.detail.name == 'submit') {
                this.callApex();
                this.isCancelledWindow = false;
            } else if (event.detail.name == 'cancel') {
                //this.callApex();
                console.log('cancel ' + event)
                deleteAppointment({
                    appointmentId: event.detail.appId
                })
                .then(result => {
                    this.isCancelledWindow = false;
                })
                .catch(error => {
                    console.log('error', error);
                })               
            }
        }

        createAppointment() {
            createAppointment({
                    contactId: this.contactId
                })
                .then(result => {
                    if (result != null) {
                        this.appointmentID = result;
                        console.log('New appointment>>' + this.appointmentID);
                      /* this.dispatchEvent(new ShowToastEvent({
                            title: '',
                            variant: 'success',
                            message: 'Appointment created Successfully',
                        }));*/
                        this.isCancelledWindow = true;
                        this.multipleAppt = true;
                    }

                })
                .catch(error => {
                    console.log('error', error);
                })
        }


        
    editProfile(){
        const editprofile = new CustomEvent("editprofile", {
            detail: 'editProfile'
        });
        this.dispatchEvent(editprofile);
    }
    }