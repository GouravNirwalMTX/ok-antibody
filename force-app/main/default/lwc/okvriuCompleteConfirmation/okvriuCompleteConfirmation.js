import { LightningElement,api,track } from 'lwc';
import markComplete from '@salesforce/apex/DC_ScheduleAppointment.completeVitalAppointment';
import markCancel from '@salesforce/apex/DC_ScheduleAppointment.cancelVitalAppointment';
import markInProgress from '@salesforce/apex/DC_ScheduleAppointment.inProgressVitalAppointment';

// import getBarcode from '@salesforce/apex/DC_ScheduleAppointment.returnBarcodeNumber';
// import setBarcode from '@salesforce/apex/DC_ScheduleAppointment.setBarcodeNumber';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class OkvriuCompleteConfirmation extends LightningElement {
    @track modaltype = 'complete';
    @track barcodeId = '';
    @api 
    get type() {
        return this.modaltype;
    }
    set type(val){
        console.log('val',val);
        this.modaltype = val;
    }
    @api appointmentId;
    @track labId = '';
    @track showSpinner = false;
    @track laboptions =[];
    @track reason;
    @track isComplete = false;
    @track isCancelled = false;
    @track isInProgress = false;
    @track isReScheduled = false;
    @api appointmentList = [];
    @api isBulk;


    get isComplte() {
        return this.modaltype == 'complete';
    }

    get heading() {
        let heading = '';
        if(this.type == 'complete') {
            this.isComplete = true;
            heading = 'Complete Appointment';
        }else if(this.type == 'inProgress') {
            this.isInProgress = true;
            heading = 'In Progress Appointment';
        }else if(this.type == 'cancel') {
            this.isCancelled = true;
            heading = 'Cancel Appointment';
        }
        return heading;
    }

    // connectedCallback() {
    //     getLabRecords().then(result => {
    //         this.laboptions = result;
    //     }).catch(error => { console.log('Error',error)});
    // }

    handleInputChange(event) {
        this.reason = event.target.value;
        /*if (event.target.name == 'cancelationReason') {
            this.reason = event.target.value;
        }*/
    }

    cancelAppointment() {
        console.log('reason' + this.reason );
        if(typeof this.reason == 'undefined' || this.reason == '' || this.reason === null){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter reason for cancellation',
            }));
            return;
        }
        if(this.isBulk){
            this.showSpinner = true;
            let calloutParams = {"appointmentId" : this.appointmentList,
                            "reasonForCancelation" : this.reason};
    
        markCancel(calloutParams).then(result => {
            this.showSpinner = false;
            this.dispatchEvent(new CustomEvent('close', {
                detail: 'cancel'
            }));
        })
        .catch(error => {
            this.showSpinner = false;
            this.error = error;
        });
        }else {
            this.showSpinner = true;
            this.appointmentList = [this.appointmentId];
            console.log('Single List for cancelation',this.appointmentList)
            let calloutParams = {"appointmentId" : this.appointmentList,
                            "reasonForCancelation" : this.reason};
    
        markCancel(calloutParams).then(result => {
            this.showSpinner = false;
            this.dispatchEvent(new CustomEvent('close', {
                detail: 'cancel'
            }));
        })
        .catch(error => {
            this.showSpinner = false;
            this.error = error;
        });
        }
        
    }
 
    handlebarcodechange(event) {
        this.barcodeId = event.target.value;
    }

    completeAppointment() {
      
        markComplete({appointmentId: this.appointmentId})
        .then(result =>{
            if(result == 'success'){
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Some error occoured.',
                }));
            }
        })
        .catch(error =>{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }

    //Added by Anushka S-09912 R6
    inprogressAppointment() {
       /* if(typeof this.reason == 'undefined' || this.reason == '' || this.reason === null){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter reason.',
            }));
            return;
        }*/
        markInProgress({appointmentId: this.appointmentId, reason : this.reason})
        .then(result =>{
            if(result == 'success'){
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: result,
                }));
            }
        })
        .catch(error =>{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }
  
    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
}