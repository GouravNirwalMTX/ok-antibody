import { LightningElement, track, api } from 'lwc';

export default class TimePicklist extends LightningElement {
    @api label;
    @api isRequired;
    @api minTime;
    @api maxTime;
    @api interval;
    @api value;
    // @track options = [];
    
    
    get timeOptions() {
        let minhr = 0;
        let maxhr = 23;
        let minmnt = 0;
        let maxmnt = 60;
        if(this.minTime) {
            let timeObj = this.getFormattedTime(this.minTime);
            minhr = timeObj.hourT;
            minmnt = timeObj.minT;
        }
        if(this.maxTime) {
            let timeObj = this.getFormattedTime(this.maxTime);
            maxmnt = timeObj.minT;
            maxhr = parseInt(maxmnt) < 30 ? timeObj.hourT : parseInt(timeObj.hourT) - 1;
        }
        let timeInterval = this.interval;
        let options = [];
        if(!timeInterval || timeInterval == 0)
            timeInterval = 30; 
        for(let hr = parseInt(minhr); hr <= parseInt(maxhr); hr++) {
            for(let mnt = 0; mnt < 60; mnt = mnt + parseInt(timeInterval)) {
                if(parseInt(minmnt) <= parseInt(mnt) || parseInt(hr) > parseInt(minhr))
                    options.push(this.generateTimeOption(hr,mnt));
            }
        }
        return options;
    }

    generateTimeOption(hr,mnt) {
        let radian = hr < 12 ? 'AM' : 'PM';
        let timehr = hr <= 12 ? (hr == 0 ? 12 : hr) : hr - 12;
        let minute = mnt < 10 ? '0' + mnt : '' +  mnt;
        return {label : (timehr < 10 ? '0' + timehr : timehr) + ':' + minute + ' ' + radian, value: (hr < 10 ? '0' + hr : hr) + ':' + (mnt < 10 ? '0' + mnt : mnt) + ':00.000' };
    }

    getFormattedTime(timeInstance) {
        let hourT = parseInt(timeInstance.substr(0,2));
        let minT = parseInt(timeInstance.substr(3,2));
        let amPM = hourT <= 11 ? 'AM' : 'PM';
        return {hourT: hourT, minT: minT, amPM: amPM};
    }
    
    handleChange(event) {
        const selectedEvent = new CustomEvent("change", {
            detail: {value:event.detail.value,label:this.label}
        });
        this.dispatchEvent(selectedEvent);
        
    }
}