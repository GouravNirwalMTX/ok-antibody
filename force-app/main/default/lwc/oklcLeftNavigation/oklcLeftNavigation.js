import {
    LightningElement,api,track
} from 'lwc';
import { NavigationMixin } from "lightning/navigation";

export default class OklcLeftNavigation extends NavigationMixin(LightningElement)  {
    @api showFooter;
    @track selectedName;
    @api compName;
    @track redirectPage;
    @track home = true;
    @track dashboard = false;
    @track result = false;
    @api selectedTab;
    @track homeTabCss = 'padding_space_button';
    @track testingSampletabCss = 'padding_space_button';
    @track uploadResultCss = 'padding_space_button';
    @track downloadResultCss = 'padding_space_button';
    
    connectedCallback(){
        if(this.selectedTab == 'dashboard' || this.selectedTab == 'default'){
            this.homeTabCss = this.homeTabCss + ' active_tab';
        } else if(this.selectedTab == 'testingSample'){
            this.testingSampletabCss = this.testingSampletabCss + ' active_tab';
        }
        else if(this.selectedTab == 'uploadResult'){
            this.uploadResultCss = this.uploadResultCss + ' active_tab';
        }else if(this.selectedTab == 'downloadResult'){
            this.downloadResultCss = this.downloadResultCss + ' active_tab';
        }
    }

    navigateToOther(pageName){
        console.log('selectedName-->', pageName);
        if (pageName == "testingSample") {
            this.navigateToWebPage('/Lab/s/antibodies-testing-sample');
        }else if (pageName == "dashboard") {
            this.navigateToWebPage('/Lab/s/');
        }else if (pageName == "uploadResult") {
            this.navigateToWebPage('/Lab/s/oklc-upload-result');
        }else if (pageName == "downloadResult") {
            this.navigateToWebPage('/Lab/s/oklc-download-result');
        }
    }

    navigateToPage(event) {
        console.log('navigateToPage');
        this.redirectPage = event.currentTarget.getAttribute("data-navigatepage");
        console.log('redirectPage-->', this.redirectPage);
        this.navigateToOther(this.redirectPage);
        
    }

    navigateToWebPage(url) {
        // Navigate to a URL
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: url
            }
        },
        true // Replaces the current page in your browser history with the URL
        );
    }
}