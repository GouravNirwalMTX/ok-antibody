import {
    LightningElement,api,track
} from 'lwc';
import { NavigationMixin } from "lightning/navigation";
import formFactorPropertyName from '@salesforce/client/formFactor';
import isAdminUser from '@salesforce/apex/OkpcContactInformationController.isAdminContact';

export default class OktcLeftNavigation extends NavigationMixin(LightningElement)  {
    
    @api showFooter;
    @track selectedName;
    @track isAdmin = false;
    @api compName;
    @track redirectPage;
    @track home = true;
    @track dashboard = false;
    @track result = false;
    @api selectedTab;
    @track homeTabCss = 'padding_space_button';
    @track appointmenttabCss = 'padding_space_button';
    @track schedulertabCss = 'padding_space_button';
    @track downloadstabcss = 'padding_space_button';
    @track createContacttabcss = 'padding_space_button';
    
    get isMobile() {
        return formFactorPropertyName != 'Large';
    }

    connectedCallback(){
        this.getUserDetails();
        if(this.selectedTab == 'dashboard' || this.selectedTab == 'default'){
            this.homeTabCss = this.homeTabCss + ' active_tab';
        } else if(this.selectedTab == 'appointments'){
            this.appointmenttabCss = this.appointmenttabCss + ' active_tab';
        } else if(this.selectedTab == 'scheduler'){
            this.schedulertabCss = this.schedulertabCss + ' active_tab';
        }else if(this.selectedTab == 'downloads'){
            this.downloadstabcss = this.downloadstabcss + ' active_tab';
        }else if(this.selectedTab == 'createContact'){
            this.createContacttabcss = this.createContacttabcss + ' active_tab';
        }
    }

    getUserDetails(){
        isAdminUser()
        .then(result => {
           this.isAdmin = result;
           console.log('isAdmin' + this.isAdmin)
        })
        .catch(error => {
            console.log('Error-->' + error);
        });

    }
   
    navigateToPage(event) {
        console.log('navigateToPage');
        this.redirectPage = event.currentTarget.getAttribute("data-navigatepage");
        console.log('redirectPage-->', this.redirectPage);
        // if (this.redirectPage == "home") {
        //     this.home = true;
        //     this.result = false;
        // } else if(this.redirectPage == "result") {
        //     this.result = true;
        //     this.home = false;

        // }
        this.navigateToOther(this.redirectPage);
        
    }

    navigateToOther(pageName){
        console.log('selectedName-->', pageName);
        if (pageName == "appointments") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/okvriu-appointments');
        } else if (pageName == "dashboard") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/');
        } else if (pageName == "scheduler") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/okvriu-manage-slots?isAdmin=' + this.isAdmin);
        }
        /*else if (pageName == "viewSlots") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/view-slots');
        }*/
        else if (pageName == "downloads") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/okvriu-downloads');
        }else if (pageName == "createContact") {
            this.navigateToWebPage('/VitalRecordsLobbyPortal/s/create-contact');
        }
    }
    navigateToWebPage(url) {
        // Navigate to a URL
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: url 
            }
        },
        true // Replaces the current page in your browser history with the URL
        );
    }
    
}