import { LightningElement, api } from "lwc";

export default class OkInsuranceCompanyList extends LightningElement {
  @api record;
  @api iconname;

  handleSelect(event) {
    event.preventDefault();
    const selectedRecord = new CustomEvent("select", {
      detail: this.record
    });
    this.dispatchEvent(selectedRecord);
  }
}