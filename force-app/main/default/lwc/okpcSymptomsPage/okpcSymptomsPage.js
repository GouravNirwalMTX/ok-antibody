import {
    LightningElement,
    track,
    api
} from 'lwc';
import getSymptoms from "@salesforce/apex/OkpcContactInformationController.getSymptoms";
import saveSymptoms from "@salesforce/apex/OkpcContactInformationController.saveSymptoms";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcSymptomsPage extends LightningElement {
    @track dataObj = {};
    @track contactId;
    @track showSpinner = false; // defaults to false
    @track showDateFields = false; //defaults to false
    @api currentStep;
    @track todayDate;

    connectedCallback() {
        console.log('inside conn symptoms');
        this.todayDate = this.getTodayDate();
        this.getContact();
    }

    get yesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
            {
                label: "I do not know",
                value: "I do not know"
            }
        ];
    }

    get onlyYesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
        ];
    }

    handleInputChange(event) {
        if (event.target.name == 'beenSickMoreThan1day') {
            this.dataObj.beenSickMoreThan1day = event.target.value;
        } else if (event.target.name == 'feverOrCough') {
            this.dataObj.feverOrCough = event.target.value;
        } else if (event.target.name == 'firstFeltSick') {
            this.dataObj.firstFeltSick = event.target.value;
        } else if (event.target.name == 'firstFeltWell') {
            this.dataObj.firstFeltWell = event.target.value;
        }else if (event.target.name == 'prescriptionMed') {
            this.dataObj.prescriptionMed = event.target.value;
        }else if (event.target.name == 'medsInlastThreedays') {
            this.dataObj.medsInlastThreedays = event.target.value;
        }

        if (this.dataObj.beenSickMoreThan1day == 'Yes' || this.dataObj.feverOrCough == 'Yes') {
            this.showDateFields = true;
        } else {
            this.showDateFields = false;
            this.dataObj.firstFeltSick = null;
            this.dataObj.firstFeltWell = null;
        }
    }

    getContact() {
        this.showSpinner = true;
        getSymptoms()
            .then(result => {
                console.log('symptoms=>', JSON.stringify(result));
                this.dataObj.firstFeltWell = result.firstFeltWell;
                this.dataObj.firstFeltSick = result.firstFeltSick;
                this.dataObj.feverOrCough = result.feverOrCough;
                this.dataObj.beenSickMoreThan1day = result.beenSickMoreThan1day;
                this.dataObj.medsInlastThreedays = result.medsInlastThreedays;
                this.dataObj.prescriptionMed = result.prescriptionMed;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                if (this.dataObj.beenSickMoreThan1day == 'Yes' || this.dataObj.feverOrCough == 'Yes') {
                    this.showDateFields = true;
                } else {
                    this.showDateFields = false;
                    this.dataObj.firstFeltSick = null;
                    this.dataObj.firstFeltWell = null;
                }
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @api
    saveContacts() {
        this.dataObj.contactId = this.contactId;
        if(!this.isValid()){
            return;
        }
        if (this.dataObj.beenSickMoreThan1day == '' || typeof this.dataObj.beenSickMoreThan1day == 'undefined' || this.dataObj.feverOrCough == '' || typeof this.dataObj.feverOrCough == 'undefined' ||this.dataObj.medsInlastThreedays == '' || typeof this.dataObj.medsInlastThreedays == 'undefined' || this.dataObj.prescriptionMed == '' || typeof this.dataObj.prescriptionMed == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }
        
        this.showSpinner = true;
        saveSymptoms({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep
            })
            .then(result => {
                console.log('Successfully Saved');
                // this.extraWrapper = {...this.dataObj};
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));
                
                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "3";
                this.sendEventToParent(tempObj);
                
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                this.showSpinner = false;
            })
    }
    sendEventToParent(obj) {

        const selectedEvent = new CustomEvent("symptomssave", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    isValid() {
        let valid = true;
       let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        return valid = isAllValid;
    }


    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

}