import { LightningElement,api,track } from 'lwc';
import markComplete from '@salesforce/apex/DC_ScheduleAppointment.completeAppointment';
import markCancel from '@salesforce/apex/DC_ScheduleAppointment.cancelAppointment';
import markInProgress from '@salesforce/apex/DC_ScheduleAppointment.inProgressVitalAppointment';
import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateCovidAppointment';
import getLabRecords from '@salesforce/apex/DC_ScheduleAppointment.retrieveLab';
// import getBarcode from '@salesforce/apex/DC_ScheduleAppointment.returnBarcodeNumber';
// import setBarcode from '@salesforce/apex/DC_ScheduleAppointment.setBarcodeNumber';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class Oktccompleteconfirmation extends LightningElement {
    @track modaltype = 'complete';
    @track barcodeId = '';
    @api 
    get type() {
        return this.modaltype;
    }
    set type(val){
        this.modaltype = val;
    }
    @api appointmentId;
    @api appointmentList = [];
    @api isBulk;
    @track labId = '';
    @track showSpinner = false;
    @track laboptions =[];
    @track reason;
    @track isComplete = false;
    @track isInProgress = false;
    @track isCancelled = false;
    @track isRescheduled = false;
    @track isAddAppointment = false;
    @api rescheduleData = {};
    @api pcrTest;
    @api rapidTest;
    @api antibodyTest;
    @track result = [{label:'Positive',value:'Positive'},{label:'Negative',value:'Negative'}, {label:'Invalid Result',value:'Invalid Result'}];
    @track testNameOptions = [ {label : 'Abbot ID Now COVID-19', value : 'Abbot ID Now COVID-19'},
    {label : 'BD Veritor Plus System SARS-CoV-2', value : 'BD Veritor Plus System SARS-CoV-2'},
    {label : 'BinaxNOW SARS-CoV-2 Ag Card', value : 'BinaxNOW SARS-CoV-2 Ag Card'},
    {label : 'Sofia SARS Antigen FIA', value : 'Sofia SARS Antigen FIA'}];

    @track testingTypeOptions = [{label:'PCR Testing',value:'PCR Testing'},{label:'Rapid Testing',value:'Rapid Testing'}];
    @track selectedResult;
    @track rapidNotes;
    @track selectedTestName = 'Abbot ID Now COVID-19';
    @track selectedTestingType;
    @track isResultSubmitted = false;
    @track verbalConsent = false;


    get heading() {
        let heading = '';
        if(this.type == 'complete') {
            this.isComplete = true;
            heading = 'Complete Appointment';
        }else if(this.type == 'inProgress') {
            this.isInProgress = true;
            heading = 'In Progress Appointment';
        }else if(this.type == 'cancel') {
            this.isCancelled = true;
            heading = 'Cancel Appointment';
        }else if(this.type == 'reschedule') {
            this.isRescheduled =  true;
            heading = 'Re-Schedule Appointment';
        }else if(this.type == 'reschedule') {
            this.isRescheduled =  true;
            heading = 'Re-Schedule Appointment';
        }else if(this.type == 'addAppointment') {
            this.isAddAppointment =  true;
            heading = 'Add Appointment';
        }

        return heading;
    }
   
    
    connectedCallback() {
     }

     handleInput(event){
        if (event.target.name == 'result') {
            this.selectedResult = event.target.value;
         }else if (event.target.name == 'testingType') {
            this.selectedTestingType = event.target.value;
         }else if (event.target.name == 'testName') {
            this.selectedTestName = event.target.value;
         }else if (event.target.name == 'notes') {
            this.rapidNotes = event.target.value;
         }else if (event.target.name == 'verbalConsent') {
            this.verbalConsent = event.target.checked;
         }
     }

     submitRapidResult(){
         if(!this.selectedResult){

         }else{
            this.isResultSubmitted = true;
         }
         
     }
     
     handleInputChange(event) {
        this.reason = event.target.value;
    }
    cancelAppointment() {
        if(typeof this.reason == 'undefined' || this.reason == '' || this.reason === null){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter reason for cancellation',
            }));
            return;
        }
        if(this.isBulk){
        this.showSpinner = true;
            let calloutParams = {"appointmentId" : this.appointmentList,
                                "reason" : this.reason};
            markCancel(calloutParams).then(result => {
                this.showSpinner = false;
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'cancel'
                }));
            })
            .catch(error => {
                this.showSpinner = false;
                this.error = error;
            });

        }else{
            
        this.showSpinner = true;
        this.appointmentList = [this.appointmentId];
        let calloutParams = {"appointmentId" : this.appointmentList,
                                "reason" : this.reason};
        markCancel(calloutParams).then(result => {
            this.showSpinner = false;
            this.dispatchEvent(new CustomEvent('close', {
                detail: 'cancel'
            }));
        })
        .catch(error => {
            this.showSpinner = false;
            this.error = error;
        });
            
        }        
    }


    handlebarcodechange(event) {
        this.barcodeId = event.target.value;
    }

    completeAppointment() {
        
      
        if(this.antibodyTest && this.barcodeId == '' || typeof this.barcodeId == 'undefined'){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please enter a barcode.',
            }));
            return;
        }
        markComplete({appointmentId: this.appointmentId, labId: null, barcodeId: this.barcodeId, result : this.selectedResult, testName : this.selectedTestName, notes : this.rapidNotes})
        .then(result =>{
            if(result == 'success'){
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Some error occoured.',
                }));
            }
        })
        .catch(error =>{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }
    rescheduleAppointment(){
        updateAppointmentrec({ accountId : this.rescheduleData.accountId, appointmentId : this.appointmentId, slotId : this.rescheduleData.slotId, reason : this.reason, testingType : this.rescheduleData.testingType}).then(result => {
            if (result.type == 'error') {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: result.message,
                });
                this.dispatchEvent(event);
            } else if (result.type == 'success') {
                const event = new ShowToastEvent({
                    title: 'Success!',
                    variant: 'success',
                    message: result.message,
                });
                this.dispatchEvent(event);
                // this.createCalendar(result);
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }
            
        })
        .catch(error => {
            //console.log('Error-->' + JSON.stringify(error));
            const event = new ShowToastEvent({
                title: 'Error!',
                variant: 'error',
                message: error.message,
            });
            this.dispatchEvent(event);
        });
    }
    inProgressAppointment(){
        markInProgress({appointmentId: this.appointmentId, reason : this.reason})
        .then(result =>{
            if(result == 'success'){
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.dispatchEvent(new CustomEvent('close', {
                    detail: 'save'
                }));
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: result,
                }));
            }
        })
        .catch(error =>{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }
    completeAppointmentWithBarcode(){               
        window.location.href = '/TestingSite/BarCodeLiveStream?id='+this.appointmentId;
      
    }
    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    addAppointment() {
        console.log('valid -->  ', this.isValid())
       if(this.isValid()){
            console.log('fbhfh');
            this.dispatchEvent(new CustomEvent('addappointment', {
                detail: this.selectedTestingType
            }));
      }
    }

    isValid() {
        
        let isAllValid1 = [
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        if(isAllValid1)
            return true;
        else
            return false;
        
    }
}