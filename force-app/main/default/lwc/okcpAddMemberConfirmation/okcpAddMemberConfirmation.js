import { LightningElement } from 'lwc';

export default class OkcpAddMemberConfirmation extends LightningElement {

    get heading() {
        let heading = 'Add Person';
        return heading;
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }
    addMember() {
        this.dispatchEvent(new CustomEvent('addmember'));
    }
}