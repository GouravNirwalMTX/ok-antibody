import {
    LightningElement,
    track,
    api
} from 'lwc';
import getAntibody from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getAntibody';
import getLabId from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getLabId';
import getBatch from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.retrieveBatch';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class OklcAntibodyContainer extends LightningElement {
    @track columns = [{
            label: 'Testing Id',
            name: 'Antibody_Testing_Number__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Date of Birth',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Date of Specimen Collected',
            name: 'Date_of_Specimen_Collection__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Container Barcode',
            name: 'Container_Barcode_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Testing Site',
            name: 'Testing_Site__r.Name',
            type: 'text',
            action: ''
        },

    ];

    @track columns2 = [{
            label: 'Testing Id',
            name: 'Antibody_Testing_Number__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Date of Specimen Collected',
            name: 'Date_of_Specimen_Collection__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Testing Site',
            name: 'Testing_Site__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Result',
            name: 'Results__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Lab Test Completion Date',
            name: 'Formatted_lab_test_completion_date__c',
            type: 'text',
            action: ''
        },

    ];

    @track showPaginator = false;
    @track data = [];
    @track labId ='';
    batchId = null;
    @track loadTable = false;
    @track batchoptions;
    @track whereClausetemp = ' id = null';
    
    connectedCallback(){
        this.getlabIdFromContact();
        console.log('connectd',this.labId);
       
    }

    async handleChange(event) {
        await this.handleBatchId(event);
        this.template.querySelector('c-oktcgenericpaginator').resetPagination();
    }

    handleBatchId(event){
        this.batchId = event.detail.value;
        if(this.batchId != null && this.batchId != '' && this.batchId != undefined){
            this.whereClausetemp =  ' (Results__c = null) AND Laboratory__c = \''+this.labId+'\' AND Batch__c = \''+this.batchId+'\'';
        }else{
            this.whereClausetemp =  ' (Results__c = null) AND Laboratory__c = \''+this.labId+'\'';
        }
    }
    
    get whereClause(){
        return this.whereClausetemp;
    }
    
    get whereClausepositive(){
        return ' Results__c LIKE \'%positive%\' AND Laboratory__c = \''+this.labId+'\'';
    }
    get whereClausenegative(){
        return ' Results__c LIKE \'%negative%\' AND Laboratory__c = \''+this.labId+'\'';
    }
    get whereClausereprocess(){
        return ' Results__c LIKE \'%Rejected%\' AND Laboratory__c = \''+this.labId+'\'';
    }


    getlabIdFromContact(){
        getLabId()
        .then(result =>{
            this.labId = result.AccountId;
            //this.getAntibodyList();
            this.whereClausetemp = ' (Results__c = null) AND Laboratory__c = \''+this.labId+'\'';
            // this.whereClause = ' (Results__c = null) AND Laboratory__c = \''+this.labId+'\' AND Batch__c = \''+this.batchId+'\'';
            this.loadTable = true;
            console.log(this.labId);
            this.getBatchList();
        })
        .catch(error =>{
            console.error(error);
        })
    }

    getBatchList(){
        getBatch({labId: this.labId})
        .then(result => {
            console.log('result batch: ',result );
            this.batchoptions = result;
        })
        .catch(error => { 
            console.log('Error ',error);
        });
    }
    

    getAntibodyList() {
        this.data = [];
        console.log('labId: ', this.labId);
        console.log('batchId: ', this.batchId);
        if(this.labId == '' || typeof this.labId == 'undefined'){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'No Lab is associated to the account.',
            }));
            return;
        }
        if(this.batchId == '' || typeof this.batchId == 'undefined' || this.batchId == null){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a batch',
            }));
            return;
        }
        getAntibody({labId: this.labId,
                   batchId: this.batchId })
            .then(result => {
                console.log(JSON.stringify(result));
                // this.data = result;
                result.forEach(el =>{
                    let temparr = {};
                    temparr.Id = el.Id;
                    temparr.TestNumber = el.TestNumber;
                    temparr.patientId = el.patientId;
                    temparr.firstName = el.firstName;
                    temparr.middleName = el.middleName;
                    temparr.lastname = el.lastname;
                    temparr.gender = el.gender;
                    temparr.dateOfBirth = el.dateOfBirth;
                    temparr.organizationName = el.organizationName;
                    temparr.barCoderNumber = el.barCoderNumber;
                    temparr.dateOfSpecimenCollection = el.dateOfSpecimenCollection;
                    temparr.sourceOfSpecimen = el.sourceOfSpecimen;
                    temparr.containerBarCode = el.containerBarCode;
                    temparr.batchId = el.batchId;
                    temparr.result = el.result;
                    temparr.dateOfLabTestCompletion = el.dateOfLabTestCompletion;
                    this.data.push(temparr);
                })
                if(this.data){
                    console.log('this.data: ', JSON.stringify(this.data));
                    this.downloadCSVFile();
                }
            })
            .catch(error => {
                this.error = error;
                this.data = undefined;
            });
    }

    // create csv file
    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        });

        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);

        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;

        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }

        // Creating anchor element to download
        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'ATData.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }



    // Begining file upload functionality

    // filesUploaded = [] ;
    // @track fileName;
    // file;
    // fileContents;
    // fileReader;
    // content;
    // MAX_FILE_SIZE = 1500000;

    // handleFileChange(event){
    //     console.log('uploading');
    //     this.filesUploaded = event.target.files;
    //     this.fileName = event.target.files[0].name;
    //     console.log('uploading ',this.fileName );
    // }

    // handleUpload() {
    //     if(this.filesUploaded.length > 0) {
    //         this.uploadHelper();
    //     }
    //     else {
    //         this.fileName = 'Please select file to upload!!';
    //     }
    // }

    // uploadHelper(){
    //     this.file = this.filesUploaded[0];
    //     if (this.file.size > this.MAX_FILE_SIZE) {
    //         window.console.log('File Size is to long');
    //         return ;
    //     }
    //     this.fileReader = new FileReader();
    //     this.fileReader.readAsText(this.file, "UTF-8");

    //     this.fileReader.onload = (() => {
    //         this.fileContents = this.fileReader.result;
            
    //        let result = this.csvToJson(this.fileContents);
    //        this.insertResults(result);
    //     });
    //     this.fileReader.onerror = function (evt) {
    //         //console.log("error reading file");
    //     }
    // }

    // csvToJson(csv){
    //     var arr = []; 
        
    //     arr =  csv.split('\n');
    //     //console.log('Array  = '+array);
    //     // console.log('arr = '+arr);
    //     arr.pop();
    //     var jsonObj = [];
    //     var headers = arr[0].split(',');
    //     for(var i = 1; i < arr.length; i++) {
    //         var data = arr[i].split(',');
    //         var obj = {};
    //         for(var j = 0; j < data.length; j++) {
    //             obj[headers[j].trim()] = data[j].trim();
    //             //console.log('obj headers = ' + obj[headers[j].trim()]);
    //         }
    //         jsonObj.push(obj);
    //     }
    //     var json = JSON.stringify(jsonObj);
    //     //console.log('json = '+ json);
    //     return json;
    // }

    // insertResults(jsonstr){
    //     insertData({strfromle : jsonstr})
    //     .then(result =>{
    //         console.log('success ', JSON.stringify(result));
    //     })
    //     .catch(error =>{
    //         console.log('error ',JSON.stringify(error));
    //     })
    // }
}