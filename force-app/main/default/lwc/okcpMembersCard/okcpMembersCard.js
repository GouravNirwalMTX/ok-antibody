import { LightningElement, api } from 'lwc';

export default class OkcpDependentCard extends LightningElement {
    @api patient;

    get cardStyle() {
        return 'slds-box slds-m-top_small ' + (this.patient.isdependent ? '' : 'community-user');
    }

    handleClick() {
        this.dispatchEvent(new CustomEvent('updatedetails', {
            bubbles: true, composed: true, detail: {
                patientId: this.patient.id,
                isDependentContact: this.patient.isCommunityUser
            }
        }));
        // add event handler on the topmost component
    }
}