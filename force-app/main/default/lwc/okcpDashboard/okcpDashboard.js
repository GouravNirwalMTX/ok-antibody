import { LightningElement,track } from 'lwc';
import retrieveResult from '@salesforce/apex/OKPCDashboardController.retrieveResultCovidPortal';
import getAccountdetails from '@salesforce/apex/OKPC_LocationMapController.getAccountdetails';
import createAppointment from '@salesforce/apex/OKPCDashboardController.createAppointment';
import deleteAppointment from '@salesforce/apex/OKPCDashboardController.deleteAppointment';
import euaPatientPDF from '@salesforce/label/c.EUA_Patient_PDF';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
export default class OkpcDashboard extends LightningElement {
    @track contactName; 
    @track contactId; 
    @track streetAddress; 
    @track accountIdForReschedule;
    @track appointmentID;
    appointmentList = [];
    @track appointment = {}; 
    @track showMap = false;
    @track showConfirmation = false;
    @track showAppointmentModal = false;
    @track address;
    @track mapMarkers = [];
    @track results = {};
    @track isCancelledWindow = false;
    @track hasResults = false; 
    @track showpick = true;
    @track testingType = 'Covid Testing';
    @track multipleAppt = false;
    @track isReschedule = false;
    @track patientPDF;
    @track showMemberConfirmation = false;
    @track isdependent = false;
    @track showSelectContact = false;
    @track appointmentDetails = {};
    contactList = [];

    get hasAppointment() {
        return this.appointmentList.length > 0;
    }
   
    connectedCallback(){
        this.patientPDF = euaPatientPDF;
        this.callApex();
    }
    callApex() {
        this.appointment = {};
        retrieveResult()
        .then(result =>{
            if(result != null){
            
                console.log('### ',JSON.stringify(result));
               /* if(result.contact) {
                    this.contactId = result.contact.Id;
                    let contact = result.contact;
                    this.contactName = contact.Name;
                    this.streetAddress = contact.MailingStreet;
                    let addressString = '';
                    if(contact.MailingCity != null && typeof contact.MailingCity != 'undefined')
                        addressString += contact.MailingCity;
                    if(contact.MailingState != null && typeof contact.MailingState != 'undefined')
                        addressString += addressString == '' ? contact.MailingState : ', ' + contact.MailingState;
                    if(contact.MailingPostalCode != null && typeof contact.MailingPostalCode != 'undefined')
                        addressString += ' ' + contact.MailingPostalCode;
                    this.address = addressString;
                } */
                if (result.contact) {
                    let tmpContactList = [...result.contact];
                    let tmpContacts = [];
                    tmpContactList.forEach(contactRecord => {
                        let addressString = '';
                        if (contactRecord.MailingCity != null && typeof contactRecord.MailingCity != 'undefined') {
                            addressString += contactRecord.MailingCity;
                        }
                        if (contactRecord.MailingState != null && typeof contactRecord.MailingState != 'undefined') {
                            addressString += addressString == '' ? contactRecord.MailingState : ', ' + contactRecord.MailingState;
                        }
                        if (contactRecord.MailingPostalCode != null && typeof contactRecord.MailingPostalCode != 'undefined') {
                            addressString += ' ' + contactRecord.MailingPostalCode;
                        }
                        tmpContacts.push({
                            id: contactRecord.Id,
                            name: contactRecord.Name,
                            streetAddress: contactRecord.MailingStreet,
                            address: addressString,
                            isdependent: contactRecord.Dependent__c
                        });
                    });
                    this.contactList = tmpContacts;
                }

                if (result.appointment){
                   // console.log('##>>> ', result.appointment);
                   
                    const resultList = [...result.appointment.filter(word => word.Status__c != 'Eligible' )];
                    //console.log('Test Array >>>' + JSON.stringify(resultList))
                    //const resultListForComplete = result.appointment;
                    //console.log('FinalCompleteLsit>>>', JSON.stringify(resultListForComplete));
                    let tmpAppointments = [];
                    resultList.forEach(apt => {
                      //  console.log('Innnnnn')
                        var aptRecord = {};
                        let isScheduled = apt.Status__c == 'Scheduled';
                        let isToBeScheduled = apt.Status__c == 'To Be Scheduled';
                        let isInProgress = apt.Status__c == 'In Progress';
                        let isCancelled = apt.Status__c == 'Cancelled';
                        //let isPCR = apt.Testing_Site_Type__c == 'PCR Testing';
                        let isRapid = apt.Testing_Site_Type__c == 'Rapid Testing';
                        let isAntibody = apt.Testing_Site_Type__c == 'Antibody Testing';
                        let isCompleted = false;
                        if ((apt.Status__c == 'Completed') || (apt.Status__c == 'Closed')) {
                            isCompleted = true;
                        }
                        aptRecord.id = apt.Id;
                        aptRecord.startTime = isScheduled || isInProgress  ? this.msToTimeAMPM(apt.Appointment_Slot__r.Start_Time__c) : this.msToTimeAMPM(apt.Patient__r.Preferred_Time__c);
                        aptRecord.startDate = isScheduled || isInProgress  ? apt.Appointment_Slot__r.Date__c : apt.Patient__r.Preferred_Date__c;

                        if (isCancelled) {
                            aptRecord.startTime = this.msToTimeAMPM(apt.Appointment_Start_Time_v1__c);
                            aptRecord.startDate = apt.Appointment_Start_Date_v1__c;
                        }
                        if(isCompleted){
                            aptRecord.completedDateTime = apt.Appointment_Complete_Date__c;
                            aptRecord.startDateTime = apt.Appointment_Start_Date_Time__c;
                            aptRecord.isInProgressDateTime =apt.Appointment_Inprogress_Date_Time__c;
                        }
                        
                        let addressString = '';
                        if (apt.Testing_Site_Account__r.BillingStreet != null && typeof apt.Testing_Site_Account__r.BillingStreet != 'undefined')
                            addressString += apt.Testing_Site_Account__r.BillingStreet;
                        if (apt.Testing_Site_Account__r.BillingCity != null && typeof apt.Testing_Site_Account__r.BillingCity != 'undefined')
                            addressString += addressString == '' ? apt.Testing_Site_Account__r.BillingCity : ', ' + apt.Testing_Site_Account__r.BillingCity;
                        if (apt.Testing_Site_Account__r.BillingState != null && typeof apt.Testing_Site_Account__r.BillingState != 'undefined')
                            addressString += addressString == '' ? apt.Testing_Site_Account__rBillingState : ', ' + apt.Testing_Site_Account__r.BillingState;
                        if (apt.Testing_Site_Account__r.BillingPostalCode != null && typeof apt.Testing_Site_Account__r.BillingPostalCode != 'undefined')
                            addressString += ' ' + apt.Testing_Site_Account__r.BillingPostalCode;
                        aptRecord.address = addressString;
                       // console.log('>>>>>>>>' + addressString);

                        aptRecord.patientName = apt.Patient__r.Name;
                        aptRecord.isScheduled = isScheduled;
                        aptRecord.isInProgress = isInProgress;
                        aptRecord.isRapid = isRapid;
                        aptRecord.isAntibody = isAntibody;
                        aptRecord.testingType = apt.Testing_Site_Type__c;
                       // console.log('>>>>>>>>' + aptRecord.isScheduled);
                        aptRecord.isCancelled = isCancelled;
                        aptRecord.isToBeScheduled = isToBeScheduled;
                        aptRecord.siteId = apt.Testing_Site_Account__c;
                        console.log('Site' + aptRecord.siteId);
                        aptRecord.siteName = apt.Testing_Site_Account__r.Name;
                        aptRecord.isCompleted = isCompleted;
                        // console.log('## this.appointment ', this.appointment);
                        // this.retrieveMarker();
                       // tmpAppointments.push(aptRecord);
                       // console.log('LoopList>> ', JSON.stringify(tmpAppointments))

                        if(apt.Antibodies_Testing__r){
                            aptRecord.result = apt.Antibodies_Testing__r[0];
                            aptRecord.labName = aptRecord.result.Laboratory__r.Name;
                           console.log('##@ ', JSON.stringify(aptRecord.result.Laboratory__r.Name));
                            if(aptRecord.result.Results__c){
                                aptRecord.notes = aptRecord.result.Notes__c;
                                aptRecord.testName = aptRecord.result.Test_Name__c;
                                aptRecord.hasResult = true;
                                aptRecord.isCompleted = false;
                               // console.log('changing >>> ');
                                 this.hasResults = true;
                              //  this.appointment.isCompleted = false; 
                            }
                        }
                        tmpAppointments.push(aptRecord);
                        //console.log('LoopList>> ', JSON.stringify(tmpAppointments))
                    });
                    this.appointmentList = tmpAppointments;
                       console.log('llastList>> ', JSON.stringify(this.appointmentList))
                }
              /*  if(result.appointment) {
                    console.log('## ',result.appointment);
                    let app = result.appointment;
                    let isScheduled = app.Status__c == 'Scheduled';
                    let isToBeScheduled = app.Status__c == 'To Be Scheduled';
                    let isCancelled = app.Status__c == 'Cancelled';
                    let isCompleted = false;
                    if((app.Status__c == 'Completed') || (app.Status__c == 'Closed')){
                        isCompleted = true;
                    }
                    console.log('isScheduled' + isScheduled);
                    console.log('Is autooo' + result.contact.Auto_Scheduled__c);
                   */
                    //commented by Sameer 07/22/2020 as we updated th field.
                  /*  if(isScheduled && result.contact.Auto_Scheduled__c){
                        console.log('in<<<');
                        this.appointment.startTime = this.msToTimeAMPM(app.Appointment_Start_Time_v1__c);
                        this.appointment.startDate = app.Appointment_Start_Date_v1__c;
                        console.log('Start Time>>'+ this.appointment.startTime);
                        console.log('Start Date>>'+ this.appointment.startDate );
                    } */
                    /*    this.appointment.startTime = isScheduled ? this.msToTimeAMPM(app.Appointment_Start_Time_v1__c) : this.msToTimeAMPM(app.Patient__r.Preferred_Time__c) ;
                        this.appointment.startDate = isScheduled ? app.Appointment_Start_Date_v1__c : app.Patient__r.Preferred_Date__c ;
                       
                    this.appointment.id = app.Id;
                  
                    if(isCancelled){
                        console.log('in>>');
                        this.appointment.startTime = this.msToTimeAMPM(app.Appointment_Start_Time_v1__c);
                        this.appointment.startDate = app.Appointment_Start_Date_v1__c;
                        console.log('Start Time>>'+ this.appointment.startTime);
                        console.log('Start Date>>'+ this.appointment.startDate );
                    }
                    let addressString = '';
                    if(app.Patient__r.Testing_Site__r.BillingStreet != null && typeof app.Patient__r.Testing_Site__r.BillingStreet != 'undefined')
                        addressString += app.Patient__r.Testing_Site__r.BillingStreet;
                    if(app.Patient__r.Testing_Site__r.BillingCity != null && typeof app.Patient__r.Testing_Site__r.BillingCity != 'undefined')
                        addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingCity : ', ' + app.Patient__r.Testing_Site__r.BillingCity;
                    if(app.Patient__r.Testing_Site__r.BillingState != null && typeof app.Patient__r.Testing_Site__r.BillingState != 'undefined')
                        addressString += addressString == '' ? app.Patient__r.Testing_Site__r.BillingState : ', ' + app.Patient__r.Testing_Site__r.BillingState;
                    if(app.Patient__r.Testing_Site__r.BillingPostalCode != null && typeof app.Patient__r.Testing_Site__r.BillingPostalCode != 'undefined')
                        addressString += ' ' + app.Patient__r.Testing_Site__r.BillingPostalCode;
                    this.appointment.address = addressString;
                    this.appointment.isScheduled = isScheduled;
                    this.appointment.isCancelled = isCancelled;
                    this.appointment.isToBeScheduled = isToBeScheduled;
                    this.appointment.siteId = app.Patient__r.Testing_Site__c;
                    this.appointment.siteName = app.Patient__r.Testing_Site__r.Name;
                    this.appointment.isCompleted = isCompleted;
                    console.log('## this.appointment ', this.appointment);
                    this.retrieveMarker();
                }
                if(result.results){
                    this.results = result.results;
                    console.log('##@ ', JSON.stringify(this.results));
                    if(result.results.result){
                        console.log('changing >>> ');
                        this.hasResults = true;
                        this.appointment.isCompleted = false; 
                    }
                }
                console.log('this.appointment.isCompleted;  ',this.appointment.isCompleted); 
                */
            }
        })
        .catch(result =>{
            //
        })
        // this.initColSelection();
        // this.callApex();
    }

    retrieveMarker(event) {
        this.accountId = event.target.dataset.id;
        this.mapMarkers = [];
        if(this.mapMarkers.length <= 0){
            getAccountdetails({
                accountId: this.accountId
            })
            .then(result => {
                this.accountDetails = JSON.parse(JSON.stringify(result));
                if(this.accountDetails){
                    this.mapMarkers.push(this.accountDetails);
                    this.showMap = true;
                }
            })
            .catch(error => {
                console.log('error',error);
            })
        }
        
    }
 /*   viewCertificate(){
        console.log('testing...');
        var url = window.location.href;
        url = url.substring( 0, url.indexOf( "/s/" ) );
        url += '/apex/ResultCertificate?id='+this.contactId;
        console.log('#$ ',url);
        window.open(url,'_blank');
    } */

    viewLocationOnMap() {
        this.showMap = true;
    }

    editAppointment() {
        this.showAppointmentModal = true;
    }

    cancelAppointment(event) {
        this.appointmentID = event.target.dataset.id
        console.log('Cancel Id',  this.appointmentID);
        this.showConfirmation = true;
    }
    openCancel(event){
        this.appointmentID = event.target.dataset.id;
        this.accountIdForReschedule = event.target.dataset.accid;
        console.log('openId>>', this.appointmentID);
        console.log('AccId', this.accountIdForReschedule);
        this.isReschedule = true;
        this.multipleAppt = true;
        this.isCancelledWindow = true;
    }
    hideModal(event) {
        this.callApex();
        this.showConfirmation = false;
        this.showAppointmentModal = false;
        this.showMap = false;
    }
    navigateToScheduleAppointment(event){
       if (event.target.name == 'next') {
            this.template.querySelector('c-ok-covid-symptomspage').sendSymptomsDetailsToDashboard();
        } 
    }
    saveSymptoms(event) {
       
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else{
            this.appointmentDetails = event.detail.appointmentDetails;
            this.isdependent = true;
            this.multipleAppt = true;
        }
    }
    msToTimeAMPM(s) {
        if(s == '' || s == null || typeof s == 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if(hrs >= 12) {
            radian = 'PM';
            if(hrs > 12){
                hrs = hrs - 12;
            }
            
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' '  + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    handleSubmitData(event) {
        console.log('Milla Event' + event)
        console.log('Milla Event' + JSON.stringify(event))
        if (event.detail.name == 'submit'  ) {
            this.callApex();
            this.isdependent = false;
            this.isCancelledWindow = false;
            this.isReschedule = false;
            this.multipleAppt = false;
        }
        else if (event.detail.name == 'cancel') {
            this.isdependent = false;
            this.isCancelledWindow = false;
            this.isReschedule = false;
            this.multipleAppt = false;
        }
      /*  else if (event.detail.name == 'cancel') {
            //this.callApex();
            // this.appointmentID = event.detail.appId
            // this.cancelAppointments();
            // console.log('cancel ' + event)
            // deleteAppointment({
            //     appointmentId: event.detail.appId
            // })
            // .then(result => {
            //     this.isCancelledWindow = false;
            // })
            // .catch(error => {
            //     console.log('error', error);
            // })          
        } */
    }
    cancelAppointments(){
        this.isCancelledWindow = false;
        this.isdependent = false;
        this.isReschedule = false;
        this.multipleAppt = false;
      //  console.log('Appointmnt id>' + this.appointmentID)
       /* deleteAppointment({
            appointmentId: this.appointmentID
        })
        .then(result => {
            this.isCancelledWindow = false;
        })
        .catch(error => {
            console.log('error', error);
        })   */
    }
 
    /*  createAppointment() {
        createAppointment({
                contactId: this.contactId
            })
            .then(result => {
                if (result != null) {
                    this.appointmentID = result;
                    console.log('New appointment>>' + this.appointmentID);
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'success',
                        message: 'Appointment created Successfully',
                    }));
                    this.multipleAppt = false;
                    this.isReschedule = false;
                    this.isCancelledWindow = true;
                }

            })
            .catch(error => {
                console.log('error', error);
            })
    } */

    openFile(){
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                //recordIds: '069xx0000000001AAA,069xx000000000BAAQ',
                selectedRecordId:'069r0000002SQKfAAO'
            }
          })
        }
    addmemberPage(){
        console.log('in add member')
        this.showMemberConfirmation = false;
        this.isdependent = true;
        this.dispatchEvent(new CustomEvent('addmember',{
            detail: this.isdependent
        }));
    }
    closeModal(){
        this.showMemberConfirmation = false;
    }
   /* openModal(){
        this.showMemberConfirmation = true;
    }*/

    handleClick(event) {
        switch (event.target.name) {
            case 'scheduleAppointment':
                this.showSelectContact = true;
                break;
            case 'addMember':
                this.showMemberConfirmation = true;
                break;
            default:
                break;
        }
    }
    handleClose(event) {
        this.showSelectContact = false;
    }
    handleContactSelected(event){
        console.log('event in dashboard',JSON.stringify(event.detail))
        this.contactId = event.detail;
        console.log('contactId in dashborad>>>',this.contactId)
        this.isCancelledWindow = true;
        this.showSelectContact = false;
    }
}