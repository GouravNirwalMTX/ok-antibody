import { LightningElement, track ,api} from 'lwc';
import getAppintments from '@salesforce/apex/OKTC_ScheduleAppointment.getAccountSlots';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';

export default class OktcCalendar extends LightningElement {

    @track recid;
    @track data = {};
    @track currentSelRecordId = '';
    @track allSelectedSlots = new Set();
    @track columnSelectionMap = new Map();
    @track rowSelectionMap = new Map();
    @track allSlots = new Map();

    @track loaded = false;

    @track startTime;
    @track endTime;
    @track appointmentFrequency = 30;
    
    timeSlots = [];

    @api
    get accountid(){
        return this.recid;
    }
    set accountid(val){
       this.recid = val;
       getBusinessTimeOnAccount()
        .then(result =>{
            if(result != null){
                this.startTime = this.msToTime(result.Business_Start_Time__c);
                this.endTime = this.msToTime(result.Business_End_Time__c);
                if(result.Appointment_Frequency__c){
                    this.appointmentFrequency = result.Appointment_Frequency__c;
                }
                this.callApex();
            }
        })
        .catch(result =>{
            //
        })
    }

    @api
    getSlots() {
        let slots = this.allSlots.size > 0 ? Array.from(this.allSlots.values()) : [];
        return slots;
    }
    @api
    getAppointmentFrequency() {
        return this.appointmentFrequency;
    }

    @api
    refreshCalendarView() {
        this.currentSelRecordId = '';
        this.allSelectedSlots = new Set();
        this.allSlots = new Map();
        this.callApex();
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }

    callApex() {
        if(this.recid) {
            this.loaded = false;
            getAppintments({ accountId: this.recid,
                             testingtype: 'Rapid Testing'
            }).then(result => {
                this.createCalendar(result);
                this.loaded = true;
            })
            .catch(error => {
                this.error = error;
                this.loaded = true;
            });
        }
    }

    getFormattedTime(timeInstance) {
        let hourT = parseInt(timeInstance.substr(0,2));
        let minT = parseInt(timeInstance.substr(3,2));
        let amPM = hourT <= 11 ? 'AM' : 'PM';
        return {hourT: hourT, minT: minT, amPM: amPM};
    }

    createTimeSlots() {
        let minhr = 0;
        let maxhr = 23;
        let minmnt = 0;
        let maxmnt = 60;
        if(this.startTime) {
            let timeObj = this.getFormattedTime(this.startTime);
            minhr = timeObj.hourT;
            minmnt = timeObj.minT;
        }
        if(this.endTime) {
            let timeObj = this.getFormattedTime(this.endTime);
            maxmnt = timeObj.minT;
            maxhr = timeObj.hourT;
        }

        let timeInterval = this.appointmentFrequency;
        for(let hr = parseInt(minhr); hr <= parseInt(maxhr); hr++) {
            for(let mnt = 0; mnt < 60; mnt = mnt + parseInt(timeInterval)) {
                if(((parseInt(minmnt) <= parseInt(mnt) || parseInt(hr) > parseInt(minhr)) && parseInt(hr) != parseInt(maxhr)) || 
                    (parseInt(hr) == parseInt(maxhr) && parseInt(maxmnt) > parseInt(mnt))) {
                    let timeStr = this.generateTimeOption(hr,mnt);
                    this.timeSlots.push(timeStr);    
                    this.addSlots(timeStr);
                }
            }
        }
    }

    generateTimeOption(hr,mnt) {
        let radian = hr < 12 ? 'AM' : 'PM';
        let timehr = hr <= 12 ? (hr == 0 ? 12 : hr) : hr - 12;
        let minute = mnt < 10 ? '0' + mnt : '' +  mnt;
        return (timehr < 10 ? '0' + timehr : timehr) + ':' + minute + ' ' + radian;
    }

    createCalendar(result) {
        this.columnSelectionMap = new Map();
        this.rowSelectionMap = new Map();
        
        this.data = {};
        let weekDays = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
        
        this.data.dates = result.dateHeaderList;
        this.data.weeks = result.weekHeaderList;
        
        this.data.rows = [];
        this.timeSlots = [];
        
        this.createTimeSlots();

        let columnCount = 0;

        for (var m in result.records){
            let recds = result.records[m];
            let rowCount = 0;
            this.columnSelectionMap.set(columnCount, false);
            this.timeSlots.forEach(element => {
                this.rowSelectionMap.set(rowCount, false);
                if(typeof recds[element] != 'undefined' && recds[element] != null && recds[element].length > 0) {
                    this.data.rows[rowCount].cols[columnCount].isAvailable = true;
                    this.data.rows[rowCount].cols[columnCount].availableSlots = recds[element].length;
                    this.data.rows[rowCount].cols[columnCount].slotId = recds[element][0].Id;
                    this.data.rows[rowCount].cols[columnCount].recId = recds[element][0].Id;
                    this.data.rows[rowCount].cols[columnCount].newRecord = false;
                }
                rowCount++;
            });
            columnCount++;
        } 
        columnCount = 0;
        for (var m in result.bookedRecords){
            let bookedRecords = result.bookedRecords[m];
            let rowCount = 0;
            this.timeSlots.forEach(element => {
                if(typeof bookedRecords[element] != 'undefined' && bookedRecords[element] != null && bookedRecords[element].length > 0) {
                    this.data.rows[rowCount].cols[columnCount].bookedSlots = bookedRecords[element].length;
                }
                rowCount++;
            });
            columnCount++;
        } 

    }

    addSlots( timeStr ){
        let cols = [];
        var numOfSlots = this.data.dates.length;

        for(var i= 0; i< numOfSlots ; i++){
            cols.push({
                availableSlots : 0,
                bookedSlots: 0,
                recId : '',
                slotDate: this.data.dates[i],
                newRecord: true,
                isAvailable : false,
                isSelected : false, 
                slotId : this.makeid(18)
            });
        }
      
        this.data.rows.push({
            timeSt : timeStr,
            cols : cols
        });
    }

    handleSelectedSlots(event){
        this.currentSelRecordId = event.target.dataset.recid;
        let tempSlotid = event.target.dataset.slotid;
        let rowindex = event.target.dataset.rowindex;
        let colindex = event.target.dataset.colindex;

        if(this.data.rows[rowindex].cols[colindex].slotId == tempSlotid) {
            if(this.allSelectedSlots.has(tempSlotid)) {
                this.data.rows[rowindex].cols[colindex].isSelected = false;
                this.allSelectedSlots.delete(tempSlotid);
                this.allSlots.delete(tempSlotid);
            } else {
                this.allSelectedSlots.add(tempSlotid);
                this.data.rows[rowindex].cols[colindex].isSelected = true;
                this.allSlots.set(tempSlotid, JSON.parse(JSON.stringify(event.target.dataset)));
                console.log(JSON.parse(JSON.stringify(event.target.dataset)));
            }
        }
    }

    manageRowclickAction(event) {
        let rowIndex = event.target.dataset.rowindex;
        let currentAttr = this.rowSelectionMap.get(rowIndex);
        let row = this.data.rows[rowIndex];
        console.log('rowIndex',rowIndex);
        console.log('row',row);
        row.cols.forEach((col) => {
            col.isSelected = !currentAttr;
            if(!currentAttr) {
                this.allSelectedSlots.add(col.slotId);
                let slot = {
                    recid: col.recid,
                    slotid: col.slotId,
                    slotdate: col.slotDate,
                    slottime: row.timeSt,
                    available : col.availableSlots
                };
                this.allSlots.set(col.slotId, slot);
            } else {
                this.allSelectedSlots.delete(col.slotId);
                this.allSlots.delete(col.slotId);
            }
        });
        this.rowSelectionMap.set(rowIndex, !currentAttr);
        console.log('allSlots',this.allSlots);
    }

    manageColumnSelection(event){
        let colIndex = event.target.dataset.colindex;
        let currentAttr = this.columnSelectionMap.get(colIndex);
        let rowIndex = 0;
        this.data.rows.forEach((row) => {
            row.cols[colIndex].isSelected = !currentAttr;
            if(!currentAttr) {
                this.allSelectedSlots.add(row.cols[colIndex].slotId);
                let slot = {
                    recid: row.cols[colIndex].recid,
                    rowindex: rowIndex,
                    colindex: colIndex,
                    slotid: row.cols[colIndex].slotId,
                    slotdate: row.cols[colIndex].slotDate,
                    slottime: row.timeSt,
                    available : row.cols[colIndex].availableSlots
                };
                this.allSlots.set(row.cols[colIndex].slotId, slot);
                rowIndex++;
            } else {
                this.allSelectedSlots.delete(row.cols[colIndex].slotId);
                this.allSlots.delete(row.cols[colIndex].slotId);
            }
        });
        this.columnSelectionMap.set(colIndex, !currentAttr);
        console.log('allSlots',this.allSlots);
    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}