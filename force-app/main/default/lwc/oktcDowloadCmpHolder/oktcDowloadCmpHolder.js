import {
    LightningElement,
    track,
    api
} from 'lwc';
import getAntibody from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getAntibodyTestingSite';
import getAntibodyBulk from '@salesforce/apex/oklcAntibodyBulkDownload.getAntibodyTestingSite';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetails';
import getLabId from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.getLabId';
//import getBatch from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.retrieveBatch';
import getBatchforTesting from '@salesforce/apex/oklcAntibodyDownloadAnduploadController.retrieveBatchforTestingSite';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OklcAntibodyContainer extends LightningElement {
    @track columns = [{
            label: 'Testing Id',
            name: 'Antibody_Testing_Number__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Date of Birth',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Appointment Completition Date',
            name: 'Appointment__r.Formatted_Complete_Date__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Barcode Id',
            name: 'Barcode_ID__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Lab Name',
            name: 'Laboratory__r.Name',
            type: 'text',
            action: ''
        },

    ];

    @track columns2 = [{
            label: 'Testing Id',
            name: 'Antibody_Testing_Number__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Id',
            name: 'Patient__r.Patient_Id__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Patient Name',
            name: 'Patient__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Phone',
            name: 'Patient__r.MobilePhone',
            type: 'text',
            action: ''
        },
        {
            label: 'Date of Birth',
            name: 'Patient__r.formatted_Date_of_Birth__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Appointment Status',
            name: 'Appointment__r.Status__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Appointment Date',
            name: 'Appointment__r.Appointment_Start_Date_v1__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Appointment Time',
            name: 'Appointment__r.Formatted_Appointment_Start_Time__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Lab Name',
            name: 'Laboratory__r.Name',
            type: 'text',
            action: ''
        },
        {
            label: 'Result',
            name: 'Results__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Appointment Completion Date',
            name: 'Appointment__r.Formatted_Complete_Date__c',
            type: 'text',
            action: ''
        },
        {
            label: 'Lab Test Completion Date',
            name: 'Formatted_lab_test_completion_date__c',
            type: 'text',
            action: ''
        },

    ];
    @track accountId;
    @track showPaginator = false;
    @track data = [];
    @track dataBulk = [];
    @track labId = '';
    batchId = null;
    @track loadTable = false;
    @track batchoptions;
    @track whereClausetemp = ' id = null';
    @track whereClausedate = ' id = null';
    @track fromDate;
    @track toDate;
    @track todaysDate = '';
    @track showSpinner = false;
    @track testingTypeValue;
    @track testingTypeOptions = [];
    @track testTypeOptions = [];
    @track isAntiBody = false;

    connectedCallback() {
        this.getlabIdFromContact();
        this.getTestingSitedata();
        this.getTodaysDate();
       // console.log('connectd', this.labId);

    }


    getTodaysDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;
        this.todaysDate = today;
        //console.log('today' + today)
       // console.log('today----' + this.todaysDate)
    }

    async handleInputChange(event) {
        await this.handleInputChangeDate(event);
       this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
            element.retrieveRecordsOnChange();
        });

       // this.template.querySelectorAll('c-oktcgenericpaginator').retrieveRecordsOnChange();
    }

    handleInputChangeDate(event) {
        if (event.target.name == 'fromDate') {
            this.fromDate = event.target.value;
           // console.log(this.fromDate);
        } else if (event.target.name == 'toDate') {
            this.toDate = event.target.value;
         //   console.log(this.toDate);
        }else if(event.target.name == 'testingType'){
            this.testingTypeValue = event.target.value;
          //  console.log('testingType>>',this.testingTypeValue)
        }
        if (this.fromDate && this.toDate && this.testingTypeValue) {
            this.whereClausedate = 'Testing_Site__c = \'' + this.labId + '\'AND appointment__r.Appointment_Start_Date_v1__c >=' + this.fromDate + ' AND appointment__r.Appointment_Start_Date_v1__c <= ' + this.toDate + 'AND appointment__r.testing_site_type__c = \'' + this.testingTypeValue + '\'';
        }

    }

    getTestingSitedata() {
        getTestingSite({})
            .then(data => {
                if (data.ContactId)
                    this.accountId = data.Contact.AccountId;
                this.showPaginator = true;
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
    }



    async handleChange(event) {
        
            await this.handleBatchId(event);
         this.template.querySelector('c-oktcgenericpaginator').retrieveRecordsOnChange();
        
        
    }

    handleBatchId(event) {
        this.batchId = event.detail.value;
        if (this.batchId != null && this.batchId != '' && this.batchId != undefined) {
            this.whereClausetemp = 'Testing_Site__c = \'' + this.labId + '\' AND Batch__c = \'' + this.batchId + '\' AND (Results__c = null)';
        } else {
            this.whereClausetemp = 'Testing_Site__c = \'' + this.labId + '\'';
        }
    }

    get whereClause() {
        return this.whereClausetemp;
    }

    get whereClausepositive() {
        return this.whereClausedate;
    }



    getlabIdFromContact() {
        getLabId()
            .then(result => {
               // console.log('result>>' , result)
             //   console.log('result>>' , result.Account.Testing_Site_Type__c)
                this.labId = result.AccountId;
                //this.getAntibodyList();
                this.whereClausetemp = ' (Results__c = null) AND Testing_Site__c = \'' + this.labId + '\'';
                // this.whereClause = ' (Results__c = null) AND Laboratory__c = \''+this.labId+'\' AND Batch__c = \''+this.batchId+'\'';
                this.loadTable = true;
               // console.log(this.labId);
                if(result.Account.Testing_Site_Type__c){
                    if(result.Account.Testing_Site_Type__c.includes(";")){
                        this.testingTypeOptions = result.Account.Testing_Site_Type__c.split(';');
                     this.testingTypeOptions = this.getTestingOptions(this.testingTypeOptions);
                    }else{
                        this.testingTypeOptions = [result.Account.Testing_Site_Type__c];   
                        this.testingTypeOptions = this.getTestingOptions(this.testingTypeOptions);  
                        
                    }
                }
               
                this.getBatchList();
            })
            .catch(error => {
                console.error(error);
            })
    }
   
    getTestingOptions(options){
       options.forEach(element => {
           if(element == 'Antibody Testing'){
                this.isAntiBody = true;
           }
            let option = {label : element, value : element};
            this.testTypeOptions.push(option);
        });
        return this.testTypeOptions;
    }
    getBatchList() {
        getBatchforTesting({
                testingSiteId: this.labId
            })
            .then(result => {
              //  console.log('result batch: ', result);
                this.batchoptions = result;
             
            })
            .catch(error => {
                console.log('Error ', error);
            });
    }

    getAntibodyListForBatch() {
      
        this.data = [];
      //  console.log('labId: ', this.labId);
        if (this.batchId == '' || typeof this.labId == 'undefined' || this.batchId == null) {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select batch',
            }));
            return;
        }


        if (this.labId == '' || typeof this.labId == 'undefined') {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'No Lab is associated to the account.',
            }));
            return;
        }

        getAntibody({
                labId: this.labId,
                batchId: this.batchId
            })
            .then(result => {
              //  console.log(JSON.stringify(result));
                if (result == null) {

                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'warning',
                        message: 'No record present for the Batch provided',
                    }));
                    return;

                }
                // this.data = result;
                result.forEach(el => {
                    let temparr = {};
                    temparr.Id = el.Id;
                    temparr.TestNumber = el.TestNumber;
                    temparr.patientId = el.patientId;
                    temparr.firstName = el.firstName;
                    temparr.middleName = el.middleName;
                    temparr.lastname = el.lastname;
                    temparr.gender = el.gender;
                    temparr.dateOfBirth = el.dateOfBirth;
                    temparr.organizationName = el.organizationName;
                    temparr.barCoderNumber = el.barCoderNumber;
                    temparr.dateOfSpecimenCollection = el.dateOfSpecimenCollection;
                    temparr.sourceOfSpecimen = el.sourceOfSpecimen;
                    temparr.containerBarCode = el.containerBarCode;
                    temparr.batchId = el.batchId;
                    temparr.result = el.result;
                    temparr.dateOfLabTestCompletion = el.dateOfLabTestCompletion;
                    this.data.push(temparr);
                })
                if (this.data) {
                   // console.log('this.data: ', JSON.stringify(this.data));
                    this.downloadCSVFile();
                }
            })
            .catch(error => {
                this.error = error;
                this.data = undefined;
            });
    }
    getAntibodyList() {
     //   console.log('In data antibody');
     //   console.log('isValid' + this.isValid())
        if (!this.isValid()) {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please complete the required fields',
            }));
            return;
        }
        this.dataBulk = [];
      //  console.log('labId: ', this.labId);

        if (this.labId == '' || typeof this.labId == 'undefined') {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'No Lab is associated to the account.',
            }));
            return;
        }

        getAntibodyBulk({
                labId: this.labId,
                toDate: this.toDate,
                fromDate: this.fromDate,
                testingType : this.testingTypeValue
            })
            .then(result => {
               // console.log('Data kya hai' + JSON.stringify(result));
                if (result == null) {

                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'warning',
                        message: 'No record present for the provided range of dates.Please select the correct range',
                    }));
                    return;

                }
                // this.data = result;
                result.forEach(bl => {
                    let temparrBulk = {};
                  /*  temparrBulk.reportingFacility = bl.testingSiteName;
                    temparrBulk.Id = bl.Id;
                    temparrBulk.patientId = bl.patientId;
                    temparrBulk.Name = bl.firstName +' ' +bl.middleName + '' +bl.lastname;
                    temparrBulk.dateOfBirth = bl.dateOfBirth;
                    temparrBulk.address = bl.patientAddress;
                    temparrBulk.state = bl.patientState;
                    temparrBulk.phone = bl.patientPhone;
                    temparrBulk.TestNumber = bl.TestNumber;
                    temparrBulk.orderProvidingOrFacility = bl.orderingFacilty;
                    temparrBulk.dateOfSpecimenCollection = bl.dateOfSpecimenCollection;
                    temparrBulk.sourceOfSpecimen = bl.sourceOfSpecimen;
                    temparrBulk.testPerformedDescription = bl.testPerformedDescription;
                    temparrBulk.testResultDescription = bl.testResultDescription;
                    temparrBulk.result = bl.result;
                    temparrBulk.dateOfLabTestCompletion = bl.dateOfLabTestCompletion;
                    temparrBulk.testingLabName = bl.testingLabName;
                    temparrBulk.organizationName = bl.organizationName;
                    temparrBulk.gender = bl.gender;
                    temparrBulk.barCoderNumber = bl.barCoderNumber;
                    temparrBulk.containerBarCode = bl.containerBarCode;
                    temparrBulk.batchId = bl.batchId; 
                    temparrBulk.Id = bl.Id; */
                    temparrBulk.TestNumber = bl.TestNumber;
                    temparrBulk.patientId = bl.patientId;
                    temparrBulk.Name = bl.firstName +' ' +bl.lastname;
                    temparrBulk.phone = bl.phone;
                    temparrBulk.dateOfBirth = bl.dateOfBirth;
                    temparrBulk.appointmentStatus = bl.appointmentStatus;
                    temparrBulk.appointmentDate = bl.appointmentDate;
                    temparrBulk.appointmentTimes = bl.appointmentTimes;
                    temparrBulk.testingLabName = bl.testingLabName;
                    temparrBulk.result = bl.result;
                    temparrBulk.appointmentCompleteDate = bl.appointmentCompleteDate;
                    temparrBulk.dateOfLabTestCompletion = bl.dateOfLabTestCompletion;
                    this.dataBulk.push(temparrBulk);
                })
                if (this.dataBulk) {
                   //console.log('this.data: ', JSON.stringify(this.dataBulk));
                    this.downloadCSVFileBulk();
                }
            })
            .catch(error => {
                this.error = error;
                this.dataBulk = undefined;
            });
    }

    // create csv file
    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        });
     
        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);
        
        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;
       
        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                   
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }
       
        // Creating anchor element to download
        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'ATData.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    downloadCSVFileBulk() {
       // console.log('In downloadsa');
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = new Set();

        // getting keys from data
       this.dataBulk.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
                rowData.add(key);
            });
        }); 
       
        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);
        //rowData = ["TESTING ID","PATIENT ID","PATIENT NAME","Phone No","DATE OF BIRTH","Appointment Status","Appointment Date","Appointment Time","LAB NAME","Result","Appointment Completion Date","Lab Test Completion Date"];
        
        // splitting using ','
        //csvString += rowData.join(',');
        csvString += ["Testing Id,Patient Id,Patient Name,Phone No,Date of Birth,Appointment Status,Appointment Date,Appointment Time,Lab Name,Result,Appointment Completion Date,Lab Test Completion Date"];
        csvString += rowEnd;
        
        // main for loop to get the data based on key value
        for (let i = 0; i < this.dataBulk.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.dataBulk[i][rowKey] === undefined ? '' : this.dataBulk[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }
        
        // Creating anchor element to download
        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'Result.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    isValid() {
        let valid = true;
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        let isAllValid1 = [
            ...this.template.querySelectorAll("lightning-combobox")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        if(isAllValid1 && isAllValid){
            valid = true;
        }else{
            valid = false;
        }
        //valid = isAllValid;
        return valid;
    }
}