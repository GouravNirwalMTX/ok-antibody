import { LightningElement,api,track } from 'lwc';
import getLabRecords from '@salesforce/apex/DC_ScheduleAppointment.retrieveLab';
import closeAppointment from '@salesforce/apex/DC_ScheduleAppointment.closeAppointment';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
export default class OkvriuAssignBatchModal extends LightningElement {
    @track modaltype = 'complete';
    @track barcodeId = '';
    @track batchId = '';
    @api 
    get type() {
        return this.modaltype;
    }
    set type(val){
        console.log('val',val);
        this.modaltype = val;
    }
    @api appointmentId;
   
    @track labId = '';
    @track showSpinner = false;
    @track laboptions =[];
    _heading;
    _appointmentList = [];

    get isComplte() {
        return this.modaltype == 'complete';
    }

    @api
    get heading() {
        return this._heading;
    }
    set heading(val){
        this._heading = val;
    }

    @api
    get appointmentList() {
        return this._appointmentList;
    }
    set appointmentList(val){
        val.forEach(el =>{
            // this._appointmentList.push(el);
            this._appointmentList = [...this._appointmentList, el];
        })
        return this._appointmentList;
    }

    connectedCallback() {
        console.log('appointment list: ', this.appointmentList);
        getLabRecords().then(result => {
            this.laboptions = result;
        }).catch(error => { console.log('Error',error)});
    }

    handleChange(event) {
        this.labId = event.detail.value;
    }
    handlebarcodechange(event) {
        this.barcodeId = event.detail.value;
       
    }

    closeAppointmentandcreateBatch(event) {
        var buttonName = event.target.name;
        if(this.appointmentList.length < 1 ){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select atleast one appointment.',
            }));
            return;
        }
        if(this.labId == '' || typeof this.labId == 'undefined'){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a lab.',
            }));
            return;
        }

        closeAppointment({appointmentIdList: this.appointmentList, labId: this.labId,barcodeId: this.barcodeId})
        .then(result =>{
            if(result){
                console.log('result--'+ result)
                this.batchId = result;
                console.log('batchId'+ this.batchId);
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Successfully completed.',
                }));
                this.dispatchEvent(new CustomEvent('closebatch', {
                    detail: 'save'
                }));
                console.log('Out The other WIndow')
                console.log('eventname',event.target.name);
                if(buttonName == 'ScanBarcode'){
                    console.log('Inside The other WIndow')
                    window.location.href = '/TestingSite/BarCodeLiveStream?id='+this.batchId;
                } 
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Some error occoured.',
                }));
            }
        }).catch(error =>{ 
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        });
    }
  /*  createBatchBarcode(event) {
      //  var buttonName = event.target.name;
        if(this.appointmentList.length < 1 ){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select atleast one appointment.',
            }));
            return;
        }
        if(this.labId == '' || typeof this.labId == 'undefined'){
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a lab.',
            }));
            return;
        }

        createbatchBarcode({labId: this.labId})
        .then(result =>{
            if(result){
                console.log('result--'+ result)
                this.batchId = result;
                console.log('batchId'+ this.batchId);
                this.dispatchEvent(new CustomEvent('closebatch', {
                    detail: 'save'
                }));
                console.log('Out The other WIndow')
                console.log('eventname',event.target.name);
                window.location.href = '/TestingSite/BarCodeLiveStream?id='+this.batchId;
                
            }else{
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Some error occoured.',
                }));
            }
        }).catch(error =>{ 
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        });
    } */
       /*  completeAppointmentWithBarcode(){
        
        window.location.href = '/TestingSite/BarCodeLiveStream?id='+this.appointmentId;
}*/
    handleCloseModal() {
        // console.log('close: ', this.appointmentList);
        //this.dispatchEvent(new CustomEvent('closebatch'));
        this.dispatchEvent(new CustomEvent('closebatch', {
            detail: 'close'
        }));
    }
}