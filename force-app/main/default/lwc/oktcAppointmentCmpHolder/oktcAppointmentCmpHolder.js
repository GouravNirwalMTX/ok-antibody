import { LightningElement, track } from 'lwc';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetails';
import formFactorPropertyName from '@salesforce/client/formFactor';
import getBarCode from '@salesforce/apex/OKCP_ViewBarcodeController.getBarCode';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class OktcAppointmentCmpHolder extends LightningElement {
    /*get isMobile() {
        return formFactorPropertyName != 'Large';
    }*/

    //Scheduled Tab - Covid
    @track columns = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Date',name:'Appointment_Start_Date_v1__c',type:'text',action:'',class:''},
        {label:'Time',name:'Appointment_Start_Time_v1__c',type:'time',action:'',class:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Re-Schedule',class:'paginator-button'},{label:'In Progress',class:'paginator-button'},{label:'Cancel',class:'paginator-button'}]}
    ];

    //Scheduled Tab - Anitbody
    @track columnsAntibody = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Date',name:'Appointment_Start_Date_v1__c',type:'text',action:'',class:''},
        {label:'Time',name:'Appointment_Start_Time_v1__c',type:'time',action:'',class:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Re-Schedule',class:'paginator-button'},{label:'Complete',class:'paginator-button'},{label:'Cancel',class:'paginator-button'}]}
    ];
    @track whereClause;
    @track showPaginator = false; 

    @track unscheduledColumns = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Preferred Date',name:'Patient__r.Formatted_preferred_Date__c',type:'text',action:''},
        {label:'Preferred Time',name:'Patient__r.Preferred_Time__c',type:'time',action:''},
        {label:'Action',name:'',type:'button',action:'Schedule',varient:'brand',buttonlabel:'Schedule'}
    ];
    @track whereClause2;
    @track showPaginator2 = false;

   /* Not in use anymore 
   @track columns3 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Preferred Date',name:'Patient__r.Formatted_preferred_Date__c',type:'text',action:''},
        {label:'Preferred Time',name:'Patient__r.Preferred_Time__c',type:'time',action:''},
        {label:'Action',name:'',type:'button',action:'Schedule',varient:'brand',buttonlabel:'Schedule'}
    ];*/
    /*@track columns4 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Completed Date',name:'Formatted_Complete_Date__c',type:'text',action:''},
        {label:'Result',name:'Results__c',type:'child',action:''}
    ];
    //In Progress - antibody Tab
    @track columns5 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Sample Collected By',name:'Sample_Collected_By__c',type:'text',action:''},
        {label:'Source of Specimen',name:'Source__c',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Complete',class:'paginator-button'}]}
    ];
    //In Progress - antibody Tab
    @track columns5Antibody = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Complete',class:'paginator-button'}]}
    ];*/
    //Cancelled Tab
    @track cancelledColumns = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Cancellation Date',name:'Formatted_Appointment_Cancellation_Date__c',type:'text',action:''},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Re-Schedule',class:'paginator-button'}]}
    ];
    @track whereClause4;
    @track whereClause3;
    @track whereClause5;
    @track whereClause6;
    @track showPaginator3 = false;
    
    @track resetTable = false;
    @track reloadTable = false;
    @track showCalendar = false;
    @track showConfirmation = false;
    @track appointmentId;
    @track accountId;
    @track modalType = '';
    @track isBulk = false;
    @track selectedAppointmentIds = [];
    @track showBatchModal =false;
    @track testType = [];
    @track selectedTestingType = '';

    //variables to denote the selected testing site type
    @track pcrTest = false;
    @track rapidTest = false;
    @track antibodyTest = false;

    @track eventData;
    @track showInProgressModal = false;
    @track defaultLab = '';
    @track data={} ;
    @track newAppointment = false;

    //variable to show if the type is available on the testing site
    @track isPcr = false;
    @track isRapid = false;
    @track isAnti = false;
    @track isFirstLoad = true;
    @track showCancelButton = false;
    @track showConfirmationOnReschedule = false;
    
    get type() {
        return this.modalType;
    }

    get completedColumns(){
        var col = [ 
            {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
            {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
            {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
            {label:'Completed Date',name:'Formatted_Complete_Date__c',type:'text',action:''}
        ];
        if(!this.antibodyTest){
            col.push({
                label:'Result',name:'Results__c',type:'child',action:''
            });
        }

        if(this.pcrTest){
            col.push(
                {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'View Barcode',class:'paginator-button'}]}
            );
        }

        if(this.rapidTest){
            col.push(
                {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Add Appointment',class:'paginator-button'}]}
            );
        }
        return col;
    }

    get inProgressColumns(){
        var col = [
            {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
            {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
            {label:'Mobile',name:'Patient__r.MobilePhone',type:'text',action:''},
            {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''}
        ];         

        if(!this.antibodyTest){
            col.push(
                {label:'Sample Collected By',name:'Sample_Collected_By__c',type:'text',action:''},
                {label:'Source of Specimen',name:'Source__c',type:'text',action:''}
            );
        }
        col.push(
            {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Complete',class:'paginator-button'}]}
        );
        return col;
    }

    connectedCallback() {
        this.isFirstLoad = true;
        this.getData();
    }

    getData(){
        this.showPaginator = false;
        getTestingSite({})
        .then(data => {
            if(data.ContactId) 
                this.accountId = data.Contact.AccountId;
            if(data.ContactId && data.Contact.AccountId) {
                if(this.isFirstLoad){
                    if(data.Contact.Account.Testing_Site_Type__c.includes(";")){
                        this.testType = data.Contact.Account.Testing_Site_Type__c.split(';');
                    }else{
                        this.testType.push(data.Contact.Account.Testing_Site_Type__c);                       
                    }
                    this.testType.forEach(element => {
                        if(element == 'Antibody Testing'){
                            this.isAnti = true;  
                        }else if(element == 'Rapid Testing'){
                            this.isRapid = true;
                        }else if(element == 'PCR Testing'){
                            this.isPcr = true;
                        }  
                        this.selectedTestingType = element;               
                    });
                    var index = this.testType.indexOf('Antibody Testing');
                    if(index > -1)
                    this.testType.splice(index, 1);
                    
                    if(this.selectedTestingType == 'PCR Testing')
                        this.pcrTest = true;
                    else if(this.selectedTestingType == 'Rapid Testing')
                        this.rapidTest = true;
                    else if(this.selectedTestingType == 'Antibody Testing')
                        this.antibodyTest = true;

                    if(!this.antibodyTest){
                        this.showConfirmationOnReschedule = true;
                    }
                }
                this.isFirstLoad = false;
                
                let commonWhereClause = ' AND Testing_Site_Type__c = \'' + this.selectedTestingType + '\' AND Testing_Site_Account__c = \'' + this.accountId + '\''
                this.whereClause = ' Status__c = \'Scheduled\'' + commonWhereClause;
                this.whereClause2 = ' Status__c = \'To Be Scheduled\'' + commonWhereClause;
                this.whereClause3 = ' Status__c = \'Eligible\'' + commonWhereClause;
                this.whereClause4 = ' Status__c = \'Completed\'' + commonWhereClause;
                this.whereClause5 = ' Status__c = \'In Progress\'' + commonWhereClause;
                this.whereClause6 = ' Status__c = \'Cancelled\'' + commonWhereClause;
            } else {
                this.whereClause = ' Id = null ';
                this.whereClause2 = ' Id = null ';
                this.whereClause3 = ' Id = null ';
                this.whereClause4 = ' Id = null ';
            }
            this.showPaginator = true;
        })
        .catch(error => {
            console.log('Error-->' + error);
        });
    }

    handlePaginatorAction(event) {
        let data = event.detail;
      //  this.selectedAppointmentIds = [];
        if(data.actionName == 'Re-Schedule' || data.actionName == 'Schedule') {
            if(data.showConfirmation){
                this.data = data;
                this.showConfirmation = true;
                this.modalType = 'reschedule';
                this.showCalendar = false;
                
            }else{
                this.showCalendar = true;
            }
            this.appointmentId = data.recordId;
            
        } else if(data.actionName == 'Complete'){
            
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'complete';
        } else if(data.actionName == 'Cancel') {
            this.isBulk = false;
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'cancel';
        }else if(data.actionName == 'In Progress'){
            this.appointmentId = data.recordId;
            this.showInProgressModal = true;
          //  this.modalType = 'inProgress';
        } else if(data.actionName == 'View Barcode'){
            this.appointmentId = data.recordId;
            getBarCode({appointmentId : this.appointmentId})
            .then(data => {
                if(data){
                    var url = window.location.href;
                    url = url.substring( 0, url.indexOf( "/s/" ) );
                    url += '/apex/ViewBarcode?id='+this.appointmentId;
                    window.open(url,'_blank');
                }else{
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'warning',
                        message: 'Barcode Id not available',
                    }));
                }
            }).catch(error => {
                console.log('Error-->' + JSON.stringify(error));
            });
            
        } else if(data.actionName == 'Add Appointment'){
            this.showConfirmation = true;
            this.appointmentId = data.recordId;
            this.modalType = 'addAppointment';
        }
    }
    hideModal(event) {
        this.showCalendar = false;
        this.showConfirmation = false;
        this.showInProgressModal = false;
        this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
            element.retrieveRecords();
        });
    }

    handleSwitchTabs(event){
        this.hideModal(event);
        this.pcrTest = false;
        this.rapidTest = false;
        this.antibodyTest = false;
        if( this.selectedTestingType == 'PCR Testing'){           
            this.pcrTest = true;
        }else if( this.selectedTestingType == 'Rapid Testing'){           
            this.rapidTest = true;
        }
        this.template.querySelector('lightning-tabset').activeTabValue = 'scheduled';
    }

    handleTypeSelection(event){
        this.pcrTest = false;
        this.rapidTest = false;
        this.antibodyTest = false;
        this.selectedTestingType = event.target.label;
        if( event.target.label == 'PCR Testing'){           
            this.pcrTest = true;
        }else if( event.target.label == 'Rapid Testing'){           
            this.rapidTest = true;
        }if( event.target.label == 'Antibody Testing'){           
            this.antibodyTest = true;
        }
        if(!this.antibodyTest){
            this.showConfirmationOnReschedule = true;
        }else{
            this.showConfirmationOnReschedule = false;
        }
        console.log('show confirm ', this.showConfirmationOnReschedule)
        this.getData();
    }
    resetFlag(event) {
        // this.resetTable = event.detail;
    }

    getSelectedIds(event){
       
        let temparr= [];
        this.selectedAppointmentIds = [];
        temparr = [...event.detail.selectedIds]
        this.selectedAppointmentIds = [...this.selectedAppointmentIds, ...temparr];
       
        if(event.detail.buttonLabel == 'Assign'){
            this.showBatchModal =true;
        } else if(event.detail.buttonLabel == 'Cancel Appointments'){
            if(this.selectedAppointmentIds < 1){
                this.showError();
                return;
            }
            this.isBulk = true;
            this.showConfirmation = true;
            this.modalType = 'cancel';
            
        }   
        
    }
    showError(){
      
        this.dispatchEvent(new ShowToastEvent({
            title: '',
            variant: 'error',
            message: 'Please select atleast one appointment.',
        }));
        
    }

    hideBatchModal(event){
        this.showBatchModal =false;
        if(event.detail == 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        }
    }

    addAppointment(event){
        this.showConfirmation = false;
        this.newAppointment = true;
        this.selectedTestingType = event.detail;
        this.showCalendar = true;
    }
}