import {
  LightningElement,
  track,
  api
} from "lwc";
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getContactDetails";
import saveInsuranceDetails from "@salesforce/apex/okWrapperClass.saveInsuranceDetails";
import getInsuranceDetails from "@salesforce/apex/okWrapperClass.getInsuranceDetails";
import setDefaultPrimaryInsuranceCompany from "@salesforce/apex/okLookUpController.setDefaultPrimaryInsuranceCompany";

export default class OkcpInsuranceInfo extends LightningElement {
  @track dataObj = {};
  @track insuranceType = [];
  @track medicareTypeVal = [];
  @track haveMediPlanOpt = [];
  @track companyNameValues = [];
  @track relationToPlaceHolderValues = [];
  @track isRequired = false;
  @track conditionallyRequired = false;
  @track medicInfo1 = false;
  @track medicInfo2 = false;
  @track showOtherInfo = false;
  @track memberIdLable = "Member Id";
  @track memberIdLengthIFMedicade = '30';
  @track contactDetais = {};
  @track insuranceId = "";
  @track dynamicRegex = "^[a-zA-Z0-9]+$";
  @track groupNumberRequired = false;
  @track selectedCompanyName = {};
  @track defaultInsuranceCompanyRecords;
  @track isMedicare = false;
  @track isMedicAid = false;
  @track showUnknown = false;
  @track showError = false;
  @track error = "Please select a company";
  @api docs = [];
  @track docUploaded = [];
  @track showRemove = false;

  @api isDependent = false;
  @api currentStep;
  @api contactId;
  //   @track contactData = {};

  connectedCallback() {
    console.log("contactId>>Insurance", this.contactId);
    setDefaultPrimaryInsuranceCompany()
      .then((result) => {
        this.defaultInsuranceCompanyRecords = result;
        // if (result) {
        //   for (let key in result) {
        //     if (result.hasOwnProperty(key)) {
        //       this.insuranceCompanyMap.push({ value: result[key], key: key });
        //     }
        //   }
        // }
        console.log("insuranceCompanyMap:" + JSON.stringify(result));
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });

    if (this.docs != undefined || this.docs != null)
      this.docUploaded = JSON.parse(JSON.stringify(this.docs));
    console.log(">>>>>Insuracne>>", this.docUploaded);
    this.getContact();
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Insurance_Type__c"
    }).then((result) => {
      this.insuranceType = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Select_the_Medicare_type__c"
    }).then((result) => {
      this.medicareTypeVal = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Primary_Insurance_Company_Name__c"
    }).then((result) => {
      this.companyNameValues = result;
    });
    fetchPicklist({
      objectName: "Insurance__c",
      fieldName: "Relation_to_PolicyHolder__c"
    }).then((result) => {
      this.relationToPlaceHolderValues = result;
    });
    fetchPicklist({
        objectName: "Insurance__c",
        fieldName: "Do_you_have_a_Medicare_Supplement_plan__c"
      })
      .then((result) => {
        this.haveMediPlanOpt = result;
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });

    this.getInsurance();
  }

  handleCompanyNameSelection(event) {
    console.log("selectedEvent>>" + JSON.stringify(event.detail));
    console.log(
      "selectedEvent>>" + JSON.stringify(event.detail.memberIdValidationRegex)
    );
    this.selectedCompanyName = event.detail;
    this.dataObj.primaryInsuranceCompanyName = this.selectedCompanyName.id;
    if ((this.selectedCompanyName.name).includes('OTHER - NOT LISTED')) {
      this.showUnknown = true;
    }
    this.groupNumberRequired = this.selectedCompanyName.isGroupNumberRequired;
    if (this.selectedCompanyName.memberIdValidationRegex != undefined) {
      this.dynamicRegex = this.selectedCompanyName.memberIdValidationRegex;
    }
  }

  handleCompanyNameRemoval() {
    this.selectedCompanyName = {};
    this.dataObj.primaryInsuranceCompanyName = null;
    this.dataObj.otherPrimaryInsuranceCompanyName = null;
    this.groupNumberRequired = false;
    this.showUnknown = false;
    this.dynamicRegex = "^[a-zA-Z0-9]+$";
  }
  getContact() {
    console.log("In contactinfor>>", this.contactId);
    console.log("In contactinfor>> is dependent", this.isDependent);
    this.showSpinner = true;
    getContactDetails({
        contactId: this.contactId
      })
      .then((result) => {
        this.contactDetais = result;
        console.log(
          "insuranceContactDetails>>>",
          JSON.stringify(this.contactDetais)
        );
        // console.log('data=>', JSON.stringify(result));
        //   this.contactData.firstName = result.firstName;
        //  this.contactData.middleName = result.middleName;
        //   this.contactData.lastName = result.lastName;
        this.dataObj.firstName = this.contactDetais.firstName;
        this.dataObj.middleName = this.contactDetais.middleName;
        this.dataObj.lastName = this.contactDetais.lastName;
        this.dataObj.contactId = this.contactId;

        console.log("insuranceContact>>>", JSON.stringify(this.dataObj));
        this.showSpinner = false;
      })
      .catch((error) => {
        console.log("error=>", JSON.stringify(error));
        this.showSpinner = false;
      });
  }

  getInsurance() {
    console.log("In contactinfor>>GetDetails", this.contactId);
    this.showSpinner = true;
    getInsuranceDetails({
        contactId: this.contactId
      })
      .then((result) => {
        console.log(
          "insuranceContact>>>ResultApex",
          JSON.parse(JSON.stringify(result))
        );
        console.log("insuranceContact>>>ResultApex", result.length);
        if (result.length > 0) {
          let resultdata = result[0];
          this.insuranceId = resultdata.insuranceId;
          this.dataObj.insuranceId = resultdata.insuranceId;
          this.dataObj.insuranceType = resultdata.insuranceType;
          this.dataObj.medicareType = resultdata.medicareType;
          this.dataObj.medicareSupplementPlan =
            resultdata.medicareSupplementPlan;
          this.dataObj.primaryInsuranceCompanyName = resultdata.primaryInsuranceCompanyName;
          this.dataObj.otherPrimaryInsuranceCompanyName =
            resultdata.otherPrimaryInsuranceCompanyName;
          this.dataObj.memberId = resultdata.memberId;
          this.dataObj.groupNumber = resultdata.groupNumber;
          this.dataObj.groupName = resultdata.groupName;
          this.dataObj.relationToPolicyHolder =
            resultdata.relationToPolicyHolder;
          this.dataObj.policyholderLastName = resultdata.policyholderLastName;
          this.dataObj.policyholderFirstName = resultdata.policyholderFirstName;
          this.dataObj.policyholderMiddleName =
            resultdata.policyholderMiddleName;
          this.dataObj.otherPrimaryInsuranceCompanyName =
            resultdata.otherPrimaryInsuranceCompanyName;
          if (
            resultdata.primaryInsuranceCompanyLabel != null || resultdata.primaryInsuranceCompanyLabel != undefined || resultdata.primaryInsuranceCompanyLabel != "") {
            this.selectedCompanyName.id = resultdata.primaryInsuranceCompanyName;
            this.selectedCompanyName.name = resultdata.primaryInsuranceCompanyLabel;
            this.selectedCompanyName.isGroupNumberRequired = resultdata.isGroupNumberRequired;
            if (resultdata.memberIdValidationRegex != undefined) {
              this.dynamicRegex = resultdata.memberIdValidationRegex;
            }
            // this.selectedCompanyName.memberIdValidationRegex = resultdata.memberIdValidationRegex;
            console.log("getSelectedId >>> ", this.selectedCompanyName);
          }

          if (this.dataObj.insuranceType == "Medicare") {
            this.memberIdLengthIFMedicade = '30';
            this.isMedicare = true;
            this.isMedicAid = false;
            this.medicInfo1 = true;
            this.medicInfo2 = true;
            this.showOtherInfo = true;
            this.isRequired = true;
            this.conditionallyRequired = true;
          } else if (this.dataObj.insuranceType == "No Insurance") {
            this.memberIdLengthIFMedicade = '30';
            this.medicInfo1 = false;
            this.medicInfo2 = false;
            this.showOtherInfo = false;
            this.isRequired = false;
            this.conditionallyRequired = false;
          } else if (this.dataObj.insuranceType == "Private Insurance") {
            this.memberIdLengthIFMedicade = '30';
            this.isMedicare = false;
            this.isMedicAid = false;
            this.medicInfo1 = false;
            this.medicInfo2 = false;
            this.showOtherInfo = true;
            this.isRequired = true;
            this.conditionallyRequired = false;
          } else if (this.dataObj.insuranceType == "Medicaid") {
            this.memberIdLengthIFMedicade = '9';
            this.isMedicare = false;
            this.isMedicAid = true;
            this.medicInfo1 = false;
            this.medicInfo2 = false;
            this.showOtherInfo = true;
            this.isRequired = true;
            this.conditionallyRequired = false;
          }
          if (this.dataObj.medicareType == "Part A") {
            // this.medicInfo1 = false;
            //   this.dataObj ={};
            this.dataObj.insuranceType = "Medicare";
            this.dataObj.medicareType = "Part A";
            this.medicInfo2 = false;
            this.showOtherInfo = false;
            this.isRequired = false;
            this.conditionallyRequired = false;
          } else if (this.dataObj.medicareType == "Medicare Advantage Plan") {
            this.medicInfo2 = false;
            this.dataObj.medicareSupplementPlan = null;
            this.memberIdLable = "Member Id";
          }
          if (this.dataObj.medicareSupplementPlan === "Yes") {
            this.memberIdLable = "Member Id";
          } else if (this.dataObj.medicareSupplementPlan === "No") {
            this.memberIdLable = "Medicare Number";
          }

        }

        console.log("insuranceContact>>>", JSON.stringify(this.dataObj));
        this.showSpinner = false;
      })
      .catch((error) => {
        console.log("error=>", JSON.stringify(error));
        this.showSpinner = false;
      });
  }

  handleInputChange(event) {
    this.dataObj[event.target.name] = event.target.value;
    if (
      event.target.name == "insuranceType" &&
      event.target.value == "No Insurance"
    ) {
      // this.dataObj.medicareType = null;
      this.dataObj = {};
      this.dataObj.insuranceId = this.insuranceId;
      this.dataObj.firstName = this.contactDetais.firstName;
      this.dataObj.middleName = this.contactDetais.middleName;
      this.dataObj.lastName = this.contactDetais.lastName;
      this.dataObj.insuranceType = event.target.value;
      this.dataObj.contactId = this.contactId;
    } else if (
      event.target.name == "insuranceType" &&
      event.target.value != "Medicare"
    ) {
      this.dataObj.medicareType = null;
      this.dataObj.medicareSupplementPlan = null;
    }
    if (
      event.target.name === "insuranceType" ||
      event.target.name === "medicareType" ||
      event.target.name === "medicareSupplementPlan"
    ) {
      this.selectedCompanyName = undefined;
      this.dataObj.primaryInsuranceCompanyName = null;
      this.isGroupNumberRequired = false;
      this.dynamicRegex = "^[a-zA-Z0-9]+$";
      this.showRemove = false;
    }

    if (this.dataObj.insuranceType == "Medicare") {
      this.memberIdLengthIFMedicade = '30';
      //  this.selectedCompanyName = undefined;
      // this.dataObj.primaryInsuranceCompanyName = null;
      this.isMedicare = true;
      this.isMedicAid = false;
      this.medicInfo1 = true;
      this.medicInfo2 = true;
      this.showOtherInfo = true;
      this.isRequired = true;
      this.conditionallyRequired = true;
    } else if (this.dataObj.insuranceType == "No Insurance") {
      this.memberIdLengthIFMedicade = '30';
      // this.selectedCompanyName = undefined;
      // this.dataObj.primaryInsuranceCompanyName = null;
      this.medicInfo1 = false;
      this.medicInfo2 = false;
      this.showOtherInfo = false;
      this.isRequired = false;
      this.conditionallyRequired = false;
    } else if (this.dataObj.insuranceType == "Private Insurance") {
      this.memberIdLengthIFMedicade = '30';
      // this.selectedCompanyName = undefined;
      //this.dataObj.primaryInsuranceCompanyName = null;
      this.isMedicare = false;
      this.isMedicAid = false;
      this.medicInfo1 = false;
      this.medicInfo2 = false;
      this.showOtherInfo = true;
      this.isRequired = true;
      this.conditionallyRequired = false;
    } else if (this.dataObj.insuranceType == "Medicaid") {
      this.memberIdLengthIFMedicade = '9';
      this.showRemove = true;
      this.selectedCompanyName = JSON.parse(JSON.stringify(this.defaultInsuranceCompanyRecords.find(
        (record) => record.isMedicaid === true
      )));
      console.log('this.selectedName >>>>>', this.selectedCompanyName)
      this.dataObj.primaryInsuranceCompanyName = this.selectedCompanyName.id;
      if (this.selectedCompanyName.memberIdValidationRegex !== undefined)
        this.dynamicRegex = this.selectedCompanyName.memberIdValidationRegex;
      this.groupNumberRequired = this.selectedCompanyName.isGroupNumberRequired;
      if (this.selectedCompanyName) {
        this.memberIdLable = "Member Id";
        this.isMedicare = false;
        this.isMedicAid = true;
        this.medicInfo1 = false;
        this.medicInfo2 = false;
        this.showOtherInfo = true;
        this.isRequired = true;
        this.conditionallyRequired = false;
      }


    }
    if (this.dataObj.medicareType == "Part A") {
      // this.medicInfo1 = false;
      this.dataObj = {};
      //  this.selectedCompanyName = undefined;
      this.dataObj.insuranceId = this.insuranceId;
      this.dataObj.firstName = this.contactDetais.firstName;
      this.dataObj.middleName = this.contactDetais.middleName;
      this.dataObj.lastName = this.contactDetais.lastName;
      this.dataObj.contactId = this.contactId;
      this.dataObj.insuranceType = "Medicare";
      this.dataObj.medicareType = "Part A";
      this.medicInfo2 = false;
      this.showOtherInfo = false;
      this.isRequired = false;
      this.conditionallyRequired = false;
    } else if (this.dataObj.medicareType == "Medicare Advantage Plan") {
      this.medicInfo2 = false;
      this.dataObj.medicareSupplementPlan = null;
      this.memberIdLable = "Member Id";
    }
    if (this.dataObj.medicareSupplementPlan === "Yes") {
      this.memberIdLable = "Member Id";
    } else if (this.dataObj.medicareSupplementPlan === "No") {
      this.memberIdLable = "Medicare Number";
      this.showRemove = true;
      this.selectedCompanyName = JSON.parse(JSON.stringify(this.defaultInsuranceCompanyRecords.find(
        (record) => record.isMedicare === true
      )));
      this.dataObj.primaryInsuranceCompanyName = this.selectedCompanyName.id;
      if (this.selectedCompanyName.memberIdValidationRegex !== undefined)
        this.dynamicRegex = this.selectedCompanyName.memberIdValidationRegex;
      this.groupNumberRequired = this.selectedCompanyName.isGroupNumberRequired;
    }

    if (event.target.name === "relationToPolicyHolder") {
      if (this.dataObj.relationToPolicyHolder === "Self = 1") {
        this.dataObj.policyholderLastName = this.dataObj.lastName;
        this.dataObj.policyholderFirstName = this.dataObj.firstName;
        this.dataObj.policyholderMiddleName = this.dataObj.middleName;
      } else {
        this.dataObj.policyholderLastName = "";
        this.dataObj.policyholderFirstName = "";
        this.dataObj.policyholderMiddleName = "";
      }
    }
    if (this.dataObj.insuranceType == 'Medicare' && this.dataObj.medicareType == 'Part A and B' && this.dataObj.medicareSupplementPlan === "Yes") {
      this.isMedicAid = false;
      this.isMedicare = false;
    }
    if (this.dataObj.insuranceType == 'Medicare' && this.dataObj.medicareType == 'Medicare Advantage Plan') {
      this.isMedicAid = false;
      this.isMedicare = false;
    }

    console.log("dataObj>>>", JSON.stringify(this.dataObj));
  }

  isValid() {
    let valid = true;
    /* let ifParentEmpty = false;
        if(this.dataObj.insuranceType != 'No Insurance = 0' || this.dataObj.medicareType == 'Part A' ){
            ifParentEmpty = this.dataObj.primaryInsuranceCompanyName == undefined || this.dataObj.primaryInsuranceCompanyName == null ? true : false;
        if(ifParentEmpty){
            this.showError = true;
        }else{
            this.showError = false;
        }
        } */
    let isAllValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    let isAllValid1 = [
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    if (isAllValid && isAllValid1) {
      valid = true;
    } else {
      valid = false;
    }
    // valid = isAllValid;
    return valid;
  }

  @api
  saveInsurance() {
    console.log("BEFOR SAVEinsuranceContact>>>", JSON.stringify(this.dataObj));
    if (!this.isValid()) {
      return;
    }
    if (
      this.dataObj.insuranceType != "No Insurance" &&
      this.dataObj.medicareType != "Part A"
    ) {
      if (
        this.dataObj.primaryInsuranceCompanyName == undefined ||
        this.dataObj.primaryInsuranceCompanyName == null
      ) {
        let tempObj = {};
        tempObj.message = "Please Provide the Company Name";
        tempObj.status = "error";
        this.sendEventToParent(tempObj);
        return;
      }
    }
    saveInsuranceDetails({
        JsonData: JSON.stringify(this.dataObj),
        currentstep: this.currentStep
      })
      .then((result) => {
        console.log("Successfully Saved", result);
        console.log("ContactId>>", this.contactId);
        this.disableInputs = true;
        // this.extraWrapper = {...this.dataObj};
        let tempObj = {};
        tempObj.message = "saved successfully";
        tempObj.status = "success";
        this.sendEventToParent(tempObj);
      })
      .catch((error) => {
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }

  handleUploadFinished(event) {
    // var document = {};
    var fileDets = event.detail.files;
    //   var index = fileDets.name.lastIndexOf(".");
    //  var type = fileDets.name.substring(index+1, fileDets.name.length);
    console.log("hi  file>>>" + JSON.stringify(fileDets));
    console.log("hi  file>>>EVENT" + JSON.stringify(event.detail.files));

    fileDets.forEach((fileDocc) => {
      this.docUploaded.push({
        name: fileDocc.name
      });
    });

    const handleUpload = new CustomEvent("upload", {
      detail: this.docUploaded
    });
    // Fire the custom event
    this.dispatchEvent(handleUpload);
    // document.name = fileDets.name;
    // document.type = type;
    //  document.id = fileDets.documentId ;
    // this.docs.push(document);
    // Get the list of uploaded files
    //  console.log('hi '+JSON.stringify(fileDets));
  }

  sendEventToParent(obj) {
    const selectedEvent = new CustomEvent("insurancesave", {
      detail: obj
    });
    this.dispatchEvent(selectedEvent);
  }

  changeMemberIdToUppercase(event) {
    console.log("here in ");
    const valToChange = event.target.value;
    this.dataObj.memberId = valToChange.toUpperCase();
    console.log(this.dataObj.memberId);
  }
}