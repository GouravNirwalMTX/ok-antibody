import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    NavigationMixin
} from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/OK_SelfRegistration.saveUser';
import saveRegisterContact from '@salesforce/apex/OK_SelfRegistration.saveRegisterContact';
import contactType from '@salesforce/label/c.OK_Registration_Type_VR_Citizen_Consultation';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
// import { data } from 'okpcContactInfo/data';

export default class OkvrcpSelfRegistration extends NavigationMixin(LightningElement) {

    // label = {
    //     contactType
    // }

    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track emailOpt = true;
    @track smsOpt = true;
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '';
    @track portal = [{
        label: 'Citizen (COVID)',
        value: "COVID"
    }, {
        label: 'Citizen (Antibody)',
        value: 'Antibody'
    }];

    @track account;
    @track contactType;
    @track accountId;

    customImage = nysdohResource;

    firstNameChange(event) {
        this.firstName = event.target.value;
        console.log('firstName=> ', this.firstName);
    }
    lastNameChange(event) {
        this.lastName = event.target.value;
        console.log('lastName=> ', this.lastName);
    }
    phoneChange(event) {
        this.phone = event.target.value;
        console.log('phone=> ', this.phone);
    }
    emailChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }
    emailOptChange(event) {
        this.emailOpt = this.template.querySelector('[data-field="email"]').checked;
        console.log('emailOpt=> ', this.emailOpt);
    }
    smsOptChange(event) {
        this.smsOpt = this.template.querySelector('[data-field="sms"]').checked;
        console.log('smsOpt=> ', this.smsOpt);
        console.log('smsOpt value=> ', event.target.value);
    }
    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    connectedCallback() {
        /* getAccount()
         .then(result => {
             console.log('result'+JSON.stringify(result));
             this.accounts = result;
             console.log('acc'+this.accounts )
         })
         .catch((error) => {
             let message = error.message || error.body.message;
             console.log(message);
         }); */

         var sessionKey = sessionStorage.getItem('accId');
         if(sessionKey)
             this.accountId = sessionKey;
          
        getBusinessTimeOnAccount({selectedAccountId : this.accountId})
            .then(result => {
                this.account = result;
                console.log('self account: ', JSON.stringify(this.account));
                if (result.Type_of_Contact__c === 'Consultation') {
                    this.contactType = 'Citizen_Consultation_Vital_Records';
                    this.delimiter = '.vrcpuser';
                } else if (result.Type_of_Contact__c === 'Lobby') {
                    this.contactType = 'Citizen_Lobby_Vital_Records';
                    this.delimiter = '.vrlpuser';
                }
            })
            .catch(error => {
                console.log('error while fetching self account: ', JSON.stringify(error));

            });
    }

    doRegisteration() {
        if(!this.isValid){
            return;
        }
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        if (this.firstName == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if (this.lastName == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        // if ((this.phone == '' || typeof this.phone == 'undefined') && (this.email == '' || typeof this.email == 'undefined')) {
        //     this.errorMessage = 'Please provide Mobile Number or an Email';
        //     this.showError = true;
        //     return;
        // }
        if (this.email && !this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }

        if (!this.email && !this.phone) {
            saveRegisterContact({
                    firstName: this.firstName,
                    lastName: this.lastName,
                    type: this.contactType,
                    accountId: this.account.Id,
                    emailOpt: !this.emailOpt,
                    smsOpt: !this.smsOpt
                })
                .then(result => {
                    if (result.type == 'error') {
                        this.showError = true;
                        this.errorMessage = result.message;
                    } else if (result.type == 'success') {
                        this.showThankYou = true;
                        this.thankYouMessage = result.message;
                        console.log('navigateToForgotPassword ' + result.redirectURL);
                        //location.href = data.redirectURL;
                    }
                })
                .catch(error => {
                    this.showError = true;
                    console.log('Error-->' + JSON.stringify(error));
                    var msg = JSON.stringify(error.body.message);
                    // this.errorMessage = msg.substring(1, msg.length - 1);
                });
        } else {
            register({
                    firstName: this.firstName,
                    lastName: this.lastName,
                    phone: this.phone,
                    email: this.email,
                    type: this.contactType,
                    accountId: this.account.Id,
                    emailOpt: !this.emailOpt,
                    smsOpt: !this.smsOpt,
                    delimiter: this.delimiter
                }) //Added delimiter by Sajal
                .then(data => {
                    console.log('Data-->' + JSON.stringify(data));
                    if (data.type == 'error') {
                        this.showError = true;
                        this.errorMessage = data.message;
                    } else if (data.type == 'success') {
                        this.showThankYou = true;
                        this.thankYouMessage = data.message;
                        console.log('navigateToForgotPassword ' + data.redirectURL);
                        //location.href = data.redirectURL;
                    }

                })
                .catch(error => {
                    this.showError = true;
                    var msgArray = error.body.fieldErrors.Username;
                    let result = msgArray.map(({
                        statusCode
                    }) => statusCode);
                    let resultmessage = msgArray.map(({
                        message
                    }) => message);
                    // console.log('result >>>' +  JSON.stringify(result));
                    // console.log('error message>>'+ msg);
                    // console.log('Error >>>>-->' + JSON.stringify(msg.substring(1, msg.length - 1)));
                    // this.errorMessage = msg.substring(1, msg.length - 1);
                    if (result == 'DUPLICATE_USERNAME') {
                        this.errorMessage = 'User already exists';
                    } else {
                        this.errorMessage = resultmessage;
                    }
                });
        }
    }



    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    isValid() {
        let valid = true;
       let isAllValid = [
            ...this.template.querySelectorAll("input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        return valid = isAllValid;
    }
}