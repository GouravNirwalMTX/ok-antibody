import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import login from '@salesforce/apex/okpcLoginController.communityLogin';
import forgotPasswordURL from '@salesforce/label/c.OKCP_Forgot_Passsword';
import communityURL from '@salesforce/label/c.OKPC_Community_url';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
export default class Okpc_nysdohLoginPage extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';
    @track password = '';
    @track delimiter = '.vrcpuser';
    @track showSpinner = false;
    @api portalName = 'Vital Records Consultation Portal';
    customImage = nysdohResource;
    navigateToForgotPassword() {
        console.log('navigateToForgotPassword');
        location.href = forgotPasswordURL;
    }
    navigateToHome(data) {
        console.log('navigateToForgotPassword');
        location.href = data;
    }

    nameChange(event) {
        this.username = event.target.value;
    }
    passwordChange(event) {
        this.password = event.target.value;
    }

    doLogin() {
        console.log(this.username);
        console.log(this.password);
        this.showSpinner = true;
        login({ firstName : this.username, userInfo: this.password, delimiter:  this.delimiter})//Added delimiter by Sajal
            .then(data => {
                this.showSpinner = false;
                console.log('Success-->', data);
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Congratulations, your contact information has been verified.',
                    variant: 'success',
                    message: 'Success',
                }));
                this.navigateToHome(data);
            })
            .catch(error => {
                this.showSpinner = false;
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error.body.message));
                var msg = JSON.stringify(error.body.message);
                this.errorMessage = 'Please enter valid personal details for login.';
            });
    }

}