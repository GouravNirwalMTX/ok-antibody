import { LightningElement,track} from 'lwc';
import saveCase from '@salesforce/apex/okpc_saveCasetRecord.saveCaseRecord';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// importing Case fields

export default class OkpcSupportPage extends LightningElement {
    @track description = '';
    @track email = '';

    descriptionChange(event) {
        this.description = event.target.value;
        console.log('description=> ', this.description);
    }
    caseChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }

    customImage = nysdohResource;

    handleSave() {
        console.log('result'+this.description)
        console.log('result'+this.email)
        saveCase({description: this.description, email: this.email})
        .then(result => {
            // Clear the user enter values
            this.description = '';
            this.email = '';

            window.console.log('result ===> '+result);
            // Show success messsage
            this.dispatchEvent(new ShowToastEvent({
                title: 'Success!!',
                message: 'Case Created Successfully!!',
                variant: 'success'
            }),);
        })
        .catch(error => {
            this.error = error.message;
        });
    }
}