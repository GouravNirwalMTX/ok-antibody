import {
    LightningElement,
    track,
    api
} from 'lwc';
import savePriorExposure from "@salesforce/apex/OkpcContactInformationController.savePriorExposure";
import getPriorExposure from "@salesforce/apex/OkpcContactInformationController.getPriorExposure";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class OkpcPriorSymptoms extends LightningElement {
    @track dataObj = {};
    @track contactId;
    @track showSpinner = false;
    @track showLastDateOfContact = false; //defaults to false.
    @track testedForCovid = false;
    @api currentStep;
    @track todayDate;

    connectedCallback() {
        this.todayDate = this.getTodayDate();
        console.log('inside conn symptoms');
        this.getContact();
    }

    get yesNoOptions() {
        return [{
                label: "Yes",
                value: "Yes"
            },
            {
                label: "No",
                value: "No"
            },
            {
                label: "I do not know",
                value: "I do not know"
            },
        ];
    }

    handleInputChange(event) {
        if (event.target.name == 'testResult') {
            this.dataObj.testResult = event.target.value;
        } else if (event.target.name == 'dateYouWereTested') {
            this.dataObj.dateYouWereTested = event.target.value;
        } else if (event.target.name == 'lastDateOfContact') {
            this.dataObj.lastDateOfContact = event.target.value;
        } else if (event.target.name == 'inContactWithDiagnosedPerson') {
            this.dataObj.inContactWithDiagnosedPerson = event.target.value;
        } else if (event.target.name == 'inContactWithCovidPositive') {
            this.dataObj.inContactWithCovidPositive = event.target.value;
        } else if (event.target.name == 'hadNasalSwab') {
            this.dataObj.hadNasalSwab = event.target.value;
        }

        if (this.dataObj.hadNasalSwab == 'Yes') {
            this.testedForCovid = true;
        } else {
            this.testedForCovid = false;
            this.dataObj.dateYouWereTested = null;
            this.dataObj.testResult = null;
        }
        if (this.dataObj.inContactWithDiagnosedPerson == 'Yes') {
            this.showLastDateOfContact = true;
        } else {
            this.showLastDateOfContact = false;
            this.dataObj.lastDateOfContact = null;
        }
    }

    get options() {
        return [{
                label: 'Positive',
                value: 'Positive'
            },
            {
                label: 'Negative',
                value: 'Negative'
            },
            {
                label: 'Have not recieved',
                value: 'Have not recieved'
            },
            {
                label: 'I do not know',
                value: 'I do not know'
            },
        ];
    }

    getContact() {
        this.showSpinner = true;
        getPriorExposure()
            .then(result => {
                console.log('prior=>', JSON.stringify(result));
                this.dataObj.testResult = result.testResult;
                this.dataObj.dateYouWereTested = result.dateYouWereTested;
                this.dataObj.lastDateOfContact = result.lastDateOfContact;
                this.dataObj.inContactWithDiagnosedPerson = result.inContactWithDiagnosedPerson;
                this.dataObj.inContactWithCovidPositive = result.inContactWithCovidPositive;
                this.dataObj.hadNasalSwab = result.hadNasalSwab;
                this.contactId = result.contactId;
                this.dataObj.contactId = this.contactId;
                
                if (this.dataObj.hadNasalSwab == 'Yes') {
                    this.testedForCovid = true;
                } else {
                    this.testedForCovid = false;
                    this.dataObj.dateYouWereTested = null;
                    this.dataObj.testResult = null;
                }
                if (this.dataObj.inContactWithDiagnosedPerson == 'Yes') {
                    this.showLastDateOfContact = true;
                } else {
                    this.showLastDateOfContact = false;
                    this.dataObj.lastDateOfContact = null;
                }

                this.showSpinner = false;
            })
            .catch(error => {
                console.log('prior error=>', JSON.stringify(error));
                this.showSpinner = false;
            })
    }

    @api
    saveContacts() {

        if(!this.isValid()){
            let tempObj = {};
            tempObj.message = "Please Check the error on the page.";
            tempObj.status = "error";
            //this.sendEventToParent(tempObj);
            const selectedEvent = new CustomEvent("priorsymptomseditprofile", {
                detail: tempObj
            });
            this.dispatchEvent(selectedEvent);
            return;
        }

        if (this.dataObj.hadNasalSwab == '' || typeof this.dataObj.hadNasalSwab == 'undefined' || this.dataObj.inContactWithDiagnosedPerson == '' || typeof this.dataObj.inContactWithDiagnosedPerson == 'undefined'
        || this.dataObj.inContactWithCovidPositive == '' || typeof this.dataObj.hadNasalSwab == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return;
        }

        this.showSpinner = true;
        this.dataObj.contactId = this.contactId;
        savePriorExposure({
                jsonData: JSON.stringify(this.dataObj),
                currentstep: this.currentStep
            })
            .then(result => {
                console.log('Successfully Saved');
                // this.extraWrapper = {...this.dataObj};
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: 'Saved successfully',
                }));

                let tempObj = {};
                tempObj.message = "saved successfully";
                tempObj.status = "success";
                tempObj.currentStep = "3";
                this.sendEventToParent(tempObj);
                this.showSpinner = false;
            })
            .catch(error => {
                console.log('error in saving: ', JSON.stringify(error));
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'error',
                    message: 'Error',
                }));
                this.showSpinner = false;
            })
    }

    sendEventToParent(obj) {
        const selectedEvent = new CustomEvent("priorsymptoms", {
            detail: obj
        });
        this.dispatchEvent(selectedEvent);
    }

    isValid() {
        let valid = true;
       let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);

        return valid = isAllValid;
    }


    getTodayDate() {
        let today = new Date(),
            day = today.getDate(),
            month = today.getMonth() + 1, //January is 0
            year = today.getFullYear();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        today = year + '-' + month + '-' + day;
        return today;
    }

}