import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import forgotPassword from '@salesforce/apex/okpcRegisterController.forgotPassword';
import loginURL from '@salesforce/label/c.OKLC_Login_URL';

export default class OklcForgotPassword extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';

    customImage = nysdohResource;

    navigateToLogin() {
        console.log('navigateToLogin');
        location.href = loginURL;
    }

    nameChange(event) {
        this.username = event.target.value;
    }

    doForgotPasswordCheck() {
        console.log('doForgotPasswordCheck---username-->', this.username);
        this.showError = false;

        if (this.username == '' || typeof this.username == 'undefined') {
            this.errorMessage = 'Please provide an email address.';
            this.showError = true;
            return;
        }

        if (!this.validateEmail(this.username)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }

        forgotPassword({ userName: this.username, delimiter: '.labuser' })//Added delimiter by Sajal
            .then(data => {
                console.log('Data-->' + data);
                this.navigateToLogin();
            })
            .catch(error => {
                console.log('Error-->' + JSON.stringify(error.body.message));
            });
    }

    validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

}