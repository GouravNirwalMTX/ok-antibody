import {
    LightningElement,
    track,
    api
} from 'lwc';
import getBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.getBusinessTimeOnAccount';
import setBusinessTimeOnAccount from '@salesforce/apex/OkpcContactInformationController.setBusinessTimeOnAccount';
import setNumberOfWeeksOnAccount from '@salesforce/apex/OkpcContactInformationController.setNumberOfdaysAndFrequency';
import retrieveDashboardValue from '@salesforce/apex/OkpcContactInformationController.retrieveDashboardValue';
import isAdminUser from '@salesforce/apex/OkpcContactInformationController.isAdminContact';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getAllAccountsForStaff from '@salesforce/apex/OkpcContactInformationController.getAllAccountsForStaff';

export default class OkvriuDashboard extends LightningElement {
    @track startTime = null;;
    @track endTime = null;
    @track siteName = null;
    @track streetAddress = null;
    @track address = null;
    @track endTime = null;
    @track appointmentCounts = [];
    @track loaded = false;
    @track numberOfWeeks = '1';
    @track countType = 'Overall';
    @track frequency = '30';
    @track isAdmin = false;
    @track accounts = [];
    @track selectedAccountId = '';
    
    connectedCallback(){
        this.getUserDetails();
        this.getAccounts();
       // this.getAccountBusinessTime();        
    }

    getAccounts(){
        getAllAccountsForStaff()
        .then(result =>{
            if(result != null){
                console.log('>>>>>>' + JSON.stringify(result));
                this.accounts = result;
                let accId ;
                var sessionKey = sessionStorage.getItem('accId');
                if(sessionKey)
                    this.selectedAccountId = sessionKey;
                else{
                    this.accounts.forEach(function(acc){
                        console.log('acc ' + acc.value);
                        
                        if(acc.selected === true){
                           // alert('fjj' + acc.value);
                            accId = acc.value;
                            console.log('3')
                        }
                        console.log('2')
                    });
                    this.selectedAccountId = accId;
                       console.log('1')
                    sessionStorage.setItem('accId', this.selectedAccountId);
                }
            }
            
            this.getAccountBusinessTime(); 
            this.getDashboardData();
        })
        .catch(result =>{})
    }

    getAccountBusinessTime(){
        getBusinessTimeOnAccount({selectedAccountId : this.selectedAccountId})
        .then(result =>{
            if(result != null){
                this.startTime = (result.Business_Start_Time__c != null && typeof result.Business_Start_Time__c != 'undefined')? this.msToTime(result.Business_Start_Time__c) : '';
                this.endTime = (result.Business_End_Time__c != null && typeof result.Business_End_Time__c != 'undefined')? this.msToTime(result.Business_End_Time__c) : '';
                this.siteName = result.Name;
                this.streetAddress = result.BillingStreet;
                let addressString = '';
                if(result.BillingCity != null && typeof result.BillingCity != 'undefined')
                    addressString += result.BillingCity;
                if(result.BillingState != null && typeof result.BillingState != 'undefined')
                    addressString += addressString == '' ? result.BillingState : ', ' + result.BillingState;
                if(result.BillingPostalCode != null && typeof result.BillingPostalCode != 'undefined')
                    addressString += ' ' + result.BillingPostalCode;
                this.address = addressString;
                this.numberOfWeeks = result.Number_of_Weeks_Available__c;
                this.frequency = result.Appointment_Frequency__c;
            }
            this.loaded = true;
        })
        .catch(result =>{})
       // this.getDashboardData();
    }

    getDashboardData(){
        console.log('selected acc Id ' + this.selectedAccountId);
    retrieveDashboardValue({selectedAccountId : this.selectedAccountId, countType : this.countType, portal : 'vital'})
        .then(result =>{
            if(result != null){
                this.appointmentCounts = [];
                console.log('>>>>>>' + JSON.stringify(result))
                result.appointmentCounts.forEach(el => {
                    if(el.status != 'Closed' && el.status != 'Eligible'){
                        this.appointmentCounts.push(el);
                    }
                });
            }

        })
        .catch(result =>{})
    }


    getUserDetails(){
        isAdminUser()
        .then(result => {
           this.isAdmin = result;
           console.log('isAdmin' + this.isAdmin)
        })
        .catch(error => {
            console.log('Error-->' + error);
        });

    }

    handleInputChange(event) {
        let data = event.detail;
        if (data.label == 'Start Time' ) {
            this.startTime = data.value;
        } else if (data.label == 'End Time' ) {
            this.endTime = data.value;
        }
    }
    get options() {
        return [
            { label: '1', value: '1' },
            { label: '2', value: '2' },
            { label: '3', value: '3' },
            { label: '4', value: '4' },
            { label: '5', value: '5' },
            { label: '6', value: '6' },
            { label: '7', value: '7' },
            { label: '8', value: '8' }
        ];
    }
    get optionsType() {
        return [
            { label: 'Overall Counts', value: 'Overall' },
            { label: 'Daily Counts', value: 'Daily' }
        ];
    }

    get optionsTime() {
        return [
            { label: '5', value: '5' },
            { label: '10', value: '10' },
            { label: '15', value: '15' },
            { label: '20', value: '20' },
            { label: '30', value: '30' },
            { label: '60', value: '60' }
        ];
    }

    handleChange(event) {
       if(event.target.name == 'FutureCalendar') {
           this.numberOfWeeks = event.target.value;
           console.log('....'+ this.numberOfWeeks)
       }else if(event.target.name == 'AppointmentFrequency'){
        this.frequency = event.target.value;
        console.log('....'+ this.frequency)
       }else if(event.target.name == 'Type'){
           this.countType = event.target.value;
           this.getDashboardData();
       }else if(event.target.name == 'accounts'){
           this.selectedAccountId = event.target.value;
           sessionStorage.setItem('accId', this.selectedAccountId);
            this.getAccountBusinessTime();
            this.getDashboardData();
        }
    }

    handleClick(){
        
        if(this.startTime && this.endTime) {
            if(this.startTime > this.endTime) {
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: 'End time must be greater than the start time.',
                });
                this.dispatchEvent(event);
                return;
            }
            this.loaded = false;
            setBusinessTimeOnAccount({startTime: this.startTime, endTime: this.endTime})
            .then(result =>{
                this.loaded = true;
                console.log(result);
                if(result == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: 'Updated successfully.',
                    });
                    this.dispatchEvent(event);
                }
                else {
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result,
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error =>{
                console.log('error-->',error);
            })
        } else {
            const event = new ShowToastEvent({
                title: 'Error!',
                variant: 'error',
                message: 'Please fill required fields.',
            });
            this.dispatchEvent(event);
        }
    }

    handleClickSave(){
      
            this.loaded = false;
            setNumberOfWeeksOnAccount({numberOfWeeks: this.numberOfWeeks, timeFrequency: this.frequency, accountId : this.selectedAccountId})
            .then(result =>{
                this.loaded = true;
                console.log(result);
                if(result == 'success') {
                    const event = new ShowToastEvent({
                        title: 'Success!',
                        variant: 'success',
                        message: 'Updated successfully.',
                    });
                    this.dispatchEvent(event);
                }
                else {
                    this.loaded = true;
                    const event = new ShowToastEvent({
                        title: 'Error!',
                        variant: 'error',
                        message: result,
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error =>{
                this.loaded = true;
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: error.body.message,
                });
                this.dispatchEvent(event);
                console.log('error-->',error);
            })
         
    }

    msToTime(s) {
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;

        return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs) + '.' + this.pad(ms, 3);
    }
    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
}