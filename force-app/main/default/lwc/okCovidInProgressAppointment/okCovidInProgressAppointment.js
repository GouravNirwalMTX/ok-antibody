import { LightningElement,api,track } from 'lwc';
import getAppointmentData from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentData';
import updateAppointment from '@salesforce/apex/DC_ScheduleAppointment.updateAppointmentRec';
import getLabNames from '@salesforce/apex/DC_ScheduleAppointment.getLabNames';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import updateApptmentWithNewSlot from '@salesforce/apex/DC_ScheduleAppointment.updateApptmentWithNewSlot';
import consentError from '@salesforce/label/c.OK_Verbal_Consent_Error';
export default class OkCovidInProgressAppointment extends LightningElement {

    label = {
        consentError
    };

    @api labDefault;
    @api testType = [];
    @track typeChanged = false;    
    @track testTypeValue;    
    @api appointmentId;
    @track testTypeOptions = [];
    @track labNames = [];  
    @track sources = [];
    @track appSlot;

    @track selectedSource;
    @track selectedLab ;
    @track sampleCollectedBy;
    @api currentTestType;
    @track isSubmitted = false;
    @track verbalConsent = false;
    @track error = false;

    connectedCallback() {
        this.testType.forEach(element => {
            let option = {label : element, value : element};
            this.testTypeOptions.push(option);
        });
        this.testTypeValue = this.currentTestType;
        this.getData();
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
    getData(){
        getAppointmentData({testingType : this.currentTestType}).then(data => {
           console.log('data ' + JSON.stringify(data));
            this.sources = data.sources;
            this.labNames = data.labs;

            this.labNames.forEach(element => {
                console.log('selected ', element.selected);
                if(element.selected)
                    this.selectedLab = element.value;
            });
            console.log('this.selectedLab >> ', this.selectedLab);

        })
        .catch(error => {
            console.log('Error-->' + error);
        });
    }

    handleSubmit(){
        console.log('data ' + this.isSubmitted);

        if(!this.verbalConsent){
            this.error = true;
            return;
        }


        let data = {id: this.appointmentId, testingSiteType : this.currentTestType, 
            source : this.selectedSource, labCenter : this.selectedLab, sampleCollectedBy : this.sampleCollectedBy,
            typeChanged : this.typeChanged};

        if(!this.isSubmitted){
            updateAppointment({appJSON : JSON.stringify(data)}).then(result => {
                console.log('result>>>> ' + JSON.stringify(result));    
                if(result.success){
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'success',
                        message: 'Your Appointment has been updated successfully !',
                    }));
                    this.handleCloseModal();  
                }else{
                    this.appSlot = result.slot;
                    console.log('appslot )))) ' , this.appSlot);
                    this.isSubmitted = true;
                }               
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
        }else{
            console.log('appslot ' , this.appSlot);
            updateApptmentWithNewSlot({appSlot : JSON.stringify(this.appSlot), appJSON : JSON.stringify(data)}).then(result => {
                console.log('result>>>> ' + JSON.stringify(result));    
                if(result.success){
                    this.dispatchEvent(new ShowToastEvent({
                        title: '',
                        variant: 'success',
                        message: 'Your Appointment has been updated successfully !',
                    }));
                    this.handleCloseModal();  
                }             
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
        }
            
    }

    handleNo(){
        this.isSubmitted = false;
    }

    handleInput(event){
        if (event.target.name == 'sampleCollectedBy') {
           this.sampleCollectedBy = event.target.value;
        }else if (event.target.name == 'testType') {
            this.currentTestType = event.target.value;
            if(this.testTypeValue != this.currentTestType)
                this.typeChanged = true;
            else{
                this.typeChanged = false;
            }
            getLabNames({testingType : this.currentTestType}).then(data => {
                console.log('data lab' + JSON.stringify(data));
                 this.labNames = data;
                 this.labNames.forEach(element => {
                    if(element.selected)
                        this.selectedLab = element.value;
                });
             })
             .catch(error => {
                 console.log('Error-->' + error);
             });       
            console.log('this.typeChanged ', this.typeChanged);
            console.log('this.testTypeValue ', this.testTypeValue);
            console.log('this.currentTestType ', this.currentTestType);
         }else if (event.target.name == 'lab') {
            this.selectedLab = event.target.value;
         }else if (event.target.name == 'source') {
            this.selectedSource = event.target.value;
         }else if (event.target.name == 'verbalConsent') {
            this.verbalConsent = event.target.checked;
            if(this.verbalConsent)
                this.error = false;
         }
        
    }
}