import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import login from '@salesforce/apex/okpcLoginController.login';
import forgotPasswordURL from '@salesforce/label/c.OKPC_Forgot_Password_URL';
import communityURL from '@salesforce/label/c.OKPC_Community_url';

export default class Okpc_nysdohLoginPage extends NavigationMixin(LightningElement) {

    @track showError = false;
    @track errorMessage = '';
    @track username = '';
    @track password = '';

    customImage = nysdohResource;
    navigateToForgotPassword() {
        console.log('navigateToForgotPassword');
        location.href = forgotPasswordURL;
    }
    navigateToHome(data) {
        console.log('navigateToForgotPassword');
        location.href = data;
    }

    nameChange(event) {
        this.username = event.target.value;
    }
    passwordChange(event) {
        this.password = event.target.value;
    }

    doLogin() {
        console.log(this.username);
        console.log(this.password);
        login({ username: this.username, password: this.password, startUrl: communityURL + 's/', delimiter: '.citizen' })//Added delimiter by Sajal
            .then(data => {
                console.log('Success-->', data);
                this.navigateToHome(data);
            })
            .catch(error => {
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error.body.message));
                var msg = JSON.stringify(error.body.message);
                this.errorMessage = msg.substring(1, msg.length - 1);
            });
    }

}