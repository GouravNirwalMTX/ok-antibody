import { LightningElement, track } from 'lwc';
import retrieveLabDashboardValue from '@salesforce/apex/OkpcContactInformationController.retrieveLabDashboardValue';

export default class OklcDashboard extends LightningElement {
    @track antibodyTestingCount = [];
    @track positiveCount = 0;
    @track negativeCount = 0;
    @track resultReprocessCount = 0;
    @track blankCount = 0;

    connectedCallback(){
        retrieveLabDashboardValue()
            .then(result =>{
                if(result != null){
                    console.log('---result---'+JSON.stringify(result));
                    var antibodyList = result.antibodyTestingCounts;
                    for(var item in antibodyList){
                        console.log(item);
                        console.log(antibodyList[item]);
                        if(antibodyList[item].result === 'POSITIVE'){
                            this.positiveCount = antibodyList[item].count?antibodyList[item].count:0;
                        }
                        else if(antibodyList[item].result === 'NEGATIVE'){
                            this.negativeCount = antibodyList[item].count?antibodyList[item].count:0;
                        }
                        else if(antibodyList[item].result === 'REJECTED'){
                            this.resultReprocessCount = antibodyList[item].count?antibodyList[item].count:0;
                        }
                        else if(antibodyList[item].result === 'RESULTS BLANK'){
                            this.blankCount = antibodyList[item].count?antibodyList[item].count:0;
                        }
                    }
                    
                    //this.antibodyTestingCount = result.antibodyTestingCounts;
                    
                        
                }
    
            })
            .catch(result =>{})
            console.log('---this.antibodyTestingCount---'+this.antibodyTestingCount);
        
    }
}