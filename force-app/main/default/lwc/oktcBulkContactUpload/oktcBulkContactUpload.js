import {  LightningElement,
    track,
    api } from 'lwc';
    import {
        ShowToastEvent
    } from 'lightning/platformShowToastEvent';
import insertData from '@salesforce/apex/oktcBulkContactUpload.insertData';  
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getLoggedInTestingUserDetails';
export default class OktcBulkContactUpload extends LightningElement {

    @track contactType;
    @track accountId;
    @track data = [];
    filesUploaded = [] ;
    @track fileName;
    @track showSpinner = false;
    file;
    fileContents;
    fileReader;
    content;
    MAX_FILE_SIZE = 1500000;
    result;
    @track showFileName = false;
    @track testingType =[];
    @track emailOpt = false;
    @track smsOpt = false;
    @track isAntiBody = false;
    @track isPcr = false;
    @track isRapid = false;
    @track fileUploadedFor = '';
    @track fileRapid = false;
    @track fileAntibody = false;
    @track filePCR = false;


    connectedCallback() {
        this.getTestingSitedata();
    }
    getTestingSitedata() {
        getTestingSite({})
            .then(data => {
                if (data.user){
                    this.accountId = data.user.Contact.AccountId;
                }
                if(data.testingSiteType){
                    this.testingType = data.testingSiteType;
                }
                this.testingType.forEach(element => {
                    if(element == 'Antibody Testing'){
                        this.isAntiBody = true;                     
                    }
                    if(element == 'PCR Testing'){
                        this.isPcr = true;
                    }
                    if(element == 'Rapid Testing'){
                        this.isRapid = true;
                    }
                });
                    
                    
                this.showPaginator = true;
            })
            .catch(error => {
                console.log('Error-->' + error);
            });
    }


// Download Sample File Functionality
   /* get options() {
        return [
            { label: '--None--', value: '' },
            { label: 'COVID Citizen', value: 'Citizen_COVID' },
            { label: 'Antibody Citizen', value: 'Citizen' },
        ];
    }*/

    handleChange(event) {
        var name = event.target.name;
       if(name == 'smsOpt' || name == 'smsOpt1' || name == 'smsOpt2')
            this.smsOpt = event.target.checked;
        else if(name == 'emailOpt' || name == 'emailOpt1' || name == 'emailOpt2')
            this.emailOpt = event.target.checked; 
    }


    downloadCSVFile() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = {};
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        if(type == 'Antibody Testing'){
            rowData = ["First Name","Last Name","Email","Mobile","Gender","Date Of Birth","Race","Organization Name"];
        }else{
            rowData = ["First Name","Last Name","Email","Mobile","Gender","Date Of Birth","Race","Organization Name", "Ethnicity", "Symptomatic", "Direct Contact with Positive Case (2-14 days)"];
        }
        
        
        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        rowData = Array.from(rowData);
        
        // splitting using ','
        csvString += rowData.join(',');
        csvString += rowEnd;
        
        let downloadElement = document.createElement('a');

        // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_self';
        // CSV File Name
        downloadElement.download = 'ContactUpload.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
    }

    // Upload Functionality

    handleFileChange(event){
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        console.log('file upload foe ', type);



        this.filesUploaded = event.target.files;
        this.fileName = event.target.files[0].name;
        if(this.filesUploaded.length > 0) {
            this.showFileName = true;
            this.handleUpload();
        }
    }

    handleActive(event) {
        console.log('bdhfgdhgfgh')
        this.filesUploaded = [];
        this.fileName = '';
        this.showFileName = false;
    }

    handleFileDelete(){
        
        this.showFileName = false;
        this.result = null;
        this.filesUploaded.length = 0;
        this.fileName ='';
        this.fileAntibody = false;
        this.filePCR = false;
        this.fileRapid = false;
       
    }

    handleUpload() {
        if(this.filesUploaded.length > 0) {
            this.uploadHelper();
        }
        else {
            this.fileName = 'Please select file to upload or upload the correct format!';
        }
    }

    uploadHelper(){
        this.file = this.filesUploaded[0];
        if (this.file.size > this.MAX_FILE_SIZE) {
            window.console.log('File Size is to long');
            return ;
        }
        this.showSpinner = true;
        this.fileReader = new FileReader();
        this.fileReader.readAsText(this.file, "UTF-8");

        this.fileReader.onload = (() => {
            this.fileContents = this.fileReader.result;
           this.result = this.csvToJson(this.fileContents);

           console.log('resul *** ', this.result);
        //    this.insertResults(result);
        });
        this.fileReader.onloadend = (() =>{
            this.showSpinner = false;
        });
        this.fileReader.onerror = function (evt) {
            this.showSpinner = false;
        }
    }

    csvToJson(csv){
        var arr = []; 
        
        arr =  csv.split('\n');
      
        var jsonObj = [];
        //var headers = arr[0].split(',');
        var headers = [];
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        if(type == 'Antibody Testing'){
            headers = ["firstName","lastName","email","mobile","gender","dateOfBirth","race","organizationName"];
        }else{
            headers = ["firstName","lastName","email","mobile","gender","dateOfBirth","race","organizationName","ethnicity", "symptomatic", "positiveCase"];
        }
        

        for(var i = 1; i < arr.length; i++) {
            var data = arr[i].split(',');
        
            var obj = {};
        
            for(var j = 0; j < data.length; j++) {
                obj[headers[j].trim()] = data[j].trim();
                }
            jsonObj.push(obj);
        }
        var json = JSON.stringify(jsonObj);
        return json;
    }

    showTost(type,message){
        this.dispatchEvent(new ShowToastEvent({
            title: '',
            variant: type,
            message: message,
        }));
    }

    insertResults(jsonstr){
        this.data = [];
        this.showSpinner = true;
        
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        if(type == 'Antibody Testing')
            this.contactType = 'Citizen';
        else{
            this.contactType = 'Citizen_COVID';
        }
        insertData({strfromle : jsonstr,
                    recordType: this.contactType,
                    testingSiteId: this.accountId,
                    smsOpt :  this.smsOpt,
                    emailOpt : this.emailOpt,
                    testingSiteType : type})
        .then(result =>{
           if(result.success){
                this.showSpinner = false;
                this.dispatchEvent(new ShowToastEvent({
                    title: '',
                    variant: 'success',
                    message: result.message,
                }));
                this.showFileName = false;
                this.result = null;
                this.downloadResultFile(result.data);
            }else{
                 this.showSpinner = false;
                 this.dispatchEvent(new ShowToastEvent({
                     title: '',
                     variant: 'warning',
                     message: result.message,
                 }));
                 this.showFileName = false;
                 this.result = null;
            }
          
        })
        .catch(error =>{
            console.log('error >>>>>>', JSON.stringify(error.body.exceptionType));
            this.showSpinner = false;
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: error.body.message,
            }));
        })
    }

    downloadResultFile(object){
        this.data = [];
        object.forEach(record => {
            
            let temparr = {};
            temparr.firstName = record.firstName;
            temparr.lastName = record.lastName;
            temparr.email = record.email;
            temparr.mobile = record.mobile;
            temparr.gender = record.gender;
            temparr.dateOfBirth = record.dateOfBirth;
            temparr.race = record.race;
            temparr.organizationName = record.organizationName;
            temparr.ethnicity = record.ethnicity;
            temparr.positiveCase = record.positiveCase;
            temparr.symptomatic = record.symptomatic;
            temparr.status = record.status;
            temparr.reason = record.reason;
            this.data.push(temparr);
        })
        if (this.data) {
            this.downloadCSVFileForStatus();
        }
    }

    handleSave(){
        
        if(this.result){
            this.insertResults(this.result);
        }else{
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: 'Please select a CSV file or upload the correct format !',
            }));
        }
    }

    downloadCSVFileForStatus() {
        let rowEnd = '\n';
        let csvString = '';
        // this set elminates the duplicates if have any duplicate keys
        let rowData = [];
        let rowHeaders ;
        // getting keys from data
        this.data.forEach(function (record) {
            Object.keys(record).forEach(function (key) {
               
               // rowData.add(key);
             
            });
        });

        rowData.push('firstName');
        rowData.push('lastName');
        rowData.push('email');
        rowData.push('mobile');
        rowData.push('gender');
        rowData.push('dateOfBirth');
        rowData.push('race');
        rowData.push('organizationName');
        
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        if(type != 'Antibody Testing'){
            rowData.push('ethnicity');
            rowData.push('positiveCase');
            rowData.push('symptomatic');
        }
        
        rowData.push('status');
        rowData.push('reason');
        
        // Array.from() method returns an Array object from any object with a length property or an iterable object.
        //rowData = Array.from(rowData);
        // splitting using ','
        //csvString += rowData.join(',');
        var type = this.template.querySelector('lightning-tabset').activeTabValue ;
        if(type == 'Antibody Testing'){
            csvString += ["First Name","Last Name","Email","Mobile","Gender","Date Of Birth","Race","Organization Name","Status", "Reason"];
        }else{
            csvString += ["First Name","Last Name","Email","Mobile","Gender","Date Of Birth","Race","Organization Name", "Ethnicity", "Symptomatic", "Direct Contact with Positive Case (2-14 days)", "Status", "Reason"];
        }
        
        csvString += rowEnd;

        // main for loop to get the data based on key value
        for (let i = 0; i < this.data.length; i++) {
            let colValue = 0;

            // validating keys in data
            for (let key in rowData) {
                if (rowData.hasOwnProperty(key)) {
                    // Key value 
                    // Ex: Id, Name
                    let rowKey = rowData[key];
                    // add , after every value except the first.
                    if (colValue > 0) {
                        csvString += ',';
                    }
                    // If the column is undefined, it as blank in the CSV file.
                    let value = this.data[i][rowKey] === undefined ? '' : this.data[i][rowKey];
                    csvString += '"' + value + '"';
                    colValue++;
                }
            }
            csvString += rowEnd;
        }

       // Creating anchor element to download
       let downloadElement = document.createElement('a');
       var universalBOM = "\uFEFF";
       // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
       downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM+csvString);
       downloadElement.target = '_self';
       // CSV File Name
       downloadElement.download = 'Bulk Upload Status.csv';
        // below statement is required if you are using firefox browser
        document.body.appendChild(downloadElement);
        // click() Javascript function to download CSV file
        downloadElement.click();
        
    }
    
}