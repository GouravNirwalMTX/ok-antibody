import { LightningElement, track, api } from "lwc";
import getContactDetails from "@salesforce/apex/OkpcContactInformationController.getPatientHistoryDetails";
import saveContactDetails from "@salesforce/apex/OkpcContactInformationController.savePatientDetails";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import fetchPicklist from "@salesforce/apex/OkpcContactInformationController.fetchPicklist";

export default class OkpcContactInfo extends LightningElement {
  @track dataObj = {};
  @api appointmentDataIntake = {};
  @track appointmentData = {};
  @track extraWrapper = {};
  @api contactId;
  @track showSpinner = false;
  @track disableInputs = true;
  @track testingType;
  @track diseaseSymptoms = [];
  @track EmployedinhighRisk = [];
  @track PatientResidencyType = [];
  @track pregnantOptions = [];
  @track reasonOptions = [];
  @track selectedValues = [];
  @track age;
  @track isSymptomatic = false;
  @api currentStep;
  @track todayDate;
  @track showHighRisk = false;
  @track showResidencyType = false;
  @track ifResidencyType = false;
  @track secondRowRequired = false;
  @track thirdRowRequired = false;
  @track isReasonRequired = false;
  @track dynamicCSS2ndRow = "col-sm-6";
  //  @track dynamicCSS3nrdRow = 'col-sm-6';
  @track dynamicCSS1stdRow = "col-sm-6";
  @track dynamicCSS4stdRow = "col-sm-12";
  @api isDependent = false;
  @track fieldsRequiredForPrimaryContact = false;
  @track fieldsRequiredForDependentContact = false;

  get onlyYesNoOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      }
    ];
  }

  get YesNoUnKnwOptions() {
    return [
      {
        label: "Yes",
        value: "Yes"
      },
      {
        label: "No",
        value: "No"
      },
      {
        label: "Unknown",
        value: "Unknown"
      }
    ];
  }
  connectedCallback() {
    console.log(
      "App data in patient >>>",
      JSON.stringify(this.appointmentData)
    );
    console.log(
      "App data in patient intake se >>>",
      JSON.stringify(this.appointmentDataIntake)
    );
    console.log(
      "App JOSN Stringify",
      JSON.parse(JSON.stringify(this.appointmentDataIntake))
    );
    this.appointmentData = JSON.parse(
      JSON.stringify(this.appointmentDataIntake)
    );
    console.log(
      "App data in patient <<<<<<<<>>>>>>>>",
      JSON.stringify(this.appointmentData)
    );
    if (!this.isDependent) {
      this.fieldsRequiredForPrimaryContact = true;
      this.fieldsRequiredForDependentContact = true;
    } else {
      this.fieldsRequiredForDependentContact = true;
      this.fieldsRequiredForPrimaryContact = false;
    }
    this.getContact();

    fetchPicklist({
      objectName: "Contact",
      fieldName: "Disease_symptoms__c"
    }).then((result) => {
      this.diseaseSymptoms = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Employed_in_high_risk_setting__c"
    }).then((result) => {
      this.EmployedinhighRisk = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Patient_Residency_Type__c"
    }).then((result) => {
      this.PatientResidencyType = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "Pregnant__c"
    }).then((result) => {
      this.pregnantOptions = result;
    });
    fetchPicklist({
      objectName: "Contact",
      fieldName: "What_is_the_reason_for_your_test_today__c"
    })
      .then((result) => {
        this.reasonOptions = result;
      })
      .catch((error) => {
        let message = error.message || error.body.message;
        console.log(message);
      });

    this.todayDate = this.getTodayDate();
    console.log("ToDayDate>>", this.todayDate);
  }

  getTodayDate() {
    let today = new Date(),
      day = today.getDate(),
      month = today.getMonth() + 1, //January is 0
      year = today.getFullYear();
    if (day < 10) {
      day = "0" + day;
    }
    if (month < 10) {
      month = "0" + month;
    }
    today = year + "-" + month + "-" + day;
    return today;
  }

  getContact() {
    // if(!this.isDependent){
    this.showSpinner = true;
    getContactDetails({
      contactId: this.contactId
    })
      .then((result) => {
        console.log("data=>", JSON.stringify(result));
        this.dataObj.firstTest = result.firstTest;
        // this.dataObj.empHealthCare = result.empHealthCare;
        this.dataObj.parentOccupation = result.parentOccupation;
        this.dataObj.isTestRequiredByEmp = result.isTestRequiredByEmp;
        this.dataObj.reasonForTest = result.reasonForTest;
        // this.dataObj.ifSymptomatic = result.ifSymptomatic;
        this.appointmentData.ifSymptomatic = this.appointmentData.ifSymptomatic;
        this.appointmentData.contactId = result.contactId;
        this.dataObj.illnessDate = result.illnessDate;
        this.dataObj.patientId = result.patientId;
        //  this.dataObj.whereYouLive = result.whereYouLive;
        this.appointmentData.whereYouLive = this.appointmentData.whereYouLive;
        this.dataObj.empHighRisk = result.empHighRisk;
        this.appointmentData.inContactWithpositiveCase = this.appointmentData.inContactWithpositiveCase;
        //this.dataObj.inContactWithpositiveCase = result.inContactWithpositiveCase;
        // this.contactId = result.contactId;
        if (result.diseaseSymptoms) {
          if (result.diseaseSymptoms.includes(";")) {
            this.selectedValues = result.diseaseSymptoms.split(";");
          } else {
            this.selectedValues = [result.diseaseSymptoms];
          }
        }
        this.dataObj.diseaseSymptoms = result.diseaseSymptoms;
        this.dataObj.residencyType = result.residencyType;
        this.dataObj.residentSetting = result.residentSetting;
        if (this.dataObj.residentSetting == "Yes") {
          this.showResidencyType = true;
          this.dynamicCSS1stdRow = "col-sm-4";
        } else {
          this.showResidencyType = false;
          this.dynamicCSS1stdRow = "col-sm-6";
          this.dataObj.residencyType = null;
        }
        this.dataObj.Pregnant = result.Pregnant;
        //    this.dataObj.verbalConsent = result.verbalConsent;
        this.dataObj.contactId = result.contactId;
        if (this.appointmentData.ifSymptomatic == "Yes") {
          this.isSymptomatic = true;
          //  this.dynamicCSS3nrdRow = 'col-sm-4';
          this.thirdRowRequired = true;
        } else {
          this.isSymptomatic = false;
          this.dataObj.illnessDate = null;
          this.thirdRowRequired = false;
          //   this.dynamicCSS3nrdRow = 'col-sm-4';
        }
        if (this.appointmentData.whereYouLive == "Yes") {
          this.showHighRisk = true;
          this.dynamicCSS2ndRow = "col-sm-4";
          this.secondRowRequired = true;
        } else {
          this.dataObj.empHighRisk = null;
          this.secondRowRequired = false;
          this.showHighRisk = false;
          this.dynamicCSS2ndRow = "col-sm-6";
        }
        if (
          this.appointmentData.ifSymptomatic == "No" &&
          this.appointmentData.inContactWithpositiveCase == "No" &&
          this.dataObj.isTestRequiredByEmp == "No"
        ) {
          this.isReasonRequired = true;
          // this.dynamicCSS4stdRow = 'col-sm-6';
        } else {
          this.isReasonRequired = false;
        }
        console.log(JSON.stringify(this.dataObj));
        this.showSpinner = false;
      })
      .catch((error) => {
        console.log("error=>", JSON.stringify(error));
        this.showSpinner = false;
      });
    /* }
      else {
        console.log('IN Patinet Histroy ContactId transferred from ersonal Info>', this.contactId)
          this.dataObj = {};
          
      } */
  }

  handleInputChange(event) {
    this.dataObj[event.target.name] = event.target.value;
    console.log("dataOnbj>>", JSON.stringify(this.dataObj));
    if (event.target.name == "diseaseSymptoms") {
      var tempArray = this.dataObj.diseaseSymptoms;
      console.log(">>>>>temparray>>" + JSON.stringify(tempArray));
      var x = tempArray.join(";");
      console.log("<<<<>>>", JSON.stringify(x));
      this.dataObj.diseaseSymptoms = x;
    }
    if (this.dataObj.residentSetting == "Yes") {
      this.dynamicCSS1stdRow = "col-sm-4";
      this.showResidencyType = true;
    } else {
      this.dynamicCSS1stdRow = "col-sm-6";
      this.showResidencyType = false;
      this.dataObj.residencyType = null;
    }
    if (
      this.appointmentData.ifSymptomatic == "No" &&
      this.appointmentData.inContactWithpositiveCase == "No" &&
      this.dataObj.isTestRequiredByEmp == "No"
    ) {
      this.isReasonRequired = true;
      // this.dynamicCSS4stdRow = 'col-sm-6';
    } else {
      this.isReasonRequired = false;
    }

    /*  if (this.dataObj.ifSymptomatic == 'Yes') {
            this.thirdRowRequired = true;
            this.isSymptomatic = true;
            this.dynamicCSS3nrdRow = 'col-sm-4';
        } else {
            this.dataObj.diseaseSymptoms = "";
            this.selectedValues = [];
            this.thirdRowRequired = false;
            this.isSymptomatic = false;
            this.dataObj.illnessDate = null;
            this.dynamicCSS3nrdRow = 'col-sm-4';
        }
        if(this.dataObj.whereYouLive == 'Yes'){
            this.showHighRisk = true;
            this.secondRowRequired = true;
            this.dynamicCSS2ndRow = 'col-sm-8'
        }else{
            this.showHighRisk = false;
            this.secondRowRequired = false;
            this.dataObj.empHighRisk = null;
            this.dynamicCSS2ndRow = 'col-sm-12'
        } */
    console.log(JSON.stringify(this.dataObj));
  }

  handleAppointmentInputChange(event) {
    this.appointmentData[event.target.name] = event.target.value;
    if (this.appointmentData.ifSymptomatic == "Yes") {
      this.thirdRowRequired = true;
      this.isSymptomatic = true;
      // this.dynamicCSS3nrdRow = 'col-sm-4';
    } else {
      this.dataObj.diseaseSymptoms = "";
      this.selectedValues = [];
      this.thirdRowRequired = false;
      this.isSymptomatic = false;
      this.dataObj.illnessDate = null;
      //  this.dynamicCSS3nrdRow = 'col-sm-4';
    }
    if (this.appointmentData.whereYouLive == "Yes") {
      this.showHighRisk = true;
      this.secondRowRequired = true;
      this.dynamicCSS2ndRow = "col-sm-4";
    } else {
      this.showHighRisk = false;
      this.secondRowRequired = false;
      this.dataObj.empHighRisk = null;
      this.dynamicCSS2ndRow = "col-sm-6";
    }
    if (
      this.appointmentData.ifSymptomatic == "No" &&
      this.appointmentData.inContactWithpositiveCase == "No" &&
      this.dataObj.isTestRequiredByEmp == "No"
    ) {
      this.isReasonRequired = true;
      // this.dynamicCSS4stdRow = 'col-sm-6';
    } else {
      this.isReasonRequired = false;
    }
    console.log(JSON.stringify(this.appointmentData));
  }

  @api
  saveContacts() {
    console.log("data=>", JSON.stringify(this.dataObj));
    // this.dataObj.contactId = this.contactId;

    if (!this.isValid()) {
      let tempObj = {};
      tempObj.message = "Please complete the required fields.";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
      return;
    }
    if (
      this.appointmentData.ifSymptomatic == "Yes" ||
      this.appointmentData.whereYouLive == "Yes" ||
      this.appointmentData.inContactWithpositiveCase == "Yes"
    ) {
      // this.testingType = 'Rapid Testing';
      this.appointmentData.testingType = "Rapid Testing";
    } else {
      // this.testingType = 'PCR Testing';
      this.appointmentData.testingType = "PCR Testing";
    }
    console.log("final app data>>>", JSON.stringify(this.appointmentData));
    console.log(
      "final app data>>>patient History b4 save",
      JSON.stringify(this.dataObj)
    );
    saveContactDetails({
      jsonData: JSON.stringify(this.dataObj),
      currentstep: this.currentStep,
      testingType: this.testingType,
      stepName: "patientHistory",
      isDependent: this.isDependent
    })
      .then((result) => {
        console.log("Successfully Saved");
        this.disableInputs = true;
        // this.extraWrapper = {...this.dataObj};
        let tempObj = {};
        tempObj.message = "saved successfully";
        tempObj.status = "success";
        tempObj.appointmentDetails = this.appointmentData;
        this.sendEventToParent(tempObj);
      })
      .catch((error) => {
        console.log("error in saving: ", JSON.stringify(error));
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Error"
          })
        );
      });
  }

  @api
  handleCancelButton() {
    this.disableInputs = true;
    // this.dataObj = {...this.extraWrapper};
  }

  sendEventToParent(obj) {
    const selectedEvent = new CustomEvent("contactsave", {
      detail: obj
    });
    this.dispatchEvent(selectedEvent);
  }

  /*  validateTextField(field) {
        if (field == '' || typeof field == 'undefined') {
            let tempObj = {};
            tempObj.message = "Please complete the required fields.";
            tempObj.status = "error";
            this.sendEventToParent(tempObj);
            return false;
        } else {
            return true;
        }
    }
*/
  validateDate(date) {
    var regExpPhone = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
    var isValid = date.match(regExpPhone);
    console.log("isValid", isValid);
    if (!isValid) {
      let tempObj = {};
      tempObj.message = "Please provide a valid format for date";
      tempObj.status = "error";
      this.sendEventToParent(tempObj);
    }
    return isValid;
  }

  // validateZip(zip) {
  //     var regExpZip = /^[0-9]*$/;
  //     var isValid = (zip.match(regExpZip) && zip.length == 5);
  //     console.log("isValid", isValid);
  //     if(!isValid){
  //         let tempObj = {};
  //         tempObj.message = 'Please enter a valid format ("XXXXX") for Zip code.';
  //         tempObj.status = "error";
  //         this.sendEventToParent(tempObj);
  //     }
  //     return isValid;
  // }

  isValid() {
    let valid = true;
    let isAllValid = [
      ...this.template.querySelectorAll("lightning-input")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid2 = [
      ...this.template.querySelectorAll("lightning-radio-group")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid3 = [
      ...this.template.querySelectorAll("lightning-combobox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    let isAllValid4 = [
      ...this.template.querySelectorAll("lightning-dual-listbox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);
    if (isAllValid && isAllValid2 && isAllValid3 && isAllValid4) {
      valid = true;
    } else {
      valid = false;
    }
    // valid = isAllValid2
    return valid;
  }

  handlePhoneFocus(event) {
    event.target.value = this.dataObj.phone;
  }

  formatPhone(obj) {
    var numbers = obj.value.replace(/\D/g, ""),
      char = {
        0: "(",
        3: ") ",
        6: "-"
      };
    obj.value = "";
    for (var i = 0; i < numbers.length; i++) {
      obj.value += (char[i] || "") + numbers[i];
    }
  }
  formatPhoneInput(obj) {
    var numbers = obj.replace(/\D/g, ""),
      char = {
        0: "(",
        3: ") ",
        6: "-"
      };
    obj = "";
    for (var i = 0; i < numbers.length; i++) {
      obj += (char[i] || "") + numbers[i];
    }
    return obj;
  }
}