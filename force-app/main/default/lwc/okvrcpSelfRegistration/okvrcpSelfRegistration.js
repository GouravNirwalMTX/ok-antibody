import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import nysdohResource from '@salesforce/resourceUrl/okpcLogoForPDF';
import register from '@salesforce/apex/OK_SelfRegistration.registerUser';
import contactType from '@salesforce/label/c.OK_Registration_Type_VR_Citizen_Consultation';

export default class OkvrcpSelfRegistration extends NavigationMixin(LightningElement) {

    label = {
        contactType
    }
    @track showError = false;
    @track showSuccess = false;
    @track errorMessage = '';
    @track thankYouMessage = '';
    @track firstName = '';
    @track lastName = '';
    @track email = '';
    @track phone = '';
    @track accountId;
    @track portalName;
    @track showThankYou = false;
    @track accounts = [];
    @track delimiter = '.vrcpuser'
    @track portal = [{label:'Citizen (COVID)',value:"COVID"},{label:'Citizen (Antibody)',value:'Antibody'}];
    @track showSpinner = false;
    @api portalName = 'Vital Records Consultation Portal';
    customImage = nysdohResource;

    firstNameChange(event) {
        this.firstName = event.target.value;
        console.log('firstName=> ', this.firstName);
    }
    lastNameChange(event) {
        this.lastName = event.target.value;
        console.log('lastName=> ', this.lastName);
    }
    phoneChange(event) {
        this.phone = event.target.value;
        console.log('phone=> ', this.phone);
    }
    emailChange(event) {
        this.email = event.target.value;
        console.log('email=> ', this.email);
    }
    accountChange(event) {
        this.accountId = event.target.value;
        console.log('accountId=> ', this.accountId);
    }
    portalChange(event) {
        this.portalName = event.target.value;
        console.log('portalName=> ', this.portalName);
    }

    connectedCallback() {
       /* getAccount()
        .then(result => {
            console.log('result'+JSON.stringify(result));
            this.accounts = result;
            console.log('acc'+this.accounts )
        })
        .catch((error) => {
            let message = error.message || error.body.message;
            console.log(message);
        }); */
    }

    doRegisteration() {
        this.showError = false;
        this.showSuccess = false;
        this.showThankYou = false;
        if (this.firstName.trim() == '' || typeof this.firstName == 'undefined') {
            this.errorMessage = 'Please provide First Name.';
            this.showError = true;
            return;
        }
        if(!this.validateName(this.firstName)){
            this.errorMessage = 'First Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }
        
        if (this.lastName.trim() == '' || typeof this.lastName == 'undefined') {
            this.errorMessage = 'Please provide Last Name';
            this.showError = true;
            return;
        }
        if(!this.validateName(this.lastName)){
            this.errorMessage = 'Last Name cannot contain any numbers or special characters';
            this.showError = true;
            return;
        }
        if (this.email.trim() == '' || typeof this.email == 'undefined') {
            this.errorMessage = 'Please provide Email';
            this.showError = true;
            return;
        }
        if (!this.validateEmail(this.email)) {
            this.errorMessage = 'Please provide a valid email address. example: "abc@xyz.com"';
            this.showError = true;
            return;
        }
        if ((this.phone != '' && typeof this.phone != 'undefined') && !this.validatePhone(this.phone)) {
            this.errorMessage = 'Phone number cannot contain any special characters';
            this.showError = true;
            return;
        }
        if ((this.phone == '') && (this.email == '')) {
            this.errorMessage = 'Please provide Mobile Number or an Email';
            this.showError = true;
            return;
        }
        
        this.showSpinner = true;
        register({ firstName: this.firstName, lastName: this.lastName, phone: this.phone, email: this.email, type : this.label.contactType, delimiter : this.delimiter})//Added delimiter by Sajal
            .then(data => {
                console.log('Data-->' + JSON.stringify(data));
                if (data.type == 'error') {
                    this.showError = true;
                    this.errorMessage = data.message;
                } else if (data.type == 'success') {
                    this.showThankYou = true;
                    this.thankYouMessage = data.message;
                    console.log('navigateToForgotPassword ' + data.redirectURL);
                    //location.href = data.redirectURL;
                }
                this.showSpinner = false;

            })
            .catch(error => {
                this.showSpinner = false;
                this.showError = true;
                console.log('Error-->' + JSON.stringify(error));
              //  var msg = JSON.stringify(error.body.message);
                var msgArray = error.body.fieldErrors.Username;
                let result = msgArray.map(({ statusCode }) => statusCode);
                let resultmessage = msgArray.map(({ message }) => message);
                // console.log('result >>>' +  JSON.stringify(result));
                // console.log('error message>>'+ msg);
                // console.log('Error >>>>-->' + JSON.stringify(msg.substring(1, msg.length - 1)));
               // this.errorMessage = msg.substring(1, msg.length - 1);
               if(result == 'DUPLICATE_USERNAME'){
                this.errorMessage = 'User already exists';
               }else{
                this.errorMessage = resultmessage;
               }
            });
    }



    /*validateEmail(email) {
        var regExpEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }*/
    validateEmail(email) {
        var regExpEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\+[a-zA-Z0-9-]+)?@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/;
        //var regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var isValid = email.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validateName(name){
        var regExpEmail = /^[a-zA-Z][a-zA-Z\s]*$/;
        var isValid = name.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

    validatePhone(mobile){
        var regExpEmail = /^[0-9]*$/;
        var isValid = mobile.match(regExpEmail);
        console.log("isValid", isValid);
        return isValid;
    }

}