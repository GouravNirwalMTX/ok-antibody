import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    registerListener,
    unregisterAllListeners,
    fireEvent
} from "c/pubsub";
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getContactInformation from "@salesforce/apex/OkpcContactInformationController.getContactInformation";
import changeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.changeAppointmentStatus";
import notChangeAppointmentStatus from "@salesforce/apex/OkpcContactInformationController.notChangeAppointmentStatus";
// import setCurrentStep from "@salesforce/apex/OkpcContactInformationController.setCurrentStep";

export default class OkpcIntakeForm extends LightningElement {
    @track currentStep = 1;
    @track originalStep = 1;
    @track showPersonalInformation = false;
    @track showConsent = true;
    @track showAppointmentPreference = false;
    @track showPriorExposure = false;
    @track showInsurance = false;
    @track showSymptoms = false;
    @track patientHistory = false;
    @track showSpinner = false;
    @track edit = false;
    @track showthankYou = false;
    @track source;
    @track hideStep3 = false; //ad
    @track showDashboard;
    @track testingType = 'Covid Testing';
    @track isDisabled = false;
    @track isDependent = false;
    @track contactId;
    @track appointmentDetails = {};
    @track docsUploaded = [];

    connectedCallback() {
        registerListener("handleSideBarClick", this.handleSideBarClick, this);
        console.log('inside conn');
        this.isDisabled = true;
        
            this.getStep();
       
        
    }
    docUploaded(event){
        console.log('evenrINtakeUpload >> ' , JSON.stringify(event.detail))
        this.docsUploaded = event.detail;
        console.log('evenrINtakeUpload  DOCS>> ' , JSON.stringify(this.docUploaded))
    }
    // setStep(step){
    //     setCurrentStep({currentStep: step})
    //     .then(result =>{
    //         console.log('step saved');
    //     })
    //     .catch(error =>{
    //         console.log('error');
    //     })
    // }

    getStep() {
        getContactInformation()
            .then(result => {
                console.log(JSON.stringify(result));
                if (result.Source__c == 'MMS') {
                    this.hideStep3 = true;
                } else {
                    this.hideStep3 = false;
                }
                if (result.Current_Step__c) {
                    this.originalStep = result.Current_Step__c;
                    this.handleSideBarClick(this.originalStep);
                }
                if (result.Form_Submitted__c == true) {
                    this.showDashboard = true;
                } else {
                    this.showDashboard = false;
                }

            })
            .catch(error => {
                console.log('error');
            })
    }

    clearAllSteps() {
        this.showConsent = false;
        this.showPersonalInformation = false;
        this.showAppointmentPreference = false;
        this.showInsurance = false;
        this.patientHistory = false;
        this.showSymptoms = false;
        this.showthankYou = false;
    }

    navigateToPersonalInfo(event) {
        if (event.target.name == 'next') {
            console.log('In Intake save Consnet')
            this.template.querySelector('c-okcp-consent-page').saveConsent();
        } else {
            this.clearAllSteps();
            this.showPersonalInformation = true;
            this.currentStep = 2;
            this.determineOriginalStep();
        }

    }

    navigateToAppointment(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okcp-patient-history').saveContacts();
        } else {

            this.clearAllSteps();
            this.showAppointmentPreference = true;
            this.currentStep = 3;
            this.determineOriginalStep();


        }


    }
    
    patientHistorySave(event){
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            console.log('in intake patient history save app details >>', event.detail.appointmentDetails)
            this.appointmentDetails = event.detail.appointmentDetails
            console.log('Patient His App>>', this.appointmentDetails)
            this.clearAllSteps();
            this.currentStep = 4;
            this.determineOriginalStep();
            // this.showAppointmentPreference = true;
           this.showAppointmentPreference = true;
           
        }
    }

    navigateToShowSymptoms(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okcp-patient-history').saveContacts();
        } else {
            if (this.hideStep3 == true) {
                if (event.target.name == 'next') {
                    this.clearAllSteps();
                    this.currentStep = 4;
                    this.determineOriginalStep();
                    //this.showPriorExposure = true;
                   // this.patientHistory = true;
                    this.showSymptoms = true;
                } else {
                    this.clearAllSteps();
                    //this.currentStep = 2;
                    this.currentStep = 4;
                    this.determineOriginalStep();
                   // this.showPersonalInformation = true;
                   this.showSymptoms = true;
                  // this.patientHistory = true;
                }

            } else {
                this.clearAllSteps();
                this.currentStep = 4;
                this.determineOriginalStep();
                this.showSymptoms = true;
               // this.patientHistory = true;
            }
        }

    }

    navigateToPatientHistory(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okcp-insurance-info').saveInsurance();
        } else {
            if (this.hideStep3 == true) {
                this.clearAllSteps();
                this.patientHistory = true;
                this.currentStep = 4;
                this.determineOriginalStep();
            } else {
                this.clearAllSteps();
                this.patientHistory = true;
                this.currentStep = 4;
                this.determineOriginalStep();
            }

        }
    }

    navigateToInsurancePage(event) {
        if (event.target.name == 'next') {
            this.template.querySelector('c-okcp-contact-info').saveContacts();
        } else {
            if (this.hideStep3 == true) {
                this.clearAllSteps();
                this.showInsurance = true;
                this.currentStep = 3;
                this.determineOriginalStep();
            } else {
                this.clearAllSteps();
                this.showInsurance = true;
                this.currentStep = 3;
                this.determineOriginalStep();
            }

        }
    }

    navigateToConsent() {
        this.clearAllSteps();
        this.currentStep = 1;
        this.determineOriginalStep();
        this.showConsent = true;
    }

    handleEdit() {
        this.edit = true;
        this.template.querySelector('c-okpc-contact-info').editContacts();
    }

    handleCancel() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').handleCancelButton();
    }
    handleSave() {
        this.edit = false;
        this.template.querySelector('c-okpc-contact-info').saveContacts();
    }

    determineOriginalStep() {
        if (this.originalStep >= this.currentStep) {
            this.originalStep = this.originalStep;
        } else {
            this.originalStep = this.currentStep;
        }
    }

    handleSideBarClick(currentStep) {
        console.log('isxode cnj');
        this.currentStep = currentStep;
        this.clearAllSteps();

        switch (currentStep) {
            case 1: {
                this.showConsent = true;
                break;
            }
            case 2: {
                this.showPersonalInformation = true;
                break;
            }
            case 3: {
                this.showInsurance = true;
                break;
            }
            case 4: {
                this.patientHistory = true;
                break;
            }
           /* case 4: {
                this.showSymptoms = true;
                break;
            } */
            case 5: {
                this.showAppointmentPreference = true;
                break;
            }

        }


    }

    handleSubmitData(event) {
        if (event.detail.name == 'submit') {
            this.handleSubmit(event);
            //     this.template.querySelector('c-okcp-preference-component').setValues();

            //     this.clearAllSteps();
            //     this.currentStep = 3;
            // this.determineOriginalStep();
            // // this.showthankYou = true;
            // console.log(this.currentStep, this.originalStep);

            // changeAppointmentStatus({
            //         currentstep: this.currentStep
            //     })
            //     .then(result => {
            //         this.showthankYou = false;
            //         // window.open('/s/dashboard', '_self');
            //         this.showDashboard = true;
            //     })
            //     .catch(error => {
            //         console.log('error status change');
            //     })
        }
    }

    handleSubmit(event) {
        this.clearAllSteps();
       // this.currentStep = 3;
        this.determineOriginalStep();
        this.showDashboard = true;
        this.isDependent = false;
        this.showAppointmentPreference = false;
        this.contactId = null;
        this.appointmentDetails = {};
        this.docsUploaded = [];
        /*
        if (event.target.name == 'submit' || event.detail.name == 'submit') {
            this.template.querySelector('c-okcp-preference-component').setValues();
            this.isDisabled = false;
        }
        this.clearAllSteps();
        this.currentStep = 3;
        this.determineOriginalStep();
        // this.showthankYou = true;
        console.log(this.currentStep, this.originalStep);
        if (event.target.name == 'submit') {
            changeAppointmentStatus({
                    currentstep: this.currentStep
                })
                .then(result => {
                    this.showthankYou = false;
                    // window.open('/s/dashboard', '_self');
                    this.showDashboard = true;
                })
        } */
        if(!this.isDependent){
            notChangeAppointmentStatus({
                currentstep: this.currentStep
            })
            .then(result => {
                this.showthankYou = false;
                // window.open('/s/dashboard', '_self');
                this.showDashboard = true;
            })
            .catch(error => {
                console.log('error status change');
            })
        }
       
           
    }

    handleThankYouBack() {
        // this.navigateToConsent();
        this.showthankYou = false;
        window.open('/s/dashboard', '_self');
    }

    consentSave(event) {

        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.clearAllSteps();
            this.showPersonalInformation = true;
            this.currentStep = 2;
            this.determineOriginalStep();
        }

    }

    saveSymptoms(event) {
        console.log('Consent=> ', event.detail.status);
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.clearAllSteps();
          //  this.showPriorExposure = true;
          this.showAppointmentPreference = true;
            this.currentStep = 4;
            this.determineOriginalStep();
        }
    }

     priorSymptomsSave(event) {
        console.log('Consent=> ', event.detail.status);
       if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
             title: '',
             variant: 'error',
              message: event.detail.message,
          }));
         } else {
             if (this.hideStep3 == true) {
                this.clearAllSteps();
               //  this.showAppointmentPreference = true;
               this.showSymptoms = true;
                 this.currentStep = 4;
                 this.determineOriginalStep();
             } else {
                 this.clearAllSteps();
                // this.showAppointmentPreference = true;
                this.showSymptoms = true;
                 this.currentStep = 4;
                 this.determineOriginalStep();
             }
        }
     }

    saveContacts(event) {
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.contactId = event.detail.contactId;
            console.log('In Intake passed On contactId>>', this.contactId);
            this.clearAllSteps();
            this.currentStep = 3;
            this.determineOriginalStep();
            // this.showAppointmentPreference = true;
          //  this.showSymptoms = true;
              this.showInsurance = true;
        }

    }

    saveInsuranceDetails(event) {
        if (event.detail.status == "error") {
            this.dispatchEvent(new ShowToastEvent({
                title: '',
                variant: 'error',
                message: event.detail.message,
            }));
        } else {
            this.clearAllSteps();
            this.currentStep = 4;
            this.determineOriginalStep();
            // this.showAppointmentPreference = true;
          //  this.showSymptoms = true;
              this.patientHistory = true;
        }

    }
/*---------------Sprint 3 EnhanceMent--------------*/
//Story : S-11778

navigateToDashboard(event){
    this.clearAllSteps();
    this.isDependent = false;
    this.showDashboard = true;
}

addMember(event){
    console.log('in intakeAdd',event.detail);
    this.isDependent = event.detail;
    this.clearAllSteps();
    this.currentStep = 1;
    this.originalStep = 1;
    this.patientHistory = false;
    this.showDashboard = false;
    this.showAppointmentPreference = false;
    this.showInsurance = false;
    this.showConsent = true;
}
}