import { LightningElement, api, track } from 'lwc';
import updateAppointmentrec from '@salesforce/apex/DC_ScheduleAppointment.updateAppointment';
import getAppointmentDetailrec from '@salesforce/apex/DC_ScheduleAppointment.getAppointmentDetail';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class OkvriuScheduleAppointment extends LightningElement {
    @api recordid;
    @api ifcovid = false;
	@track options = [];
	@track selectedOption;
	@track isAttributeRequired = false;
	@api fieldName;
	@track contactid;
	@api objectName;
    @track fieldLabelName;
    @track accountList = [];
    @track isSiteSelected = false;
    @track preferredTime;
    @track preferredDate;
    @track buttonLabelValue;
    @api selectvalue;

    @track slotId;
    @track showSchedule = false;

    @track showMapModal = false;

	connectedCallback() {
        console.log('+++++++' + this.ifcovid )
        if(this.ifcovid){
            this.buttonLabelValue = 'Schedule & Submit';
        }else {
            this.buttonLabelValue = 'Schedule Appointment';
        }
        console.log('###@ accountId', this.selectvalue);
        console.log('###@ appointment ', this.recordid);
        getAppointmentDetailrec({appointmentId: this.recordid})
        .then(data => {
            let appointment = data.appointment;
            this.contactid = appointment.Patient__c;
            this.preferredTime = this.msToTimeAMPM(appointment.Patient__r.Preferred_Time__c);
            this.preferredDate = appointment.Patient__r.Preferred_Date__c;
            console.log(this.preferredTime);
            console.log(this.preferredDate);
            if(appointment.Status__c == 'In Progress' || appointment.Status__c == 'Cancelled' || appointment.Status__c == 'Scheduled' || appointment.Status__c == 'To Be Scheduled' || appointment.Status__c == 'Eligible' || appointment.Status__c == null || typeof appointment.Status__c == 'undefined') {
                // this.retrieveTestingSites();
                this.isSiteSelected = true;
            } else {
                this.dispatchEvent(new CustomEvent('close', {}));
                const event = new ShowToastEvent({
                    title: 'Error!',
                    variant: 'error',
                    message: 'You are not allowed to update this appointment.',
                });
                this.dispatchEvent(event);
            }
        })
        .catch(error => {
            console.log('Error-->' + error);
        });
    }
    
    selectionChangeHandler(event) {
        this.isSiteSelected = true;
        this.selectvalue = event.target.value;
    }
    setId(event) {
        this.showSchedule = true;
        this.slotId = event.detail;
    }
    disableButton(event) {
        this.showSchedule = false;
        //this.slotId = event.detail;
    }
    createAppointmentRecord() {
        
        updateAppointmentrec({ accountId : this.selectvalue, appointmentId : this.recordid, slotId : this.slotId}).then(result => {
            // this.createCalendar(result);
            this.dispatchEvent(new CustomEvent('close', {
                detail: 'save'
            }));
        })
        .catch(error => {
            console.log('Error-->' + JSON.stringify(error));
        });
        const event = new ShowToastEvent({
            title: 'Success!',
            variant: 'success',
            message: 'Your appointment has been scheduled.',
        });
        this.dispatchEvent(event);
        
    }
    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }
    justCloseModal(){
        this.dispatchEvent(new CustomEvent('close', {
            detail: 'close'
        }));
    }

    handleMapModal() {
        this.showMapModal = true;
    }

    handleMapModalClose(event) {
        this.showMapModal = false;
        
        if(event.detail != 'close') {
            this.selectvalue = event.detail
        }
        
    }

    msToTimeAMPM(s) {
        if(s == '' || s == null || typeof s == 'undefined')
            return '';
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        var hrs = (s - mins) / 60;
        let radian = 'AM';
        if(hrs > 12) {
            radian = 'PM';
            hrs = hrs - 12;
        }
        return this.pad(hrs) + ':' + this.pad(mins) + ' '  + radian;
    }

    pad(n, z) {
        z = z || 2;
        return ('00' + n).slice(-z);
    }
    
}