import {
    LightningElement,
    api,
    track
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent'



export default class OkpcPatientSelectModalToScheduleAppointment extends LightningElement {
    @api contactList;
    @track value = '';
    @track labelName = '';
    @track errorMessage = '';
    @track isValidated = true;

    get options() {
        let optionList = [];
        optionList.push({
            label: 'Select Person',
            value: ''
        });
        if (this.contactList && this.contactList.length) {
            this.contactList.forEach(con => {
                optionList.push({
                    label: con.name,
                    value: con.id
                });
            });
        }
        return optionList;
    }

    handleChange(event) {
        console.log('change: ' + event.detail.value);
        console.log('handleChange: ' + event.currentTarget.value);
        this.value = event.currentTarget.value;
    }

    handleClick(event) {
        switch (event.currentTarget.name) {
            case 'submit':
                if (this.value) {
                  this.dispatchEvent(new CustomEvent('submit', {
                     bubbles: true,
                     composed: true,
                     detail: this.value
                            }));
                }else{
                    const event = new ShowToastEvent({
                        variant: 'error',
                        title: '',
                        message: 'Please select a contact to proceed',
                    });
                    this.dispatchEvent(event);
              
                }
                   
                break;

            default:
                this.dispatchEvent(new CustomEvent('close'));
                break;
        }
        
    }
}