import { LightningElement, track, api } from "lwc";
import appTitle from '@salesforce/label/c.OKPC_Location_Map_Title';
import getTestingSites from "@salesforce/apex/OKPC_LocationMapController.getTestingSites";

export default class OkpcOpenMap extends LightningElement {
    label = {
        appTitle
    }

    @api mapMarkers = [];
    @track isLoaded = false;
    @track center = {
        location: {
            State: 'OK',
        },
    };
    @track zoomLevel = 9;
    @track showFooter = true;
    @track listView = 'auto';
    @track error;
    @track selectedMarkerValue = '';
    @track mapIdToAccount = {};

    connectedCallback() {
        // this.handleLoad();
        if(this.mapMarkers){
            this.mapMarkers.forEach((marker) => {
                this.mapIdToAccount[marker.value] = marker.title;
            });
            this.isLoaded = true;
        }
    }

    handleLoad() {
        getTestingSites( {})
            .then(result => {
                // Markers
                this.mapMarkers = JSON.parse(JSON.stringify(result));
                this.mapMarkers.forEach((marker) => {
                    this.mapIdToAccount[marker.value] = marker.title;
                });

                // All done, load the page
                this.isLoaded = true;
            })
            .catch(error => {
                this.error = error;
                this.isLoaded = false;
            });
    }

    @api
    get selectedSite(){
        return this.selectedMarkerValue ? this.mapIdToAccount[this.selectedMarkerValue] : '';
    }

    handleMarkerSelect(event) {
        this.selectedMarkerValue = event.target.selectedMarkerValue;
    }

    handleSaveModal() {
        this.dispatchEvent(new CustomEvent('closesingle', {
            detail: this.selectedMarkerValue
        }));
    }

    handleCloseModal() {
        this.dispatchEvent(new CustomEvent('closesingle', {
            detail: 'close'
        }));
    }
}