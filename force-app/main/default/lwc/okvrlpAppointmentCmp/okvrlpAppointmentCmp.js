import { LightningElement, track } from 'lwc';
import getTestingSite from '@salesforce/apex/OKPCHeaderController.getUserDetails';
import formFactorPropertyName from '@salesforce/client/formFactor';

export default class OkvrlpAppointmentCmp extends LightningElement {
    get isMobile() {
        return formFactorPropertyName != 'Large';
    }
    @track columns = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Date',name:'Appointment_Slot__r.Formatted_Date__c',type:'text',action:'',class:'custom-hide-comp'},
        {label:'Time',name:'Appointment_Slot__r.Start_Time__c',type:'time',action:'',class:'custom-hide-comp'},
        {label:'Action',name:'',type:'buttons',action:'Schedule',varient:'brand',buttons:[{label:'Re-Schedule',class:'custom-hide-comp paginator-button'},{label:'Complete',class:'paginator-button'},{label:'Cancel',class:'custom-hide-comp paginator-button'}]}
    ];
    @track whereClause;
    @track showPaginator = false; 

    @track columns2 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Preferred Date',name:'Patient__r.Formatted_preferred_Date__c',type:'text',action:''},
        {label:'Preferred Time',name:'Patient__r.Preferred_Time__c',type:'time',action:''},
        {label:'Action',name:'',type:'button',action:'Schedule',varient:'brand',buttonlabel:'Schedule'}
    ];
    @track whereClause2;
    @track showPaginator2 = false;

    @track columns3 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Preferred Date',name:'Patient__r.Formatted_preferred_Date__c',type:'text',action:''},
        {label:'Preferred Time',name:'Patient__r.Preferred_Time__c',type:'time',action:''},
        {label:'Action',name:'',type:'button',action:'Schedule',varient:'brand',buttonlabel:'Schedule'}
    ];
    @track columns4 = [ 
        {label:'Patient Name',name:'Patient__r.Name',type:'text',action:''},
        {label:'DOB',name:'Patient__r.formatted_Date_of_Birth__c',type:'text',action:''},
        {label:'Patient Id',name:'Patient__r.Patient_Id__c',type:'text',action:''},
        {label:'Completed Date',name:'Formatted_Complete_Date__c',type:'text',action:''}
    ];
    @track whereClause4;
    @track whereClause3;
    @track showPaginator3 = false;
    
    @track resetTable = false;
    @track reloadTable = false;
    @track showCalendar = false;
    @track showConfirmation = false;
    @track appointmentId;
    @track accountId;
    @track modalType = '';
    selectedAppointmentIds = [];
    @track showBatchModal =false;
    
    get type() {
        return this.modalType;
    }

    connectedCallback() {
       getTestingSite({})
        .then(data => {
            if(data.ContactId) 
                this.accountId = data.Contact.AccountId;
            if(data.ContactId && data.Contact.AccountId) {
                this.whereClause = ' Status__c = \'Scheduled\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause2 = ' Status__c = \'To Be Scheduled\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause3 = ' Status__c = \'Eligible\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
                this.whereClause4 = ' Status__c = \'Completed\' AND Patient__r.Testing_Site__c = \'' + this.accountId + '\' AND Patient__r.Testing_Site__c != null';
            } else {
                this.whereClause = ' Id = null ';
                this.whereClause2 = ' Id = null ';
                this.whereClause3 = ' Id = null ';
                this.whereClause4 = ' Id = null ';
            }
            this.showPaginator = true;
        })
        .catch(error => {
            console.log('Error-->' + error);
        });
    }

    handlePaginatorAction(event) {
        let data = event.detail;
        console.log(data);
        if(data.actionName == 'Re-Schedule' || data.actionName == 'Schedule') {
            this.appointmentId = data.recordId;
            this.showCalendar = true;
        } else if(data.actionName == 'Complete') {
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'complete';
        } else if(data.actionName == 'Cancel') {
            this.appointmentId = data.recordId;
            this.showConfirmation = true;
            this.modalType = 'cancel';
        }
    }
    hideModal(event) {
        this.showCalendar = false;
        this.showConfirmation = false;
        
        if(event.detail == 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        } else if(event.detail == 'complete' || event.detail == 'cancel') {
            this.template.querySelector('c-oktcgenericpaginator').retrieveRecords();
        }
    }

    resetFlag(event) {
        // this.resetTable = event.detail;
    }

    getSelectedIds(event){
        let temparr= [];
        temparr = [...event.detail]
        temparr.forEach(record =>{
            this.selectedAppointmentIds = [...this.selectedAppointmentIds, record];
        })
        this.showBatchModal =true;
    }

    hideBatchModal(event){
        this.showBatchModal =false;
        if(event.detail == 'save') {
            this.template.querySelectorAll('c-oktcgenericpaginator').forEach(element => {
                element.retrieveRecords();
            });
        }
    }
}