@isTest
public class okWrapperClassTest {

    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'OK';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Testing_Site_type__c = 'Rapid Testing';
        createSlot.Record_type__c = 'Citizen'  ;
        createSlot.Appointment_Count__c = 0;
        insert createSlot;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Testing_Site_Type__c = 'rapid Testing';
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
        
        Appointment_Slot__c createPCRSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createPCRSlot.Testing_Site_type__c = 'PCR Testing';
        createPCRSlot.Record_type__c = 'Citizen'  ;
        createPCRSlot.Appointment_Count__c = 0;
        insert createPCRSlot;
        Appointment__c  createPCRAppointment = TestDataFactoryAppointment.createAppointment(false);
        createPCRAppointment.Patient__c = con.Id;
        createPCRAppointment.Lab_Center__c = createAccountTestSite.id;
        createPCRAppointment.Appointment_Slot__c = createPCRSlot.id;
        createPCRAppointment.Testing_Site_Type__c = 'PCR Testing';
        createPCRAppointment.Status__c = 'Scheduled';

        insert createPCRAppointment;
        
        Insurance_Company_Details__c insCompanyDetail = TestDataFactoryInsuranceCompanyDetail.createInsuranceCompanyDetail('TestInsCompany', true, false, false, false);
       	insert insCompanyDetail;
        
        Insurance__c createInsurance = TestDataFactoryInsurance.createInsurance(con.Id,  false);
        createInsurance.Insurance_Type__c = 'Medicare';
        createInsurance.Contact__c = con.id;
        createInsurance.Select_the_Medicare_type__c = 'Part A';
        createInsurance.Do_you_have_a_Medicare_Supplement_plan__c = 'Yes';
        createInsurance.Primary_Insurance_Company_Name__c = insCompanyDetail.Id;
        createInsurance.Member_Id__c = '123';
        createInsurance.Relation_to_PolicyHolder__c = 'Self = 1';
        createInsurance.Policyholder_First_Name__c = 'TestF';
        createInsurance.Policyholder_Middle_Name__c = 'TestM';
        createInsurance.Policyholder_Last_Name__c = 'TestL';
        insert createInsurance;
    }
    
    
    public static testMethod void testMethod1(){
    
   Schema.DescribeSObjectResult r = Contact.sObjectType.getDescribe();
         List<String> apiNames =  new list<String>();
         for(string apiName : r.fields.getMap().keySet()){
            apiNames.add(apiName);
         }
    Schema.DescribeSObjectResult insr = Insurance__c.sObjectType.getDescribe();
         List<String> insApiNames =  new list<String>();
         for(string apiName : insr.fields.getMap().keySet()){
            insApiNames.add(apiName);
         } 
      List<Contact> contactList = Database.query('select ' + string.join(apiNames, ',') + ' from Contact WHERE lastName = \'portalTestUserv1\' LIMIT 1');  
    // Build a Dynamic Query String.
    List<Contact> c = Database.query('select ' + string.join(apiNames, ',') + ' from Contact');
    List<Insurance__c> ins = Database.query('select '+ string.join(insApiNames, ',') +',Primary_Insurance_Company_Name__r.Name,Primary_Insurance_Company_Name__r.Is_Group_Number_Required__c' + ' from Insurance__c');
     // List<Contact> c = [Select ID, Name FROM Contact LIMIT 1];
      okWrapperClass.ContactInformation wrapper = new okWrapperClass.ContactInformation();
          wrapper.getContact(c[0]);
          wrapper.getPatientHistoryDetails(c[0]);
          wrapper.getSymptoms(c[0]);
          wrapper.getCovidSymptoms(c[0]);
          wrapper.getPriorExposure(c[0]);
          wrapper.getInsuranceDetails(ins[0]);
          wrapper.saveContact(c[0]);
          wrapper.saveCovidResponses(c[0]);
          wrapper.saveCovidSymptoms(c[0]);
          wrapper.saveSymptoms(c[0]);
          wrapper.savePriorExposure(c[0]);
          wrapper.saveInsuranceDetails(ins[0]);
          okWrapperClass.getInsuranceDetails(contactList[0].id);
        okWrapperClass.saveInsuranceDetails(JSON.serialize(wrapper.getContact(contactList[0])), 3);
    }
    
}