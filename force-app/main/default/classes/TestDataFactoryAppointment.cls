public class TestDataFactoryAppointment {
    public static Appointment__c createAppointment(Boolean isInsert){
        Appointment__c appointment = new Appointment__c();
        appointment.Status__c = 'Scheduled';
        if(isInsert){
        insert appointment;
        return appointment;
    }
    return appointment;
    }
    
}