public without sharing class okLookUpController {
    
    @AuraEnabled(cacheable=true)
    public static List<ResultWrapper> searchDB(String searchText , Boolean isMedicare, Boolean isMedicaid){
        system.debug('searchText>'+ searchText);
        String query = '';
        searchText='\'OTHER - NOT LISTED OR *' + String.escapeSingleQuotes(searchText.trim()) + '*\'';
        system.debug('searchText<<<' + searchText);
        Boolean isOsecUser = false;
        
        query = 'FIND '+ searchText +' IN NAME FIELDS RETURNING Insurance_Company_Details__c (Id, Name,Is_Group_Number_Required__c,Medicaid__c,Medicare__c,Member_Id_Validation_Regex__c WHERE Medicare__c = :isMedicare AND Medicaid__c =: isMedicaid) LIMIT 10';
        
        system.debug('query'+query);
        List<List<SObject>> sobjList = Search.query(query);
         system.debug('sobjList>>'+sobjList);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList[0]) {
            ResultWrapper obj = new ResultWrapper();
            obj.objName = 'Insurance';
            obj.name = String.valueOf(s.get('Name'));
            obj.id = String.valueOf(s.get('Id'));
            obj.isGroupNumberRequired = boolean.valueOf(s.get('Is_Group_Number_Required__c'));
            obj.isMedicaid = boolean.valueOf(s.get('Medicaid__c'));
            obj.isMedicare = boolean.valueOf(s.get('Medicare__c'));
            obj.memberIdValidationRegex = String.valueOf(s.get('Member_Id_Validation_Regex__c'));
            obj.iconName = 'standard:contact';
            lstRet.add(obj);
        } 
        system.debug('lstRet>>' + JSON.serialize(lstRet));
        return lstRet ;
    }

    @AuraEnabled(cacheable=true)
    public static List<ResultWrapper> setDefaultPrimaryInsuranceCompany() {
        List<ResultWrapper> primaryInsCmpList = new List<ResultWrapper>();
        for(Insurance_Company_Details__c insCompany : [SELECT Id, Name, Is_Group_Number_Required__c, Medicaid__c, Medicare__c, Member_Id_Validation_Regex__c 
                                                       FROM Insurance_Company_Details__c 
                                                       WHERE (Name LIKE 'OKLAHOMA HEALTH CARE AUTHORITY (MEDICAID)%' 
                                                       AND Medicaid__c = true) 
                                                       OR (NAME LIKE 'OKLAHOMA MEDICARE (PART B ONLY)%' 
                                                       AND Medicare__c = true)]) {
            ResultWrapper obj = new ResultWrapper();
            obj.name = String.valueOf(insCompany.get('Name'));
            obj.id = String.valueOf(insCompany.get('Id'));
            obj.isGroupNumberRequired = boolean.valueOf(insCompany.get('Is_Group_Number_Required__c'));
            obj.memberIdValidationRegex = String.valueOf(insCompany.get('Member_Id_Validation_Regex__c'));
            obj.iconName = 'standard:contact';
            obj.isMedicaid = boolean.valueOf(insCompany.get('Medicaid__c'));
            obj.isMedicare = boolean.valueOf(insCompany.get('Medicare__c'));
            primaryInsCmpList.add(obj);
        }
        return primaryInsCmpList;
    }

  /*  @AuraEnabled(cacheable=true) 
    public static Map<String, Insurance_Company_Details__c> setDefaultPrimaryInsuranceCompany1(){
        Map<String, Insurance_Company_Details__c> primaryInsCmpMap = new Map<String, Insurance_Company_Details__c>();
        for(Insurance_Company_Details__c insCompany : [SELECT Id, Name, Is_Group_Number_Required__c, Medicaid__c, Medicare__c, Member_Id_Validation_Regex__c 
                                                       FROM Insurance_Company_Details__c 
                                                       WHERE Medicare__c = true
                                                       OR  Medicaid__c = true]) {
            // ResultWrapper obj = new ResultWrapper();
            // obj.name = String.valueOf(insCompany.get('Name'));
            // obj.id = String.valueOf(insCompany.get('Id'));
            // obj.isGroupNumberRequired = boolean.valueOf(insCompany.get('Is_Group_Number_Required__c'));
            // obj.memberIdValidationRegex = String.valueOf(insCompany.get('Member_Id_Validation_Regex__c'));
            // obj.iconName = 'standard:contact';

            String mapKey = '';
            if(boolean.valueOf(insCompany.get('Medicaid__c')) == true) {
                mapKey = 'Medicaid';
            } else if(boolean.valueOf(insCompany.get('Medicare__c')) == true){
                mapKey = 'Medicare';
            }
            
            primaryInsCmpMap.put(mapKey, insCompany);
        }
        return primaryInsCmpMap;
    }
*/

    public class ResultWrapper{
        @AuraEnabled  public String objName {get;set;}
        @AuraEnabled  public String name{get;set;}
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public Boolean isGroupNumberRequired{get;set;}
        @AuraEnabled  public Boolean isMedicaid{get;set;}
        @AuraEnabled public Boolean isMedicare{get;set;}
        @AuraEnabled  public String memberIdValidationRegex{get;set;}
        @AuraEnabled public String iconName {get;set;}
    }
}