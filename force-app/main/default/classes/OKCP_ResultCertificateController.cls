public class OKCP_ResultCertificateController {
    public Contact con{get;set;}
    public OKCP_ResultCertificateController() {
        String contactId = ApexPages.CurrentPage().getparameters().get('id');
        con = new Contact();
        System.debug('## '+contactId);
        if(String.isNotBlank(contactId)){
            
            List<Contact> contacts = [SELECT Id, Name FROM Contact WHERE Id = :contactId];
            if(!contacts.isEmpty()){
                con = contacts[0];
            }
        }
    }
}