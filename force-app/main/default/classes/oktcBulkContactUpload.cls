public without sharing  class oktcBulkContactUpload {
    private static Map < String, Schema.RecordTypeInfo > recordTypeInfosMapContact = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    private static Map < String, Schema.RecordTypeInfo > recordTypeInfosMapAccount = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    
    @AuraEnabled
    public static  Map<String, Object> insertData(String strfromle,string recordType ,String testingSiteId, Boolean smsOpt, Boolean emailOpt, String testingSiteType){
        Map<String, Object> result = new Map<String, Object>();
        Map<String,String> accountNameIdMap = new Map<String,String>();
        Map<String,String> contactNameIdMap = new Map<String,String>();
        List<String> newAccount = new List<String>();
        List<String> DupliacteContact = new List<String>();
        List<Contact> contactListInsert = new List<Contact>();
        
        Map<String, Object> response = new Map<String, Object>();
        
        List<fieldWrapper> duplicateData = new List<fieldWrapper>(); 
         Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
         Profile_Name__c, Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE Contact_RT_Developer_Name__c =: recordType LIMIT 1];
        List<Account> citizenAccountList = [SELECT ID,Name FROM Account WHERE RecordTypeId = :recordTypeInfosMapAccount.get('Citizen').RecordTypeId]; 
        for(Account selectedAccount : citizenAccountList){
            if(!accountNameIdMap.containsKey(selectedAccount.Name)){
                accountNameIdMap.put(selectedAccount.Name.toLowercase(), selectedAccount.Id);
            }
        }
        List<Contact> contactList = [Select Id,FirstName, LastName, Name,Email From Contact Where  RecordTypeId = :recordTypeInfosMapContact.get(recordType).RecordTypeId];
        system.debug('contactList *** ' + contactList);
        for(Contact selectedContact : contactList){
            
                if(selectedContact.Email != null && !contactNameIdMap.containsKey(selectedContact.Email)){
                    contactNameIdMap.put((selectedContact.Email).toLowercase(), selectedContact.Id);
                }else{
                    contactNameIdMap.put((selectedContact.FirstName).toLowercase()+ (selectedContact.LastName).toLowercase(), selectedContact.Id);
                }
        }
        system.debug('Contact Map>>>' + contactNameIdMap);
        List<fieldWrapper> datalist = (List<fieldWrapper>)JSON.deserialize(strfromle, List<fieldWrapper>.class);
        for(fieldWrapper wrapper : datalist){
            if(wrapper.organizationName != null && !String.isBlank(wrapper.organizationName)){
                //to check if account is aready in system.
                if(!accountNameIdMap.containsKey(wrapper.organizationName.toLowercase())){
                    //to check if account is being repeated in the CSV again and again. 
                    if(!newAccount.contains(wrapper.organizationName)){
                        newAccount.add(wrapper.organizationName);
                    }
                } 
            }
        }
        if(newAccount.size() > 0){
            List<Account> newCreatedAccount = createAccount(newAccount);
            for(Account a : newCreatedAccount){
                if(!accountNameIdMap.containsKey(a.Name)){
                    accountNameIdMap.put(a.Name.toLowercase(),a.Id);
                }
            }
        } 
        
        List<String> fields = new List<String>{'firstName', 'lastName', 'email', 'phone'};
        List<String> race = fetchPicklist('Race__c', 'Contact');
        List<String> gender = fetchPicklist('Gender__c', 'Contact');
        List<String> ethnicity = fetchPicklist('Ethnicity_Hispanic_Origin__c', 'Contact');
        List<String> symptomatic = fetchPicklist('Symptomatic__c', 'Appointment__c');
        List<String> positiveCase = fetchPicklist('Direct_Contact_with_Positive_Case__c', 'Appointment__c');
        Map<Integer, fieldWrapper> randomNumberToDataMap = new Map<Integer, fieldWrapper>();   
        Integer random = Integer.valueof((Math.random() * 1000));
        Integer totalCount = datalist.size();
        
        for(fieldWrapper wrapper : datalist){
       // system.debug('validateRecord(wrapper)' +validateRecord(wrapper));
            
            String message = '';
            List<String> blankError = new List<String>();
            List<String> invalidValueError = new List<String>();
            
            if(String.isBlank(wrapper.firstName) && String.isBlank(wrapper.lastName) && String.isBlank(wrapper.email) && String.isBlank(wrapper.gender) && String.isBlank(wrapper.dateOfBirth) && String.isBlank(wrapper.ethnicity) && String.isBlank(wrapper.positiveCase) && String.isBlank(wrapper.mobile) && String.isBlank(wrapper.symptomatic)){
                --totalCount;
                continue;
            }
            
            if(String.isBlank(wrapper.firstName)){
                blankError.add('first name');
            }
                
            if(String.isBlank(wrapper.lastName))
                blankError.add('last name');
            if(String.isBlank(wrapper.race))
                blankError.add('race');
            if(String.isBlank(wrapper.gender))
                blankError.add('gender');
            if(String.isBlank(wrapper.dateOfBirth))
               blankError.add('date of birth');
               
               
           if(testingSiteType != 'Antibody Testing'){
               if(String.isBlank(wrapper.ethnicity))
                   blankError.add('ethnicity');
               if(String.isBlank(wrapper.symptomatic))
                   blankError.add('symptomatic');            
               if(String.isBlank(wrapper.positiveCase))
                   blankError.add('direct contact with Positive Case (2-14 days)');
           }
            
                         
            if(!blankError.isEmpty())
                message += 'Values for following fields are mandatory :  ' + String.join(blankError, ', ') + '. ';
            
            if(String.isNotBlank(wrapper.race) && !race.contains(wrapper.race.toLowerCase()))
                invalidValueError.add('Race');
            if(String.isNotBlank(wrapper.gender) && !gender.contains(wrapper.gender.toLowerCase()))
                invalidValueError.add('Gender');
                
            if(testingSiteType != 'Antibody Testing'){
                if(String.isNotBlank(wrapper.ethnicity) && !ethnicity.contains(wrapper.ethnicity.toLowerCase()))
                    invalidValueError.add('Ethnicity');
                if(String.isNotBlank(wrapper.symptomatic) && !symptomatic.contains(wrapper.symptomatic.toLowerCase()))
                    invalidValueError.add('Symptomatic');
                if(String.isNotBlank(wrapper.positiveCase) && !positiveCase.contains(wrapper.positiveCase.toLowerCase()))
                    invalidValueError.add('Positive Case');
            }
            
            if(!invalidValueError.isEmpty())
                message += 'Not a valid value for ' + String.join(invalidValueError, ', ');
              
            if(String.isNotBlank(wrapper.dateOfBirth) && getFormattedDate(wrapper.dateOfBirth) == null)
               message += 'Not a valid value for birthdate ';
            
            if(!String.isBlank(wrapper.firstName) && !String.isBlank(wrapper.lastName)){
                if(!String.isBlank(wrapper.email)){
                if(contactNameIdMap.containsKey(wrapper.email.toLowercase()) || DupliacteContact.contains(wrapper.email.toLowercase())){
                    message = 'Contact of same details already present in system';  
                }
                
                }else if(String.isBlank(wrapper.email) || wrapper.email == ''){
                    if(contactNameIdMap.containsKey(wrapper.firstName.toLowercase()+wrapper.lastName.toLowercase()) 
                    || DupliacteContact.contains(wrapper.firstName.toLowercase()+wrapper.lastName.toLowercase())){
                        message = 'Contact of same details already present in system';  
                    } 
                }
            }
            
         
            if(String.isNotBlank(message)){
                wrapper.status = 'Failed';
                wrapper.reason = message;
            }else{
                
                     
                     Contact c = new Contact();
                     c.FirstName = wrapper.firstName;
                     c.LastName = wrapper.lastName;
                     c.Email = wrapper.email;
                     c.MobilePhone = wrapper.mobile;
                     c.Gender__c = wrapper.gender;  
                     c.Race__c =   wrapper.race ; 
                     c.Auto_Scheduled__c = true;
                     c.Form_Submitted__c = true;
                     c.Opt_out_for_SMS_Updates__c = !smsOpt;
                     c.HasOptedOutOfEmail = !emailOpt;
                     c.Birthdate = getFormattedDate(wrapper.dateOfBirth);
                     if(testingSiteType == 'Antibody Testing')
                        c.Testing_Site__c = testingSiteId;
                    if(!String.isBlank(wrapper.organizationName) && accountNameIdMap.containskey( wrapper.organizationName.toLowercase())){
                                   c.AccountId = accountNameIdMap.get(wrapper.organizationName.toLowercase());
                    }else{
                        c.AccountId = settings[0].Account_Id__c;
                    }
                    c.RecordTypeId = recordTypeInfosMapContact.get(recordType).RecordTypeId;
                    c.Random_Number__c = random;
                    randomNumberToDataMap.put(random, wrapper);
                    contactListInsert.add(c);
                    
                    if((wrapper.email) != null && wrapper.email != '')
                        DupliacteContact.add(wrapper.email);
                    else{
                        DupliacteContact.add(wrapper.firstName.toLowerCase() + wrapper.lastName.toLowerCase());
                    }
                    wrapper.status = 'Success';
                    wrapper.reason = 'Uploaded Successfully !';
                    ++random;                  
                }                         
            }
           
       response.put('data', datalist); 
       System.debug('**data ** '+ datalist);
       System.debug('**DupliacteContact '+ DupliacteContact);
        
       
        if(!contactListInsert.isEmpty()){
            try{
                insert contactListInsert;
                
                if(testingSiteType != 'Antibody Testing'){
                    List<Appointment__c> appointments = new List<Appointment__c>();
                    for(Contact contact : contactListInsert){
                        Appointment__c app = new Appointment__c();
                        app.Status__c = 'Scheduled';
                        app.Patient__c = contact.Id;
                        app.Testing_Site_Type__c = testingSiteType;
                        app.Testing_Site_Account__c = testingSiteId;
                        app.Appointment_Start_Date_v1__c = System.today();
                        app.Appointment_Start_Time_v1__c = Datetime.now().time();
                        if(randomNumberToDataMap.containsKey(Integer.valueOf(contact.Random_Number__c))){
                           fieldWrapper data =  randomNumberToDataMap.get(Integer.valueOf(contact.Random_Number__c));
                            app.Symptomatic__c = data.symptomatic;
                            app.Direct_Contact_with_Positive_Case__c = data.positiveCase;
                        }
                        appointments.add(app);                   
                    }
                    if(!appointments.isEmpty())
                        insert appointments;
                }               
             } catch(Exception ex){
                response.put('success', false);
                response.put('message', 'Could not upload file');
            }
        }
        
        Integer successCount = !contactListInsert.isEmpty() ? contactListInsert.size() : 0;
        response.put('success', true);
        response.put('message', successCount + ' out of ' + totalCount + ' contacts uploaded successfully !') ;            
     
        return response;
    }
    
    public static List<Account> createAccount(List<String> newAccountlist){
        List<Account> newAccounts = new List<Account>();
        for(String s : newAccountlist){
            Account acc = new Account();
            acc.name = s;
            acc.RecordTypeId = recordTypeInfosMapAccount.get('Citizen').RecordTypeId;
            acc.OwnerId = System.Label.System_Admin_Id;
            newAccounts.add(acc);
        }
        if(newAccounts.size() > 0){
            insert newAccounts;
        }
        return newAccounts;
    }
    
 /*   public static Boolean getValue(List<String> options, String str){
        if(options.contains(str))
            return true;
        return false;
        //List<String> options = fetchPicklist(fieldName, 'Contact');
        //str = str.trim();
        //system.debug('value --> ' + str + ' fieldName --> ' + fieldName);
        
    }
*/    
    public static List<String> fetchPicklist(String fieldName, String objectName ) {
        List <String> opts = new List <String> ();
        
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult r = s.getDescribe();
        Map < String, Schema.SObjectField > fields = r.fields.getMap();
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry pickListVal: ple) {
            opts.add(pickListVal.getLabel().toLowerCase());
        }
        
        return opts;
    }
    
    
    public static date getFormattedDate(String input){
        try{
            String str = input.substring(6,10)+'-'+input.substring(0,2)+'-'+input.substring(3,5);
            return date.valueOf(str);
        }catch(Exception ex){ return null; }

    }
 /*   public static Boolean validateRecord(fieldWrapper data){
        try{
            system.debug('firstNAme ##### ' + data.firstName);
            String str = data.firstName;
            return true;
        }catch(Exception ex){
            return false;
        }

    }*/
    
    public class fieldWrapper{ 
       // @AuraEnabled  public String contactId {get; set;}       
        @AuraEnabled  public String firstName {get; set;}
        @AuraEnabled  public String lastName {get; set;}
        @AuraEnabled  public String email {get; set;}
        @AuraEnabled  public String mobile {get; set;}
        @AuraEnabled  public String gender {get; set;}
        @AuraEnabled  public String dateOfBirth {get; set;}
        @AuraEnabled  public String race {get; set;}
        @AuraEnabled  public String organizationName {get; set;}
        @AuraEnabled  public String status {get; set;}
        @AuraEnabled  public String reason {get; set;}
        @AuraEnabled  public String ethnicity {get; set;}
        @AuraEnabled  public String symptomatic {get; set;}
        @AuraEnabled  public String positiveCase {get; set;}
        
    }
    
    
}