/**
 * Created by ashishPandey on 10-05-2020.
 */

public class AntibodyTestingSendConsentBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{

    public final String Query;
    public final List<Id> recordIdList;
    public final String smsbody;

    public AntibodyTestingSendConsentBatch(String query, List<Id> recordIdList, String body){
        this.recordIdList = recordIdList;
        this.Query=query + ' WHERE ID IN :recordIdList';
        this.smsbody = body;
        system.debug('fnjf');
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        if(scope.size()>0){
            String body = smsbody;
            Boolean sendSMS = false;
            System.debug('smsbody>>'+smsbody);
           /* if(smsbody == 'AntiBody'){
                body = 'Oklahoma State Department of Health (OSDH): Thank you for providing your mobile number. As discussed when you were scheduled for COVID-19 testing site, OSDH would like to text you information related to your COVID-19 testing. Do you agree to receive and respond to messages from OSDH? Carrier charges may apply. (Please reply Y or N)';
                sendSMS = true;
            }
            if(smsbody == 'wrongReply'){
                body = 'Please reply Y or YES for yes, and N or NO for no.';
                sendSMS = true;
            }
            //Result update and Communication OKU COVID
            if(smsbody == 'SendResult'){
                System.debug('inside send result batch Communication OKU COVID');
                List<Antibody_Testing__c> sendResults = new List<Antibody_Testing__c>();
                sendResults = [Select Id,Results__c,Patient__r.MobilePhone,Patient__r.Name_Initial__c,Patient__r.Birthdate,Patient__r.MobileTrimmer__c,Patient__r.FirstName,Patient__r.LastName from Antibody_Testing__c where Patient__r.MobilePhone=:String.valueOf(scope[0].get('MobilePhone'))];
                if(sendResults.size()>0){
                    sendSMS = true;
                    body =   'You have a message waiting for you on the secure '+String.valueOf(scope[0].get('Community_Link__c')) +'. Please  log in to retrieve your message.';
                }
            }
            ////Send Result for COVID Results
            if(smsbody == 'TestResult'){
                String dateString='';
                List<Antibody_Testing__c> result = new List<Antibody_Testing__c>();
                result = [Select Id,Results__c,Patient__r.MobilePhone,Patient__r.Name_Initial__c,Patient__r.Birthdate,Patient__r.MobileTrimmer__c,Patient__r.FirstName,Patient__r.LastName from Antibody_Testing__c where Patient__r.MobilePhone=:String.valueOf(scope[0].get('MobilePhone')) AND Patient__r.Birthdate!=null];
                if(result.size()>0){
                    DateTime dt = result[0].Patient__r.Birthdate;
                    dateString = dt.formatGMT('MM/dd/yyyy');
                    System.debug('Results__c>>>'+result);

                    if(result[0].Results__c=='Rejected'){
                        sendSMS = true;
                        body =   'Test rejected.  Please call your local county health department or reschedule on the patient portal '+String.valueOf(scope[0].get('Community_Link__c'));
                    }
                    if(result[0].Results__c=='Negative'){
                        sendSMS = true;
                        body =   'We have received your test results, '+result[0].Patient__r.Name_Initial__c +' with a DOB of '+dateString +''+
                                ' and it is Negative, '+
                                'If you begin to experience symptoms, such as cough, fever and runny nose, please contact your health care provider. For more information go the OSDH website: https://coronavirus.health.ok.gov/';
                    }
                    if(result[0].Results__c=='Positive'){
                        sendSMS = true;
                        body =   'We have received your test results, '+result[0].Patient__r.Name_Initial__c +' with a DOB of '+dateString +''+
                                ' and it is '+result[0].Results__c+', '+
                                'Except to get medical care, it is important for you to stay home and away from the public for the safety of others. You will be contacted by someone from the Texas CHD or OSDH with additional instructions. For more information go the OSDH website: https://coronavirus.health.ok.gov/';
                    }
                }else{
                    result = [Select Id,Results__c,Patient__r.MobilePhone,Patient__r.Name_Initial__c,Patient__r.Birthdate,Patient__r.MobileTrimmer__c,Patient__r.FirstName,Patient__r.LastName from Antibody_Testing__c where Patient__r.MobilePhone=:String.valueOf(scope[0].get('MobilePhone')) AND Results__c!=null];
                    if(result.size()>0){
                        if(result[0].Results__c=='Rejected'){
                            sendSMS = true;
                            body =   'Test rejected.  Please call your local county health department or reschedule on the patient portal '+String.valueOf(scope[0].get('Community_Link__c'));
                        }
                        if(result[0].Results__c=='Negative'){
                            sendSMS = true;
                            body =   'We have received your test results, '+result[0].Patient__r.Name_Initial__c +' with a DOB of '+dateString +''+
                                    ' and it is Negative, '+
                                    'If you begin to experience symptoms, such as cough, fever and runny nose, please contact your health care provider. For more information go the OSDH website: https://coronavirus.health.ok.gov/';
                        }
                        if(result[0].Results__c=='Positive'){
                            sendSMS = true;
                            body =   'We have received your test results, '+result[0].Patient__r.Name_Initial__c +' with a DOB of '+dateString +''+
                                    ' and it is '+result[0].Results__c+', '+
                                    'Except to get medical care, it is important for you to stay home and away from the public for the safety of others. You will be contacted by someone from the Texas CHD or OSDH with additional instructions. For more information go the OSDH website: https://coronavirus.health.ok.gov/';
                        }
                    }else {
                        sendSMS = true;
                        body='Result is not available yet.';
                    }
                }
            }*/
            if(smsbody == 'resultSMS'){
                body = 'You have a message waiting for you on the patient portal ' + System.Label.OKCP_Login_URL + '. Please log in to retrieve your message.';
                sendSMS = true;
            }
            if(smsbody == 'faultyResultSMS'){
                body = 'Test rejected.  Please call your local county health department or reschedule on the patient portal ' + System.Label.OKCP_Login_URL + '.';
                sendSMS = true;
            }
            
            if(sendSMS){
                System.debug('inside send sms');
                TwilioSendSMS.sendSms(String.valueOf(scope[0].get('MobilePhone')), body);
            }
        }
    }
    public void finish(Database.BatchableContext BC){

    }

}