@isTest
public class okCompanyInformationIntegration_Test {
	
    static testMethod void insertCompanyInformation(){
        String body = '[{ "privateInsuranceID": 2, "comboBoxDesc": "INSURANCE MANAGEMENT SERVICES  (IMSMS)", "carrierCode": "IMSMS", "isMedicaid": false, "isMedicare": false, "isOther": false, "isGroupNumberRequired": false,"memberIdValidationRegex": "","privateINsuranceDesc": "INSURANCE MANAGEMENT SERVICES", "resubmitWithOriginalStatus": 0,"claimFilingIndicatorCode": "CH","publicPayerCode": null,"streetAddress": "PO BOX 15688","city": "AMARILLA", "state": "TX", "zip": "79105" }]';
   		Map<String,Map<String,Object>> responseMap = new Map<String,Map<String,Object>>();
        responseMap.put('https://privateinsurance.azurewebsites.net/api/PrivateInsurance', OKHS_MockGenerator.retriveResponseBody(body, '200'));
        Test.setMock(HttpCalloutMock.class, new OKHS_MockGenerator(responseMap));
        Test.startTest();
      // okCompanyInformationIntegration sh = new okCompanyInformationIntegration();
        //  Database.executeBatch(new okCompanyInformationIntegration());
         okCompanyInformationIntegration c = new okCompanyInformationIntegration();
        String sch = '0 0 2 * * ?'; 
    system.schedule('create slot batch', sch, c); 
        Test.stopTest();
    }
}