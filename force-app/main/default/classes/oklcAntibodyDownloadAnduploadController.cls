public without sharing class oklcAntibodyDownloadAnduploadController {
    
      @AuraEnabled
    public static List<OptionWrapper> retrieveBatch(String labId){
        List<OptionWrapper> options = new List<OptionWrapper>();
        options.add(new OptionWrapper('--None--', ''));
        Set<Id> batchListId = new Set<Id>();
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Id citizenCovidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
        for(Antibody_Testing__c antibody : [SELECT Id, Batch__c, Batch__r.name FROM Antibody_Testing__c WHERE Results__c = null AND Laboratory__c = :labId AND batch__r.status__c = 'Unprocessed' AND (RecordTypeId =: antibodyRecordTypeId OR RecordTypeId =: citizenCovidRecordTypeId) ORDER By Batch__r.name desc]){
            if(!batchListId.contains(antibody.Batch__c)){
                batchListId.add(antibody.Batch__c);
                options.add(new OptionWrapper(antibody.Batch__r.name, antibody.Batch__c));
            }   
        }
        // for(Batch__c batchList: [SELECT Id, Name FROM Batch__c WHERE Status__c = 'Unprocessed' AND laboratory__c =: labId ]) {
        //     options.add(new OptionWrapper(batchList.Name, batchList.Id));
        // }
        return options;
    }

    @AuraEnabled
    public static List<OptionWrapper> retrieveBatchforTestingSite(String testingSiteId){
        List<OptionWrapper> options = new List<OptionWrapper>();
        options.add(new OptionWrapper('--None--', ''));
        Set<Id> batchListId = new Set<Id>();
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Id citizenCovidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
        for(Antibody_Testing__c antibody : [SELECT Id, Batch__c, Batch__r.name FROM Antibody_Testing__c WHERE  Testing_Site__c = :testingSiteId AND Results__c = null AND Batch__c != null AND (RecordTypeId =: antibodyRecordTypeId OR RecordTypeId =: citizenCovidRecordTypeId) ORDER BY Batch__c DESC]){
            if(!batchListId.contains(antibody.Batch__c)){
                batchListId.add(antibody.Batch__c);
                options.add(new OptionWrapper(antibody.Batch__r.name, antibody.Batch__c));
            }   
        }
        // for(Batch__c batchList: [SELECT Id, Name FROM Batch__c WHERE Status__c = 'Unprocessed' AND laboratory__c =: labId ]) {
        //     options.add(new OptionWrapper(batchList.Name, batchList.Id));
        // }
        return options;
    }
    
    @AuraEnabled
    public static Contact getLabId(){
        List<User> userList = [SELECT Id, ContactId From User WHERE Id = :UserInfo.getUserId()];
        Id conId;
        if(userList.size()>0){
            conId = userList[0].contactId;
            List<Contact> contactList = new List<Contact>();
            contactList = [SELECT Id, Name, AccountId,Account.Testing_Site_Type__c	 from CONTACT WHERE Id = :conId];
            if(contactList.size()>0){
                return contactList[0];
            }else{
                return null;
            }
        }else{
            return null;
        }

    }
    
    @AuraEnabled
    public static List<AppointmentWrapper> getAntibody(String labId, string batchId){
     List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
     Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
     Id citizenCovidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
     for(Antibody_Testing__c app : [SELECT Id,Batch__c,Batch__r.Name,Container_Barcode_Id__c,Antibody_Testing_Number__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                    Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,
                                     createddate,Results__c FROM Antibody_Testing__c WHERE Results__c = null AND Laboratory__c = :labId AND Batch__c =: batchId AND ( RecordTypeId =: antibodyRecordTypeId OR RecordTypeId =: citizenCovidRecordTypeId) ORDER BY Antibody_Testing_Number__c ]){
        wrapper.add(new AppointmentWrapper(app));
      }
      if(wrapper.size()>0){
          return wrapper;
      }else{
          return null;
      }
    }

    @AuraEnabled
    public static List<AppointmentWrapper> getAntibodyTestingSite(String labId, string batchId){
     List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
     Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
     Id citizenCovidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
     for(Antibody_Testing__c app : [SELECT Id,Batch__c,Batch__r.Name,Container_Barcode_Id__c,Antibody_Testing_Number__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                    Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,
                                     createddate,Results__c FROM Antibody_Testing__c WHERE Results__c = null AND Testing_Site__c = :labId AND Batch__c =: batchId  AND ( RecordTypeId =: antibodyRecordTypeId OR RecordTypeId =: citizenCovidRecordTypeId) ORDER BY Antibody_Testing_Number__c ]){
        wrapper.add(new AppointmentWrapper(app));
      }
      if(wrapper.size()>0){
          return wrapper;
      }else{
          return null;
      }
    }

    public class AppointmentWrapper{
        @AuraEnabled public string Id;
        @AuraEnabled public string TestNumber;
        @AuraEnabled public string patientId;
        @AuraEnabled public string firstName;
        @AuraEnabled public string middleName;
        @AuraEnabled public string lastname;
        @AuraEnabled public string gender;
        @AuraEnabled public String dateOfBirth;
        @AuraEnabled public String organizationName;
        @AuraEnabled public string barCoderNumber;
        @AuraEnabled public String dateOfSpecimenCollection;
        @AuraEnabled public string sourceOfSpecimen;
        @AuraEnabled public string batchId;
        @AuraEnabled public string result;
        @AuraEnabled public string dateOfLabTestCompletion;
        @AuraEnabled public string containerBarCode;
        
        public AppointmentWrapper(Antibody_Testing__c app){
            this.Id = app.Id ;
            this.TestNumber = app.Antibody_Testing_Number__c ;
            this.patientId = app.Patient__r.Patient_Id__c != null ? app.Patient__r.Patient_Id__c : '' ;
            this.firstName = app.Patient__r.FirstName != null ? app.Patient__r.FirstName : '';
          	this.middleName = app.Patient__r.MiddleName != null ? app.Patient__r.MiddleName : '';
            this.lastname = app.Patient__r.LastName != null ? app.Patient__r.LastName : '';
            this.gender = app.Patient__r.Gender__c != null ? app.Patient__r.Gender__c : '';
            this.dateOfBirth = string.valueOf(app.Patient__r.formatted_Date_of_Birth__c) != null ? string.valueOf(app.Patient__r.formatted_Date_of_Birth__c) : '';
            this.organizationName = app.Patient__r.Account.Name != null ? app.Patient__r.Account.Name : '';
            this.barCoderNumber = app.Barcode_ID__c != null ? app.Barcode_ID__c : '';  
            this.dateOfSpecimenCollection = string.valueOf(Date.valueOf(app.createddate)) != null ? string.valueOf(Date.valueOf(app.createddate)) : '';
            this.sourceOfSpecimen = app.Source_of_Specimen__c != null ? app.Source_of_Specimen__c : 'Serum';
            this.batchId = app.Batch__r.Name != null ? app.Batch__r.Name : '' ;
            this.containerBarCode = app.Container_Barcode_Id__c != null ? app.Container_Barcode_Id__c : '';
            this.result = '' ;
            this.dateOfLabTestCompletion = '';
        }
    }

    @AuraEnabled
    public static Map<String,Object> insertData(String strfromle){
        Map<String, Object> result = new Map<String, Object>();
        String returnresponse ='';
        List<String> antibodyIdList = new List<String>();
        List<Antibody_Testing__c> AccoutnListtoInsert = new List<Antibody_Testing__c>();
        List<fieldWrapper> datalist = (List<fieldWrapper>)JSON.deserialize(strfromle, List<fieldWrapper>.class);
        for(fieldWrapper wrapper: datalist){
            if(!antibodyIdList.contains(wrapper.Id)){
                Antibody_Testing__c acc = new  Antibody_Testing__c();
                if(wrapper.Id != null && wrapper.Id != ''){
                    acc.Id = wrapper.Id;
                    if(wrapper.dateOfLabTestCompletion != null && wrapper.dateOfLabTestCompletion != '' && wrapper.result != null && wrapper.result != '' ){
                        if(wrapper.dateOfLabTestCompletion.length() < 10){
                        result.put('message', 'Please select the correct date format for Lab Test Completion Date');
                        result.put('type','error');
                        return result;
                        }else{
                            acc.Date_of_Lab_Test_Completion__c = date.valueOf(getFormattedDate(wrapper.dateOfLabTestCompletion));
                            acc.Results__c = getResult(wrapper.result);
                            AccoutnListtoInsert.add(acc); 
                            antibodyIdList.add(wrapper.Id);
                        }
                       
                    }     
                } 
            }    
        }
        
        if(AccoutnListtoInsert.size() > 0){
            system.debug('acc List: '+ AccoutnListtoInsert);
            try {
                update AccoutnListtoInsert;
                if(AccoutnListtoInsert.size() == datalist.size()){
                    result.put('message',string.valueOf(AccoutnListtoInsert.size())+' records updated successfully');
                    result.put('type','success');
                }else{
                    result.put('message',string.valueOf(AccoutnListtoInsert.size())+' records updated successfully. '+string.valueOf(datalist.size()-AccoutnListtoInsert.size())+' records contains bad values or duplicate results.');
                    result.put('type','warning');
                }
                
            }
            catch(Exception ex){
                result.put('message', ex);
                result.put('type','error');
            }
        }else{
            result.put('message', '0 records updated. Please check the CSV file for bad values.');
            result.put('type','error');
        }
        return result;
    }

    public static String getResult(String str){
        String result = '';
        List<String> strList = str.split(' ');
        For(string s : strList){
            result = result +' '+ s.toLowerCase().capitalize();
        }
        result = result.trim();
        if(result == 'Positive' || result == 'Negative' || result == 'Rejected'){
            return result;
        }else{
            throw new AuraHandledException('Please select a valid result value.');
        }  
    }

    public static String getFormattedDate(String input){
        String str = input.substring(6,10)+'-'+input.substring(0,2)+'-'+input.substring(3,5);
        return str;
    }
    
    public class fieldWrapper{        
        public string Id;
        public string TestNumber;
        public string lastname;
        public string firstName;
        public string patientId;
        public string result;
        public string dateOfLabTestCompletion;
    }
    
     public class OptionWrapper {
        @AuraEnabled public string label {get; set;}
        @AuraEnabled public string value {get; set;}
        public OptionWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

}