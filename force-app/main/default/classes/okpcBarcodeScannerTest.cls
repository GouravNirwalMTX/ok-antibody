@isTest
public class okpcBarcodeScannerTest {
      private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        private static final String testingSiteContact = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
    static testMethod void testMethod1(){
         Account createAccountTestSite = TestDataFactoryAccount.createAccount('LabSite', testingSite, true);
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.RecordTypeId = testingSiteContact;
        createContact.Testing_Site__c = createAccountTestSite.id;
        insert createContact;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        insert createAppointment;
        
        
       
            Test.StartTest(); 
            ApexPages.currentPage().getParameters().put('id', String.valueOf(createAppointment.Id));
        ApexPages.currentPage().getParameters().put('labId', String.valueOf(createAccountTestSite.Id));
            okpcBarcodeScanner  testScan = new okpcBarcodeScanner();
            testScan.saveTextValue();
            Test.StopTest();
     
            
    
    }
    static testMethod void testMethod2(){
        Batch__c b = new Batch__c();
        b.Container_Barcode_Id__c = '12345';
        insert b;
        
        Antibody_Testing__c a = new Antibody_Testing__c();
        //a.Name = 'test';
        a.Batch__c = b.id;
        insert a;
        
         Test.StartTest(); 
            ApexPages.currentPage().getParameters().put('id', String.valueOf(b.Id));
       // ApexPages.currentPage().getParameters().put('labId', String.valueOf(createAccountTestSite.Id));
            okpcBarcodeScanner  testScan = new okpcBarcodeScanner();
            testScan.saveTextValue();
            Test.StopTest();
     
    }
    static testMethod void testMethod3(){
        Batch__c b = new Batch__c();
        b.Container_Barcode_Id__c = '12345';
        insert b;
        
        Appointment__c app = new Appointment__c();
        app.Batch__c = b.id;
        app.Lab_Center__c = null;
        app.Status__c = 'Closed';
        insert app;
        
        Antibody_Testing__c a = new Antibody_Testing__c();
       // a.Name = 'test';
        a.Batch__c = b.id;
        a.Laboratory__c = null;
        insert a;
        
         Test.StartTest(); 
            ApexPages.currentPage().getParameters().put('id', String.valueOf(b.Id));
       // ApexPages.currentPage().getParameters().put('labId', String.valueOf(createAccountTestSite.Id));
            okpcBarcodeScanner  testScan = new okpcBarcodeScanner();
            testScan.cancelValue();
            Test.StopTest();
     
    }

}