@isTest
public class OKPCDashboardControllerTest {
    
    @TestSetup
    static void makeData(){
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            MailingCity = 'Tulsa',
            MailingStreet = '12 N Cheyenne Ave',
            MailingCountry = 'USA',
            MailingState = 'OK',
            MailingPostalCode = '74103',
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Completed';
        insert createAppointment;
    
        Antibody_Testing__c antibodyTesting = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.Id, createAppointment.Id, createSlot.Id, con.Id);
    }

    public static testMethod void retrieveResultTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            Map<String,object> maoObj = OKPCDashboardController.retrieveResult();
            Map<String,object> maoObjCovid = OKPCDashboardController.retrieveResultCovidPortal();
        }
    }
    
    public static testMethod void createAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            OKPCDashboardController.createAppointment(contactList[0].id);
        }
    }
    
    public static testMethod void deleteAppointmentTest(){
        Appointment__c app = [SELECT Id FROm Appointment__c LIMIT 1];
        OKPCDashboardController.deleteAppointment(app.Id);
    }
}