@isTest
public class AccountTriggerHandlerTest {
    
	@testsetup
    private static void setUpData(){
        Id Vital_Records_RT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', Vital_Records_RT, true);
        
        
        Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
        
        Contact createContact = TestDataFactoryContact.createCitizenContact(false , 'TestLastName','TestFirstName');
        createContact.Notify_Patients__c = true;
        createContact.AccountId=createAccountTestSite.id;
        createContact.Opt_out_for_SMS_Updates__c = false;
        createContact.recordtypeID=citizenConsultationVitalRecords_RT;
        insert createContact;
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
    }
    private static testmethod void test(){
        try{
            account[] acc=[select id,Appointment_Frequency__c from account where name='TestSite' limit 1];
            
            acc[0].Appointment_Frequency__c='10';
            update acc[0];
        } catch(system.DmlException dmlExp){
            string dmlMsg='You can\'t update Appointment Frequency.';
            system.assertEquals(dmlMsg, dmlExp.getDmlMessage(0));
            
        }
        
    }
    
    private static testmethod void InsertAccountTest(){
        test.startTest();
        Id Test_Center_RT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Test_Center').getRecordTypeId();
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', Test_Center_RT, false);
        createAccountTestSite.Business_End_Time__c=null;
        createAccountTestSite.Business_Start_Time__c=null;
        createAccountTestSite.Ordering_Provider_First_Name__c = 'Jon';
        insert createAccountTestSite;
        test.stopTest();
        account[] acc =[select id,Business_End_Time__c,name,Business_Start_Time__c from account where name=:'TestSite' limit 1];
        system.debug('acc========='+acc[0]);
        system.debug('start date========'+acc[0].Business_Start_Time__c);
        system.debug('end date========'+acc[0].Business_End_Time__c);
    }
}