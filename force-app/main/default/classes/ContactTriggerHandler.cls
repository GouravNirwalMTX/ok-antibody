public class ContactTriggerHandler {
    public static void createAppointmentOnInsert(List<Contact>newList, Map<Id,Contact>oldMap){
        List<Id> contactIdList = new List<Id>();
        List<Id> confirmAppointmentContactId = new List<Id>();
        Set<String> appointmentSet = new Set<String>{'Reschedule','Declined'};
            List<Appointment__c> appointmentList = new List<Appointment__c>();
        //Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
        Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        Id contactCitizen_AntiBodyTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
        Id contact_RT1 = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
        Id contact_RT2 = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Lobby_Vital_Records').getRecordTypeId();
        String query = 'SELECT Id,Testing_Site__c,AccountId,Account.Communication__c, MobilePhone,Community_Link__c,Name,Patient_Id__c  FROM Contact ';
        Map<Id,List<Appointment__c>> mapOfConIdsToAppointments  = new Map<Id ,List<Appointment__c>>();
        List<Id> scheduledContacts = new List<Id>();
        
        for(Appointment__c appointmentRecord : [SELECT id,status__c,Patient__r.Pending_Confirmation__c,Patient__c,Appointment_Slot__c  FROM Appointment__c WHERE Status__c = 'Scheduled' AND Patient__c IN :newList ]){
            scheduledContacts.add(appointmentRecord.Patient__c);
        }
        
        for(Contact selectedContact : newList){
            /*
             // commented by Sameer as for COVID contact we are not creating appointment at the inital contact creation.
            */
          //  || selectedContact.RecordTypeId==contactCitizen_COVIDTypeId 
        
            
            if(selectedContact.RecordTypeId==contactCitizen_AntiBodyTypeId
               || selectedContact.RecordTypeId == contact_RT2 || selectedContact.RecordTypeId == contact_RT1){
                   if(oldMap == null && !selectedContact.Dependent__c ){
                       System.debug('inside oldMap == null');
                       Appointment__c appointment = new Appointment__c();
                       if(selectedContact.Portal_Contact__c){
                           appointment.Status__c = 'To Be Scheduled';
                       }else if(selectedContact.Auto_Scheduled__c){
                            appointment.Appointment_Start_Date_v1__c = System.today();
                            appointment.Appointment_Start_Time_v1__c = Datetime.now().time();
                            appointment.Status__c = 'Scheduled';
                            
                            /*if(selectedContact.RecordTypeId==contactCitizen_AntiBodyTypeId){
                                appointment.Testing_Site_Type__c = 'Antibody Testing';
                            }*/
                            
                       }else{
                           appointment.Status__c = 'Eligible';
                       }
                       appointment.Patient__c = selectedContact.Id;
                       appointment.Testing_Site_Account__c = selectedContact.Testing_Site__c;
                       appointmentList.add(appointment);
                       
                   }
                   else{                       
                       if(appointmentSet.contains(selectedContact.Pending_Confirmation__c) && selectedContact.Pending_Confirmation__c != oldMap.get(selectedContact.Id).Pending_Confirmation__c){
                           for(Appointment__c appointmentRecord : [SELECT id,status__c,Patient__r.Pending_Confirmation__c,Patient__c,Appointment_Slot__c  FROM Appointment__c 
                                                                   WHERE Patient__c = :selectedContact.Id and Patient__r.Pending_Confirmation__c in: appointmentSet]){
                                                                       if(appointmentRecord.Patient__r.Pending_Confirmation__c.equals('Reschedule')){
                                                                           appointmentRecord.Status__c = 'To Be Scheduled';
                                                                       }else if(appointmentRecord.Patient__r.Pending_Confirmation__c.equals('Declined')){
                                                                           appointmentRecord.Status__c = 'Cancelled';
                                                                           appointmentRecord.Cancellation_Date__c = System.today();
                                                                       }
                                                                       appointmentRecord.Appointment_Slot__c = null;
                                                                       appointmentList.add(appointmentRecord);
                                                                       
                                                                   }
                           
                       }
                   }
               }
            
        }
        if(appointmentList.size()>0){
            upsert appointmentList;
        }
        
    }
    
    /* Method Name : sendWelcomeSMS
    * Return Type : void
    * Parameters : List,Map
    * Created By/On : Anushka Bansal 26/10/2020.
    */
    public static void sendWelcomeSMS(List<Contact>newList, Map<Id,Contact>oldMap){
        String query = 'SELECT Id,Testing_Site__c,AccountId, Account.Communication__c, RecordType.DeveloperName, RecordTypeId, MobilePhone,Community_Link__c,Name,Patient_Id__c  FROM Contact ';
        List<Id> contactIdList = new List<Id>(); 
        for(Contact con : newList){
            if((oldMap == null || (oldMap != null && con.Notify_Patients__c != oldMap.get(con.Id).Notify_Patients__c))  
               && con.MobilePhone != null && con.Notify_Patients__c == true && con.Opt_out_for_SMS_Updates__c == false){
                   contactIdList.add(con.Id);
               }
        }
        
        if(!contactIdList.isEmpty()){
            SendSMSBatch smsBatch = new SendSMSBatch(query, contactIdList, 'welcomeSMS');
            Database.executeBatch(smsBatch, 1);
        }       
    }
    
    
    /* Method Name : updateUsers
    * Return Type : void
    * Parameters : List,Map
    * Created By/On : Ritvik Jain on 06/26/2020.
    */
    public static void updateUsers(List<Contact> newList, Map<Id,Contact> oldMap){
        Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
        Id citizenLobbyVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Lobby_Vital_Records').getRecordTypeId();
        List<String> contactIdList = new List<String>();
        List<Contact> filterdContacts = new List<Contact>();
        List<User> updateUserList = new List<User>();
        List<Credential__c> updateCredList = new List<Credential__c>();
        Map<String,User> contactUserMap = new Map<String,User>();
        Map<String,Credential__c> contactCredsMap = new Map<String,Credential__c>();
        for(Contact record : newList){
            if((record.RecordTypeId == citizenConsultationVitalRecords_RT || record.RecordTypeId == citizenLobbyVitalRecords_RT) && 
               (record.FirstName != oldMap.get(record.Id).FirstName || record.LastName != oldMap.get(record.Id).LastName || record.MobilePhone != oldMap.get(record.Id).MobilePhone || record.Email != oldMap.get(record.Id).Email)){
                   contactIdList.add(record.Id);
                   filterdContacts.add(record); 
               }
        }
        
        for(User record : [SELECT Id,FirstName,LastName,Email,Phone,ContactId FROM User WHERE ContactId IN: contactIdList]){
            contactUserMap.put(record.ContactId,record);
        }
        for(Credential__c credRecord : [SELECT id, Password__c, Username__c,Email__c,phone__c,contact__c FROM Credential__c WHERE contact__c IN: contactIdList]){
            contactCredsMap.put(credRecord.contact__c,credRecord);
        }
        
        if(contactUserMap != null && contactCredsMap != null){
            for(Contact con : filterdContacts){
                if(contactUserMap.containsKey(con.Id) && contactCredsMap.containsKey(con.Id)){
                    User user = new User(Id = contactUserMap.get(con.Id).Id);
                    Credential__c creds = new Credential__c(Id = contactCredsMap.get(con.Id).Id);
                    if(con.FirstName != contactUserMap.get(con.Id).FirstName)
                        user.FirstName = con.FirstName;
                    creds.Name =  con.FirstName;
                    
                    if(con.LastName != contactUserMap.get(con.Id).LastName)
                        user.LastName = con.LastName;
                    
                    if(con.Email != contactUserMap.get(con.Id).Email && con.Email !=null)
                        user.Email = con.Email;
                    creds.Email__c =  con.Email;
                    
                    if(con.MobilePhone != contactUserMap.get(con.Id).Phone)
                        user.Phone = con.MobilePhone;
                    creds.Phone__c =  con.MobilePhone;
                    
                    updateUserList.add(user);
                    updateCredList.add(creds);
                }
                
            }
        }
        
        if(!updateUserList.isEmpty())
            update updateUserList;
        if(!updateCredList.isEmpty())
            update updateCredList;
        
    }
}