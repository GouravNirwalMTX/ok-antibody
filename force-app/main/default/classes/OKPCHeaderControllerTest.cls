@isTest
public class OKPCHeaderControllerTest {
     private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    static testMethod void testMethod1(){
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
            Account acc1 = new Account (
                Name = 'newAcc1',Testing_Site_Type__c = 'Antibody Testing'
            );  
        insert acc1;
        Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            MobilePhone = '123123123'
        );
        insert conCase;
        //Create user
        
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
        
        User newUser1 = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = conCase.id
        );
        insert newUser1;
         Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);
        Contact createContact = TestDataFactoryContact.createContact(true , 'TestLastName','TestFirstName');
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
       Antibody_Testing__c createAntiTest = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.id, createAppointment.id, createSlot.id, conCase.id);
        system.runAs(newUser1){
            OKPCHeaderController.getUserDetails();
        OKPCHeaderController.getAntibodyList();
            OKPCHeaderController.getLoggedInTestingUserDetails();
        }
        
    }
}