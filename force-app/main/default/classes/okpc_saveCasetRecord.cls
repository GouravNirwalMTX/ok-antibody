public without sharing class okpc_saveCasetRecord {
    @AuraEnabled
    public static void saveCaseRecord(string description , string email){
        try{ 

            string logineduserId = UserInfo.getUserId();
            
            string contactId = [SELECT Username,ContactId, Contact.Name, Contact.AccountId, Contact.Account.Name
                               FROM User WHERE Id =: logineduserId].contactId;
            system.debug('contactId'+ contactId);
            if(contactId != null){
                case createCase = new case();
                createCase.ContactId = contactId;
                createCase.Description = description;
                createCase.SuppliedEmail = email;
                insert createCase;
            }
                               
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
}