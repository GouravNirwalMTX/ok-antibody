@isTest
public class oklcAntibodyControllerTest {
    @TestSetup
    static void makeData(){
        String laboratoryRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;
        Id labUserProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%Lab User%'][0].Id;
        Account labAccount = TestDataFactoryAccount.createAccount('TestLabAccount', laboratoryRecTypeId, false);

        labAccount.BillingCity = 'testCity';
        labAccount.BillingCountry = 'testCountry';
        labAccount.BillingState = 'testState';
        labAccount.BillingStreet = 'testStreet';
        labAccount.BillingPostalCode = '12345';
        labAccount.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        labAccount.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert labAccount;

        
        Contact con1 = new Contact (
            AccountId = labAccount.id,
            LastName = 'LabTestUser1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testLabUser@domain.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            //Testing_Site__c = labAccount.Id,
            RecordTypeId = labUserRecTypeId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes	',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = '	Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con1;

        User labUser = new User(
            ProfileId = labUserProfileId,
            Username = System.now().millisecond() + 'testlabuser@test.com',
            Alias = 'labus',
            Email='labuser@domain.com',
            EmailEncodingKey='UTF-8',
            Firstname='Test',
            Lastname='Lab user',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            ContactId = con1.Id,
            CommunityNickname = 'test12345'
        );

        insert labUser;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, labAccount.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con1.Id;
        createAppointment.Lab_Center__c = labAccount.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;

        Batch__c batchtest = new Batch__c();
        batchtest.Status__c = 'Unprocessed';
        insert batchtest;

        Antibody_Testing__c antibodyTesting = TestDataFactoryAntiBody.createAppointment(false, labAccount.Id, createAppointment.Id, createSlot.Id, con1.Id);
        antibodyTesting.Laboratory__c = labAccount.Id;
        antibodyTesting.Batch__c = batchtest.Id;
        insert antibodyTesting;
    }

    @isTest
    static void getLabIdTest(){
        List<User> userList = [SELECT ID FROM User WHERE Username LIKE '%testlabuser@test.com%'];
        Test.startTest();
        System.runAs(userList[0]){
            oklcAntibodyDownloadAnduploadController.getLabId();
        }
        
        Test.stopTest();
    }
    
    @isTest
    static void getAntibodyTest(){
        List<Account> labAccount = [SELECT Id FROM Account WHERE Name LIKE '%TestLabAccount%'];
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'LabTestUser1' LIMIT 1];
        Appointment__c app = [SELECT Id, Name, Patient__c FROM Appointment__c WHERE Patient__c = :contactList[0].id];
        DC_ScheduleAppointment.completeAppointment(app.Id, labAccount[0].Id, '12345','Positive','Aptima SARS-CoV-2 Assay','test');
        
        List<Batch__c> batch = [SELECT Id FROM Batch__c LIMIT 1 ];
                
        Test.startTest();
        	oklcAntibodyDownloadAnduploadController.getAntibody(labAccount[0].Id , batch[0].Id);
        	//oklcAntibodyBulkDownload.getAntibodyBulk(labAccount[0].Id ,null,null);
        Test.stopTest(); 
    }
       
    @isTest
    static void insertDataTest(){
        //StaticResource testdoc = [Select Id,Body from StaticResource where name ='testMethodCSVUpload'];
        List<Account> labAccount = [SELECT Id FROM Account WHERE Name LIKE '%TestLabAccount%'];
        List<Contact> contactList = [Select Id , Name , FirstName , MiddleName , LastName , Gender__c FROM Contact WHERE lastName = 'LabTestUser1' LIMIT 1];
        Appointment__c app = [SELECT Id, Name, Patient__c FROM Appointment__c WHERE Patient__c = :contactList[0].id];
        DC_ScheduleAppointment.completeAppointment(app.Id, labAccount[0].Id, '12345','Positive','Aptima SARS-CoV-2 Assay','test');
        Antibody_Testing__c antibody = [SELECT Id , Antibody_Testing_Number__c FROM Antibody_Testing__c WHERE Laboratory__c = :labAccount[0].Id LIMIT 1];
        System.debug('AntiBody'+antibody);
        String data = '[{"antibodyId": "'+antibody.Id+'","antibodyTestNumber": "'+antibody.Antibody_Testing_Number__c+'","patientId": "'+contactList[0].Id+'","firstName": "'+contactList[0].FirstName+'","middleName": "'+contactList[0].MiddleName+'","lastname": "'+contactList[0].LastName+'","gender": "'+contactList[0].Gender__c+'","barCoderNumber": "","dateOfSpecimenCollection": "2020-04-23","sourceOfSpecimen": "Blood","result": "Positive","dateOfLabTestCompletion": "04-24-2020"}]';
        String data2 = '[{"Id":"'+antibody.Id+'","TestNumber":"'+antibody.Antibody_Testing_Number__c+'","patientId":"'+contactList[0].Id+'","firstName":"'+contactList[0].FirstName+'","middleName":"'+contactList[0].MiddleName+'","lastname":"'+contactList[0].LastName+'","gender":"'+contactList[0].Gender__c+'","barCoderNumber":"","dateOfSpecimenCollection":"2020-04-23","sourceOfSpecimen":"Blood","result":"Positive","dateOfLabTestCompletion":"04-24-2020"}]';
        Test.startTest();
        oklcAntibodyDownloadAnduploadController.insertData(data2);
        Test.stopTest();  
    }

    @isTest
    static void retrieveBatchTest(){
        List<Account> labAccount = [SELECT Id FROM Account WHERE Name LIKE '%TestLabAccount%'];
        Test.startTest();
        oklcAntibodyDownloadAnduploadController.retrieveBatch(labAccount[0].Id);
        Test.stopTest();
    }
}