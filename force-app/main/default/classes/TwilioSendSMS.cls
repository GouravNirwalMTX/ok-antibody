public class TwilioSendSMS { 
    public static void sendSms(String toPhoneNumber, String body){
        String phNumber = toPhoneNumber;       
        String accountSid = System.Label.OKPC_twillio_account_SID;
        String token = System.Label.OKPC_twillio_authtoken;
        String fromPhNumber = System.Label.OKPC_twillio_Phone_Number;
        String smsBody = body; 
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.Label.OKPC_Twilio_Endpoint+accountSid+'/Messages.json');
        req.setMethod('POST');
        String VERSION  = '3.2.0';
        req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
        req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Accept-Charset', 'utf-8');
        req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountSid+':' +token)));
        req.setBody('To='+EncodingUtil.urlEncode(phNumber,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromPhNumber,'UTF-8')+'&Body='+smsBody);
        Http http = new Http();
        
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
        if(res.getStatusCode()==201)
            System.Debug('Message sending Successful');
        else{
            System.Debug('Message sending Unsuccessful');
        }   
    }
}