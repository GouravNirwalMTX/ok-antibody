@isTest
public class okLookUpController_Test {

    public static testMethod void  test1(){
        
        Insurance_Company_Details__c insCompanyDetail = TestDataFactoryInsuranceCompanyDetail.createInsuranceCompanyDetail('test12', false, false, false, true);
       //	insCompanyDetail.Member_Id_Validation_Regex__c = null;
       // insCompanyDetail.Is_Group_Number_Required__c = true;
      //  insert insCompanyDetail;
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(insCompanyDetail.Id); 
        Test.setFixedSearchResults(fixedSearchResults);
        okLookUpController.searchDB('test12', false, false);
    }
    
     public static testMethod void  test2(){
        
        Insurance_Company_Details__c insCompanyDetail = TestDataFactoryInsuranceCompanyDetail.createInsuranceCompanyDetail('OKLAHOMA HEALTH CARE AUTHORITY (MEDICAID)', true, true, false, true);
        okLookUpController.setDefaultPrimaryInsuranceCompany();
    }
}