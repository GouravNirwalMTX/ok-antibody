public class GoogleBigQuery {

    private String projectId; // Project ID from Google BigQuery
    private String datasetId; // Dataset ID from Google BigQuery
    private String tableId; // Table ID from Google BigQuery
    private Boolean isSuccess = false;
    private String response;
    private String authName; //ImplementationID from Metadata Record
    private String accessToken; 
 
    //Constructor
    public GoogleBigQuery(String authName){
        this.authName = authName;
    }
	
    //Sets up authentication for the Request, calls the request, and logs its response
    public Boolean add(Object data) {
        GoogleAuthProvider auth = new GoogleAuthProvider(this.authName); // Custom Google OAuth Provider
        if (auth.authorize()) {
            this.tableId = auth.getTableID();
            this.datasetId = auth.getDatasetID();
            this.projectId = auth.getProjectID();
            this.accessToken = auth.getAccessToken();
            sendRequest(data);
        } else {
            this.response = auth.getErrorMessage();
        }
        return this.isSuccess;
    }

    //Getters
    public Boolean isSuccess() {
        return this.isSuccess;
    }
    
    public String getResponse() {
        return this.response;
    }
    
	//Sends a request to the server that contains the data from the trigger and POSTs it to Google Big Query
    public void sendRequest(Object data){
        String accessToken;
        String jsonData = '';
	    
        String url = 'https://www.googleapis.com/bigquery/v2/projects/' + this.projectId + '/datasets/' + this.datasetId + '/tables/' + this.tableId + '/insertAll';
        jsonData = System.JSON.serialize(data);

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + this.accessToken);
        req.setBody(jsonData);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        this.response = res.getBody();
        if (res.getStatusCode()==200) {
           this.isSuccess = true;
        } else {
           Map<String, Object> errorResponse = (Map<String, Object>)JSON.deserializeUntyped(this.response);
           Map<String, Object> error = (Map<String, Object>)errorResponse.get('error');
           this.response = (String)error.get('message');
        }
    }
    

    // Class for constructing an InsertALL to the specifications of the BigQueryAPI
    public class InsertAll {
        String kind = 'bigquery#tableDataInsertAllRequest';
        Boolean skipInvalidRows = false;
        Boolean ignoreUnknownValues = true;
        List<Object> rows = new List<Object>();

        public void addObject(Object data) {
            Map<String, Object> details = new Map<String, Object>();
            details.put('json', data);
            rows.add(details);
        }
    }

}