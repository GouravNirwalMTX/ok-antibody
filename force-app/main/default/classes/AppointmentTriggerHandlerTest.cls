@isTest
public class AppointmentTriggerHandlerTest {
    private static final String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    private static final String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;

    @testsetup
    public static void setUpData(){
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.Testing_Site__c = createAccountTestSite.id;
        insert createContact;
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        Antibody_Testing__c createAntiTest = TestDataFactoryAntiBody.createAppointment(true, createAccountTestSite.id, createAppointment.id, createSlot.id, createContact.id);
    }
    static testMethod void testUpdate(){
         
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c WHERE Lab_Center__c =: fetchAccount.id LIMIT 1];
        Antibody_Testing__c fetchAntibody = [SELECT Id FROM Antibody_Testing__c WHERE  Appointment__c =: fetchAppointment.id LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        fetchAppointment.Appointment_Slot__c = createSlotLab.Id;
        update fetchAppointment;
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.stopTest();
    }
    static testMethod void testDelete(){
        
        Test.startTest();
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c LIMIT 1];
		delete fetchAppointment;
        undelete fetchAppointment;
        Test.stopTest();
    }
    
    static testMethod void testUpdateStartDateStartTime(){
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment__c fetchAppointment = [Select Id FROM Appointment__c WHERE Lab_Center__c =: fetchAccount.id LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        fetchAppointment.Appointment_Slot__c = createSlotLab.Id;
        Test.startTest();
        update fetchAppointment;
        Test.stopTest();
    }
    static testMethod void testInsertStartDateStartTime(){
        Account fetchAccount = [SELECT Id FROM Account WHERE Name = 'TestSite' LIMIT 1];
        Contact fetchContact = [SELECT Id FROM Contact WHERE LastName = 'TestLastName' LIMIT 1];
        Appointment_Slot__c createSlotLab = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, fetchAccount.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Appointment_Slot__c = createSlotLab.Id;
        Test.startTest();
        insert createAppointment;
        Test.stopTest();
    }
}