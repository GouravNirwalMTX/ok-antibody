public without sharing class OKPC_LocationMapController {
    @AuraEnabled(cacheable=true)
    public static List<MapMarker> getTestingSites(string testingType) {

        List<MapMarker> mapMarkers = new List<MapMarker>();
        List<String> conditions = new List<String>();
        Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
 
        String query = 'Select Id, Name, BillingStreet, BillingCity,Business_Start_Time__c, Business_End_Time__c, BillingState, BillingCountry, BillingPostalCode, BillingLatitude, BillingLongitude,testing_site_type__c, Phone FROM Account WHERE ';
        system.debug('&& ' + recordTypeInfosMap.get('Test_Center'));
        conditions.add('RecordTypeId = \'' + recordTypeInfosMap.get('Test_Center').RecordTypeId + '\'');
        //conditions.add('( Testing_Type__c = \''+'Both'+ '\' OR Testing_Type__c = \''+testingType+'\')');
        //condition.add((testing_site_type__c includes ('PCR Testing','Rapid esting')));
       // conditions.add('( testing_site_type__c includes \('+'\'PCR Testing \'\,'+'\'Rapid Testing\''+'\)'+'\)');
        if(testingType == 'Covid Testing'){
            conditions.add('( testing_site_type__c includes (\'' + 'PCR Testing' +'\',\'' +'Rapid Testing'+ '\'))');
        }else{
            conditions.add(' testing_site_type__c includes (\'' + 'Antibody Testing' + '\')');
        }
       	  
        conditions.add('BillingLatitude != null');
        conditions.add('BillingLongitude != null');

        query += String.join(conditions, ' AND ');
           
        query += ' ORDER BY Name';
		system.debug('QUery>>' + query);
        system.debug('QUery>>Result' + Database.query(query).size());
        for(Account acc : Database.query(query)) {
            mapMarkers.add(new MapMarker(acc));
        }

        return mapMarkers;
    }

    @AuraEnabled(cacheable=true)
    public static List<MapMarker> getLobbys() {

        List<MapMarker> mapMarkers = new List<MapMarker>();
        List<String> conditions = new List<String>();
        Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;

        String query = 'Select Id, Name, BillingStreet, BillingCity,Business_Start_Time__c, Business_End_Time__c, BillingState, BillingCountry, BillingPostalCode, BillingLatitude, BillingLongitude,Testing_Site_Type__c,Phone FROM Account WHERE ';

        conditions.add('RecordTypeId = \'' + recordTypeInfosMap.get('Vital_Records').RecordTypeId + '\'');
        conditions.add('BillingLatitude != null');
        conditions.add('BillingLongitude != null');
        conditions.add('Id != \'' + System.Label.OKC_Public_Consults + '\'');
        query += String.join(conditions, ' AND ');
           
        query += ' ORDER BY Name';

        for(Account acc : Database.query(query)) {
            mapMarkers.add(new MapMarker(acc));
        }

        return mapMarkers;
    }

    

    @AuraEnabled
    public static MapMarker getAccountdetails(String accountId){
        List<MapMarker> mapMarkers = new List<MapMarker>();
        for(Account acc : [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, BillingLatitude, BillingLongitude, 
                           Business_Start_Time__c, Business_End_Time__c,testing_site_type__c, Phone FROM Account WHERE Id = :accountId ]){
            mapMarkers.add(new MapMarker(acc));
        }
        return mapMarkers[0];
    }

    public class MapMarker {
        @AuraEnabled public string title;
        @AuraEnabled public string description;
        @AuraEnabled public Location location;
        @AuraEnabled public String value;
        @AuraEnabled public Time businessStartTime;
        @AuraEnabled public Time businessEndTime;
        @AuraEnabled public string testingType;

        public MapMarker(Account acc) {
            this.value = acc.Id;
            this.title = acc.Name;
            this.description = acc.BillingStreet  +
                              (acc.Phone != null ? ' \n Phone: ' + acc.Phone : '');
            this.location = new Location(acc);
            this.businessStartTime = acc.Business_Start_Time__c;
            this.businessEndTime = acc.Business_End_Time__c;
            this.testingType = acc.testing_site_type__c;
        }
    }

    public class Location {
        @AuraEnabled public string Street;
        @AuraEnabled public string City;
        @AuraEnabled public string State;
        @AuraEnabled public string Country;
        @AuraEnabled public string PostalCode;
        @AuraEnabled public Decimal Latitude;
        @AuraEnabled public Decimal Longitude;

        public Location(Account acc) {
            this.Street = acc.BillingStreet;
            this.City = acc.BillingCity;
            this.State = acc.BillingState;
            this.Country = acc.BillingCountry;
            this.PostalCode = acc.BillingPostalCode;
            this.Latitude = acc.BillingLatitude;
            this.Longitude = acc.BillingLongitude;
        }
    }
}