@isTest
public class oktcBulkContactUploadTest {
    @TestSetup
    static void makeData(){
        String citizen = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String citizenAntibody = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        String citizenCovid = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen_COVID').RecordTypeId;
        Account createAccountCitizen = TestDataFactoryAccount.createAccount('citizenAccount', citizen, true);
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('Testing Site Bulk Upload', testingCenter, true);
        Contact con = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            RecordTypeId = citizenAntibody);
        insert con;
    }
    public static testMethod void bulkInsert(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"test","lastName":"contact","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"","gender":"Female","dateOfBirth":"09/24/2000","race":"Asian","organizationName":"Test Account", "ethnicity" : "Hispanic" , "symptomatic" : "Yes", "positiveCase" : "Yes"}]';
        oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id, true, true, 'Rapid');
        Test.stopTest();
    }
    public static testMethod void bulkInsertBlankValues(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"test","lastName":"contact","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"","gender":"","dateOfBirth":"09/24/2000","race":"Asian","organizationName":"Test Account", "ethnicity" : "" , "symptomatic" : "", "positiveCase" : ""}]';
        oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id, true, true, 'Rapid');
        Test.stopTest();
    }
    
   /* public static testMethod void bulkInsertNewAccount(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khanAccount","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"","gender":"","dateOfBirth":"","race":"","organizationName":"TestBULK"}]';
        oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
        Test.stopTest();
    }
    
    public static testMethod void bulkInsertNullData(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"","lastName":"","email":"","mobile":"","gender":"","dateOfBirth":"","race":"","organizationName":"TestBULK"}]';
        oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
        Test.stopTest();
    }
    
    public static testMethod void bulkInsertFullData(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khanAccount","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"9413609333","gender":"Male","dateOfBirth":"02-06-1991","race":"Asian","organizationName":"TestBULK"}]';
        oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
        Test.stopTest();
    }
    
     public static testMethod void bulkInsertFullDataDuplicatedata(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khan","email":"sameer.khan+duplicateAccount@mtxb2b.com","mobile":"941360933","gender":"male","dateOfBirth":"02-06-1991","race":"Asian","organizationName":"TestBULK"},{"firstName":"sameer","lastName":"khan","email":"sameer.khan+duplicateAccount@mtxb2b.com","mobile":"941360933","gender":"male","dateOfBirth":"02-06-1991","race":"Asian","organizationName":"TestBULK"}]';
        oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
        Test.stopTest();
    }
    
    public static testMethod void bulkInsertAntiBodyContact(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khanAccount","email":"testportalTestUserv1@gmail.com","mobile":"9413609333","gender":"Male","dateOfBirth":"02-06-1991","race":"Asian","organizationName":"TestBULK"}]';
        oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen',testCenter.id);
        Test.stopTest();
    }
    
      public static testMethod void bulkInsertRaceType(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khanAccount","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"9413609333","gender":"male","dateOfBirth":"02-06-1991","race":"American Indian/Alaska Native","organizationName":"TestBULK"}]';
          try{
               oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
          }catch(Exception ex){
               system.debug('error Message' + ex.getMessage());
            }
         
        Test.stopTest();
    }
    
     public static testMethod void bulkInsertWrongGender(){
        Account testCenter = [SELECT Id,Name FROM Account WHERE Name = 'Testing Site Bulk Upload' LIMIT 1];
        Test.startTest();
        String json = '[{"firstName":"sameer","lastName":"khanAccount","email":"sameer.khan+covidTest001@mtxb2b.com","mobile":"9413609333","gender":"m","dateOfBirth":"02-06-1991","race":"American Indian/Alaska Native","organizationName":"TestBULK"}]';
          try{
               oktcBulkContactUpload.resultWrapper data =  oktcBulkContactUpload.insertData(json,'Citizen_COVID',testCenter.id);
          }catch(Exception ex){
               system.debug('error Message' + ex.getMessage());
            }
         
        Test.stopTest();
    }*/
    
}