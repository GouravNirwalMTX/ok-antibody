public without sharing class okvriusBulkDownload {
    
        @AuraEnabled
        public static List<AppointmentWrapper> getAppointmentBulk(String labId ,Date toDate ,Date fromDate){ 
            List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
               Date FromdateChange = Date.valueOf(fromDate);
            Date TodateChnage = Date.valueOf(toDate);
            system.debug('FromdateChange' + FromdateChange);
            for(Appointment__c app : [SELECT Id,Name,Patient__r.MobilePhone,Patient__r.email,
                                           Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,Patient__r.What_type_of_record_do_you_have_question__c,Patient__r.Which_specialist_do_you_need__c,
                                           Patient__r.Account.Name,status__c,Formatted_Appointment_Start_Date__c,Reason_for_cancellation__c,Patient__r.Need_Spanish_Interpreter__c,
                                           createddate,Formatted_Appointment_Start_Time__c, Formatted_Complete_Date__c,Appointment_Schedule_Generated_Date__c,Appointment_Complete_Date__c FROM Appointment__c WHERE Patient__r.Testing_Site__c =:labId AND Appointment_Start_Date_v1__c  >=: fromDate AND Appointment_Start_Date_v1__c  <=: toDate  ORDER BY Name ]){
                                               wrapper.add(new AppointmentWrapper(app));
                                           }
            if(wrapper.size()>0){
                system.debug('wrapperSize' + wrapper.size());
                system.debug('wrapper---' + wrapper);
                return wrapper;
            }else{
                return null;
            }
        }
        
        public class AppointmentWrapper{
            
            @AuraEnabled public string patientId;
            @AuraEnabled public string firstName;
            @AuraEnabled public string lastName;
            @AuraEnabled public string email;
            @AuraEnabled public string mobileNumber;
         // @AuraEnabled public string gender;
            @AuraEnabled public String appointmentStatus;
         // @AuraEnabled public String organizationName;
         // @AuraEnabled public string barCoderNumber;
            @AuraEnabled public String appointmentDate;
            @AuraEnabled public string appointmentTime;
          //@AuraEnabled public string batchId;
            @AuraEnabled public string cancellationReason;
            @AuraEnabled public string appointmentScheduleGenerationDate;
            @AuraEnabled public string appointmentCompleteDate;
            @AuraEnabled public string isSpanishTranslator;
            @AuraEnabled public string consultReason;
            @AuraEnabled public string consultReasonRecordtype;
            
            
            
            public AppointmentWrapper(Appointment__c app){
                
                this.patientId = app.Patient__r.Patient_Id__c != null ? app.Patient__r.Patient_Id__c : '';
                this.firstName = app.Patient__r.FirstName != null ? app.Patient__r.FirstName : '';
                this.lastname = app.Patient__r.LastName != null ? app.Patient__r.LastName : '';
                this.email =  app.Patient__r.email != null ? app.Patient__r.email : '';
                this.mobileNumber = app.Patient__r.MobilePhone != null ? app.Patient__r.MobilePhone : '' ;
                // this.gender = app.Patient__r.Gender__c != null ? app.Patient__r.Gender__c : '';
                this.appointmentStatus = app.status__c != null ? app.status__c : '';
                // this.organizationName = app.Patient__r.Account.Name != null ? app.Patient__r.Account.Name : '';
                // this.barCoderNumber = app.Barcode_ID__c != null ? app.Barcode_ID__c : '';  
                // this.dateOfSpecimenCollection = string.valueOf(Date.valueOf(app.createddate)) != null ? string.valueOf(Date.valueOf(app.createddate)) : '';
                this.appointmentDate = app.Formatted_Appointment_Start_Date__c != null ? app.Formatted_Appointment_Start_Date__c : '';
                // this.batchId = app.Batch__r.Name != null ? app.Batch__r.Name : '' ;
                this.appointmentTime = String.valueof(app.Formatted_Appointment_Start_Time__c) != null ? String.valueof(app.Formatted_Appointment_Start_Time__c) : '';
                this.appointmentCompleteDate = string.valueOf(app.Formatted_Complete_Date__c) != null ? string.valueOf(app.Formatted_Complete_Date__c) : '';
                this.cancellationReason = app.Reason_for_cancellation__c != null ? app.Reason_for_cancellation__c : '';
                this.appointmentScheduleGenerationDate = app.Appointment_Schedule_Generated_Date__c != null ? app.Appointment_Schedule_Generated_Date__c : '';
                this.isSpanishTranslator = String.valueOf(app.Patient__r.Need_Spanish_Interpreter__c) != null ? string.valueOf(app.Patient__r.Need_Spanish_Interpreter__c): '';
                this.consultReason = app.Patient__r.What_type_of_record_do_you_have_question__c != null ? app.Patient__r.What_type_of_record_do_you_have_question__c : '' ;
                this.consultReasonRecordtype =  app.Patient__r.Which_specialist_do_you_need__c != null ? app.Patient__r.Which_specialist_do_you_need__c : '' ;
               
            }
        }
        
    
}