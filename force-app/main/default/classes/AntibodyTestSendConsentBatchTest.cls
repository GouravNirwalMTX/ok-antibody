@isTest
private class AntibodyTestSendConsentBatchTest {
      @testsetup
    public static void setUpData(){
        Contact createContact = TestDataFactory.createContact('TestLastName','TestFirstName',false);
        createContact.Birthdate = System.today().addYears(-20);
        createContact.PHOCIS_Response_Count__c = '2';
        insert createContact;
        List<Antibody_Testing__c> testList = new List<Antibody_Testing__c>();
        Map<Integer,String> resultsMap = new Map<Integer,String> { 
            0 => 'Rejected', 
            1 => 'Negative' ,
            2 =>'Indeterminate Result'   
                };
        for(Integer i=0;i<3;i++){
            Antibody_Testing__c test = TestDataFactory.createTesting('Test'+i,false);
            test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test.Patient__c = createContact.Id;
            testList.add(test);
        }
        insert testList;
    }
    static testMethod void testConsentBatchForAntibody(){
         test.startTest(); 
         test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'body');
        Contact con = [select Id FROM Contact LIMIT 1];
        string q = 'SELECT Id,LastName, MobilePhone FROM Contact';
       
        Database.executeBatch(new AntibodyTestingSendConsentBatch (q,new List<Id>{con.Id},'resultSMS'));
        test.stopTest();
        
    }
    
    static testMethod void testFaultyResult(){
         test.startTest(); 
         test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'body');
        Contact con = [select Id FROM Contact LIMIT 1];
        string q = 'SELECT Id,LastName, MobilePhone FROM Contact';
       
        Database.executeBatch(new AntibodyTestingSendConsentBatch (q,new List<Id>{con.Id},'faultyResultSMS'));
        test.stopTest();
        
    }
}