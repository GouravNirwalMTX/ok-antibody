public class TestDataFactoryAppointmentSlot {
    public static Appointment_Slot__c createAppointmentSlot(Boolean isInsert,Id account){
        Appointment_Slot__c insertAppointmentSlot = new Appointment_Slot__c();
        insertAppointmentSlot.Account__c = account;
        insertAppointmentSlot.Date__c = system.today();
        insertAppointmentSlot.Start_Time__c = Time.newInstance(1, 0, 0, 0);
        insertAppointmentSlot.Start_Time__c = Time.newInstance(2, 0, 0, 0);
        insertAppointmentSlot.End_Time__c = Time.newInstance(2, 30, 0, 0);
        if(isInsert){
        insert insertAppointmentSlot;
        return insertAppointmentSlot;
    }
    return insertAppointmentSlot;
    }

}