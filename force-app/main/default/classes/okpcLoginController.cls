global class okpcLoginController {
    public static final String EMAIL_STRING = '@okhla.com';
    public okpcLoginController() {

    }
   
    @AuraEnabled
    public static String login(String username, String password, String startUrl, String delimiter) {//Added by Sajal
        System.debug('---username---'+username+delimiter);
        System.debug('---password---'+password);
        try{
            ApexPages.PageReference lgn = Site.login(username+delimiter, password, '/s');//Added by Sajal
            aura.redirect(lgn);
            return lgn.getUrl();
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()); 
        }
    }
    
    @AuraEnabled
    public static String communityLogin(String firstName, String userInfo, String delimiter) {//Added by Sajal
        
        String username = firstName+userInfo+EMAIL_STRING+delimiter;
        System.debug('userInfo ---'+ userInfo);
        System.debug('username '+ username);

        try{
            // AND (Email =: userInfo OR Phone =: userInfo) 
            ApexPages.PageReference lgn;
            List<User> users = [SELECT Password__c, Username FROM User WHERE FirstName =: firstName AND (Email =: userInfo OR Phone =: userInfo)  LIMIT 1];
            system.debug('users>>>>' + users);
            if(!users.isEmpty()){
                lgn = Site.login(users[0].Username, users[0].Password__c, '/s/home');//Added by Sajal
                aura.redirect(lgn);
            }else{
                Credential__c u = [SELECT Password__c, Username__c FROM Credential__c WHERE (Email__c =: userInfo OR Phone__c =: userInfo) AND Name =: firstName ];
                System.debug('user: '+u);
                lgn = Site.login(u.Username__c, u.Password__c, '/s/home');//Added by Sajal
                aura.redirect(lgn);
            }
            
            return lgn.getUrl();
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage()); 
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }

    @AuraEnabled
    global static String setExperienceId(String expId) {
        // Return null if there is no error, else it will return the error message 
        try {
            if (expId != null) {
                Site.setExperienceId(expId);
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }
    }   
}