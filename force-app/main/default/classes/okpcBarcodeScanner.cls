public without sharing class okpcBarcodeScanner {
    public String scanCode{get;set;}
    
    public Pagereference  saveTextValue(){
        Id appointmentid = ApexPages.currentPage().getParameters().get('id');
        string objName = appointmentid.getSobjectType().getDescribe().getName();
        // Id labId = ApexPages.currentPage().getParameters().get('labId');
        system.debug('scanCode' + scanCode);
        if(objName != 'Batch__c'){
            try {
                Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
                Id covidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
                Id covidRecordTypeIdForContact = OKPCRecordTypeUtility.retrieveRecordTypeId('Contact','Citizen_COVID');
                List<Appointment__c> appointmentList = new List<Appointment__c>();
                Antibody_Testing__c antiBodyTesting = new Antibody_Testing__c();
                for(Appointment__c appointment : [SELECT Id, Status__c, Appointment_Slot__c,Patient__r.RecordTypeId, Patient__c, Patient__r.Testing_Site__c FROM Appointment__c WHERE Id = : appointmentId]) {
                    appointment.Status__c = 'Completed';
                    //  appointment.Lab_Center__c = labId;
                    appointmentList.add(appointment);
                    if(appointment.Patient__r.RecordTypeId == covidRecordTypeIdForContact){
                        antiBodyTesting.RecordTypeId = covidRecordTypeId;
                    }else{
                        antiBodyTesting.RecordTypeId = antibodyRecordTypeId;
                    }
                    antiBodyTesting.Appointment__c = appointment.Id;
                    antiBodyTesting.Patient__c = appointment.Patient__c;
                    antiBodyTesting.Barcode_Id__c = scanCode;
                    antiBodyTesting.Testing_Site__c = appointment.Patient__r.Testing_Site__c;
                    //  antiBodyTesting.Laboratory__c = labId;
                }
                update appointmentList;
                insert antiBodyTesting;
            } catch(DMLException ex) {
                // return ex.getDMLMessage(0);
            } catch(Exception ex) {
                // return ex.getMessage();
            }
            
            return new Pagereference('/TestingSite/s/oktc-appointments');
            
            
        }else{
            try{
                List<Batch__c> toUpdateBatch = new List<Batch__c>();
                List<Antibody_Testing__c> antibodyList = new List<Antibody_Testing__c>();
                for(Batch__c b : [SELECT Id,Name,(SELECT ID,Name,Container_Barcode_Id__c FROM Antibodies_Testing__r),Container_Barcode_Id__c FROM Batch__c WHERE Id =: appointmentid]){
                    b.Container_Barcode_Id__c = scanCode;
                    toUpdateBatch.add(b);
                    for(Antibody_Testing__c a : b.Antibodies_Testing__r){
                        a.Container_Barcode_Id__c = scanCode;
                        antibodyList.add(a);
                    }
                }
                if(toUpdateBatch.size() > 0){
                    update toUpdateBatch;
                }
                if(antibodyList.size() > 0){
                    update antibodyList;
                }
             
            }catch(DMLException ex) {
                // return ex.getDMLMessage(0);
            } catch(Exception ex) {
                // return ex.getMessage();
            }
            return new Pagereference('/TestingSite/s/oktc-appointments');  
        }
        
        
    }
    
    public Pagereference  cancelValue(){
        Id appointmentid = ApexPages.currentPage().getParameters().get('id');
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Id covidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
        List<Appointment__c> appointmentList = new List<Appointment__c>();
       List<Antibody_Testing__c> antiBodyTesting = new List<Antibody_Testing__c>();
        string objName = appointmentid.getSobjectType().getDescribe().getName();
        if(objName == 'Batch__c'){
            Batch__c b = [SELECT Id,Name FROM Batch__C WHERE id =: appointmentid];
            for(Appointment__c appointment : [SELECT Id,Name,Batch__c, Status__c FROM Appointment__c WHERE Batch__c =: appointmentid ]){
                appointment.Batch__c = null;
                appointment.Lab_Center__c = null;
                appointment.Status__c = 'Completed';
                appointmentList.add(appointment);
            }
            for(Antibody_Testing__c antibody : [SELECT Id,Name,Batch__c FROM Antibody_Testing__c WHERE Batch__c =: appointmentid AND (RecordTypeId =: antibodyRecordTypeId OR RecordTypeId = :covidRecordTypeId)]){
                antibody.Batch__c = null;
                antibody.Laboratory__c = null;
                antiBodyTesting.add(antibody);             
            }
            
             if(appointmentList.size() > 0){
            update appointmentList;
        }
        if(antiBodyTesting.size() > 0){
            update antiBodyTesting;
        }
        delete b;
        return new Pagereference('/TestingSite/s/oktc-appointments');
        }else{
            return new Pagereference('/TestingSite/s/oktc-appointments');
        }
       
    }
}