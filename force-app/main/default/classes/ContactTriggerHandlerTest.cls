@isTest
public class ContactTriggerHandlerTest {
    @testsetup
    public static void setUpData(){
        User adminUser = [Select Id, UserRoleId From User Where isActive = true AND Profile.Name='System Administrator' Limit 1];
        system.runAs(adminUser){
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
        
        Contact createContact = TestDataFactoryContact.createCitizenContact(false , 'TestLastName','TestFirstName');
        createContact.Notify_Patients__c = true;
        createContact.AccountId=createAccountTestSite.id;
        createContact.Opt_out_for_SMS_Updates__c = false;
        createContact.recordtypeID=citizenConsultationVitalRecords_RT;
        insert createContact;
        
        contact[] createContact2=[select id,email,firstName,lastName,phone from contact where lastname='TestLastName' limit 1];
        
        string profileId=[select id,name from profile where name='Vital Records Citizen Lobby User' limit 1].id;
       // string roleId=[select id,name from userrole where name='CEO' limit 1].id;
        string contactType=System.Label.OKSF_Registration_Covid_Citizen;
        
        String newUsername=createContact2[0].email+'.dev';
        user u=new user();
        u.Username = createContact2[0].email; //Modified by Sajal
        u.put('Email', createContact2[0].email);
        u.ProfileId = profileId;
      //  u.UserRoleId=roleId;
        u.FirstName = createContact2[0].FirstName;
        u.LastName = createContact2[0].LastName;
        u.Password__c = 'test1233@N';
        u.Phone = createContact2[0].phone;
        u.ContactId = createContact2[0].Id;
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.timezonesidkey = 'America/Los_Angeles';
        u.alias = createContact2[0].FirstName.substring(0, 3);

        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
        u.put('CommunityNickname', nickname);
        insert u;
         
        Credential__c c = new Credential__c();
        c.username__c = createContact2[0].email+'.dev';
        c.Password__c = 'Covid2020#';
        c.Contact__c = createContact2[0].Id;
        c.Name = createContact2[0].FirstName;
        c.Email__c = createContact2[0].email;
        c.Phone__c = createContact2[0].phone;
        insert c;    
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        }
    }
    static testMethod void testUpdate(){
        contact[] createContact3=[select id,email,firstName,lastName,phone from contact where lastname='TestLastName' limit 1];
        
        user[] portalUser=[Select Id, UserRoleId From User Where isActive = true AND Profile.Name='System Administrator' Limit 1];
        system.runAs(portalUser[0]){
            createContact3[0].Pending_Confirmation__c = 'Reschedule';
            createContact3[0].Notify_Patients__c = true;
            createContact3[0].FirstName='satishtest';
            createContact3[0].lastName='satishtest';
            createContact3[0].email='satishtest@mtxwe.com';
            
            update createContact3[0];
        }
        
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
    } 
     static testMethod void testUpdateDeclineStatus(){
        
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSiterevr', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Id contactCitizen_AntiBodyTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
        
        Contact createContact3 = TestDataFactoryContact.createCitizenContact(false , 'TestLastName34','TestFirstName34');
        createContact3.Notify_Patients__c = false;
        createContact3.Opt_out_for_SMS_Updates__c = false;
        createContact3.AccountId=createAccountTestSite.id;
        createContact3.recordtypeID= contactCitizen_AntiBodyTypeId;
        insert createContact3;
         
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact3.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Scheduled';

        insert createAppointment;
         
        createContact3.Pending_Confirmation__c = 'Declined';
        createContact3.Notify_Patients__c = true;
        update createContact3;
         
        
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
    }
}