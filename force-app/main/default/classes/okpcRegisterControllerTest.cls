@isTest
public class okpcRegisterControllerTest {
		private static final string citizenId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        private static final string citizenAccount = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
    @testSetup static void setup() {
    
       

        Account createAccountCitizen = TestDataFactoryAccount.createAccount('TestSite', citizenAccount, false);
        createAccountCitizen.BillingCity = 'testCity';
        createAccountCitizen.BillingCountry = 'testCountry';
        createAccountCitizen.BillingState = 'testState';
        createAccountCitizen.BillingStreet = 'testStreet';
        createAccountCitizen.BillingPostalCode = '12345';
        createAccountCitizen.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountCitizen.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountCitizen;

        Contact con = new Contact (
            AccountId = createAccountCitizen.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            RecordTypeId = citizenId
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountCitizen.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountCitizen.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        
    }

    public static testMethod void test_01() {
        //List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'testLast' LIMIT 1];
        
        List<Contact> contactList = [Select Id , Name , Patient_Id__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'testUser@gmail.com.test.citizen',
            email = 'testUser@gmail.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id,
            IsActive = true
        );
        insert newUser;

        System.runAs(newUser){
            Map < String, Object > mapVal = okpcRegisterController.selfRegister(contactList[0].Patient_Id__c, 'testportalTestUserv1@gmail.com', 'testUser@gmail.com.test', 'lo9(ki', 'lo9(ki', '.citizen');
        }
    }

    public static testMethod void test_0() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        insert con;
        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'test@gmail.com', 'test123@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen');
    }

    public static testMethod void test() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.Email = 'test@gmail.com';
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Citizen (COVID)').getRecordTypeId();
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where LastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'test@gmail.com', 'testtest123@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen');
    }

    
    public static testMethod void test1() {
        System.assertEquals(okpcRegisterController.forgotPassword('fakeUser', '.citizen'), Label.Site.invalid_email);
        System.assertEquals(okpcRegisterController.forgotPassword('testtest123@gmail.com', '.citizen'), 'success');

        // String s = okpcRegisterController.forgotPassword('testtest123@gmail.com');

    }
    public static testMethod void test2() {
        Id s = okpcRegisterController.getProfileId('oktc');
        Id s1 = okpcRegisterController.getProfileId('okcp');
        Id s2 = okpcRegisterController.getProfileId('oklc');

    }
    public static testMethod void test3() {
        Id s = okpcRegisterController.getProfileId('abc');
    }
    public static testMethod void test4() {
        Contact con = TestDataFactoryContact.createContact(true, 'test', 'testFirst');
        okpcRegisterController.ContactInfoWrapper wrapper = okpcRegisterController.getContactInformation(con.Id);
    }

    
    public static testMethod void test6() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister('oktc', 'test123@gmail.com', 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen');
        Map < String, Object > mapVal1 = okpcRegisterController.selfRegister('okcp', 'test123@gmail.com', 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen');
        Map < String, Object > mapVal2 = okpcRegisterController.selfRegister('oklc', 'test123@gmail.com', 'testtest@gmail.com', 'lo9(ki8*ju7&', 'lo9(ki8*ju7&', '.citizen');

    }

    public static testMethod void test7() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = citizenId;
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'testtestRegister@gmail.com', 'testtest@gmail.com.citizen', 'lo9(ki', 'lo9(ki', '.citizen');

    }

    public static testMethod void test8() {
        Contact con = TestDataFactoryContact.createContact(false, 'testRegister', 'testFirst');
        con.MobilePhone = '(702)379-44151';
        con.RecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Contact','Lab_User');
        insert con;

        Contact con1 = [Select Id, Patient_Id__c FROM Contact where lastName = 'testRegister'
            limit 1
        ];
        System.debug('con--' + con1);
        //[Selct Id,Patient_Id__c,Email from Contact WHERE Id =:con.Id]
        Map < String, Object > mapVal = okpcRegisterController.selfRegister(con1.Patient_Id__c, 'testtestRegister@gmail.com', 'testtest@gmail.com.citizen', 'lo9(ki', 'lo9(ki', '.citizen');

    }






}