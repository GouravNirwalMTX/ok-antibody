public without sharing class OKPCDashboardController {
    @AuraEnabled
    public static Map<String,Object> retrieveResult(){
        Map<String,Object> result = new Map<String,Object>();
        List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId
                               FROM User WHERE Id = : UserInfo.getUserId()];
        if(userList.size() > 0) {
            List<Contact> contactList = [SELECT Id,MobilePhone,Email,Auto_Scheduled__c,Dependent__c , Name, MailingStreet ,MailingCity ,MailingState ,MailingPostalCode,Patient_Id__c FROM Contact WHERE Id =: userList[0].ContactId ];
            if(contactList.size() > 0) {
                result.put('contact',contactList[0]);
                List<Appointment__c> appointmentList = [SELECT Id,Appointment_Start_Date_v1__c,Appointment_Start_Time_v1__c, Status__c, Patient__r.Testing_Site__c, Patient__r.Testing_Site__r.Name, Patient__r.Testing_Site__r.BillingStreet ,Patient__r.Testing_Site__r.BillingCity ,Testing_Site_Type__c,Appointment_Complete_Date__c,Appointment_Start_Date_Time__c,
                                                        Patient__r.Testing_Site__r.BillingState ,Patient__r.Testing_Site__r.BillingPostalCode, Appointment_Slot__r.Start_Time__c, Appointment_Slot__r.Date__c, Patient__r.Preferred_Date__c, Patient__r.Preferred_Time__c,(SELECT Id, Laboratory__c, Laboratory__r.Name, Createddate, Results__c,Appointment__r.Status__c, Source_of_Specimen__c, Formatted_lab_test_completion_date__c  FROM Antibodies_Testing__r WHERE (Appointment__r.Status__c = 'Completed' OR Appointment__r.Status__c = 'Closed')),
                                                        Testing_Site_Account__c,Testing_Site_Account__r.Name,Testing_Site_Account__r.BillingStreet,Testing_Site_Account__r.BillingCity,Testing_Site_Account__r.BillingState,Testing_Site_Account__r.BillingPostalCode
                                                        FROM Appointment__c WHERE Patient__c =: contactList[0].Id ];
                if(appointmentList.size() > 0){
                    result.put('appointment',appointmentList);
                    result.put('vitalAppointment',appointmentList);
                  /*  if(appointmentList[0].Status__c == 'Completed' || appointmentList[0].Status__c == 'Closed'){
                        List<Antibody_Testing__c> testings = [SELECT Id, Laboratory__c, Laboratory__r.Name, Createddate, Results__c,Appointment__r.Status__c, Source_of_Specimen__c FROM Antibody_Testing__c WHERE Appointment__c IN: appointmentList AND (Appointment__r.Status__c = 'Completed' OR Appointment__r.Status__c = 'Closed')];
                        if(testings.size() > 0){
                            Testing testingRecord = new Testing();
                            testingRecord.testingId = testings[0].Id;
                            testingRecord.labName = String.isNotBlank(testings[0].Laboratory__c) ? testings[0].Laboratory__r.Name : '';
                            testingRecord.result = String.isNotBlank(testings[0].Results__c) ? testings[0].Results__c : '';
                            testingRecord.specimen = String.isNotBlank(testings[0].Source_of_Specimen__c) ? testings[0].Source_of_Specimen__c : '';
                            testingRecord.createddate = testings[0].Createddate;
                            result.put('results',testingRecord);
                        }
                    } */
                }
            }
        }
        return result;
    }
    
    @AuraEnabled
    public static Map<String,Object> retrieveResultCovidPortal(){
        Map<String,Object> result = new Map<String,Object>();
        List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId
                               FROM User WHERE Id = : UserInfo.getUserId()];
        if(userList.size() > 0) {
            List<Contact> contactList = [SELECT Id,MobilePhone,Email,Auto_Scheduled__c,Dependent__c , Name, MailingStreet ,MailingCity ,MailingState ,MailingPostalCode,Patient_Id__c FROM Contact WHERE Id =: userList[0].ContactId OR Primary_Contact__c = :userList[0].ContactId 
                                         ORDER BY CreatedDate ASC];
            if(contactList.size() > 0) {
                result.put('contact',contactList);
                List<Appointment__c> appointmentList = [SELECT Id,Appointment_Start_Date_v1__c,Appointment_Start_Time_v1__c,Appointment_Inprogress_Date_Time__c, Status__c,Patient__r.Name, Patient__r.Testing_Site__c, Patient__r.Testing_Site__r.Name, Patient__r.Testing_Site__r.BillingStreet ,Patient__r.Testing_Site__r.BillingCity ,Testing_Site_Type__c,Appointment_Complete_Date__c,Appointment_Start_Date_Time__c,
                                                        Patient__r.Testing_Site__r.BillingState ,Patient__r.Testing_Site__r.BillingPostalCode, Appointment_Slot__r.Start_Time__c, Appointment_Slot__r.Date__c, Patient__r.Preferred_Date__c, Patient__r.Preferred_Time__c,(SELECT Id, Laboratory__c, Laboratory__r.Name, Createddate, Results__c,Appointment__r.Status__c,Test_Name__c ,Notes__c , Source_of_Specimen__c, Formatted_lab_test_completion_date__c,Date_of_Lab_Test_Completion__c  FROM Antibodies_Testing__r WHERE (Appointment__r.Status__c = 'Completed' OR Appointment__r.Status__c = 'Closed')),
                                                        Testing_Site_Account__c,Testing_Site_Account__r.Name,Testing_Site_Account__r.BillingStreet,Testing_Site_Account__r.BillingCity,Testing_Site_Account__r.BillingState,Testing_Site_Account__r.BillingPostalCode
                                                        FROM Appointment__c WHERE Patient__c IN: new Map<Id, Contact>(contactList).keySet()];
                if(appointmentList.size() > 0){
                    result.put('appointment',appointmentList);
                    result.put('vitalAppointment',appointmentList);
                  /*  if(appointmentList[0].Status__c == 'Completed' || appointmentList[0].Status__c == 'Closed'){
                        List<Antibody_Testing__c> testings = [SELECT Id, Laboratory__c, Laboratory__r.Name, Createddate, Results__c,Appointment__r.Status__c, Source_of_Specimen__c FROM Antibody_Testing__c WHERE Appointment__c IN: appointmentList AND (Appointment__r.Status__c = 'Completed' OR Appointment__r.Status__c = 'Closed')];
                        if(testings.size() > 0){
                            Testing testingRecord = new Testing();
                            testingRecord.testingId = testings[0].Id;
                            testingRecord.labName = String.isNotBlank(testings[0].Laboratory__c) ? testings[0].Laboratory__r.Name : '';
                            testingRecord.result = String.isNotBlank(testings[0].Results__c) ? testings[0].Results__c : '';
                            testingRecord.specimen = String.isNotBlank(testings[0].Source_of_Specimen__c) ? testings[0].Source_of_Specimen__c : '';
                            testingRecord.createddate = testings[0].Createddate;
                            result.put('results',testingRecord);
                        }
                    } */
                }
            }
        }
        return result;
    }
    
    @AuraEnabled
    public static String createAppointment(string contactId){
        Appointment__c createAppointment = new Appointment__c();
        createAppointment.Patient__c = contactId;
        createAppointment.Status__c = 'Eligible';
        insert createAppointment;
        return String.valueOf(createAppointment.Id);
    }

    @AuraEnabled
    public static String deleteAppointment(string appointmentId){
        if(!String.isBlank(appointmentId)){
            Appointment__c appntment = new Appointment__c(Id = appointmentId);
            delete appntment;
            return 'success';
        }
        return null;
    }
    
    /*
    public class Testing{
        @AuraEnabled public string testingId {get; set;}
        @AuraEnabled public string labName {get; set;}
        @AuraEnabled public string result {get; set;}
        @AuraEnabled public string specimen {get; set;}
        @AuraEnabled public datetime createddate {get; set;}

    } */
}