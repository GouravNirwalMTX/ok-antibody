@RestResource(urlMapping='/TwilioResponseAPI/*')
global class TwilioResponseAPI{
    @HttpGet
    global static void getSMS() {

       // Store the request 
       RestRequest req = RestContext.request;
       
       // Store the HTTP parameters from the request in a Map
       Map<String, String> sms = req.params ;
 
       String fromPhNumber ;
       String smsBody ;
       
       // get the phone number from the response 
       if (sms.containsKey('From')){
           fromPhNumber = sms.get('From') ;
       }

       // get the body of sms from the response 
       if (sms.containsKey('Body')){
          smsBody = sms.get('Body') ;
       }       
       System.debug('This is you msg content you received : ' + smsBody);
    }
}