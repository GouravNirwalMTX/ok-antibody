@isTest
public class TestDataFactoryInsuranceCompanyDetail {
    public static Insurance_Company_Details__c createInsuranceCompanyDetail(String name, Boolean isMedicaid, Boolean isMedicare, Boolean isOther, Boolean isInsert) {
        Insurance_Company_Details__c insCmpDetails = new Insurance_Company_Details__c();
        insCmpDetails.Name = name;
        insCmpDetails.Medicaid__c = isMedicaid;
        insCmpDetails.Medicare__c = isMedicare;
        insCmpDetails.Other__c = isOther;
        if(isInsert){
            insert insCmpDetails;
        }
        return insCmpDetails;
    }
}