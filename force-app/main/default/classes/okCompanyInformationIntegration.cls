public class okCompanyInformationIntegration implements Schedulable, Database.Batchable<sObject>,Database.AllowsCallouts{
   
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Name FROM User LIMIT 1';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
       
        String privateInsuranceAPIkey = System.Label.Private_Insurance_API_Key;
        String privateInsuranceBaseURL = System.Label.Private_Insurance_API; 
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(privateInsuranceBaseURL + privateInsuranceAPIkey);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        List<Object> resultData = (List<Object>) JSON.deserializeUntyped(response.getBody());
        createInsuranceRecord(resultData);
        //GFN_CaseWrapper resultData = (GFN_CaseWrapper) JSON.deserialize(response.getBody(), GFN_CaseWrapper.class);
        system.debug('dese>>' + resultData);
       
    }
 /*   public static List<Object>  makeGetCallout() {
        String privateInsuranceAPIkey = System.Label.Private_Insurance_API_Key;
        String privateInsuranceBaseURL = System.Label.Private_Insurance_API; 
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(privateInsuranceBaseURL + privateInsuranceAPIkey);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        List<Object> resultData = (List<Object>) JSON.deserializeUntyped(response.getBody());
        createInsuranceRecord(resultData);
        //GFN_CaseWrapper resultData = (GFN_CaseWrapper) JSON.deserialize(response.getBody(), GFN_CaseWrapper.class);
        system.debug('dese>>' + resultData);
        return resultData;
    }
    */
    public static void createInsuranceRecord( List<Object> data){
        
        List<Insurance_Company_Details__c> companyToInsert = new List<Insurance_Company_Details__c>();
        for(Object record : data){
            Map<String,Object> rec= (Map<String,Object>)record;
            Insurance_Company_Details__c i = new Insurance_Company_Details__c();
            i.put('Private_Insurance_ID__c',rec.get('privateInsuranceID'));
            i.put('Carrier_Code__c',rec.get('carrierCode'));
            i.put('Name',rec.get('comboBoxDesc'));
            i.put('Medicaid__c',rec.get('isMedicaid'));
            i.put('Medicare__c',rec.get('isMedicare'));
            i.put('Other__c',rec.get('isOther'));
            i.put('Is_Group_Number_Required__c',rec.get('isGroupNumberRequired'));
            i.put('Member_Id_Validation_Regex__c',rec.get('memberIdValidationRegex'));
            i.put('Private_Insurance_Description__c',rec.get('privateINsuranceDesc'));
            i.put('Resubmit_with_Original_Status__c',rec.get('resubmitWithOriginalStatus') == 0 ? False : True);
            i.put('Claim_Filing_Indicator_Code__c',rec.get('claimFilingIndicatorCode'));
            i.put('Public_Payer_Code__c', rec.get('publicPayerCode'));
            i.put('Street_Address__c',rec.get('streetAddress'));
            i.put('State__c',rec.get('state'));
            i.put('Zip__c',rec.get('zip')); 
            
            companyToInsert.add(i);
        }  
        if(!companyToInsert.isEmpty()){
            Schema.SObjectField f = Insurance_Company_Details__c.Fields.Private_Insurance_ID__c;
            List<Database.UpsertResult> sr = Database.upsert(companyToInsert,f,false);
            for(Database.UpsertResult dd : sr){
                
                for(Database.Error err : dd.getErrors()){
                    system.debug('upsertResult>>>' + err);
                }
            }
        }
    }
    
     public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
    
    public void execute(SchedulableContext scon) {
      Database.executeBatch(new okCompanyInformationIntegration());
   }
}