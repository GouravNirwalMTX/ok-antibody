public with sharing class CustomLookupController {
    public CustomLookupController() {

    }
    @AuraEnabled(cacheable = true)
    public static List<SObject> findRecords(String searchKey, String objectName, String contactId){
        List<String> accountIds = new List<String>();
        for(AccountContactRelation acr : [SELECT accountid FROM AccountContactRelation WHERE ContactId =: contactId]){
           accountIds.add(acr.AccountId);
        }
        system.debug('acc Ids ' +contactId);

        String key = '%' + searchKey + '%';
        String QUERY = 'Select Id, Name From '+objectName +' Where Name LIKE :key AND Id NOT In: accountIds AND RecordType.Name = \'Vital Records\'';
        System.debug(System.LoggingLevel.DEBUG, QUERY);
        List<SObject> sObjectList = Database.query(QUERY);
        return sObjectList;
    }
    
    
    @AuraEnabled
    public static void createRelationship(String contactId, String accountId){
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
    }
}