public without sharing class OKCP_ViewBarcodeController {
    public Antibody_Testing__c testing{get;set;}
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String barcodeId{get;set;}
    public String barcodeImage{get;set;}
    public String collectionDate{get;set;}
    public String birthdate{get;set;}
    
    public OKCP_ViewBarcodeController() {
        String appId = ApexPages.CurrentPage().getparameters().get('id');
        testing = new Antibody_Testing__c();
        system.debug('apppp ' + appID);
        
        if(String.isNotBlank(appId)){
            List<Antibody_Testing__c> testings = [SELECT Id, Date_of_Specimen_Collection__c, Name, Patient__r.Birthdate, Patient__r.FirstName, Patient__r.LastName, Barcode_ID__c 
            FROM Antibody_Testing__c WHERE Appointment__c = :appId];
            if(!testings.isEmpty()){
                testing = testings[0];
                firstName = testings[0].Patient__r.FirstName;
                lastName = testings[0].Patient__r.LastName;
                barcodeId = testings[0].Barcode_ID__c;
                barcodeImage = 'https://barcode.tec-it.com/barcode.ashx?data='+testings[0].Barcode_ID__c+'&code=Code128&dpi=96&dataseparator=';
                collectionDate = testings[0].Date_of_Specimen_Collection__c != null ? testings[0].Date_of_Specimen_Collection__c : '';
                if(testings[0].Patient__r.Birthdate != null){
                    Date dToday = testings[0].Patient__r.Birthdate;
                    DateTime dt = DateTime.newInstance(dToday.year(), dToday.month(),dToday.day());  
                    birthdate = dt.format('MM/dd/yyyy');                 
                }
                
            }
        }
    }
   
    
   @AuraEnabled 
   public static Boolean getBarCode(String appointmentId){
       List<Antibody_Testing__c> slots = [SELECT Id, Barcode_ID__c FROM Antibody_Testing__c WHERE Appointment__c =: appointmentId];
       if(!slots.isEmpty() && slots[0].Barcode_ID__c != null){
           return true;
        }
        return false;
       
   }
}