public without sharing class MessageTriggerHandler {

    public static void getMessagesAndChangeStatusOnContact(List<Messages__c>newList, Map<Id,Messages__c>oldMap){
        Boolean wrongReply=false;
        Boolean isDenied=false;

        Map<String,String> mapofNumberAndText = new Map<String,String>();
        //Map<String,String> mapofNumberAndTextFormatted = new Map<String,String>();
        Map<String,Contact> mapofNumberAndPHOCIS = new Map<String,Contact>();

        List<Contact> listOfContact = new List<Contact>();
        List<Antibody_Testing__c> listOfTesting = new List<Antibody_Testing__c>();
        List<Id> listOfContactForFollowUp = new List<Id>();
        List<Id> testingContacts = new List<Id>();
        List<Id> randomReplyContacts = new List<Id>();

        for(Messages__c message : newList){
            //formatting mobilephone same as stored in SF Contact object
            //String s = '(' + message.from__c.right(10).substring(0, 3) + ') ' + message.from__c.right(10).substring(3, 6) + '-' + message.from__c.right(10).substring(6);
            //mapofNumberAndTextFormatted.put(s,message.messages__c);
            mapofNumberAndText.put(message.from__c.right(10),message.messages__c);
        }
        System.debug('mapofNumberAndText====>>'+mapofNumberAndText);
        for(Contact contactRecord : [select Pending_Confirmation__c,RecordType.DeveloperName, Account.Communication__c,PHOCIS_Response_Count__c,MobileTrimmer__c,MobilePhone from Contact where Pending_Confirmation__c = 'Pending' AND RecordType.DeveloperName = 'Citizen' and MobileTrimmer__c  in:mapofNumberAndText.keySet()]){
            system.debug('contactRecord=='+contactRecord);
            if(mapofNumberAndText.containsKey(contactRecord.MobileTrimmer__c)){
                if(mapofNumberAndText.get(contactRecord.MobileTrimmer__c)=='Y' || mapofNumberAndText.get(contactRecord.MobileTrimmer__c)=='Yes'||mapofNumberAndText.get(contactRecord.MobileTrimmer__c)=='y'||mapofNumberAndText.get(contactRecord.MobileTrimmer__c)=='yes'){
                    contactRecord.Pending_Confirmation__c = 'Confirmed';
                    listOfContactForFollowUp.add(contactRecord.Id);
                }
                if(mapofNumberAndText.get(contactRecord.MobilePhone)=='N'){
                    contactRecord.Pending_Confirmation__c = 'Declined';
                }
                if(mapofNumberAndText.get(contactRecord.MobilePhone)=='R'){
                    contactRecord.Pending_Confirmation__c = 'Reschedule';
                }
                listOfContact.add(contactRecord);
            }
        }
        system.debug('listOfContactForFollowUp ' + listOfContactForFollowUp);
        for(Antibody_Testing__c testingRecord : [select Consent__c,Consent_Content__c,Patient__r.Account.Communication__c,Patient__r.MobileTrimmer__c , Patient__r.PHOCIS_Response_Count__c,Results__c,Patient__r.MobilePhone,Patient__r.Id,Patient__r.PHOCIS__c from Antibody_Testing__c where Patient__r.MobileTrimmer__c  in:mapofNumberAndText.keySet()]){
            System.debug('testingRecord>>'+testingRecord);
            if(mapofNumberAndText.containsKey(testingRecord.Patient__r.MobileTrimmer__c) && (testingRecord.Patient__r.Account.Communication__c=='COVID Results')){
                if(testingRecord.Consent_Content__c!='Consent Denied' && (mapofNumberAndText.get(testingRecord.Patient__r.MobileTrimmer__c)=='Y' || mapofNumberAndText.get(testingRecord.Patient__r.MobileTrimmer__c)=='Yes' ||
                    mapofNumberAndText.get(testingRecord.Patient__r.MobileTrimmer__c)=='yes' || mapofNumberAndText.get(testingRecord.Patient__r.MobileTrimmer__c)=='y')){
                    testingRecord.Consent_Content__c = 'Consent received';
                    testingRecord.Consent__c = true;
                    testingRecord.Test_Results_Notified__c = true;
                    testingContacts.add(testingRecord.Patient__r.Id);
                }
                if(mapofNumberAndText.get(testingRecord.Patient__r.MobileTrimmer__c)=='N' || mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)=='No' || mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)=='no' || mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)=='n'){
                    testingRecord.Consent__c = false;
                    isDenied=true;
                    testingRecord.Consent_Content__c='Consent Denied';
                    //testingContacts.add(testingRecord.Patient__r.Id);
                }

                if(testingRecord.Consent__c==false && mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)!='Y' && mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)!='Yes'
                        && testingRecord.Consent_Content__c!='Consent Denied' && mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)!='yes' && mapofNumberAndText.get(testingRecord.Patient__r.MobilePhone)!='y'){
                    System.debug('inside wrong reply');
                    wrongReply=true;
                    randomReplyContacts.add(testingRecord.Patient__r.Id);
                }
                listOfTesting.add(testingRecord);
            }
        }
      
        String query = 'SELECT Id,Account.Communication__c,PHOCIS_Response_Count__c,MobilePhone,MobileTrimmer__c,Testing_Site__c, RecordType.DeveloperName FROM Contact ';
        //********Contact Record Handle
        if(listOfContactForFollowUp.size()>0){ 
            //This is the confirmation for your appointment with "Account_Name" located at "Billing Address" on "Appointment Date/Time". Please confirm with a "Y" or "N" to accept or decline this appointment. If this time doesn't work please respond with a "R" to request to be rescheduled.
            //String smsbody = 'Appointment Created';
            SendSMSBatch smsBatch = new SendSMSBatch(query, listOfContactForFollowUp, 'appointmentfollowup');
            Database.executeBatch(smsBatch, 1);
        }
        //********Testing Record Handle
       /* if(testingContacts.size()>0){
            System.debug('Sending result...');
            AntibodyTestingSendConsentBatch testResult = new AntibodyTestingSendConsentBatch(query, testingContacts, 'TestResult');
            Database.executeBatch(testResult, 1);
        }
        if(wrongReply==true && randomReplyContacts.size()>0 && isDenied==false){
            System.debug('wrongReply firing..');
            AntibodyTestingSendConsentBatch wrong = new AntibodyTestingSendConsentBatch(query, randomReplyContacts, 'wrongReply');
            Database.executeBatch(wrong, 1);
        }*/

        if(!listOfTesting.isEmpty()){
            update listOfTesting;
        }
        if(!listOfContact.isEmpty()){
            update listOfContact;
        }
    }
}