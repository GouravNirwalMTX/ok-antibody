public class GoogleAuthProvider {

    private String accessToken;
    private String errorMessage;
	private String authID;
    private String projectID;
    private String datasetID;
    private String tableID;
    
    //Constructor 
    public GoogleAuthProvider(String authID) {
        this.authID = authID;
    }
	
    //Gathers information from the created metadata object and checks whether or not there is access between the two systems, sets actual values of JWT callout. 
    public Boolean authorize() {
        try {
            List<Google_Oauth2__mdt> mdts = [SELECT Issuer__c, PKCS8__c, ProjectID__c, DatasetID__c, TableID__c FROM Google_Oauth2__mdt where ImplementationID__c =: this.authID];

            if (mdts.size()==0) {
                this.errorMessage = 'No custom metadata type record found for authorization';
                return false;
            }

            Google_Oauth2__mdt mdt = mdts.get(0);
            this.projectID = mdt.ProjectID__c;
            this.datasetID = mdt.DatasetID__c;
            this.tableID = mdt.TableID__c;
            
            String issuer = mdt.Issuer__c;
            String privateKey = mdt.PKCS8__c;
            privateKey = privateKey.replace('\\n', '');
			
            
            JWT jwt = new JWT('RS256');
            jwt.pkcs8 = privateKey;
            jwt.iss = issuer;
            jwt.aud = 'https://accounts.google.com/o/oauth2/token';
            jwt.sub = issuer;
            Map<String,String> claims = new  Map<String,String>();
            claims.put('scope','https://www.googleapis.com/auth/bigquery');
            jwt.claims = claims;
            Boolean status = fetchAccessToken('https://accounts.google.com/o/oauth2/token', jwt);
            return status;
        } catch (Exception e) {
            this.errorMessage = e.getMessage();
            return false;
        }
    }

    //Calls an Http Request and fetches the Access token to be used in the REST POST request 
    private Boolean fetchAccessToken(String tokenEndpoint, JWT jwt) {
        String access_token = null;
        String body = 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=' + jwt.issue();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(tokenEndpoint);
        req.setHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setBody(body);
        Http http = new Http();
        HTTPResponse res = http.send(req);

        if ( res.getStatusCode() == 200 ) {
            System.JSONParser parser = System.JSON.createParser(res.getBody());
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    access_token = parser.getText();
                    break;
                }
            }
        } else {
            this.errorMessage = res.getStatus();
            return false;
        }
        this.accessToken = access_token;
        return true;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String getAuthID() {
        return this.authID;
    }
    
    public String getProjectID() {
        return this.projectID;
    }
    
    public String getDatasetID() {
        return this.datasetID;
    }
    
    public String getTableID() {
        return this.tableID;
    }
}