@isTest
public class OKCP_ViewBarcodeControllerTest {
    @TestSetup
    static void makeData(){
         String LabSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
         Id conRT_consultation_portal = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
    
         
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'Vital Records';
        insert acc2;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, true);
        Account createAccountLabSite = TestDataFactoryAccount.createAccount('LabSite', LabSite, true);
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = conRT_consultation_portal;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        createContact2.Testing_Site__c = acc2.Id;
        insert createContact2;
      
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = createContact2.Id;
        createAppointment.Status__c=''; 
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;
        createAppointment.Status__c='Scheduled';
        update createAppointment;
        Antibody_Testing__c testing = TestDataFactory.createTesting('TestInsert',false);
        testing.Results__c = 'Positive';
        testing.Patient__c = createAppointment.Patient__c;
        testing.Appointment__c = createAppointment.Id;
        insert testing;
    }
    
     static testMethod void testMethod2(){
         
         Appointment__c app = [SELECT Id, Patient__c from Appointment__c LIMIT 1];
         Antibody_Testing__c testing = TestDataFactory.createTesting('TestInsert',false);
         testing.Results__c = 'Positive';
         testing.Patient__c = app.Patient__c;
         testing.Appointment__c = app.Id;
        
         insert testing;
         system.debug('apppp **** ' + app);
         ApexPages.currentPage().getParameters().put('Id',app.id);
         OKCP_ViewBarcodeController ok = new OKCP_ViewBarcodeController();
     }
     
      static testMethod void getBarCodeTest(){
          Appointment__c app = [SELECT Id, Patient__c from Appointment__c LIMIT 1];
          OKCP_ViewBarcodeController.getBarCode(app.Id);
      
      }
    
}