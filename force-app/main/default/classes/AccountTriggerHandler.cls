public with sharing class AccountTriggerHandler {
    Private static Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    public static void beforeInsert(List<Account> accountList) {
        updateBussinessTime(accountList);
        updateOrderProviderId(accountList,null);
    }
    public static void beforeUpdate(List<Account> accountList, Map<Id, Account> oldMap){
        checkAvailableSlots(accountList,oldMap); //Check if appoints
        updateOrderProviderId(accountList,oldMap);
    }
    
    public static void updateOrderProviderId(List<Account> accountList, Map<Id, Account> oldMap) {
        Map<String,Ordering_Physician_Config__mdt> lastNameMetaMap = new Map<String,Ordering_Physician_Config__mdt>();  
        
       List<Ordering_Physician_Config__mdt> orderProvider = [SELECT MasterLabel,First_Name__C,Last_Name__C,Id__c FROM Ordering_Physician_Config__mdt];
        for(Ordering_Physician_Config__mdt seletctedProvider : orderProvider){
            lastNameMetaMap.put(seletctedProvider.First_Name__C,seletctedProvider);
        }
        for(Account account : accountList) {
            if(account.Ordering_Provider_First_Name__c != null && recordTypeInfosMap.containsKey('Test_Center') && account.RecordTypeId == recordTypeInfosMap.get('Test_Center').RecordTypeId){
                if(lastNameMetaMap.containsKey(account.Ordering_Provider_First_Name__c)){
                    system.Debug('Old map>>>' + oldMap);
                    if(oldMap != null && oldMap.get(account.id).Ordering_Provider_First_Name__c != account.Ordering_Provider_First_Name__c || oldMap == null){
                    account.Ordering_Provider_Last_Name__c = lastNameMetaMap.get(account.Ordering_Provider_First_Name__c).Last_Name__c;
                    account.Ordering_Provider_Id__c = lastNameMetaMap.get(account.Ordering_Provider_First_Name__c).Id__c;
                    }
                }
                
            }
            
          /*  if(recordTypeInfosMap.containsKey('Test_Center') && account.RecordTypeId == recordTypeInfosMap.get('Test_Center').RecordTypeId) {
                if(account.Ordering_Provider_First_Name__c != null && lastNameMetaMap.containsKey(account.Ordering_Provider_First_Name__c)){
                    account.Ordering_Provider_Last_Name__c = lastNameMetaMap.get(account.Ordering_Provider_Last_Name__c).Last_Name__C;
                    account.Ordering_Provider_Id__c = lastNameMetaMap.get(account.Ordering_Provider_First_Name__c).Id__c;
                }
            } */
        } 
    }

    public static void updateBussinessTime(List<Account> accountList) {
       // Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
        for(Account account : accountList) {
            if(recordTypeInfosMap.containsKey('Test_Center') && account.RecordTypeId == recordTypeInfosMap.get('Test_Center').RecordTypeId) {
                if(account.Business_Start_Time__c == null) {
                    account.Business_Start_Time__c = Time.newInstance(7, 0, 0, 0);
                }
                if(account.Business_End_Time__c == null) {
                    account.Business_End_Time__c = Time.newInstance(18, 0, 0, 0);
                }
            }
        }
    }
/**************************************************************************************
* @Description  This method checks if there are any appointment slots for today or for any future date presents, if so, don't allow to changing Appointment frequency field value.
* @Param		List<Account> - Trigger.New
**************************************************************************************/ 

    public static void checkAvailableSlots(List<Account> accountList,Map<Id, Account> oldMap){
           Set<Id> afAccountIds = new Set<Id>(); //Account Ids of records only where Appointment_Frequency__c is being changed.
           Set<Id> accountIds = new Set<Id>(); 
           for(Account account :  accountList){
                if(oldMap.get(account.Id).Appointment_Frequency__c != account.Appointment_Frequency__c){
                    afAccountIds.add(account.Id);
                }
            }
            if(!afAccountIds.isEmpty()){
                for(AggregateResult ag : [SELECT COUNT(Id) slotsCount, Account__c 
                FROM Appointment_Slot__c
                WHERE Account__c  IN :afAccountIds AND Date__c >= :System.today() AND Account__r.RecordTypeId = :Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                .get('Vital_Records').getRecordTypeId()
                GROUP BY Account__c
                HAVING Count(Id) > 0]){
                    accountIds.add((Id)ag.get('Account__c'));
                }
                if(!accountIds.isEmpty()){
                    for(Account account :  accountList){
                        if(accountIds.contains(account.Id) && oldMap.get(account.Id).Appointment_Frequency__c != account.Appointment_Frequency__c){
                             account.addError('You can\'t update Appointment Frequency.');
                        }
                    }
                }
            }
           

    }
}