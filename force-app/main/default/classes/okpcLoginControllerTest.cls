@isTest
public class okpcLoginControllerTest {

    public static testMethod void test(){
        okpcLoginController controller = new okpcLoginController();
        String s = '';
          try{
             s = okpcLoginController.login('abc@gmail.com','lo9(ki8*ju','https://test.salesforce.com/','.citizen');
          }catch(exception e){
              s = null;
          }        
        System.assert(s == null);

    }
    public static testMethod void test1(){
        okpcLoginController.getIsUsernamePasswordEnabled();
    }
    public static testMethod void test2(){
        okpcLoginController.getIsSelfRegistrationEnabled();
    }
    public static testMethod void test3(){
        String s = okpcLoginController.getSelfRegistrationUrl();
        System.assert(s == null);

    }
    public static testMethod void test4(){
        okpcLoginController.getForgotPasswordUrl();
    }
    public static testMethod void test5(){
       	okpcLoginController.getAuthConfig();
    }
    public static testMethod void test6(){
        okpcLoginController.setExperienceId('test');
    }   
    
    private static testMethod void communityLoginTest(){
        try{
            
        
        
        User adminUser = [Select Id, UserRoleId,email,firstname,username From User Where Profile.Name='System Administrator' AND isActive = true Limit 1];
        system.runAs(adminUser){
            String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
            Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
            createAccountTestSite.BillingCity = 'testCity';
            createAccountTestSite.BillingCountry = 'testCountry';
            createAccountTestSite.BillingState = 'testState';
            createAccountTestSite.BillingStreet = 'testStreet';
            createAccountTestSite.BillingPostalCode = '12345';
            createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
            createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
            insert createAccountTestSite;
            
            Id citizenConsultationVitalRecords_RT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_Consultation_Vital_Records').getRecordTypeId();
            
            Contact createContact = TestDataFactoryContact.createCitizenContact(false , 'TestLastName','TestFirstName');
            createContact.Notify_Patients__c = true;
            createContact.AccountId=createAccountTestSite.id;
            createContact.Opt_out_for_SMS_Updates__c = false;
            createContact.recordtypeID=citizenConsultationVitalRecords_RT;
            insert createContact;
            
            contact[] createContact2=[select id,email,firstName,lastName,phone from contact where lastname='TestLastName' limit 1];
            
            string profileId=[select id,name from profile where name='Vital Records Customer Appointment Portal' limit 1].id;
           // string roleId=[select id,name from userrole where name='CEO' limit 1].id;
            string contactType=System.Label.OKSF_Registration_Covid_Citizen;
            
            String newUsername=createContact2[0].email+'.dev';
            user u=new user();
            u.Username = createContact2[0].email; //Modified by Sajal
            u.put('Email', createContact2[0].email);
            u.ProfileId = profileId;
          //  u.UserRoleId=roleId;
            u.FirstName = createContact2[0].FirstName;
            u.LastName = createContact2[0].LastName;
            u.Password__c = 'test1233@N';
            u.Phone = createContact2[0].phone;
            u.ContactId = createContact2[0].Id;
            u.emailencodingkey = 'UTF-8';
            u.languagelocalekey = 'en_US';
            u.localesidkey = 'en_US';
            u.timezonesidkey = 'America/Los_Angeles';
            u.alias = createContact2[0].FirstName.substring(0, 3);
    
            String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
            nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
            u.put('CommunityNickname', nickname);
            insert u;
            
            string communityResult=okpcLoginController.communityLogin(u.FirstName, u.email, 'dev');
            system.debug('communityResult======='+communityResult);
        
        }
        } catch(system.Exception exp){
			system.debug('exp============'+exp.getMessage());            
        }
        
    }
}