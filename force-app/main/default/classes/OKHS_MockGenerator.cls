@isTest
global class OKHS_MockGenerator implements HttpCalloutMock {
    
    public static Map<String,Object> retriveResponseBody(String body , String statusCode){
        Map<String,Object> responseBodyMap = new Map<String,Object>();
        responseBodyMap.put( 'body',body);
        responseBodyMap.put('statusCode',statusCode);
        return responseBodyMap;
    }
    public Map<String,Map<String,Object>> responseMap = new Map<String,Map<String,Object>>();
    
    public OKHS_MockGenerator(Map<String,Map<String,Object>> responseMap){
        this.responseMap = responseMap;
    }
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
      
        String requestBody = req.getBody();
        
        string endPoint ='';
        if(req.getEndpoint().indexOf('?') > -1){
            endPoint = req.getEndpoint().substring(0, req.getEndpoint().indexOf('?'));
        }
        Map<String,Object> responseParametersMap = responseMap.get(endPoint);
        //HttpResponse response = MockHTTPResponseGeneratorMap.responseHandler(endPoint);
        HTTPResponse response = new HTTPResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(Integer.valueOf(responseParametersMap.get('statusCode')));
       response.setBody(String.valueOf(responseParametersMap.get('body')));
        return response;
    }
    
    
   
}