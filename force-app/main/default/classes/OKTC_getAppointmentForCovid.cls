public class OKTC_getAppointmentForCovid {
	 @AuraEnabled
    public static Map<String,Object> getAccountSlotsPcr(String accountId){  
       Integer daysCount = 7; 
       List<Account> accList = [SELECT id,Name,Number_of_Weeks_Available__c FROM Account WHERE id =: accountId];
        if(accList.size() > 0 && accList[0].Number_of_Weeks_Available__c != null){
            daysCount = Integer.valueof(accList[0].Number_of_Weeks_Available__c) * 7;
            system.debug('Days>>>>>>>>' + daysCount);
        }
        Map<String,Object> result = new Map<String,Object>();
        List<Appointment_Slot__c> appointmentSlot = new List<Appointment_Slot__c>();
        
        Set<Id> appointIds = new Set<Id>();
        Map<String,List<SObject>> slotMap = new Map<String,List<SObject>>();
        Map<Date,Map<String,List<SObject>>> dateMap = new Map<Date,Map<String,List<SObject>>>();
        Map<Date,Map<String,List<SObject>>> dateMapBooked = new Map<Date,Map<String,List<SObject>>>();
        
            for(Integer counter = 0 ; counter <= daysCount ; counter++ ) {
                dateMapBooked.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
                dateMap.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
            }  
        system.debug('>>>DateMapBooked>>>' + dateMapBooked);
        system.debug('>>>dateMap>>>' + dateMap);
        Date endDate = date.today().addDays(daysCount);
        appointmentSlot = [SELECT Id,Name,Account__c, Date__c,
                                    Start_Time__c, End_Time__c,
                                    Unscheduled__c, Appointment_Count__c 
                            FROM Appointment_Slot__c
                            WHERE Account__c =: accountId
                           	AND Testing_Site_Type__c = 'PCR Testing'
                            AND Date__c <=: endDate AND
                            Date__c >= TODAY
                            AND Start_Time__c != null
                            ORDER BY Date__c ASC,Start_Time__c ASC];

        for(Appointment_Slot__c slot : appointmentSlot){
            DateTime dateTimeVar = DateTime.newInstance(Date.today(),slot.Start_Time__c);
            system.debug(dateMap.containsKey(slot.Date__c));
            if(dateMap.containsKey(slot.Date__c)) {
                Map<String,List<SObject>> slotAndAppointMentMap = dateMap.get(slot.Date__c);
                if(!slotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                    slotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                }
                if(slot.Appointment_Count__c == 0 || slot.Appointment_Count__c == null)
                    slotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                dateMap.put(slot.Date__c,slotAndAppointMentMap);
            }
            if(dateMapBooked.containsKey(slot.Date__c)) {
                Map<String,List<SObject>> bookedslotAndAppointMentMap = dateMapBooked.get(slot.Date__c);
                if(!bookedslotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                    bookedslotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                }
                if(slot.Appointment_Count__c > 0)
                    bookedslotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                dateMapBooked.put(slot.Date__c,bookedslotAndAppointMentMap);
            }
        }

        List<String> dateHeaderList = new List<String>();
        List<String> weekHeaderList = new List<String>();

        for(Integer count = 0; count <= daysCount;count++ ) {
            DateTime appointmentDate = DateTime.now().addDays(count);
            dateHeaderList.add(appointmentDate.format('MM/dd'));
            weekHeaderList.add(appointmentDate.format('E').toUpperCase());
        }
        system.debug('>>>>dateHeaderList>>>' + dateHeaderList);
        result.put('weekHeaderList',weekHeaderList);
        result.put('dateHeaderList',dateHeaderList);
        result.put('records',dateMap);
        result.put('bookedRecords',dateMapBooked);
        return result;
    }
	
     @AuraEnabled
    public static Map<String,Object> getAccountSlotsRapid(String accountId){  
       Integer daysCount = 7; 
       List<Account> accList = [SELECT id,Name,Number_of_Weeks_Available__c FROM Account WHERE id =: accountId];
        if(accList.size() > 0 && accList[0].Number_of_Weeks_Available__c != null){
            daysCount = Integer.valueof(accList[0].Number_of_Weeks_Available__c) * 7;
            system.debug('Days>>>>>>>>' + daysCount);
        }
        Map<String,Object> result = new Map<String,Object>();
        List<Appointment_Slot__c> appointmentSlot = new List<Appointment_Slot__c>();
        
        Set<Id> appointIds = new Set<Id>();
        Map<String,List<SObject>> slotMap = new Map<String,List<SObject>>();
        Map<Date,Map<String,List<SObject>>> dateMap = new Map<Date,Map<String,List<SObject>>>();
        Map<Date,Map<String,List<SObject>>> dateMapBooked = new Map<Date,Map<String,List<SObject>>>();
        
            for(Integer counter = 0 ; counter <= daysCount ; counter++ ) {
                dateMapBooked.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
                dateMap.put(Date.today().addDays(counter),new Map<String,List<SObject>>());
            }  
        system.debug('>>>DateMapBooked>>>' + dateMapBooked);
        system.debug('>>>dateMap>>>' + dateMap);
        Date endDate = date.today().addDays(daysCount);
        appointmentSlot = [SELECT Id,Name,Account__c, Date__c,
                                    Start_Time__c, End_Time__c,
                                    Unscheduled__c, Appointment_Count__c 
                            FROM Appointment_Slot__c
                            WHERE Account__c =: accountId
                           	AND Testing_Site_Type__c = 'Rapid Testing'
                            AND Date__c <=: endDate AND
                            Date__c >= TODAY
                            AND Start_Time__c != null
                            ORDER BY Date__c ASC,Start_Time__c ASC];

        for(Appointment_Slot__c slot : appointmentSlot){
            DateTime dateTimeVar = DateTime.newInstance(Date.today(),slot.Start_Time__c);
            system.debug(dateMap.containsKey(slot.Date__c));
            if(dateMap.containsKey(slot.Date__c)) {
                Map<String,List<SObject>> slotAndAppointMentMap = dateMap.get(slot.Date__c);
                if(!slotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                    slotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                }
                if(slot.Appointment_Count__c == 0 || slot.Appointment_Count__c == null)
                    slotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                dateMap.put(slot.Date__c,slotAndAppointMentMap);
            }
            if(dateMapBooked.containsKey(slot.Date__c)) {
                Map<String,List<SObject>> bookedslotAndAppointMentMap = dateMapBooked.get(slot.Date__c);
                if(!bookedslotAndAppointMentMap.containsKey(dateTimeVar.format('hh:mm aa'))) {
                    bookedslotAndAppointMentMap.put(dateTimeVar.format('hh:mm aa'),new List<SObject>());
                }
                if(slot.Appointment_Count__c > 0)
                    bookedslotAndAppointMentMap.get(dateTimeVar.format('hh:mm aa')).add(slot);
                dateMapBooked.put(slot.Date__c,bookedslotAndAppointMentMap);
            }
        }

        List<String> dateHeaderList = new List<String>();
        List<String> weekHeaderList = new List<String>();

        for(Integer count = 0; count <= daysCount;count++ ) {
            DateTime appointmentDate = DateTime.now().addDays(count);
            dateHeaderList.add(appointmentDate.format('MM/dd'));
            weekHeaderList.add(appointmentDate.format('E').toUpperCase());
        }
        system.debug('>>>>dateHeaderList>>>' + dateHeaderList);
        result.put('weekHeaderList',weekHeaderList);
        result.put('dateHeaderList',dateHeaderList);
        result.put('records',dateMap);
        result.put('bookedRecords',dateMapBooked);
        return result;
    }

}