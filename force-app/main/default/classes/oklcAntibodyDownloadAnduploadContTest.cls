@isTest
public class oklcAntibodyDownloadAnduploadContTest {
    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
        
        Batch__c batchRecord=new Batch__c();
        batchRecord.laboratory__c =createAccountTestSite.id;
        batchRecord.Status__c ='Unprocessed';
        insert batchRecord;
        
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Antibody_Testing__c antiBodyTestingRecord=new Antibody_Testing__c();
        antiBodyTestingRecord.Laboratory__c=createAccountTestSite.id;
        antiBodyTestingRecord.Appointment__c=createAppointment.id;
        antiBodyTestingRecord.Testing_Site__c=createAccountTestSite.id;
        antiBodyTestingRecord.Date_of_Lab_Test_Completion__c=date.today();
        antiBodyTestingRecord.recordtypeId=antibodyRecordTypeId;
        antiBodyTestingRecord.Batch__c=batchRecord.id;
        insert antiBodyTestingRecord;
        
        

    }
    
    private static testMethod void retrieveBatchTest(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Laboratory__c FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        String labId = String.valueOf(antiBodyLst[0].Laboratory__c);
        
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
       
        List<oklcAntibodyDownloadAnduploadController.OptionWrapper> retrieveBatchResult= oklcAntibodyDownloadAnduploadController.retrieveBatch(labId);
        system.assert(!retrieveBatchResult.isEmpty());

    }
    
    private static testMethod void retrieveBatchforTestingSiteTest(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Laboratory__c,Testing_Site__c,Container_Barcode_Id__c,Antibody_Testing_Number__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                    Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,
                                     createddate,Results__c FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        String testingSiteId = String.valueOf(antiBodyLst[0].Testing_Site__c);
        String labId = String.valueOf(antiBodyLst[0].Laboratory__c);
         String batchId = String.valueOf(antiBodyLst[0].Batch__c);
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
        String id = '"' + antiBodyLst[0].Id + '"';
        
       
        List<oklcAntibodyDownloadAnduploadController.OptionWrapper> retrieveBatchforTestingSiteResult= oklcAntibodyDownloadAnduploadController.retrieveBatchforTestingSite(testingSiteId);
        system.assert(!retrieveBatchforTestingSiteResult.isEmpty());
        oklcAntibodyDownloadAnduploadController.getLabId();
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyResult= oklcAntibodyDownloadAnduploadController.getAntibody(labId,batchId);
        system.assert(!getAntibodyResult.isEmpty());
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyTestingSiteResult= oklcAntibodyDownloadAnduploadController.getAntibodyTestingSite(labId,batchId);
        system.assert(!getAntibodyTestingSiteResult.isEmpty());
        Contact con = [SELECT ID From Contact LIMIT 1];
        String pateientId = '"' + con.Id + '"';
        String testDate = '"' + '06-30-2020' + '"';
        
        String json = '[{"result" : "Positive","id":' + id + ',"firstName":"test","lastname":"last","TestNumber":"3", "patientId":' + pateientId + ',"dateOfLabTestCompletion":' + testDate + '},{"result" : "Positive","id":' + id + ',"firstName":"test","lastname":"last","TestNumber":"3", "patientId":' + pateientId + ',"dateOfLabTestCompletion":"2020-03"}]';
        System.debug('>>' + json);
         Map<String,Object>  fieldWrapperMap = oklcAntibodyDownloadAnduploadController.insertData(json);
		System.assertEquals(fieldWrapperMap.size()> 0, true);
    }
    
    private static testMethod void retrieveBatchforTestingSiteTest2(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Laboratory__c,Testing_Site__c,Container_Barcode_Id__c,Antibody_Testing_Number__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                    Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,
                                     createddate,Results__c FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        String testingSiteId = String.valueOf(antiBodyLst[0].Testing_Site__c);
        String labId = String.valueOf(antiBodyLst[0].Laboratory__c);
         String batchId = String.valueOf(antiBodyLst[0].Batch__c);
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
        String id = '"' + antiBodyLst[0].Id + '"';
        
       
        List<oklcAntibodyDownloadAnduploadController.OptionWrapper> retrieveBatchforTestingSiteResult= oklcAntibodyDownloadAnduploadController.retrieveBatchforTestingSite(testingSiteId);
        system.assert(!retrieveBatchforTestingSiteResult.isEmpty());
        oklcAntibodyDownloadAnduploadController.getLabId();
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyResult= oklcAntibodyDownloadAnduploadController.getAntibody(labId,batchId);
        system.assert(!getAntibodyResult.isEmpty());
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyTestingSiteResult= oklcAntibodyDownloadAnduploadController.getAntibodyTestingSite(labId,batchId);
        system.assert(!getAntibodyTestingSiteResult.isEmpty());
        Contact con = [SELECT ID From Contact LIMIT 1];
        String pateientId = '"' + con.Id + '"';
        String testDate = '"' + '06-30-2020' + '"';
        
        String json = '[{"result" : "Positive","id":' + id + ',"firstName":"test","lastname":"last","TestNumber":"3", "patientId":' + pateientId + ',"dateOfLabTestCompletion":' + testDate + '}]';
        System.debug('>>' + json);
         Map<String,Object>  fieldWrapperMap = oklcAntibodyDownloadAnduploadController.insertData(json);
		System.assertEquals(fieldWrapperMap.size()> 0, true);
    }
    
    private static testMethod void retrieveBatchforTestingSiteTest3(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Laboratory__c,Testing_Site__c,Container_Barcode_Id__c,Antibody_Testing_Number__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                    Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,
                                     createddate,Results__c FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        String testingSiteId = String.valueOf(antiBodyLst[0].Testing_Site__c);
        String labId = String.valueOf(antiBodyLst[0].Laboratory__c);
         String batchId = String.valueOf(antiBodyLst[0].Batch__c);
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
        String id = '"' + antiBodyLst[0].Id + '"';
        
       
        List<oklcAntibodyDownloadAnduploadController.OptionWrapper> retrieveBatchforTestingSiteResult= oklcAntibodyDownloadAnduploadController.retrieveBatchforTestingSite(testingSiteId);
        system.assert(!retrieveBatchforTestingSiteResult.isEmpty());
        oklcAntibodyDownloadAnduploadController.getLabId();
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyResult= oklcAntibodyDownloadAnduploadController.getAntibody(labId,batchId);
        system.assert(!getAntibodyResult.isEmpty());
        List<oklcAntibodyDownloadAnduploadController.AppointmentWrapper> getAntibodyTestingSiteResult= oklcAntibodyDownloadAnduploadController.getAntibodyTestingSite(labId,batchId);
        system.assert(!getAntibodyTestingSiteResult.isEmpty());
        Contact con = [SELECT ID From Contact LIMIT 1];
        String pateientId = '"' + con.Id + '"';
        String testDate = '"' + '06-30-2020' + '"';
        
        String json = '[{"result" : "Positive","id":' + id + ',"firstName":"test","lastname":"last","TestNumber":"3", "patientId":' + pateientId + ',"dateOfLabTestCompletion":"2020-03"}]';
        System.debug('>>' + json);
         Map<String,Object>  fieldWrapperMap = oklcAntibodyDownloadAnduploadController.insertData(json);
		System.assertEquals(fieldWrapperMap.size()> 0, true);
    }

}