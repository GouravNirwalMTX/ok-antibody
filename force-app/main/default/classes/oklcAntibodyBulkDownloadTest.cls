@isTest
public class oklcAntibodyBulkDownloadTest {
    
    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        createAccountTestSite.Testing_Site_Type__c = 'Antibody Testing';
        insert createAccountTestSite;

        Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
        Antibody_Testing__c antiBodyTestingRecord=new Antibody_Testing__c();
        antiBodyTestingRecord.Laboratory__c=createAccountTestSite.id;
        antiBodyTestingRecord.Appointment__c=createAppointment.id;
        antiBodyTestingRecord.Testing_Site__c=createAccountTestSite.id;
        antiBodyTestingRecord.Date_of_Lab_Test_Completion__c=date.today();
        antiBodyTestingRecord.Results__c='Negative';
        insert antiBodyTestingRecord;

    }
    
    private static testMethod void getAntibodyBulkTest(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Testing_Site__r.Name,Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c,Patient__r.Street_Address1__c,Patient__r.State__c,Patient__r.MobilePhone, Antibody_Testing_Number__c,Date_of_Specimen_Collection__c,Formatted_lab_test_completion_date__c,
                                       Container_Barcode_Id__c,Test_performed_description__c,Test_Result_Description__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                       Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,Laboratory__r.Name,
                                       createddate,Results__c,Patient__r.Account.BillingCity FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        String labId = String.valueOf(antiBodyLst[0].Laboratory__c);
        
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
       
        List<oklcAntibodyBulkDownload.AppointmentWrapper> lastAppWrapperResult= oklcAntibodyBulkDownload.getAntibodyBulk(labId,TodateChnage,FromdateChange);
        system.assert(!lastAppWrapperResult.isEmpty());
       
    }
    
    private static testMethod void getAntibodyTestingSiteTest(){
        List<Antibody_Testing__c> antiBodyLst = [Select Id ,Batch__c,Batch__r.Name,Testing_Site__r.Name,Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c,Patient__r.Street_Address1__c,Patient__r.State__c,Patient__r.MobilePhone, Antibody_Testing_Number__c,Date_of_Specimen_Collection__c,Formatted_lab_test_completion_date__c,
                                       Container_Barcode_Id__c,Test_performed_description__c,Test_Result_Description__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                       Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,Laboratory__r.Name,
                                       createddate,Results__c,Patient__r.Account.BillingCity FROM Antibody_Testing__c WHERE Laboratory__r.name = 'TestSite' LIMIT 1];
       

        
        String testingSite = String.valueOf(antiBodyLst[0].Testing_Site__c);
        antiBodyLst[0].Results__c='';
        update antiBodyLst[0];
        Date TodateChnage = date.today();
        Date FromdateChange =  Date.newInstance(2020, 01, 01);
       
        
        List<oklcAntibodyBulkDownload.AppointmentWrapper> getAntibodyTestingSiteResult= oklcAntibodyBulkDownload.getAntibodyTestingSite(testingSite,TodateChnage,FromdateChange,'Antibody Testing');
        system.assert(!getAntibodyTestingSiteResult.isEmpty());
    }

}