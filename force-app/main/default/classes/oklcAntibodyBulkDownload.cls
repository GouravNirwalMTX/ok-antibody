public without sharing class oklcAntibodyBulkDownload {
    @AuraEnabled
    public static List<AppointmentWrapper> getAntibodyBulk(String labId ,Date toDate ,Date fromDate){
        List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
       	Date FromdateChange = Date.valueOf(fromDate);
        Date TodateChnage = Date.valueOf(toDate);
        system.debug('FromdateChange' + FromdateChange);
        for(Antibody_Testing__c app : [SELECT Id,Batch__c,Batch__r.Name,Testing_Site__r.Name,Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c,Patient__r.Street_Address1__c,Patient__r.State__c,Patient__r.MobilePhone,Patient__r.Phone, Antibody_Testing_Number__c,Date_of_Specimen_Collection__c,Formatted_lab_test_completion_date__c,
                                       Container_Barcode_Id__c,Test_performed_description__c,Test_Result_Description__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                       appointment__r.Formatted_Appointment_Start_Time__c, appointment__r.Status__c,appointment__r.Appointment_Date__c,appointment__r.Formatted_Complete_Date__c, appointment__r.testing_site_type__c,appointment__r.Appointment_Start_Date_v1__c,
                                       Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,Laboratory__r.Name,
                                       createddate,Results__c,Patient__r.Account.BillingCity FROM Antibody_Testing__c WHERE Laboratory__c = :labId AND Date_of_Lab_Test_Completion__c >=: fromDate AND Date_of_Lab_Test_Completion__c <=: toDate AND Results__c != null ORDER BY Antibody_Testing_Number__c ]){
                                           wrapper.add(new AppointmentWrapper(app));
                                       }
        if(wrapper.size()>0){
            system.debug('wrapperSize' + wrapper.size());
            system.debug('wrapper---' + wrapper);
            return wrapper;
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static List<AppointmentWrapper> getAntibodyTestingSite(String labId ,Date toDate ,Date fromDate,String testingType){
        List<AppointmentWrapper> wrapper = new List<AppointmentWrapper>(); 
       	Date FromdateChange = Date.valueOf(fromDate);
        Date TodateChnage = Date.valueOf(toDate);
        system.debug('FromdateChange' + FromdateChange);
        for(Antibody_Testing__c app : [SELECT Id,Antibody_Testing_Number__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,Patient__r.formatted_Date_of_Birth__c,Patient__r.Patient_Id__c,
                                       Patient__r.MobilePhone ,Laboratory__r.Name,Results__c,Date_of_Lab_Test_Completion__c,Formatted_lab_test_completion_date__c,appointment__r.Formatted_Appointment_Start_Time__c,
                                       appointment__r.Status__c,appointment__r.Appointment_Date__c,appointment__r.Formatted_Complete_Date__c,appointment__r.Appointment_Start_Date_v1__c,
                                       appointment__r.testing_site_type__c FROM Antibody_Testing__c
                                       WHERE Testing_Site__c = :labId AND appointment__r.Appointment_Start_Date_v1__c  >=: fromDate AND appointment__r.Appointment_Start_Date_v1__c  <=: toDate AND appointment__r.testing_site_type__c =: testingType AND appointment__c != null ORDER BY Antibody_Testing_Number__c ]){
                                           wrapper.add(new AppointmentWrapper(app));
                                       }
       /* for(Antibody_Testing__c app : [SELECT Id,Batch__c,Batch__r.Name,Testing_Site__r.Name,Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c,Patient__r.Street_Address1__c,Patient__r.State__c,Patient__r.MobilePhone, Antibody_Testing_Number__c,Date_of_Specimen_Collection__c,Formatted_lab_test_completion_date__c,
                                       Container_Barcode_Id__c,Test_performed_description__c,Test_Result_Description__c,Patient__r.Patient_Id__c,Patient__r.FirstName,Patient__r.LastName,Patient__r.MiddleName,
                                       Patient__r.Gender__c,Patient__r.formatted_Date_of_Birth__c,Patient__r.Account.Name,Patient__r.birthdate,Barcode_ID__c,Date_of_Lab_Test_Completion__c ,Source_of_Specimen__c  ,Laboratory__r.Name,
                                       createddate,Results__c,Patient__r.Account.BillingCity FROM Antibody_Testing__c WHERE Testing_Site__c = :labId AND Date_of_Lab_Test_Completion__c >=: fromDate AND Date_of_Lab_Test_Completion__c <=: toDate ORDER BY Antibody_Testing_Number__c ]){
                                           wrapper.add(new AppointmentWrapper(app));
                                       }
		*/
        if(wrapper.size()>0){
            system.debug('wrapperSize' + wrapper.size());
            system.debug('wrapper---' + wrapper);
            return wrapper;
        }else{
            return null;
        }
    }
	
    public class AppointmentWrapper{
      
        @AuraEnabled public string TestNumber;
        @AuraEnabled public string patientId;
        @AuraEnabled public string firstName;
        @AuraEnabled public string lastname;
         @AuraEnabled public String dateOfBirth;
        @AuraEnabled public String phone;
        @AuraEnabled public String appointmentStatus;
         @AuraEnabled public String appointmentDate;
         @AuraEnabled public String appointmentTimes;
         @AuraEnabled public string testingLabName;
        @AuraEnabled public string result;
         @AuraEnabled public String appointmentCompleteDate;
        @AuraEnabled public string dateOfLabTestCompletion;
        
        
         //  @AuraEnabled public string testingSiteName;
       // @AuraEnabled public string gender;
       // @AuraEnabled public String organizationName;
       // @AuraEnabled public string barCoderNumber;
      //  @AuraEnabled public String dateOfSpecimenCollection;
      //  @AuraEnabled public string sourceOfSpecimen;
      //  @AuraEnabled public string batchId;
      //  @AuraEnabled public string orderingFacilty;
     //   @AuraEnabled public string patientAddress;
      //  @AuraEnabled public string patientState;
      //  @AuraEnabled public string patientPhone;
      //  @AuraEnabled public string dateOfSpecimenCollectionFormula;
       // @AuraEnabled public string testPerformedDescription;
      //  @AuraEnabled public string testResultDescription;
     //   @AuraEnabled public string containerBarCode;
               
        public AppointmentWrapper(Antibody_Testing__c app){
           /* Testing side fields*/
            this.TestNumber = app.Antibody_Testing_Number__c ;
            this.patientId = app.Patient__r.Patient_Id__c != null ? app.Patient__r.Patient_Id__c : '' ;
            this.firstName = app.Patient__r.FirstName != null ? app.Patient__r.FirstName : '';
            this.lastname = app.Patient__r.LastName != null ? app.Patient__r.LastName : '';
            this.phone =  app.Patient__r.MobilePhone  != null ? app.Patient__r.MobilePhone  : '';
            this.dateOfBirth = string.valueOf(app.Patient__r.formatted_Date_of_Birth__c) != null ? string.valueOf(app.Patient__r.formatted_Date_of_Birth__c) : '';
            this.appointmentStatus = app.appointment__r.Status__c != null ? app.appointment__r.Status__c : '';
            this.appointmentDate = string.valueOf(app.appointment__r.Appointment_Start_Date_v1__c) != null ? string.valueOf(app.appointment__r.Appointment_Start_Date_v1__c) : '';
            this.appointmentTimes = string.valueOf(app.appointment__r.Formatted_Appointment_Start_Time__c) != null ? string.valueOf(app.appointment__r.Formatted_Appointment_Start_Time__c) : '';
           	this.testingLabName = app.Laboratory__r.Name != null ? app.Laboratory__r.Name : '';
           	this.result = app.Results__c != null ? app.Results__c : '';
            this.appointmentCompleteDate = app.appointment__r.Formatted_Complete_Date__c != null ? app.appointment__r.Formatted_Complete_Date__c : '';
            this.dateOfLabTestCompletion = string.valueOf(app.Formatted_lab_test_completion_date__c) != null ? string.valueOf(app.Formatted_lab_test_completion_date__c) : '';
            
           /*Lab Side Fields */ 
            
            // this.testingSiteName = app.Testing_Site__r.Name != null ? app.Testing_Site__r.Name : '';
          //  this.gender = app.Patient__r.Gender__c != null ? app.Patient__r.Gender__c : '';
           //this.organizationName = app.Patient__r.Account.Name != null ? app.Patient__r.Account.Name : '';
       	  // this.barCoderNumber = app.Barcode_ID__c != null ? app.Barcode_ID__c : '';  
            // this.dateOfSpecimenCollection = string.valueOf(Date.valueOf(app.createddate)) != null ? string.valueOf(Date.valueOf(app.createddate)) : '';
          //  this.sourceOfSpecimen = app.Source_of_Specimen__c != null ? app.Source_of_Specimen__c : 'Serum';x
          //  this.batchId = app.Batch__r.Name != null ? app.Batch__r.Name : '' ;
          //  this.orderingFacilty = app.Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c != null ? app.Testing_Site__r.Ordering_Provider_or_Ordering_Facility__c : '';x
          //  this.patientAddress = app.Patient__r.Street_Address1__c != null ? app.Patient__r.Street_Address1__c : '';
          //  this.patientState = app.Patient__r.State__c != null ? app.Patient__r.State__c : '';
          //  this.patientPhone = app.Patient__r.MobilePhone != null ? app.Patient__r.MobilePhone : '';
          //  this.dateOfSpecimenCollectionFormula = string.valueOf(app.Date_of_Specimen_Collection__c) != null ? string.valueOf(app.Date_of_Specimen_Collection__c) : '';
          //  this.testPerformedDescription = app.Test_performed_description__c != null ? app.Test_performed_description__c : '';x
          //  this.testResultDescription = app.Test_Result_Description__c != null ? app.Test_Result_Description__c : '';x
          //  this.containerBarCode = app.Container_Barcode_Id__c != null ? app.Container_Barcode_Id__c : '';
        }
    }
    
}