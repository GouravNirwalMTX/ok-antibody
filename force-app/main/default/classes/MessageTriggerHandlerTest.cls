@isTest
public class MessageTriggerHandlerTest {
    /*   @testsetup   
public static void setUpData(){

Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
createContact.Pending_Confirmation__c = 'Pending';
insert createContact;
Messages__c insertMesssage =  TestDataFactoryMessage.createMessage(true, 'Y', '1234567890');          

} */
    @testsetup   
    public  static  void setUpData(){
        String testingSite = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        String testingSiteCitizen = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Citizen').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingSite, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.communication__c  = 'OSDH Antibody';
        insert createAccountTestSite;
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName','TestFirstName');
        createContact.RecordTypeId = testingSiteCitizen;
        createContact.MobilePhone = '5187493844'; 
        createContact.Pending_Confirmation__c = 'Pending';
      //  createContact.Testing_Site__c = createAccountTestSite.Id;
        createContact.AccountId = createAccountTestSite.Id;
        insert createContact;
            List<Antibody_Testing__c> testList = new List<Antibody_Testing__c>();
        Map<Integer,String> resultsMap = new Map<Integer,String> { 
            0 => 'Rejected', 
            1 => 'Negative' ,
            2 =>'Positive'   
                };
        for(Integer i=0;i<3;i++){
            Antibody_Testing__c test = TestDataFactory.createTesting('Test'+i,false);
            test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test.Patient__c = createContact.Id;
            testList.add(test);
        }
        insert testList;
        Messages__c insertMesssage =  TestDataFactoryMessage.createMessage(true, 'Y', createContact.Mobilephone); 
        
         Contact createContact3 = TestDataFactoryContact.createContact(false , 'TestLastName55','TestFirstName63');
        createContact3.RecordTypeId = createContact.RecordTypeId;
        createContact3.Pending_Confirmation__c = 'Pending';
         createContact3.MobilePhone = '6187493844';
        createContact3.AccountId = createContact.AccountId;
       createContact3.PHOCIS__c = 'xyz';
   
        createAccountTestSite.Communication__c = 'Covid Results';
        update createAccountTestSite;
        Contact createContact2 = TestDataFactoryContact.createContact(false , 'TestLastName3','TestFirstName3');
        createContact2.RecordTypeId = createContact.RecordTypeId;
        createContact2.Pending_Confirmation__c = 'Pending';
         createContact2.MobilePhone = '7187493844';         
        createContact2.AccountId = createContact.AccountId;
         Contact createContact4 = TestDataFactoryContact.createContact(false , 'TestLastName44','TestFirstName44');
        createContact4.RecordTypeId = createContact.RecordTypeId;
        createContact4.Pending_Confirmation__c = 'Pending';
         createContact4.MobilePhone = '8187493844';         
        createContact4.AccountId = createContact.AccountId;
       insert createContact4;
        insert createContact3;
        insert createContact2;
        List<Antibody_Testing__c> antiList = new List<Antibody_Testing__c> ();
           Antibody_Testing__c test11 = TestDataFactory.createTesting('Test1',false);
           // test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test11.Patient__c = createContact2.Id;
           Antibody_Testing__c test2 = TestDataFactory.createTesting('Test',false);
           // test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test2.Patient__c = createContact3.Id;
        test2.consent__c = true;
           Antibody_Testing__c test3 = TestDataFactory.createTesting('Test3',false);
           // test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test3.Patient__c = createContact.Id;
        
        Antibody_Testing__c test4 = TestDataFactory.createTesting('Test11',false);
           // test.Results__c = resultsMap.containsKey(i) ? resultsMap.get(i) : null;
            test4.Patient__c = createContact4.Id;
        test4.consent__c = true;
       
        antiList.add(test11);
        antiList.add(test2);
        antiList.add(test3);
         antiList.add(test4);
         //insert antilist;
       
        
       // Appointment__c fetchAppointment = [SELECT Id,Name FROM Appointment__c WHERE Patient__c =: createContact.id LIMIT 1 ];
        //Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
       // createSlot.Account__c = createAccountTestSite.id;
       // createSlot.Start_Time__c = time.newInstance(7, 0, 0, 0);
       // createSlot.End_Time__c = time.newInstance(8, 8, 8, 8);
       // insert createSlot;
        
       // fetchAppointment.Appointment_Slot__c = createSlot.id;
       // update fetchAppointment;
        //insert insertMesssage;
        
        /*   Messages__c fetchMessage = [SELECT Id FROM Messages__c WHERE Messages__c = 'Y' LIMIT 1];
fetchMessage.Messages__c = 'N';
update fetchMessage; */
        
    }
   /* static testmethod void testInsert() {
        
        Contact con = [Select Id,RecordTypeId,AccountId,mobilePhone FROM Contact LIMIT 1];
        con.MobilePhone = '5187493844';
        con.Pending_Confirmation__c = 'pending';
        update con;
        Contact createContact = TestDataFactoryContact.createContact(false , 'TestLastName2','TestFirstName2');
        createContact.RecordTypeId = con.RecordTypeId;
        createContact.Pending_Confirmation__c = 'Pending';
         createContact.MobilePhone = '6187493844';
        createContact.AccountId = con.AccountId;
        Contact createContact2 = TestDataFactoryContact.createContact(false , 'TestLastName399','TestFirstName399');
        createContact2.RecordTypeId = con.RecordTypeId;
        createContact2.Pending_Confirmation__c = 'Pending';
         createContact2.MobilePhone = '7187493844';         
        createContact2.AccountId = con.AccountId;
        insert createContact;
        insert createContact2;
        List<Messages__c> messagesList = new List<Messages__c>();
         Messages__c insertMesssage =  TestDataFactoryMessage.createMessage(false, 'Y','1518749');          
        insertMesssage.FROM__C = '+15187493844';
        Messages__c insertMesssage2 =  TestDataFactoryMessage.createMessage(false, 'N','1518749');          
        insertMesssage2.FROM__C = '+16187493844';
        Messages__c insertMesssage3 =  TestDataFactoryMessage.createMessage(false, 'R','1518749');          
        insertMesssage3.FROM__C = '+17187493844';
        messagesList.add(insertMesssage);
        messagesList.add(insertMesssage2);
        messagesList.add(insertMesssage3);
        insert messagesList;
        Test.startTest();
            SingleRequestMock fakeAccountResp = new SingleRequestMock(201,'Success','{"Message" :"Your COVID-19 Antibody Resistance Testing is scheduled on"}',null);
       
        
        Map<String, HttpCalloutMock> endpoint2TestResp =
                                   new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('https://api.twilio.com/2010-04-01/Accounts/ACcb20b6bbc13b8038c59a8f230cfaed93/Messages.json',fakeAccountResp);
       
        HttpCalloutMock multiCalloutMock =
                                   new MultiRequestMock(endpoint2TestResp);
 
     
                Test.setMock(HttpCalloutMock.class, multiCalloutMock);

                
        Test.stopTest();
        
    }
    */
  
    static testmethod void testInsertAntiBodyTest() {
       SingleRequestMock fakeAccountResp = new SingleRequestMock(201,'Success','{"Message" :"Your COVID-19 Antibody Resistance Testing is scheduled on"}',null);
       
        
        Map<String, HttpCalloutMock> endpoint2TestResp =
                                   new Map<String,HttpCalloutMock>();
        //https://api.twilio.com/2010-04-01/Accounts/AC49d2287cbfc314cce969486013125b50
        endpoint2TestResp.put('https://api.twilio.com/2010-04-01/Accounts/AC49d2287cbfc314cce969486013125b50/Messages.json',fakeAccountResp);
       // endpoint2TestResp.put('http://api.example.com',fakeContactResp);
 
        HttpCalloutMock multiCalloutMock =
                                   new MultiRequestMock(endpoint2TestResp);
 
     
      
       Test.startTest();
        
                Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        Contact con = [Select Id,RecordTypeId,AccountId,mobilePhone FROM Contact LIMIT 1];
        
        con.MobilePhone = '5187493844';
        con.Pending_Confirmation__c = 'pending';
        con.PHOCIS__c = 'Y';
        update con;
        List<Messages__c> messagesList = new List<Messages__c>();
         Messages__c insertMesssage =  TestDataFactoryMessage.createMessage(false, 'Y','1518749');          
        insertMesssage.FROM__C = '+15187493844';
        Messages__c insertMesssage2 =  TestDataFactoryMessage.createMessage(false,'N','1518749');          
        insertMesssage2.FROM__C = '+16187493844';
        Messages__c insertMesssage3 =  TestDataFactoryMessage.createMessage(false, 'Y','1518749');          
        insertMesssage3.FROM__C = '+17187493844';
        
        Messages__c insertMesssage4 =  TestDataFactoryMessage.createMessage(false, 'Np','1518749');          
        insertMesssage4.FROM__C = '+18187493844';

               messagesList.add(insertMesssage);
       /* messagesList.add(insertMesssage2);
        messagesList.add(insertMesssage3);
        messagesList.add(insertMesssage4);*/
       
        insert messagesList;
           
        insertMesssage.messages__c = '345';
        update insertMesssage;
        Test.stopTest(); 
    } 
}