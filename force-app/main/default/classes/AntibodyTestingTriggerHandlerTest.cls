/**
 * Created by ashishPandey on 12-05-2020.
 */

@isTest
private class AntibodyTestingTriggerHandlerTest {
  public static final String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    @testsetup
    public static void setUpData(){
       Id contactCitizenId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        
        Account acc = TestDataFactory.createAccount('Testaccount',false);
        acc.Communication__c = 'COVID Results';
        insert acc;
        Contact createContact = TestDataFactory.createContact('TestLastName','TestFirstName',false);
        createContact.Birthdate = System.today().addYears(-20);
        createContact.RecordTypeId = contactCitizenId;
        createContact.PHOCIS_Response_Count__c = '2';
        createContact.Notify_Patients__c = true;
        createContact.Opt_out_for_SMS_Updates__c = false;
        createContact.AccountId = acc.Id;
        insert createContact;
        
	        
                
    }
        
    static testMethod void testInsert(){
        Account acc = TestDataFactory.createAccount('TestSiteAccount',false);
        acc.RecordTypeId = testingCenter;
        acc.Communication__c = 'COVID Results';
        insert acc;
        
        Contact con = [select Id FROM Contact LIMIT 1];
      
       
      //  test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
       /*  
        TwilioSendSMS.sendSms('123456789', 'body');
        Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
           test_11.Results__c = 'Positive';
            test_11.Patient__c = con.Id;
        insert test_11;
        //AntibodyTestingTriggerHandler.resetPHOCISCounter(new List<Antibody_Testing__c> {test_11});
        */
        
         Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, acc.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
       // createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Testing_Site_Type__c = 'rapid Testing';
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
        
         Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
           test_11.Results__c = '';
            test_11.Patient__c = con.Id;
        test_11.External_Unique_Id__c = '123456789';
        test_11.Appointment__c = createAppointment.id;
        //test_11.RecordTypeId = Schema.SObjectType.Antibody_Testing__c.getRecordTypeInfosByDeveloperName().get('Citizen_Covid').getRecordTypeId();
        insert test_11;
        test_11.Results__c = 'Positive';
        update test_11;
         test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
       
       
    }
   /* 
    static testMethod void testInsert2(){
         Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(); 
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'OKU COVID';
        insert acc2;
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = contactCitizen_COVIDTypeId;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        insert createContact2;
        
        Test.startTest();
         test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'body');
        Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
           test_11.Results__c = 'Positive';
            test_11.Patient__c = createContact2.Id;
        insert test_11;
        //AntibodyTestingTriggerHandler.resetPHOCISCounter(new List<Antibody_Testing__c> {test_11});
        Test.stopTest();
        
    }
    static testMethod void testInsert3(){
         Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(); 
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'COVID Results';
        insert acc2;
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = contactCitizen_COVIDTypeId;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        insert createContact2;
      
        
        Test.startTest();
         test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'body');
        Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
           test_11.Results__c = 'Positive';
            test_11.Patient__c = createContact2.Id;
            test_11.Notify_User_via_SMS__c = false;
            
        insert test_11;
        test_11.Notify_User_via_SMS__c = false;
        update test_11;
        test_11.Results__c = 'Positive';
        update test_11;
        //AntibodyTestingTriggerHandler.resetPHOCISCounter(new List<Antibody_Testing__c> {test_11});
        Test.stopTest();
        
    }
    static testMethod void testInsert4(){
         Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId(); 
        Account acc2 = TestDataFactory.createAccount('Testaccount123',false);
        acc2.Communication__c = 'COVID Results';
        insert acc2;
        Contact createContact2 = TestDataFactory.createContact('TestLastName123','TestFirstName321',false);
        createContact2.Birthdate = System.today().addYears(-20);
        createContact2.RecordTypeId = contactCitizen_COVIDTypeId;
        createContact2.PHOCIS_Response_Count__c = '2';
        createContact2.Notify_Patients__c = true;
        createContact2.Opt_out_for_SMS_Updates__c = false;
        createContact2.AccountId = acc2.Id;
        insert createContact2;
         
         
         Antibody_Testing__c test_11 = TestDataFactory.createTesting('TestInsert',false);
        test_11.Results__c = 'Positive';
        test_11.Patient__c = createContact2.Id;
            
        insert test_11;
        test_11.Notify_User_via_SMS__c = false;
        update test_11;
        
        Test.startTest();
          test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        TwilioSendSMS.sendSms('123456789', 'body');      
        
        
        
        test_11.Notify_User_via_SMS__c = true;
        update test_11;
        //AntibodyTestingTriggerHandler.resetPHOCISCounter(new List<Antibody_Testing__c> {test_11});
        Test.stopTest();
        
    }
*/
}