@isTest
public class CreateAppointmentSlotBatch_Test {

    public static testMethod void createSlot(){
         DateTime myDateTime = (DateTime) System.now();
        String dayOfWeek = myDateTime.format('E');
         
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;
        
        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Template__c = true;
        createSlot.Day_of_the_Week__c = dayOfWeek;
        createSlot.Number_of_Available_Slots__c = 2;
        insert createSlot;
        
        Test.startTest();
        CreateAppointmentSlotsBatch c = new CreateAppointmentSlotsBatch();
        String sch = '0 0 2 * * ?'; 
    system.schedule('create slot batch', sch, c); 
        Test.stopTest();
    }
}