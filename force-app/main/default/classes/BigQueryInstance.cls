public class BigQueryInstance {
    public BigQueryInstance(){
        
    }
    
    @future(callout=true)
    public static void sendToGoogleBigQuery(Id contactId){
        //ImplementationID from Metadata Record
        String authID = 'DEV_SF_BQ_Contact';
        
        //List that calls the object and fields to be upserted into Google BigQuery -- NOTE: The fields in BigQuery should match the API names here in order to show data
        //If you want to change the object that is to be 
        List<Contact> contacts = [SELECT FirstName, LastName, Email, Phone, Patient_Id__c, AccountId, SystemModstamp from Contact where Id =: contactId ];

        if (contacts.size()>0) {
            // Sets up the Google BigQuery object, and the InsertAll 
            GoogleBigQuery google = new GoogleBigQuery(authID);
            GoogleBigQuery.InsertAll insertAll = new GoogleBigQuery.InsertAll();
            insertAll.addObject(contacts.get(0));
            if (!google.add(insertAll)) {
               System.debug('Big Query Error: ' + google.getResponse());
            }
        }
    }
}