global class CreateAppointmentSlotsBatch implements Schedulable, Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        DateTime myDateTime = (DateTime) System.now();
        String dayOfWeek = myDateTime.format('E');
        system.debug('day -- ' + dayOfWeek);
        String query = 'SELECT Id, Name, Account__c, Reference_Date_For_Batch__c, End_Time__c, Start_Time__c, Number_of_Available_Slots__c FROM Appointment_Slot__c WHERE Template__c  = true AND Day_of_the_Week__c = \'' + dayOfWeek + '\'';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Appointment_Slot__c> appntSlotsList) {
        
        List<Appointment_Slot__c> appointmentSlotsToInsert = new List<Appointment_Slot__c>();
        try {
            for(Appointment_Slot__c aptSlot : appntSlotsList){           
                for(Integer i=0; i< aptSlot.Number_of_Available_Slots__c ; i++){
                    Appointment_Slot__c slot = new Appointment_Slot__c();
                    slot.Account__c = aptSlot.Account__c;
                    slot.End_Time__c = aptSlot.End_Time__c;
                    slot.Start_Time__c = aptSlot.Start_Time__c;
                    slot.Date__c = aptSlot.Reference_Date_For_Batch__c;
                    
                    appointmentSlotsToInsert.add(slot);
                }               
            }
            
            if(!appointmentSlotsToInsert.isEmpty())
                insert appointmentSlotsToInsert;
            
            system.debug('list -- ' + appointmentSlotsToInsert);
         
        } catch(Exception e) {
            System.debug(e);
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
    
    global void execute(SchedulableContext scon) {
      Database.executeBatch(new CreateAppointmentSlotsBatch(),100);
   }
    
  
}