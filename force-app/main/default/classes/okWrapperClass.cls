public without sharing class okWrapperClass {
    public class ContactInformation{
        @AuraEnabled public string firstName;
        @AuraEnabled public string middleName;
        @AuraEnabled public string lastName;
        @AuraEnabled public string email;
        @AuraEnabled public string patientId;
        @AuraEnabled public Date dob;
        @AuraEnabled public string phone;
        @AuraEnabled public string contactId;
        @AuraEnabled public string gender;
        @AuraEnabled public Decimal zip;
        @AuraEnabled public string address1;
        @AuraEnabled public string address2;
        @AuraEnabled public string city;
        @AuraEnabled public string state;
        @AuraEnabled public string beenSickMoreThan1day;
        @AuraEnabled public string feverOrCough;
        @AuraEnabled public string hadNasalSwab;
        @AuraEnabled public Date firstFeltSick;
        @AuraEnabled public Date firstFeltWell;
        @AuraEnabled public Date lastDateOfContact;
        @AuraEnabled public Date dateYouWereTested;
        @AuraEnabled public string testResult;
        @AuraEnabled public string inContactWithCovidPositive;
        @AuraEnabled public string inContactWithDiagnosedPerson;
        @AuraEnabled public string prescriptionMed;
        @AuraEnabled public string medsInlastThreedays;
        @AuraEnabled public Boolean smsCheckbox;
        @AuraEnabled public Boolean emailCheckbox;
        @AuraEnabled public string suffix;
        @AuraEnabled public string stateofBirth;
        @AuraEnabled public string countryofBirth;
        @AuraEnabled public string race;
        @AuraEnabled public string ethnicity;
        @AuraEnabled public string primaryLanguage;
        @AuraEnabled public string maritialStatus;
        @AuraEnabled public Boolean canSchedule;
        @AuraEnabled public String needSpanishInterpretor;
        @AuraEnabled public String specialistOptions;
        @AuraEnabled public String recordQuestion;
        @AuraEnabled public String ifSymptomatic;
        @AuraEnabled public String whereYouLive;
        @AuraEnabled public String highRisk;
        @AuraEnabled public String inContactWithpositiveCase;
        @AuraEnabled public String testingType;
        @AuraEnabled public String firstTest;
        @AuraEnabled public String empHealthCare;
        @AuraEnabled public String empHighRisk;
        @AuraEnabled public String parentOccupation;
        // @AuraEnabled public String Symptomatic;
        @AuraEnabled public Date illnessDate;
        @AuraEnabled public String diseaseSymptoms;
        @AuraEnabled public String residentSetting;
        @AuraEnabled public String residencyType;
        @AuraEnabled public String Pregnant;
        // @AuraEnabled public String verbalConsent;
        @AuraEnabled public String county;
        @AuraEnabled public Boolean scheduled;    
        @AuraEnabled public String socialSecurityNumber;
        @AuraEnabled public String drivingLicenseNumber;

        @AuraEnabled public String isTestRequiredByEmp;
        @AuraEnabled public String reasonForTest;
        // Added by Muskan --- Start Here
        @AuraEnabled public String insuranceId;
        @AuraEnabled public String insuranceType;
        @AuraEnabled public String medicareType;
        @AuraEnabled public String medicareSupplementPlan;
        @AuraEnabled public String primaryInsuranceCompanyName;
        @AuraEnabled public String otherPrimaryInsuranceCompanyName;
        @AuraEnabled public String memberId;
        @AuraEnabled public String groupNumber;
        @AuraEnabled public String groupName;
        @AuraEnabled public String relationToPolicyHolder;
        @AuraEnabled public String policyholderLastName;
        @AuraEnabled public String policyholderFirstName;
        @AuraEnabled public String policyholderMiddleName;
        @AuraEnabled public String primaryInsuranceCompanyLabel;
        @AuraEnabled public Boolean isGroupNumberRequired;
        // --- End here
        
        public ContactInformation getContact(Contact contact){
            ContactInformation contactInformation = new ContactInformation();
            contactInformation.firstName = contact.FirstName;
            contactInformation.middleName = contact.MiddleName;
            contactInformation.lastName = contact.LastName;
            contactInformation.email = contact.Email;
            contactInformation.patientId = contact.Patient_Id__c;
            contactInformation.phone = contact.MobilePhone;
            contactInformation.contactId = contact.Id;
            contactInformation.gender = contact.Gender__c;
            contactInformation.zip = contact.ZIP__c;
            contactInformation.dob = contact.Birthdate;
            contactInformation.address1 = contact.Street_Address1__c;
            contactInformation.address2 = contact.Street_Address_2__c;
            contactInformation.city = contact.City__c;
            contactInformation.state = contact.State__c;
            contactInformation.smsCheckbox = !contact.Opt_out_for_SMS_Updates__c;
            contactInformation.emailCheckbox = !contact.HasOptedOutOfEmail;
            contactInformation.suffix = contact.Salutation ;
            contactInformation.stateofBirth = contact.State_of_Birth__c;
            contactInformation.countryofBirth = contact.Country_of_Birth__c;
            contactInformation.race = contact.Race__c;
            contactInformation.ethnicity = contact.Ethnicity_Hispanic_Origin__c;
            contactInformation.primaryLanguage = contact.Primary_Language__c;
            contactInformation.maritialStatus = contact.Marital_Status__c;
            contactInformation.canSchedule = contact.Can_Self_Schedule__c;
            contactInformation.needSpanishInterpretor = String.valueof(contact.Need_Spanish_Interpreter__c);
            contactInformation.specialistOptions = contact.Which_specialist_do_you_need__c;
            contactInformation.recordQuestion = contact.What_type_of_record_do_you_have_question__c;
            contactInformation.testingType = contact.Testing_Type__c;
            contactInformation.county = contact.County__c;
            contactInformation.socialSecurityNumber = contact.Social_Security_Number__c;
            contactInformation.drivingLicenseNumber = contact.Driving_License_Number__c;
            return contactInformation;
        } 
        
        public Contact saveContact(Contact con){
            if(!String.isBlank(contactId))
                con.Id = contactId;
            con.FirstName = firstName;
            con.MiddleName = middleName;
            con.LastName = lastName;
            con.Email = email;
            //con.Patient_Id__c = patientId;
            
            con.MobilePhone = phone;
            
            con.Gender__c  = gender;
            con.ZIP__c  = zip;
            con.Birthdate  = dob;
            con.Street_Address1__c  = address1;
            con.Street_Address_2__c  = address2;
            con.City__c  = city;
            con.State__c  = state;
            con.Opt_out_for_SMS_Updates__c  = smsCheckbox != null ? !smsCheckbox : true;
            con.HasOptedOutOfEmail  = emailCheckbox != null ? !emailCheckbox : true;
            con.MailingCity = city;
            con.MailingState = state;
            con.MailingStreet =  address1 != null ? address1 : '' + ' '+address2 != null ? address2 : '';
            con.Salutation = suffix;
            con.State_of_Birth__c = stateofBirth;
            con.Country_of_Birth__c = countryofBirth;
            con.Race__c = race;
            con.Ethnicity_Hispanic_Origin__c = ethnicity;
            con.Primary_Language__c = primaryLanguage;
            con.Marital_Status__c = maritialStatus;
            con.Need_Spanish_Interpreter__c = needSpanishInterpretor != null ? Boolean.valueOf(needSpanishInterpretor) : false;
            con.What_type_of_record_do_you_have_question__c = recordQuestion;
            con.Which_specialist_do_you_need__c = specialistOptions;
            con.County__c = county;
            con.Auto_Scheduled__c = scheduled != null ? scheduled : false;
            con.Social_Security_Number__c = socialSecurityNumber;
            con.Driving_License_Number__c = drivingLicenseNumber;
            return con;
        } 
        
        public ContactInformation getPatientHistoryDetails(Contact contact){
            ContactInformation contactInformation = new ContactInformation();
            contactInformation.firstTest = contact.First_Test__c;
            // contactInformation.empHealthCare = contact.Employed_in_healthcare__c;
            contactInformation.empHighRisk = contact.Employed_in_high_risk_setting__c;
            contactInformation.parentOccupation = contact.Patient_occupation__c;
            // contactInformation.Symptomatic = contact.Symptomatic__c;
            contactInformation.illnessDate = contact.Illness_onset_date__c;
            contactInformation.diseaseSymptoms = contact.Disease_symptoms__c;
            contactInformation.residencyType = contact.Patient_Residency_Type__c;
            contactInformation.residentSetting = contact.Resident_Congregate_Setting__c;
            contactInformation.Pregnant = contact.Pregnant__c;
            contactInformation.isTestRequiredByEmp = contact.Is_your_employer_requiring_a_test_today__c;
            contactInformation.reasonForTest = contact.What_is_the_reason_for_your_test_today__c;
            //  contactInformation.verbalConsent = contact.Verbal_Consent__c;
            // contactInformation.ifSymptomatic = contact.Symptomatic__c;
            //   contactInformation.whereYouLive = contact.Live_Work_In_High_Risk_Setting__c;
            //  contactInformation.highRisk = contact.High_Risk_Setting_Type__c;
            //  contactInformation.inContactWithpositiveCase = contact.Direct_Contact_with_Positive_Case__c;
            contactInformation.contactId = contact.Id;
            return contactInformation;
        }
        
        public contact saveCovidResponses(Contact con){
            con.Id = contactId;
            con.First_Test__c = firstTest;
            // con.Employed_in_healthcare__c = empHealthCare;
            con.Employed_in_high_risk_setting__c = empHighRisk;
            con.Patient_occupation__c = parentOccupation;
            // con.Symptomatic__c = Symptomatic;
            con.Illness_onset_date__c = illnessDate;
            con.Disease_symptoms__c = diseaseSymptoms;
            con.Patient_Residency_Type__c = residencyType;
            con.Resident_Congregate_Setting__c = residentSetting;
            con.Pregnant__c = Pregnant;
            con.Is_your_employer_requiring_a_test_today__c = isTestRequiredByEmp;
            con.What_is_the_reason_for_your_test_today__c = reasonForTest;
            //    con.Verbal_Consent__c = verbalConsent;
            //   con.Symptomatic__c = ifSymptomatic;
            // con.Live_Work_In_High_Risk_Setting__c = whereYouLive;
            //con.High_Risk_Setting_Type__c = highRisk;
            //  con.Direct_Contact_with_Positive_Case__c = inContactWithpositiveCase;
            
            return con;
        }
        
        public ContactInformation getSymptoms(Contact contact){
            ContactInformation contactInformation = new ContactInformation();
            contactInformation.firstFeltWell = contact.First_date_you_began_feeling_well__c;
            contactInformation.firstFeltSick = contact.First_Date_you_have_been_feeling_sick__c;
            contactInformation.feverOrCough = contact.Fever_Cough_or_Difficulty_breathing__c;
            contactInformation.beenSickMoreThan1day = contact.Sick_since_Jan_1_2020__c;
            contactInformation.medsInlastThreedays = contact.medication_for_fever_in_the_past_3_days__c;
            contactInformation.prescriptionMed = contact.Do_you_take_prescription_medications__c;
            contactInformation.contactId = contact.Id;
            return contactInformation;
        }
        
        public ContactInformation getCovidSymptoms(Contact contact){
            ContactInformation contactInformation = new ContactInformation();
            // contactInformation.ifSymptomatic = contact.Symptomatic__c;
            //  contactInformation.whereYouLive = contact.Live_Work_In_High_Risk_Setting__c;
            //   contactInformation.highRisk = contact.High_Risk_Setting_Type__c;
            //  contactInformation.inContactWithpositiveCase = contact.Direct_Contact_with_Positive_Case__c;
            contactInformation.contactId = contact.Id;
            return contactInformation;
        }
        
        public contact saveCovidSymptoms(Contact con){
            con.Id = contactId;
            //  con.Symptomatic__c = ifSymptomatic;
            //  con.Live_Work_In_High_Risk_Setting__c = whereYouLive;
            //   con.High_Risk_Setting_Type__c = highRisk;
            //  con.Direct_Contact_with_Positive_Case__c = inContactWithpositiveCase;
            return con;
        }
        
        public Contact saveSymptoms(Contact con){
            con.Id = contactId;
            con.First_date_you_began_feeling_well__c = firstFeltWell;
            con.First_Date_you_have_been_feeling_sick__c = firstFeltSick;
            con.Fever_Cough_or_Difficulty_breathing__c = feverOrCough;
            con.Sick_since_Jan_1_2020__c = beenSickMoreThan1day;
            con.medication_for_fever_in_the_past_3_days__c = medsInlastThreedays;
            con.Do_you_take_prescription_medications__c = prescriptionMed;
            return con;
        }
        
        public ContactInformation getPriorExposure(Contact contact){
            ContactInformation contactInformation = new ContactInformation();
            contactInformation.inContactWithDiagnosedPerson = contact.Had_contact_with_anyone_diagon__c;
            contactInformation.testResult = contact.What_was_your_test_result__c;
            contactInformation.dateYouWereTested = contact.Approximately_what_date_were_you_tested__c;
            contactInformation.lastDateOfContact = contact.what_was_the_last_date_of_contact__c;
            contactInformation.hadNasalSwab = contact.Did_you_have_a_nasal_swab__c;
            contactInformation.inContactWithCovidPositive = contact.Had_contact_with_anyone_covid_confirmed__c;
            contactInformation.contactId = contact.Id;
            return contactInformation;
        }
        
        public Contact savePriorExposure(Contact con){
            con.Id = contactId;
            con.what_was_the_last_date_of_contact__c = lastDateOfContact;
            con.Approximately_what_date_were_you_tested__c = dateYouWereTested;
            con.What_was_your_test_result__c = testResult;
            con.Did_you_have_a_nasal_swab__c = hadNasalSwab;
            con.Had_contact_with_anyone_covid_confirmed__c = inContactWithCovidPositive;
            con.Had_contact_with_anyone_diagon__c = inContactWithDiagnosedPerson;
            return con;
        }
        
        // Added by Muskan ---- Start
        public Insurance__c saveInsuranceDetails(Insurance__c ins) {
            if(!String.isBlank(insuranceId)) {
                ins.Id = insuranceId;
            }
            ins.Insurance_Type__c = insuranceType;
            ins.Select_the_Medicare_type__c = medicareType;
            ins.Do_you_have_a_Medicare_Supplement_plan__c = medicareSupplementPlan;
            ins.Primary_Insurance_Company_Name__c = primaryInsuranceCompanyName;
            ins.Other_Primary_Insurance_Company_Name__c = otherPrimaryInsuranceCompanyName;
            ins.Member_Id__c = memberId;
            ins.Group_Number__c = groupNumber;
            ins.Group_Name__c = groupName;
            ins.Relation_to_PolicyHolder__c = relationToPolicyHolder;
            ins.Policyholder_Last_Name__c = policyholderLastName;
            ins.Policyholder_First_Name__c = policyholderFirstName;
            ins.Policyholder_Middle_Name__c = policyholderMiddleName;
            return ins;
        }
        // ----- End here
        
        public ContactInformation getInsuranceDetails(Insurance__c ins) {
            ContactInformation insuranceDetails = new ContactInformation();
            insuranceDetails.insuranceId = ins.id;
            insuranceDetails.insuranceType = ins.Insurance_Type__c;
            insuranceDetails.medicareType = ins.Select_the_Medicare_type__c;
            insuranceDetails.medicareSupplementPlan = ins.Do_you_have_a_Medicare_Supplement_plan__c;
            //ins.Medicare_Number__c = medicareNumber;
            insuranceDetails.primaryInsuranceCompanyName = ins.Primary_Insurance_Company_Name__c ;
            insuranceDetails.otherPrimaryInsuranceCompanyName = ins.Other_Primary_Insurance_Company_Name__c ;
            insuranceDetails.memberId = ins.Member_Id__c;
            insuranceDetails.groupNumber =  ins.Group_Number__c;
            insuranceDetails.groupName = ins.Group_Name__c;
            insuranceDetails.relationToPolicyHolder = ins.Relation_to_PolicyHolder__c ;
            insuranceDetails.policyholderLastName = ins.Policyholder_Last_Name__c;
            insuranceDetails.policyholderFirstName = ins.Policyholder_First_Name__c ;
            insuranceDetails.policyholderMiddleName = ins.Policyholder_Middle_Name__c;
            insuranceDetails.primaryInsuranceCompanyLabel = ins.Primary_Insurance_Company_Name__r.Name;
            insuranceDetails.isGroupNumberRequired = ins.Primary_Insurance_Company_Name__r.Is_Group_Number_Required__c;
            return insuranceDetails;
        }
        
    }
    
    @AuraEnabled
    public static List<ContactInformation> getInsuranceDetails(String contactId){ 
        List<ContactInformation> contactInfoList = new List<ContactInformation>();
        for(Insurance__c inc : [SELECT Id,Insurance_Type__c,Select_the_Medicare_type__c,Do_you_have_a_Medicare_Supplement_plan__c,Primary_Insurance_Company_Name__c,Other_Primary_Insurance_Company_Name__c,Primary_Insurance_Company_Name__r.Name,Primary_Insurance_Company_Name__r.Is_Group_Number_Required__c ,
                                Member_Id__c,Group_Number__c,Group_Name__c,Relation_to_PolicyHolder__c,Policyholder_Last_Name__c,Policyholder_First_Name__c,Policyholder_Middle_Name__c FROM Insurance__c WHERE Contact__c =: contactId]){
                                  ContactInformation wrapper = new ContactInformation();  
                                    contactInfoList.add(wrapper.getInsuranceDetails(inc));
                                }
        return contactInfoList;
    }
    
    @AuraEnabled
    public static void saveInsuranceDetails(String JsonData, Integer currentstep){
        
        ContactInformation conWrapper = new ContactInformation();
        try{
            conWrapper = (ContactInformation) JSON.deserialize(JsonData, ContactInformation.class);
            system.debug('conWrapper-----'+conWrapper);
            Contact conObj = new Contact();
            List<Contact> contactList = [SELECT id, Current_Step__c FROM Contact WHERE id = :conWrapper.contactId];
            Decimal step = 0;
            if(contactList.size()>0){
                if(contactList[0].Current_Step__c != null){
                    step = contactList[0].Current_Step__c;
                }
                if(step >= Decimal.valueOf(currentStep)){
                    step = step;
                }else{
                    step = Decimal.valueOf(currentStep);
                }
            }
            conObj.Current_Step__c = step;
            conObj.Id = conWrapper.contactId;
            conObj.FirstName = conWrapper.firstName;
            conObj.LastName = conWrapper.lastName;
            conObj.MiddleName = conWrapper.middleName;
            system.debug('conObj>>>>' +conObj);
            update conObj;
            
            Insurance__c insurance = new Insurance__c();
            insurance = conWrapper.saveInsuranceDetails(insurance);
            insurance.Contact__c = conWrapper.contactId;
            upsert insurance;
            
        }catch (Exception e) {
            throw new AurahandledException(e.getMessage());
        }
    }
    
    public class appointmentDetails{
        public string contactId;
        public string ifSymptomatic; 
        public string inContactWithpositiveCase;
        public string whereYouLive ;
        public string testingType;
    }
}