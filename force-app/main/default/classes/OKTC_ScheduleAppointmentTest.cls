@isTest
public class OKTC_ScheduleAppointmentTest {
    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        createAccountTestSite.Number_of_Weeks_Available__c = '3';
        insert createAccountTestSite;

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OK',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;
    }
     
    public static testMethod void getAccountSlotsTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
      
        slot.Start_Time__c = Time.newInstance(14, 0, 0, 0);
        slot.End_Time__c = Time.newInstance(15, 0, 0, 0);
        slot.Testing_Site_Type__c = 'Rapid Testing';
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;       
        
        insert slot;
        
        String accountId = String.valueOf(accountSiteList[0].Id);
        Map<String,Object> appMap = OKTC_ScheduleAppointment.getAccountSlots(accountSiteList[0].Id,'Rapid Testing');
        system.assert(!appMap.isEmpty());
    }

    public static testMethod void createSlotsTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(7, 30, 0, 0);
        slot.End_Time__c = Time.newInstance(8, 0, 0, 0);
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;   
        insert slot;

        Appointment_Slot__c slot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot2.Date__c = Date.today().addDays(3);
        slot2.Account__c = accountSiteList[0].Id;
        slot2.Start_Time__c = Time.newInstance(11, 30, 0, 0);
        slot2.End_Time__c = Time.newInstance(12, 0, 0, 0);
        slot2.Unscheduled__c = true;
        slot2.Appointment_Count__c = 0;   
        insert slot2;
        
        String accountId = String.valueOf(accountSiteList[0].Id);

        String str = '[{"recid":"+'+slot.Id+'","slotid":"+'+slot.Id+'","slotdate": "04/18","slottime":"07:30 AM"},'+'{"recid":"+'+slot2.Id+'","slotid":"+'+slot2.Id+'","slotdate":"04/20","slottime":"11:30 AM"}]';

        OKTC_ScheduleAppointment.createSlots(accountId, str, 2);
    }
    
     public static testMethod void createSlotsPCRTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(7, 30, 0, 0);
        slot.End_Time__c = Time.newInstance(8, 0, 0, 0);
        slot.Testing_Site_Type__c = 'PCR Testing'; 
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;   
        insert slot;

        Appointment_Slot__c slot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot2.Date__c = Date.today().addDays(3);
        slot2.Account__c = accountSiteList[0].Id;
        slot2.Start_Time__c = Time.newInstance(11, 30, 0, 0);
        slot2.End_Time__c = Time.newInstance(12, 0, 0, 0);
         slot.Testing_Site_Type__c = 'PCR Testing';  
        slot2.Unscheduled__c = true;
        slot2.Appointment_Count__c = 0;   
        insert slot2;
        
        String accountId = String.valueOf(accountSiteList[0].Id);

        String str = '[{"recid":"+'+slot.Id+'","slotid":"+'+slot.Id+'","slotdate": "04/18","slottime":"07:30 AM"},'+'{"recid":"+'+slot2.Id+'","slotid":"+'+slot2.Id+'","slotdate":"04/20","slottime":"11:30 AM"}]';

        OKTC_ScheduleAppointment.createPcrSlots(accountId, str, 2);
    }
    
    public static testMethod void createSlotsRapidTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(7, 30, 0, 0);
        slot.End_Time__c = Time.newInstance(8, 0, 0, 0);
        slot.Testing_Site_Type__c = 'Rapid Testing'; 
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;   
        insert slot;

        Appointment_Slot__c slot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot2.Date__c = Date.today().addDays(3);
        slot2.Account__c = accountSiteList[0].Id;
        slot2.Start_Time__c = Time.newInstance(11, 30, 0, 0);
        slot2.End_Time__c = Time.newInstance(12, 0, 0, 0);
         slot.Testing_Site_Type__c = 'Rapid Testing';  
        slot2.Unscheduled__c = true;
        slot2.Appointment_Count__c = 0;   
        insert slot2;
        
        String accountId = String.valueOf(accountSiteList[0].Id);

        String str = '[{"recid":"+'+slot.Id+'","slotid":"+'+slot.Id+'","slotdate": "04/18","slottime":"07:30 AM"},'+'{"recid":"+'+slot2.Id+'","slotid":"+'+slot2.Id+'","slotdate":"04/20","slottime":"11:30 AM"}]';

        OKTC_ScheduleAppointment.createRapidSlots(accountId, str, 2);
    }
    
    
    public static testMethod void createTemplateSlotsTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(7, 30, 0, 0);
        slot.End_Time__c = Time.newInstance(8, 0, 0, 0);
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;  
        slot.Template__c = true;
        slot.Number_of_Available_Slots__c = 2;
        slot.Day_of_the_Week__c = 'Tue';
        insert slot;

        Appointment_Slot__c slot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot2.Date__c = Date.today().addDays(3);
        slot2.Account__c = accountSiteList[0].Id;
        slot2.Start_Time__c = Time.newInstance(11, 30, 0, 0);
        slot2.End_Time__c = Time.newInstance(12, 0, 0, 0);
        slot2.Unscheduled__c = true;
        slot2.Appointment_Count__c = 0; 
        slot2.Template__c = true;
        slot2.Number_of_Available_Slots__c = 2;
        slot2.Day_of_the_Week__c = 'Tue';       
        insert slot2;
        
        String accountId = String.valueOf(accountSiteList[0].Id);

        String str = '[{"recid":"+'+slot.Id+'","slotid":"+'+slot.Id+'","slotdate": "04/18","slottime":"07:30 AM","day" : "tue"},'+'{"recid":"+'+slot2.Id+'","slotid":"+'+slot2.Id+'","slotdate":"04/20","slottime":"11:30 AM","day" : "tue"}]';

        OKTC_ScheduleAppointment.createTemplateSlots(accountId, str, 2);
    }
    
    public static testMethod void createSlotsFrequencyTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        accountSiteList[0].Appointment_Frequency__c='';
        update accountSiteList[0];
        List<Account> accountSiteList1 = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList1[0].Id)  ;
        slot.Date__c = Date.newInstance(2020, 4, 18);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(7, 30, 0, 0);
        slot.End_Time__c = Time.newInstance(8, 0, 0, 0);
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0;   
        insert slot;

        Appointment_Slot__c slot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot2.Date__c = Date.newInstance(2020, 4, 18);
        slot2.Account__c = accountSiteList[0].Id;
        slot2.Start_Time__c = Time.newInstance(11, 30, 0, 0);
        slot2.End_Time__c = Time.newInstance(12, 0, 0, 0);
        slot2.Unscheduled__c = true;
        slot2.Appointment_Count__c = 0;   
        insert slot2;

        String accountId = String.valueOf(accountSiteList[0].Id);

        String str = '[{"recid":"+'+slot.Id+'","slotid":"+'+slot.Id+'","slotdate":"04/18","slottime":"07:30 AM"},'+'{"recid":"+'+slot2.Id+'","slotid":"+'+slot2.Id+'","slotdate":"04/20","slottime":"11:30 AM"}]';

        OKTC_ScheduleAppointment.createSlots(accountId, str, 1);
    }


}