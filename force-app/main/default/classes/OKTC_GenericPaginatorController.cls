public without sharing class OKTC_GenericPaginatorController {
    
    @AuraEnabled
    public static Map<String,Object> retrieveRecords(Map<String,Object> parameters) {
            return retrieveRecordsWithSharing(parameters);
    }

    public static Map<String,Object> retrieveRecordsWithSharing(Map<String,Object> parameters) {
        system.debug('object >> ' +  parameters.get('objectName'));
        //, (SELECT Id, Results__c FROM Antibody_Testings__r)
        Map<String,Object> results = new Map<String,Object>();
        String queryString = 'SELECT Id';// + (parameters.get('fields')).toString() + 
        for(object obj : (List<Object>)parameters.get('fields')) {
            if(!((String)obj).equalsIgnoreCase('Id'))
                queryString += ', ' + (String)obj;
        }
        
        if( parameters.get('objectName') == 'Appointment__c'){
            queryString += ', (SELECT Id, Results__c FROM Antibodies_Testing__r )';
        }
       		queryString += ' FROM ' + parameters.get('objectName');
        
        Integer pageDifference = Integer.valueof(parameters.get('newPageNo')) - Integer.valueof(parameters.get('previousPageNo'));
        Integer pageLimit = (pageDifference > 0 ? pageDifference : pageDifference * -1) * Integer.valueof(parameters.get('limit'));
        String filters = '';
        String searchString = parameters.containsKey('searchStr') ? (String)parameters.get('searchStr') : '';
        if( String.isNotBlank((searchString))) {
            filters += ' ( '; 
            for(Object obj : (List<Object>)parameters.get('fields')) {
                if((String.valueof(obj)).contains('.')) {
                    List<String> objFieldList = (String.valueof(obj)).split('\\.');
                    String parentFieldName = objFieldList[objFieldList.size() - 2].replace('__r','__c');
                    String objectName  = Schema.getGlobalDescribe().get((String)parameters.get('objectName')).getDescribe().fields.getMap().get((String)parentFieldName).getDescribe().getReferenceTo()[0].getDescribe().getName();

                    String fieldName = objFieldList[objFieldList.size() - 1];

                    Schema.DisplayType displayType = Schema.getGlobalDescribe().get((String)objectName).getDescribe().fields.getMap().get((String)fieldName).getDescribe().getType();
                    if( displayType == Schema.DisplayType.String || displayType == Schema.DisplayType.Email) {
                        filters += filters != ' ( ' ? ' OR ' : ' ';
                        filters +=  (String)obj + ' LIKE \'%' + String.escapeSinglequotes(searchString) + '%\' ';
                    }
                } else {
                    Schema.DisplayType displayType = Schema.getGlobalDescribe().get((String)parameters.get('objectName')).getDescribe().fields.getMap().get((String)obj).getDescribe().getType();
                    if( displayType == Schema.DisplayType.String || displayType == Schema.DisplayType.Email) {
                        filters += filters != ' ( ' ? ' OR ' : ' ';
                        filters +=  (String)obj + ' LIKE \'%' + String.escapeSinglequotes(searchString) + '%\' ';
                    }
                }
            }
            filters += ' )';
        }
        if(parameters.containsKey('whereClause') && String.isNotBlank((String)parameters.get('whereClause'))) {
            filters += filters != '' ? ' AND ' : '';
            filters += parameters.get('whereClause');
        }
        String countFilter = filters == '' ? '' : ' WHERE ' + filters;
        Integer reccount = Database.countQuery('SELECT Count() FROM ' + parameters.get('objectName') + countFilter);
        results.put('totalCount',reccount);
        String recId = (String)parameters.get('recId');
        String recVal = parameters.get('recVal') != null ? String.escapeSinglequotes((String)parameters.get('recVal')) : '';
        if(parameters.get('newPageNo') != 1 && parameters.get('newPageNo') != parameters.get('maxPageNo')) {
            if(pageDifference > 0 && String.isNotBlank(recId)) {
                filters += filters != '' ? ' AND ' : ' ';
                filters += ' (( Id > \'' + recId  + '\')) ';
            } else if(pageDifference < 0 && String.isNotBlank(recId)) {
                filters += filters != '' ? ' AND ' : ' ';
                filters += ' (( Id < \'' + recId   + '\')) ';
            }
            filters = filters != '' ? ' WHERE ' +  filters : filters;
            if(pageDifference > 0 && String.isNotBlank(recId)) {
                filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS FIRST , ID '  + parameters.get('sortDir');
            } else if(pageDifference < 0 && String.isNotBlank(recId)) {
                String sortDir = parameters.get('sortDir') == 'ASC' ? ' DESC ' : ' ASC '; 
                filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + sortDir + '  NULLS LAST , ID '  + sortDir;
            }

        } else if(parameters.get('newPageNo') != 1 && parameters.get('newPageNo') == parameters.get('maxPageNo')) {
            pageLimit = Math.mod(reccount , Integer.valueof(parameters.get('limit'))) == 0 ? Integer.valueof(parameters.get('limit')) : Math.mod(reccount , Integer.valueof(parameters.get('limit')));
            String sortDir = parameters.get('sortDir') == 'ASC' ? ' DESC ' : ' ASC '; 
            filters = filters != '' ? ' WHERE ' +  filters : filters;
            filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + sortDir + '  NULLS LAST  , ID '  + sortDir;
        } else {
            filters = filters != '' ? ' WHERE ' +  filters : filters;
            filters += ' ORDER BY ' + parameters.get('sortBy') + ' ' + parameters.get('sortDir') + '  NULLS FIRST , ID '  + parameters.get('sortDir');
        }
        
        if(parameters.containsKey('limit')) {
            filters += ' LIMIT ' + pageLimit;
        }
        List<Sobject> records = new List<Sobject>();
        System.debug('queryString     '+queryString + filters);
        results.put('query',queryString + filters);

        List<Sobject> queryResult = Database.query(queryString + filters);
        Integer counter = 0;
        if((parameters.get('newPageNo') != 1 && parameters.get('newPageNo') == parameters.get('maxPageNo')) ) {
            for(Integer count = queryResult.size() - 1 ;count >= 0 ; count-- ) {
                records.add(queryResult[count]);
            }
        } else if(pageDifference < 0) {
            for(Integer count = queryResult.size() - 1 ;count >= pageLimit - Integer.valueof(parameters.get('limit')) ; count-- ) {
                records.add(queryResult[count]);
            }
        } else {
            for(Sobject record : queryResult) {
                if(counter >= ((pageLimit/Integer.valueof(parameters.get('limit'))) - 1 ) * Integer.valueof(parameters.get('limit')))
                    records.add(record);
                counter++;
            }
        }
        results.put('records',records);
        return results;//;
    }
}