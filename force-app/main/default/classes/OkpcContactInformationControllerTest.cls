@isTest
public class OkpcContactInformationControllerTest {
    @TestSetup
    static void makeData(){
            

        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        String laboratoryRecTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String labUserRecTypeId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Lab_User').RecordTypeId;
        
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'OK';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Testing_Site_Type__c = 'Antibody Testing;PCR Testing';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Account labAccount = TestDataFactoryAccount.createAccount('TestLabAccount', testingCenter, false);
        labAccount.BillingCity = 'testCity';
        labAccount.BillingCountry = 'testCountry';
        labAccount.BillingState = 'OK';
        labAccount.BillingStreet = 'testStreet';
        labAccount.BillingPostalCode = '12345';
        labAccount.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        labAccount.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert labAccount;

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes ',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = ' Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;

        Contact con1 = new Contact (
            AccountId = labAccount.id,
            LastName = 'LabTestUser1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testLabUser@domain.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-44151',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            //Testing_Site__c = labAccount.Id,
            RecordTypeId = testingSiteId,
            what_was_the_last_date_of_contact__c = Date.newInstance(2020, 2, 20),
            Did_you_have_a_nasal_swab__c = 'Yes ',
            What_was_your_test_result__c = 'Negative',
            Approximately_what_date_were_you_tested__c = Date.newInstance(2020, 3, 3),
            Had_contact_with_anyone_diagon__c = 'Yes',
            Do_you_take_prescription_medications__c = ' Yes',
            medication_for_fever_in_the_past_3_days__c = 'Yes',
            First_Date_you_have_been_feeling_sick__c = Date.newInstance(2020, 3, 1) ,
            First_date_you_began_feeling_well__c= Date.newInstance(2020, 3, 10),
            Fever_Cough_or_Difficulty_breathing__c = 'Yes',
            Sick_since_Jan_1_2020__c = 'Yes',
            Opt_out_for_SMS_Updates__c = true,
            HasOptedOutOfEmail = false,
            Current_Step__c = 1,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con1;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, createAccountTestSite.Id);
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        insert createAppointment;

        Appointment_Slot__c createSlot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, labAccount.Id);
        Appointment__c  createAppointment2 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment2.Patient__c = con1.Id;
        createAppointment2.Lab_Center__c = labAccount.id;
        createAppointment2.Appointment_Slot__c = createSlot2.id;
        insert createAppointment2;
        

    }
    public static testMethod void retrieveDashboardValueTest(){ 
        List<Contact> contactList = [Select Id , Name, AccountId FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            Map<String,object> maoObj = OkpcContactInformationController.retrieveDashboardValue(contactList[0].AccountId, 'Daily', '');
        }

    }
    public static testMethod void getPriorExposureTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getPriorExposure();
            OkpcContactInformationController.savePriorExposure(JSON.serialize(wrapper), 2);
        }

    }

    public static testMethod void getSymptomsTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getSymptoms(contactList[0].id);
            OkpcContactInformationController.saveSymptoms(JSON.serialize(wrapper), 2,'Rapid Testing');
        }

    }
     
    public static testMethod void saveAntibodySymptomsTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getSymptoms(contactList[0].id);
            OkpcContactInformationController.saveSymptoms(JSON.serialize(wrapper), 2,'Antibody Testing');
        }

    }
    
    public static testMethod void getContactDetailsTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;
        
        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getContactDetails(contactList[0].id);
            OkpcContactInformationController.saveContactDetails(JSON.serialize(wrapper), 2);
        }

    }

    public static testMethod void getPreferredValuesTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account preferredTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, true);

        Date d = Date.today().addDays(5);

        System.runAs(newUser){
            Contact testContact = OkpcContactInformationController.getPreferredValues();
        }

    }

    public static testMethod void getConsentTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            Contact testContact = OkpcContactInformationController.getConsent();
            OkpcContactInformationController.setConsent(true, 2);
        }
    }

    public static testMethod void getBusinessTimeOnAccountTest(){ 
        List<Contact> contactList = [Select Id , Name , AccountId FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            Account testAccount = OkpcContactInformationController.getBusinessTimeOnAccount('');
            Account testAccount1 = OkpcContactInformationController.getBusinessTimeOnAccount(contactList[0].AccountId);
            String responseMessage = OkpcContactInformationController.setBusinessTimeOnAccount('10:0:0.0','20:0:0.0', contactList[0].AccountId);
            System.assertEquals('success', responseMessage,'Expected is equals to Actual');
        }
    }

    public static testMethod void getContactInformationTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            Contact testContact = OkpcContactInformationController.getContactInformation();
        }
    }

    public static testMethod void setCurrentStepTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        System.runAs(newUser){
            OkpcContactInformationController.setCurrentStep(String.valueOf(3));
        }
    }
    public static testMethod void changeAppointmentStatusTest(){ 
        List<Contact> contactList = [Select Id , Name , Current_Step__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        Integer currentStep = (Integer)contactList[0].Current_Step__c;

        System.runAs(newUser){
            OkpcContactInformationController.changeAppointmentStatus(currentStep);
             List<OkpcContactInformationController.SelectOptionWrapper> wrapperListStaffAccount = OkpcContactInformationController.getAllAccountsForStaff();
        }
    }

    public static testMethod void notChangeAppointmentStatusTest(){ 
        List<Contact> contactList = [Select Id , Name , Current_Step__c FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        Integer currentStep = 3;

        System.runAs(newUser){
            OkpcContactInformationController.notChangeAppointmentStatus(currentStep);
        }
    }

    public static testMethod void setPreferredValuesAndUpdateTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;

        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account preferredTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, true);

        List<Appointment__c> appList = [Select Id , Name FROM Appointment__c WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        String appointmentId = String.valueOf(appList[0].Id);
        String preferredTestSiteId = String.valueOf(preferredTestSite.Id);
        System.runAs(newUser){
            OkpcContactInformationController.setPreferredValuesAndUpdate(Date.today().addDays(5), '15:0:0.0',preferredTestSiteId , appointmentId);
        }
    }

    public static testMethod void fetchPickListTest(){ 
        String objectName = 'Appointment__c';
        String fieldName  = 'Status__c';
        List<OkpcContactInformationController.SelectOptionWrapper> wrapperList = OkpcContactInformationController.fetchPicklist(objectName, fieldName);
        
    }

    public static testMethod void getAccountdetailsTest(){ 
    
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('testingSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'OK';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Account testAccount = OkpcContactInformationController.getAccountdetails(String.valueOf(createAccountTestSite.Id));
    
    }

    public static testMethod void retrieveLabDashboardValueTest(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'LabTestUser1' LIMIT 1];
        List<Account> labAccount = [SELECT Id, Name FROM Account WHERE Name LIKE '%TestLabAccount%' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [SELECT Id,name FROM Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newLabUser@domain.com',
            email = 'newLabUser@domain.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nlabu',
            lastname='User1',
            contactId = contactList[0].id
        );
        insert newUser;

        Appointment__c app = [SELECT Id, Name, Patient__c FROM Appointment__c WHERE Patient__c = :contactList[0].id];
        DC_ScheduleAppointment.completeAppointment(app.Id, labAccount[0].Id, '12345','Positive','TaqPath COVID-19 rRT-PCR Assay','test notes');

        System.runAs(newUser){
            Map<String,object> maoObj = OkpcContactInformationController.retrieveLabDashboardValue();
        }

    }

     public static testMethod void saveContactDetails(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;
        
        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getContactDetails(contactList[0].id);
            OkpcContactInformationController.getPatientHistoryDetails(contactList[0].id);
            OkpcContactInformationController.savePatientDetails(JSON.serialize(wrapper), 2,'personalInfo',false);
        }

    }
      public static testMethod void savePatientHistory(){ 
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess', 'CustomerSuccess'};
        Profile prfile = [select Id,name from Profile where UserType in :customerUserTypes limit 1];        
        User newUser = new User(
            profileId = prfile.id,
            username = 'newUser@yahoo.com',
            email = 'pb@f.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            contactId = contactList[0].id
        );
        insert newUser;
        
        System.runAs(newUser){
            OkpcContactInformationController.ContactInformation wrapper = new  OkpcContactInformationController.ContactInformation();
            wrapper = OkpcContactInformationController.getContactDetails(contactList[0].id);
            OkpcContactInformationController.savePatientDetails(JSON.serialize(wrapper), 3,'patientHistory',true);
        }

    }
    public static testMethod void isAdmin(){ 
        OkpcContactInformationController.isAdminContact();
    }
    public static testMethod void setFrequency(){
        List<Account> acc = [SELECT id,Name FROM Account Where Name = 'TestSite'];
        OkpcContactInformationController.setNumberOfdaysAndFrequency('3','30',acc[0].id);
    }
    
     public static testMethod void zipValidation(){ 
        OkpcContactInformationController.validZipCode('73502');
    }
   
}