public without sharing class okpcRegisterController {
    
    @AuraEnabled
    public static Map<String,Object> selfRegister(String patientid ,String email, String username, String password, String confirmPassword, String delimiter) {//Added by Sajal
        Map<String, Object> result = new Map<String, Object>();
        system.debug('patientid id *** ' + patientid );
        system.debug('email***' + email);
        system.debug('password***' + password);
        system.debug('confirmPassword***' + confirmPassword);
        system.debug('delimiter***' + delimiter);
        String newUsername = username+delimiter;//Added by Sajal
        Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
        //username = username + '.' + Label.Environment + patientId == 'oktc' ? '.testsiteuser' : '.citizen';
        Savepoint sp = null;
        try{
            sp = Database.setSavepoint();
            List<Contact> con;
            if(patientid == 'oktc'){
                con = [SELECT Id,FirstName,LastName,Email,AccountId FROM Contact WHERE Email =: email AND RecordTypeId = :recordTypeInfosMap.get('Antibody_Testing_Site').RecordTypeId];
            }
            else if(patientid == 'oklc'){
                con = [SELECT Id,FirstName,LastName,Email,AccountId FROM Contact WHERE Email =: email AND RecordTypeId = :recordTypeInfosMap.get('Lab_User').RecordTypeId];
            }else if(patientid == 'okcp'){
                system.Debug('I am IN***');
                con = [SELECT Id,FirstName,LastName,Email,AccountId FROM Contact WHERE Email =: email AND RecordTypeId = :recordTypeInfosMap.get('Citizen_COVID').RecordTypeId];
                system.debug('Contact***' + con);
            }else if(patientid == 'okAdmin'){
                con = [SELECT Id,FirstName,LastName,Email,AccountId FROM Contact WHERE Email =: email AND RecordTypeId = :recordTypeInfosMap.get('Vital_Records_Internal_Users').RecordTypeId];
            }else{
                con = [SELECT Id,FirstName,LastName,Email,AccountId FROM Contact WHERE Email =: email AND RecordTypeId = :recordTypeInfosMap.get('Citizen').RecordTypeId];
                //Email =: email AND Patient_Id__c = :patientid AND RecordTypeId = :recordTypeInfosMap.get('Citizen_COVID').RecordTypeId
            }
            
            List<User> userNameList = [SELECT Id, Name FROM User WHERE userName = :newUsername AND IsActive = true];
            if(userNameList.size()>0){
                result.put('message','Username is already in use. Please enter a different username. ');
                result.put('type','error');
                return result;
            }
            if(con.size() > 0 ){
                List <User> userList = [SELECT Id FROM User WHERE ContactId = :con[0].ID AND IsActive = true];
                if(userList.size()>0){
                    result.put('message','User already exists.');
                    result.put('type','error');
                }else{
                    User u = new User();
                    u.Username = newUsername;//Modified by Sajal
                    u.put('Email',email);
                    u.ProfileId = getProfileId(patientid);
                    u.FirstName = con[0].FirstName;
                    u.LastName = con[0].LastName;   
                    u.timezonesidkey = 'America/Chicago';
                    String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0,1) : '' );
                    nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                    u.put('CommunityNickname', nickname);
                    String userId = Site.createPortalUser(u, con[0].accountId, password);
                    ApexPages.PageReference lgn = Site.login(newUsername, password, '/s');
                    aura.redirect(lgn);
                    
                    result.put('message', 'User registered successfully. Please check your e-mail.');
                    result.put('type','success');
                    result.put('redirectURL',lgn.getUrl());
                }
            }else{
                result.put('message','Information not found. Please contact your administrator.');
                result.put('type','error');
            }
            
        }catch(DMLException ex){
            Database.rollback(sp);
            result.put('message', ex.getDMLMessage(0)+ex.getStackTraceString());
            result.put('type','error');
        }
        catch (Exception ex) {
            Database.rollback(sp);
            result.put('message', ex.getMessage());
            result.put('type','error');         
        }
        return result;
    }
    
    @AuraEnabled
    public static String forgotPassword(String userName, String delimiter) {//Added delimiter by Sajal
        String newUsername = userName + delimiter;
        String userId;
        String url = './CheckPasswordResetEmail';
        try {
            if(!Site.isValidUsername(newUsername)) {//Modified by Sajal
                return Label.Site.invalid_email;
            }
            Site.forgotPassword(newUsername);
            ApexPages.PageReference checkEmailRef = new PageReference(url);
            
            If(!test.isRunningTest())
            aura.redirect(checkEmailRef);
                
            System.debug('success');
            return 'success';
        }
        catch (Exception ex) {
            System.debug('error-->'+ex.getMessage());
            return ex.getMessage();
        }
        
    }
    
    public static Id getProfileId(String type) {
        if(type == 'oktc'){
            return [SELECT Id FROM Profile WHERE Name = :System.Label.OKTC_Profile_Name].Id;
        }
        else if(type == 'oklc'){
            return [SELECT Id FROM Profile WHERE Name = :System.Label.OKLC_Profile_Name].Id;
        }else if(type == 'okcp'){
            return [SELECT Id FROM Profile WHERE Name = :System.Label.OKCP_Profile_Name].Id;
        }else if(type == 'okAdmin'){
            return [SELECT Id FROM Profile WHERE Name = :System.Label.Ok_Vital_Admin_Profile_Name].Id;
        }else{
            return [SELECT Id FROM Profile WHERE Name = :System.Label.OKPC_Profile_Name].Id;
        }
    }
    
    
    @AuraEnabled
    public static ContactInfoWrapper getContactInformation(String contactId){
        List<Contact> contactList = [SELECT Id, Name, Patient_Id__c, email FROM Contact WHERE Id = :contactId];
        ContactInfoWrapper wrapper = new ContactInfoWrapper();
        if(contactList.size()>0){
            wrapper.patientId = contactList[0].Patient_Id__c;
            wrapper.email = contactList[0].email;
            return wrapper;
        }else{
            return null;
        }
    }
    
    
    public class ContactInfoWrapper {
        @AuraEnabled public String patientId {get; set;}
        @AuraEnabled public String email {get; set;}
    } 
}