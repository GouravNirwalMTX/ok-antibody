@isTest
public class OK_SelfRegistrationTest {

  @TestSetup
static void makeData(){
        
    String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
    String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
    String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
    
    Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
    createAccountTestSite.BillingCity = 'testCity';
    createAccountTestSite.BillingCountry = 'testCountry';
    createAccountTestSite.BillingState = 'OK';
    createAccountTestSite.BillingStreet = 'testStreet';
    createAccountTestSite.BillingPostalCode = '12345';
    createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
    createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
    insert createAccountTestSite;

    Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);
    Contact con = new Contact (
        AccountId = createAccountTestSite.id,
        LastName = 'portalTestUserv1',
        MiddleName = 'm',
        FirstName = 'testFirst',
        Email = 'testportalTestUserv1@gmail.com',
        Birthdate = Date.newInstance(1994, 5, 5),
        Gender__c = 'Male',
        MobilePhone = '(702)379-44151',
        Phone = '(702)379-4415',
        Street_Address1__c ='testStreet1',
        Street_Address_2__c ='testStreet2',
        City__c = 'TestCity',
        State__c = 'OH',
        ZIP__c = 12311,
        Testing_Site__c = createAccountTestSite.Id,
        RecordTypeId = testingSiteId,
        Preferred_Date__c = Date.today(),
        Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
    );
    insert con;
}

public static testMethod void selfRegisterContactTest(){
   Test.startTest();
    Map < String, Object > selfRegisterContactResult=OK_SelfRegistration.selfRegisterContact('testFirst','portalTestUserv1','9878878877','kumarsatish@bmmc.com',System.Label.OKSF_Registration_Covid_Citizen);
   Test.stopTest(); 
    string contactType=System.Label.OKSF_Registration_Covid_Citizen;
    
    Contact[] con=[select id,FirstName,email,LastName,phone,AccountId from contact where LastName='portalTestUserv1' limit 1];
    string profileId=[select id,name from profile where name='Vital Records Citizen Lobby User' limit 1].id;
    Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
        Profile_Name__c,
        Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: contactType LIMIT 1
    ];
    String newUsername=con[0].email+'.dev';
    user u=new user();
    u.Username = con[0].email; //Modified by Sajal
    u.put('Email', con[0].email);
    u.ProfileId = profileId;
    u.FirstName = con[0].FirstName;
    u.LastName = con[0].LastName;
    u.Password__c = 'test1233@N';
    u.Phone = con[0].phone;
    u.ContactId = con[0].Id;
    u.emailencodingkey = 'UTF-8';
    u.languagelocalekey = 'en_US';
    u.localesidkey = 'en_US';
    u.timezonesidkey = 'America/Los_Angeles';
    u.alias = con[0].FirstName.substring(0, 3);

    String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
    nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
    u.put('CommunityNickname', nickname);
    insert u;
    string AccountId=con[0].AccountId;
    try{
     Map < String, Object > registerUserResult=OK_SelfRegistration.registerUser('testFirst','portalTestUserv1','9878878877','testportalTestUserv1@gmail.com',System.Label.OKSF_Registration_Covid_Citizen,'dev');
    }catch(Exception ex){
        system.debug('error Message' + ex.getMessage());
    }
  //Map < String, Object > saveRegisterContactResult=OK_SelfRegistration.saveRegisterContact('testFirst','portalTestUserv1',System.Label.OKSF_Registration_Covid_Citizen,true,true,AccountId);

}
    public static testMethod void registerUserTest(){
       Test.startTest();
        Map < String, Object > selfRegisterContactResult=OK_SelfRegistration.selfRegisterContact('testFirst','portalTestUserv1','9878878877','kumarsatish@bmmc.com',System.Label.OKSF_Registration_Covid_Citizen);
       Test.stopTest(); 
        
        Contact[] con=[select id,FirstName,email,LastName,phone,AccountId from contact where LastName='portalTestUserv1' limit 1];
        string profileId=[select id,name from profile where name='Vital Records Citizen Lobby User' limit 1].id;
        string contactType=System.Label.OKSF_Registration_Covid_Citizen;
          
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c,
            Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: contactType LIMIT 1
        ];
        System.debug('>>>>>' + settings);
        String newUsername=con[0].email+'.dev';
        user u=new user();
        u.Username = con[0].email; //Modified by Sajal
        u.put('Email', con[0].email);
        u.ProfileId = profileId;
        u.FirstName = con[0].FirstName;
        u.LastName = con[0].LastName;
        u.Password__c = 'test1233@N';
        u.Phone = con[0].phone;
        u.ContactId = con[0].Id;
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.timezonesidkey = 'America/Los_Angeles';
        u.alias = con[0].FirstName.substring(0, 3);
        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
        u.put('CommunityNickname', nickname);
        insert u;

        string AccountId=con[0].AccountId;

        contact con1 = new contact();
        con1.FirstName = 'TestTesing';
        con1.LastName = 'Site';
        con1.Email = 'testTestingsite@gmail.com';
        con1.MobilePhone = '9413609333';
            
        Map < String, Object > createContactFromTestingSite = OK_SelfRegistration.createContactFromTestingSite(con1, 'Citizen', null, AccountId);
        Map < String, Object > createContactFromTestingSite1 = OK_SelfRegistration.createContactFromTestingSite(con1, 'Citizen', JSON.serialize(con1), AccountId);
       
        contact con2 = new contact();
        con2.FirstName = 'TestTesing';
        con2.LastName = 'Site';
        con2.MobilePhone = '9413609333';
       	
        Map < String, Object > createContactFromTestingSite2 = OK_SelfRegistration.createContactFromTestingSite(con2, 'Citizen', JSON.serialize(con2), AccountId);  
        
        Map < String, Object > registerUserResult ;
        try{
            registerUserResult = OK_SelfRegistration.registerUser('testFirst441','portalTestUserv1','9878878877','testportal1TestUserv2341@gmail.com','VR Citizen Lobby','dev');
        }catch(exception e){
            registerUserResult = null;
        }        
        System.assert(registerUserResult == null);
        
        Map < String, Object > saveRegisterContactResult=OK_SelfRegistration.saveRegisterContact('testFirst','portalTestUserv1','Citizen_Consultation_Vital_Records',true,true,AccountId);
        Map < String, Object > saveRegisterContactResult1=OK_SelfRegistration.saveRegisterContact('testFirst1','portalTestUserv1','Citizen_Lobby_Vital_Records',true,true,AccountId);
        
        Map < String, Object > saveUserResult=OK_SelfRegistration.saveUser('testFirst1','portalTestUserv1','9878878877','saveUSer1@gmail.com','Citizen_Consultation_Vital_Records',AccountId,true,true,'dev');
        Map < String, Object > saveUserResult1=OK_SelfRegistration.saveUser('testFirst1','portalTestUserv1','9878878877','saveUSer1@gmail.com','Citizen_Consultation_Vital_Records',AccountId,true,true,'dev');
        Map < String, Object > saveUserResult2=OK_SelfRegistration.saveUser('testFirst2','portalTestUserv2','9878878877','','Citizen_Lobby_Vital_Records',AccountId,true,true,'dev');
        
        System.assertEquals(saveRegisterContactResult.size() > 0 , true);
        System.assertEquals(saveRegisterContactResult1.size() > 0 , true);
        
        System.assertEquals(saveUserResult.size() > 0 , true);
        System.assertEquals(saveUserResult1.size() > 0 , true);
        System.assertEquals(saveUserResult2.size() > 0 , true);
    }
}