public without sharing class okpcSelfRegistration {
    private static Map<String,Schema.RecordTypeInfo> recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
     private static Map<String,Schema.RecordTypeInfo> recordTypeInfosMapContact = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    /* @AuraEnabled
   public static List<SelectOptionWrapper> getCitizenProject() {
        
        List<SelectOptionWrapper> accountNameId = new List<SelectOptionWrapper>();
        for(Account acc : [SELECT ID,Name FROM Account WHERE RecordTypeId = :recordTypeInfosMap.get('Citizen').RecordTypeId]){
            SelectOptionWrapper options = new SelectOptionWrapper();
            options.label = acc.Name;
            options.value = acc.Id;
            accountNameId.add(options);
        }
        system.debug('accountNameId' + accountNameId);
        return accountNameId;
    } */
    
     @AuraEnabled
    public static Map<String,Object> selfRegisterContact(String firstName ,String lastName, String phone, String email) {
         Map<String, Object> result = new Map<String, Object>();
        
        List<contact> existingCon = [SELECT ID,Name,Email FROM Contact WHERE Email =: email];
        if(existingCon.size() > 0){
            result.put('message','Contact with this Email is already Present.');
            result.put('type','error');
            return result;
        }
        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        con.MobilePhone  = phone;
        con.Email = email;
        con.AccountId = System.Label.Self_Register_Account_Id;
        con.RecordTypeId = recordTypeInfosMapContact.get('Citizen_COVID').RecordTypeId;
        insert con;
        
        result.put('message', 'Thank You for providing your details for COVID-19 testing. Please check your email for further instructions.');
        result.put('type','success');
        
        return result;
        
    }
    
    Public class SelectOptionWrapper{
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
    }
}