public class TestDataFactoryAntiBody {
    public static Antibody_Testing__c createAppointment(Boolean isInsert,id testingSite,id Appointment,id appointmentSlot,id patient){
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Antibody_Testing__c antibodyTesting = new Antibody_Testing__c();
        antibodyTesting.Appointment__c = Appointment;
        //antibodyTesting.Appointment_Slot__c = appointmentSlot;
        antibodyTesting.Testing_Site__c = testingSite;
        antibodyTesting.Patient__c = patient;
        antibodyTesting.RecordTypeId = antibodyRecordTypeId;
        if(isInsert){
            insert antibodyTesting;
            return antibodyTesting;
        }
        return antibodyTesting;
    }
}