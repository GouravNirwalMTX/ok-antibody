@isTest
public class TestDataFactoryInsurance {
    public static Insurance__c createInsurance(Id contactId, Boolean isInsert) {
        Insurance__c ins = new Insurance__c();
        ins.Contact__c = contactId;
        if(isInsert){
            insert ins;
        }
        return ins;
    }
}