public without sharing class OK_SelfRegistration {
    public static final String RT_CITIZEN_COVID = 'Citizen_COVID';
    public static final String RT_CITIZEN_LOBBY_VITAL = 'Citizen_Lobby_Vital_Records';
    public static final String REG_TYPE_CITIZEN_COVID = 'Citizen_COVID';
    public static final String REG_TYPE_VR_CITIZEN_LOBBY = 'Citizen_Lobby_Vital_Records';
    public static final String REG_TYPE_VR_CITIZEN_CONSULTATION = 'Citizen_Consultation_Vital_Records';
    static Id userId = UserInfo.getUserId();
    private static Map < String, Schema.RecordTypeInfo > recordTypeInfosMap = Account.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
    public static final String EMAIL_STRING = '@okhla.com';
    private static Map < String,Schema.RecordTypeInfo > recordTypeInfosMapContact = Contact.sObjectType.getDescribe().RecordTypeInfosByDeveloperName;
   
    @AuraEnabled
    public static Map < String, Object > selfRegisterContact(String firstName, String lastName, String phone, String email,
        String type) {
        Map < String, Object > result = new Map < String, Object > ();

        List < contact > existingCon = [SELECT ID, Name, Email FROM Contact WHERE Email =: email];
        if (existingCon.size() > 0) {
            result.put('message', 'Contact with this Email is already Present.');
            result.put('type', 'error');
            return result;
        }
        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        con.MobilePhone = phone;
        con.Email = email;
        if (type == System.Label.OKSF_Registration_Covid_Citizen) {
            Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c, Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: type LIMIT 1];
            con.AccountId = settings[0].Account_Id__c;
            con.RecordTypeId = recordTypeInfosMapContact.get(RT_CITIZEN_COVID).RecordTypeId;
        }

        insert con;

        result.put('message',
            'Thank You for providing your details for COVID-19 testing. Please check your email for further instructions.');
        result.put('type', 'success');

        return result;

    }

    
    
    //Method used for Citizen Lobby and Consultation Portal User Registration
    @AuraEnabled
    public static Map < String, Object > registerUser(String firstName, String lastName, String phone, String email,
        String type, String delimiter) {


        //---------------------------------------------------------------------
        Map < String, Object > result = new Map < String, Object > ();
        String fName = firstName.replaceAll( '\\s+', '');
        String lName = lastName.replaceAll( '\\s+', '');
        system.debug('fname ' + fName);
        String emailNew = String.isBlank(email) ? fName + phone + EMAIL_STRING : email;
        Integer randomNumber = Integer.valueof((Math.random() * 1000));
        String newUsername = fName+ '.'+ lName + randomNumber+  EMAIL_STRING + delimiter;
        String searchString = '\'%' + delimiter  + '%\'';
        

        String userQuery = 'Select Id From User Where IsActive = true';
        userQuery += ' AND FirstName= :firstName AND Username Like '+searchString ; 
        if (String.isNotBlank(email)) {
            userQuery += ' AND Email =: email';
        } else if (String.isNotBlank(phone)) {
            userQuery += ' AND Phone =: Phone';
        }
        List < User > userList = Database.query(userQuery);
        system.debug('users list ' + userList);
        if (userList.size() > 0) {
            result.put('message', 'User already exists.');
            result.put('type', 'error');
            return result;
        }

        // -------------------- Create Contact ---------------
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c, Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: type LIMIT 1
        ];

        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        con.MobilePhone = phone;
        con.Email = email;
        con.Opt_out_for_SMS_Updates__c = false;
        con.HasOptedOutOfEmail = false;
        con.Testing_Site__c = settings[0].Testing_Site_Id__c;
        con.AccountId = settings[0].Account_Id__c;
        con.RecordTypeId = recordTypeInfosMapContact.get(settings[0].Contact_RT_Developer_Name__c).RecordTypeId;
        insert con;

        // -------------------- Create User ---------------

        //email = String.isBlank(email) ? firstName + phone + EMAIL_STRING : email;
        
        String password = 'Covid11@2020';

        User u = new User();
        u.Username = newUsername; //Modified by Sajal
        u.put('Email', emailNew);
        u.ProfileId = [SELECT Id FROM Profile WHERE Name =: settings[0].Profile_Name__c].Id;
        u.FirstName = con.FirstName;
        u.LastName = con.LastName;
        u.Password__c = password;
        u.Phone = phone;
        u.ContactId = con.Id;
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.timezonesidkey = 'America/Chicago';
            if(con.FirstName.length() > 3){
                u.alias = con.FirstName.substring(0, 3);
            }else{
                 u.alias = con.FirstName;
            }
        

        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
        u.put('CommunityNickname', nickname);
        insert u;
        
        Credential__c c = new Credential__c();
        c.username__c = newUsername;
        c.Password__c = password;
        c.Contact__c = con.Id;
        c.Name = firstName;
        c.Email__c = email;
        c.Phone__c = phone;
        insert c;
        System.setPassword(u.Id, password); //For direct login without password confirmation.
        
        system.debug('username ' + newUserName);
        system.debug('password' + password);
        //commented because if we don't have email address field on contact record populated it's create 2 contact record.SELECT  FROM ${1||}
        /*String userId = Site.createExternalUser(u, con.accountId, password);
           ApexPages.PageReference lgn = Site.login(newUsername, password, '/s');
           aura.redirect(lgn); */
        result.put('message', 'Please check messages for a link to verify your contact information before continuing.');
        result.put('type', 'success');
        ApexPages.PageReference lgn = Site.login(newUsername, password, '/s/home');
        system.debug('lgn ' + lgn);
        aura.redirect(lgn); 
        result.put('redirectURL',lgn.getUrl());
        return result;
    }
    
    //Method used for Contact creation through Lobby Internal Portal
    @AuraEnabled
    public static Map < String, Object > saveRegisterContact(String firstName, String lastName, String type,
        Boolean emailOpt, Boolean smsOpt, String accountId) {
        Map < String, Object > result = new Map < String, Object > ();

       
        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        
        //Added by Anushka
        String contactType = '';
        
        if (type == 'Citizen_Consultation_Vital_Records')
            contactType = System.Label.OK_Registration_Type_VR_Citizen_Consultation;
        else if (type == 'Citizen_Lobby_Vital_Records')
            contactType = System.Label.OK_Registration_Type_VR_Citizen_Lobby;
        
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c,
            Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: contactType LIMIT 1
        ];
        con.Testing_Site__c = accountId;
        con = createContact(con, settings[0]);
        
        System.debug('emailOpt: ' + emailOpt);
        System.debug('smsOpt: ' + smsOpt);
        
        con.HasOptedOutOfEmail = emailOpt;
        con.Opt_out_for_SMS_Updates__c = smsOpt;
        con.Portal_Contact__c = true;
        // }

        insert con;

        result.put('message',
            'Citizen Contact has been successfully created. Please check “To Be Scheduled” appointment section to complete this appointment.');
        result.put('type', 'success');

        return result;

    }


    //Method used for User creation through Lobby Internal Portal
    @AuraEnabled
    public static Map < String, Object > saveUser(String firstName, String lastName, String phone, String email,
        String type, String accountId, Boolean emailOpt, Boolean smsOpt, String delimiter) {


        //---------------------------------------------------------------------
        Map < String, Object > result = new Map < String, Object > ();

        String userQuery = 'Select Id From User Where IsActive = true';
        userQuery += ' AND FirstName= :firstName';
        if (String.isNotBlank(email)) {
            userQuery += ' AND Email =: email';
        } else if (String.isNotBlank(phone)) {
            userQuery += ' AND Phone =: Phone';
        }
        List < User > userList = Database.query(userQuery);
        if (userList.size() > 0) {
            result.put('message', 'User already exists.');
            result.put('type', 'error');
            return result;
        }

        // -------------------- Create Contact ---------------
        
        contact con = new contact();
        con.FirstName = firstName;
        con.LastName = lastName;
        con.MobilePhone = phone;
        con.Email = email;
        
        con.HasOptedOutOfEmail = emailOpt;
        con.Opt_out_for_SMS_Updates__c = smsOpt;
        con.Portal_Contact__c = true;
        
        
        //added by Anushka
        String contactType = '';
        System.debug('Type'+type);
        Boolean x = (type=='Citizen_Lobby_Vital_Records');
        System.debug(x);
        if (type == 'Citizen_Consultation_Vital_Records')
            contactType = System.Label.OK_Registration_Type_VR_Citizen_Consultation;
        else if (type == 'Citizen_Lobby_Vital_Records')
            contactType = System.Label.OK_Registration_Type_VR_Citizen_Lobby;
        
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c,
            Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE MasterLabel =: contactType LIMIT 1
        ];
        con.Testing_Site__c = accountId;
                con = createContact(con, settings[0]);
        
        insert con;

        // -------------------- Create User ---------------

        email = String.isBlank(email) ? firstName + phone + EMAIL_STRING : email;
        String newUsername = email + delimiter;
        String password = 'Covid11@2020';

        User u = new User();
        u.Username = newUsername; //Modified by Sajal
        u.put('Email', email);
        u.ProfileId = [SELECT Id FROM Profile WHERE Name =: settings[0].Profile_Name__c].Id;
        u.FirstName = con.FirstName;
        u.LastName = con.LastName;
        u.Password__c = password;
        u.Phone = phone;
        u.ContactId = con.Id;
        u.emailencodingkey = 'UTF-8';
        u.languagelocalekey = 'en_US';
        u.localesidkey = 'en_US';
        u.timezonesidkey = 'America/Los_Angeles';
        u.alias = con.FirstName.substring(0, 3);

        String nickname = ((newUsername != null && newUsername.length() > 0) ? newUsername.substring(0, 1) : '');
        nickname += String.valueOf(Crypto.getRandomInteger()).substring(1, 7);
        u.put('CommunityNickname', nickname);

        insert u;        
        
        Credential__c c = new Credential__c();
        c.username__c = newUsername;
        c.Password__c = password;
        c.Contact__c = con.Id;
        c.Name = firstName;
        c.Email__c = email;
        c.Phone__c = phone;
        insert c;
        
        System.setPassword(u.Id, password); //For direct login without password confirmation.
        result.put('message', 'Citizen Contact has been successfully created. Please check “To Be Scheduled” appointment section to complete this appointment.');
        result.put('type', 'success');
        return result;
    }
    
    private static Contact createContact(Contact c, Contact_Metadata_Setting__mdt settings){
        
        //c.Testing_Site__c = settings.Testing_Site_Id__c;
        c.AccountId = settings.Account_Id__c;
        c.RecordTypeId = recordTypeInfosMapContact.get(settings.Contact_RT_Developer_Name__c).RecordTypeId;
        
        return c;
    }


    @AuraEnabled
    public static Map < String, Object > createContactFromTestingSite(Contact c, String recordType, String jsonContact , String testingSite){
      
        Map < String, Object > result = new Map < String, Object > ();
        String recordTypeId = recordTypeInfosMapContact.get(recordType).RecordTypeId;
        okWrapperClass.ContactInformation contactWrapper = new okWrapperClass.ContactInformation();
       
        if(jsonContact != null){

         contactWrapper = (okWrapperClass.ContactInformation) JSON.deserialize(jsonContact, okWrapperClass.ContactInformation.class);
         system.debug('contact wrapper ' + contactWrapper);
        }
        String email = c != null ? c.Email : contactWrapper.email;
        if(!string.isBlank(email)){
            List<Contact> contacts = [SELECT FirstName, Email FROM Contact WHERE Email =: email AND RecordTypeId =: recordTypeId];
        
        
        if(!contacts.isEmpty()){
            result.put('message', 'Contact with same details already exist');
            result.put('type', 'error');
            return result;
        }
        } 
        system.debug('record type  -- ' + recordType );
        Contact_Metadata_Setting__mdt[] settings = [SELECT MasterLabel, Account_Id__c, Contact_RT_Developer_Name__c,
            Profile_Name__c, Testing_Site_Id__c FROM Contact_Metadata_Setting__mdt WHERE Contact_RT_Developer_Name__c =: recordType LIMIT 1];
        system.debug('settings ' + settings[0]);
        if(jsonContact != null){
           
            Contact contact = new Contact(); 
            contact = contactWrapper.saveContact(contact);
            contact = contactWrapper.saveCovidResponses(contact);
            contact.RecordTypeId = recordTypeId;
            contact.AccountId = settings[0].Account_Id__c;
            system.debug('>>FINAL CONTACT>>>' + contact);
            //contact.Form_Submitted__c = c.Auto_Scheduled__c;
            insert contact;
            
            Insurance__c insurance = new Insurance__c();
            insurance = contactWrapper.saveInsuranceDetails(insurance);
            insurance.Contact__c = contact.Id;
            insert insurance;
            
            Appointment__c app = new Appointment__c();
            if(contact.Auto_Scheduled__c){
               app.Status__c = 'Scheduled';
            }else{
                app.Status__c = 'Eligible';
            }
            app.Patient__c = contact.Id;
            if(!string.isBlank(testingSite)){
                app.Testing_Site_Account__c = testingSite;
            }
            app.Direct_Contact_with_Positive_Case__c = contactWrapper.inContactWithpositiveCase ; 
            app.Live_Work_In_High_Risk_Setting__c = contactWrapper.whereYouLive;
            app.Symptomatic__c = contactWrapper.ifSymptomatic ; 
            app.Testing_Site_Type__c = contactWrapper.testingType;
            insert app;

            
        }else{
            c.RecordTypeId = recordTypeId;
            c.AccountId = settings[0].Account_Id__c;
            c.Form_Submitted__c = c.Auto_Scheduled__c;
            insert c;
        }
        system.debug('contact --- '  + c);
        result.put('message', 'Contact has been successfully created.');
        result.put('type', 'success');
        return result;
        
    }
    
    
    public class ContactWrapper{
    
        @AuraEnabled public String firstName {get;set;}
        @AuraEnabled public String lastName {get;set;}
        @AuraEnabled public Date dob {get;set;}
        @AuraEnabled public String race {get;set;}
    }

}