public without sharing class OKPC_IntakeRequestController {

    @AuraEnabled
    public static Object getLoggedInUserData(){

        DataWrapper dataWrapperObj = new DataWrapper();

        Id userId = UserInfo.getUserId();
        User loggedInUser = [SELECT Id, ContactId FROM User WHERE Id = :userId LIMIT 1];

        if(loggedInUser.ContactId != null) {
            dataWrapperObj.contactId = loggedInUser.ContactId;

            List<Appointment__c> existingAppointmentList = [SELECT Id, Appointment_Date__c,Testing_Site_Type__c, Appointment_Start_Time_v1__c, Testing_Site__c,End_Time__c,Patient__r.Testing_Site__r.Name , Status__c,Patient__r.Preferred_Date__c,Patient__r.Preferred_Time__c FROM Appointment__c WHERE Patient__c =: loggedInUser.ContactId AND (Status__c = 'Cancelled' OR Status__c = 'To Be Scheduled' OR Status__c = 'Scheduled') LIMIT 1];
            if(existingAppointmentList.size() > 0) {
                dataWrapperObj.appointmentId = existingAppointmentList[0].Id;
                dataWrapperObj.appointmentObj = new AppointmentWrapper(existingAppointmentList[0]);
            }
        }
        
        return dataWrapperObj;
    }

    @AuraEnabled
    public static Object getAppointmentData(){

        DataWrapper dataWrapperObj = new DataWrapper();

        Id userId = UserInfo.getUserId();
        User loggedInUser = [SELECT Id, ContactId FROM User WHERE Id = :userId LIMIT 1];

        if(loggedInUser.ContactId != null) {
            dataWrapperObj.contactId = loggedInUser.ContactId;

            List<Appointment__c> existingAppointmentList = [SELECT Id, Appointment_Date__c,Testing_Site_Type__c, Appointment_Start_Time_v1__c, Testing_Site__c,End_Time__c,Patient__r.Testing_Site__r.Name , Status__c,Patient__r.Preferred_Date__c,Patient__r.Preferred_Time__c FROM Appointment__c WHERE Patient__c =: loggedInUser.ContactId LIMIT 1];
            if(existingAppointmentList.size() > 0) {
                dataWrapperObj.appointmentId = existingAppointmentList[0].Id;
                dataWrapperObj.appointmentObj = new AppointmentWrapper(existingAppointmentList[0]);
            }
        }
        
        return dataWrapperObj;
    }
    
      @AuraEnabled
    public static Object getAppointmentDataForCovidReschedule(String appId){

        DataWrapper dataWrapperObj = new DataWrapper();

        Id userId = UserInfo.getUserId();
        User loggedInUser = [SELECT Id, ContactId FROM User WHERE Id = :userId LIMIT 1];

        if(loggedInUser.ContactId != null) {
            dataWrapperObj.contactId = loggedInUser.ContactId;

            List<Appointment__c> existingAppointmentList = [SELECT Id, Appointment_Date__c,Testing_Site_Type__c, Appointment_Start_Time_v1__c, Testing_Site__c,End_Time__c,Patient__r.Testing_Site__r.Name , Status__c,Patient__r.Preferred_Date__c,Patient__r.Preferred_Time__c FROM Appointment__c WHERE Id =:appId LIMIT 1];
            if(existingAppointmentList.size() > 0) {
                dataWrapperObj.appointmentId = existingAppointmentList[0].Id;
                dataWrapperObj.appointmentObj = new AppointmentWrapper(existingAppointmentList[0]);
            }
        }
        
        return dataWrapperObj;
    }

    
    public class DataWrapper {
        @AuraEnabled public String contactId;
        @AuraEnabled public String appointmentId;
        @AuraEnabled public AppointmentWrapper appointmentObj;

        public DataWrapper() {
            this.contactId = '';
            this.appointmentId = '';
        }
    }

    public class AppointmentWrapper {
        @AuraEnabled public String appointmentId;
        @AuraEnabled public Date appointmentDate;
        @AuraEnabled public String startTime;
        @AuraEnabled public String endTime;
        @AuraEnabled public String siteName;
        @AuraEnabled public String status;
        @AuraEnabled public Date preferredDate;
        @AuraEnabled public Time preferredTime;
        @AuraEnabled public String preferredSite;
        @AuraEnabled public String preferredSiteId;
        @AuraEnabled Public String appTestingType;

        public AppointmentWrapper(Appointment__c appObj) {
            this.appointmentId = appObj.Id;
            this.appointmentDate = appObj.Appointment_Date__c;
            this.startTime = string.valueOf(appObj.Appointment_Start_Time_v1__c);
            this.endTime = appObj.End_Time__c;
            this.siteName = appObj.Patient__r.Testing_Site__r.Name;
            this.status = appObj.Status__c;
            this.preferredDate = appObj.Patient__r.Preferred_Date__c;
            this.preferredTime = appObj.Patient__r.Preferred_Time__c;
            this.preferredSite = appObj.Patient__r.Testing_Site__r.Name;
            this.preferredSite = appObj.Patient__r.Testing_Site__c;
            this.appTestingType = appObj.Testing_Site_Type__c;
        }
    }
    
}