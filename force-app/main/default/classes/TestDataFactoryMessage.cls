public class TestDataFactoryMessage {
    public static Messages__c createMessage(Boolean isInsert,string message ,string phoneNumber){
        Messages__c insertMessage = new Messages__c();
        insertMessage.From__c = phoneNumber;
        insertMessage.Messages__c = message;
        if(isInsert){
            insert insertMessage;
            return insertMessage;
        }
        return insertMessage;
    }
}