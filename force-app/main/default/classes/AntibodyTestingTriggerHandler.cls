public without sharing class AntibodyTestingTriggerHandler {

    public static void updateAfterMulesoftDataReceived(List<Antibody_Testing__c>newList , Map<id,Antibody_Testing__c>oldMap){
        Set<Id> appIds = new Set<Id>();
        for(Antibody_Testing__c anti : newList){
            if(anti.External_Unique_Id__c != null && anti.Results__c != oldMap.get(anti.id).Results__c){
                anti.Date_of_Lab_Test_Completion__c = system.today();
                appIds.add(anti.Appointment__c);
            }
        }
        
        appointmentsToUpdate(appIds);
    }
    
    public static void appointmentsToUpdate(Set<Id> appToUpdate){
        List<appointment__c> updateApp = new List<appointment__c>();
        for(appointment__c app : [SELECT Id,Name,Status__c FROM appointment__c WHERE Id In: appToUpdate AND Status__c != 'Completed']){
            app.Status__c = 'Completed';
            updateApp.add(app);
        }
        if(!updateApp.isEmpty()){
            update updateApp;
        }
    }
    
    public static void sendResultUpdateSMS(List<Antibody_Testing__c>newList , Map<id,Antibody_Testing__c>oldMap){
        Set<String> results = new Set<String>();
        List<Antibody_Testing__c> testings = [SELECT Id, Notify_User_via_SMS__c, Results__c, Patient__c, Patient__r.Community_Link__c, 
        Patient__r.Account.Communication__c, Patient__r.RecordType.DeveloperName, Patient__r.MobilePhone FROM Antibody_Testing__c WHERE Id IN: newList];
        List<Id> testingIds = new List<Id>();
        List<Id> faultyTestingIds = new List<Id>();
        
        Schema.DescribeSObjectResult r = Schema.getGlobalDescribe().get('Antibody_Testing__c').getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get('Results__c').getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if( pickListVal.getValue() != 'Indeterminate Result')
                results.add(pickListVal.getValue());
        } 
        
           
        for(Antibody_Testing__c testing : testings){
            if(testing.Results__c != null && testing.Results__c != oldMap.get(testing.Id).Results__c && testing.Patient__r.RecordType.DeveloperName == 'Citizen_Covid'){
               if(results.contains(testing.Results__c))
                   testingIds.add(testing.Patient__c);
               else
                   faultyTestingIds.add(testing.Patient__c);              
            }
        }
        
         String query = 'SELECT Id, MobilePhone,Community_Link__c,Name,PHOCIS_Response_Count__c,Patient_Id__c  FROM Contact ';
         system.debug('faultyTestingIds ' + faultyTestingIds);
          system.debug('testingIds ' + testingIds);
        if(!testingIds.isEmpty()){
          system.debug('testingIds ***' + testingIds);
            AntibodyTestingSendConsentBatch smsBatch = new AntibodyTestingSendConsentBatch(query, testingIds, 'resultSMS');
            Database.executeBatch(smsBatch, 1);
        }
        if(!faultyTestingIds.isEmpty()){          
            AntibodyTestingSendConsentBatch smsBatch = new AntibodyTestingSendConsentBatch(query, faultyTestingIds, 'faultyResultSMS');
            Database.executeBatch(smsBatch, 1);
        }      
    }
    
    public static void sendConsentMessage(List<Antibody_Testing__c>antiBodyTestingList){

   /*     List<Id> contactIdList = new List<Id>();
        List<Id> contactIdWithResultsUpdated = new List<Id>();
        Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        Id contactCitizen_AntiBodyTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
        Id testing_AntiBodyTypeId = Schema.SObjectType.Antibody_Testing__c.getRecordTypeInfosByDeveloperName().get('COVID_Testing').getRecordTypeId();
        
        for(Antibody_Testing__c testing : antiBodyTestingList){
                if(testing.Patient__r.MobilePhone != null && testing.Patient__r.Notify_Patients__c == true && testing.Patient__r.Opt_out_for_SMS_Updates__c == false &&
                        (testing.patient__r.Account.Communication__c=='COVID Results') && (testing.patient__r.RecordTypeId==contactCitizen_COVIDTypeId)){
                    contactIdList.add(testing.Patient__r.Id);
                }
                
                if(testing.Patient__r.MobilePhone != null && testing.Patient__r.Notify_Patients__c == true && testing.Patient__r.Opt_out_for_SMS_Updates__c == false &&
                        (testing.patient__r.Account.Communication__c=='OKU COVID') && (testing.patient__r.RecordTypeId==contactCitizen_COVIDTypeId && testing.RecordTypeId==testing_AntiBodyTypeId)){
                    System.debug('contactIdWithResultsUpdated----AntibodyTestingTriggerHandler inside for send sms');
                    contactIdWithResultsUpdated.add(testing.Patient__r.Id);
                }
                System.debug('contactIdWithResultsUpdated----AntibodyTestingTriggerHandler 1');
                System.debug('testing_AntiBodyTypeId>>'+testing_AntiBodyTypeId);

        }
        if(contactIdList.size()>0){
            List<Antibody_Testing__c> listOfTesting = new List<Antibody_Testing__c>();
            for(Antibody_Testing__c testingRecord : [select Patient__c from Antibody_Testing__c where Patient__c  in:contactIdList]){
                testingRecord.Consent_Content__c='Consent Sent';
                listOfTesting.add(testingRecord);
            }
            if(!listOfTesting.isEmpty()){
                update listOfTesting;
            }

            String query = 'SELECT Id, MobilePhone,Community_Link__c,Name,MobileTrimmer__c,PHOCIS_Response_Count__c,Patient_Id__c  FROM Contact ';
            AntibodyTestingSendConsentBatch smsBatch = new AntibodyTestingSendConsentBatch(query, contactIdList, 'AntiBody');
            Database.executeBatch(smsBatch, 1);
        }
        if(contactIdWithResultsUpdated.size()>0){
            String query = 'SELECT Id, MobilePhone,Community_Link__c,Name,PHOCIS_Response_Count__c,Patient_Id__c  FROM Contact ';
            AntibodyTestingSendConsentBatch sendResult = new AntibodyTestingSendConsentBatch(query, contactIdWithResultsUpdated, 'SendResult');
            Database.executeBatch(sendResult, 1);

        }*/
    }
}