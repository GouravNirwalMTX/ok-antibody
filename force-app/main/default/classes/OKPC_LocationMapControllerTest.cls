@isTest
public class OKPC_LocationMapControllerTest {
		
    @isTest static void test1(){
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        Account acc = new Account(
            RecordTypeId = recordTypeID,
            Name = 'test',
            BillingStreet = 'test',
            BillingCity = 'test',
            BillingState = 'test',
            BillingCountry = 'test',
            BillingPostalCode = 'test',
        	Type = 'Testing Site',
            BillingLatitude = 0.12,
            BillingLongitude = 0.13
        );
        insert acc;
        List<OKPC_LocationMapController.MapMarker> mapList = OKPC_LocationMapController.getTestingSites('Covid Testing');
        OKPC_LocationMapController.MapMarker mapValue = OKPC_LocationMapController.getAccountdetails(acc.Id);
        System.assertEquals(mapValue.value, acc.Id);
     //   OKPC_LocationMapController.getLobbys();
    }
    @isTest static void test2(){
        String recordTypeID = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Vital_Records').RecordTypeId;
        Account acc = new Account(
            RecordTypeId = recordTypeID,
            Name = 'test',
            BillingStreet = 'test',
            BillingCity = 'test',
            BillingState = 'test',
            BillingCountry = 'test',
            BillingPostalCode = 'test',
        	Type = 'Testing Site',
            BillingLatitude = 0.12,
            BillingLongitude = 0.13
        );
        insert acc;
       // List<OKPC_LocationMapController.MapMarker> mapList = OKPC_LocationMapController.getTestingSites();
     //   OKPC_LocationMapController.MapMarker mapValue = OKPC_LocationMapController.getAccountdetails(acc.Id);
        List<OKPC_LocationMapController.MapMarker> objList = OKPC_LocationMapController.getLobbys();
        System.assertEquals(objList.size() > 0 , true);
    }
    
   
}