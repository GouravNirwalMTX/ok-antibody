@isTest
public class DC_ScheduleAppointmentTest {

    @TestSetup
    static void makeData(){
            
        String testingCenter = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Test_Center').RecordTypeId;
        String labRecordId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Laboratory').RecordTypeId;
        String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Antibody_Testing_Site').RecordTypeId;
        
        Account createAccountTestSite = TestDataFactoryAccount.createAccount('TestSite', testingCenter, false);
        createAccountTestSite.BillingCity = 'testCity';
        createAccountTestSite.BillingCountry = 'testCountry';
        createAccountTestSite.BillingState = 'testState';
        createAccountTestSite.BillingStreet = 'testStreet';
        createAccountTestSite.BillingPostalCode = '12345';
        createAccountTestSite.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        createAccountTestSite.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert createAccountTestSite;

        Account labAccount = TestDataFactoryAccount.createAccount('testLab', labRecordId, true);

        Contact con = new Contact (
            AccountId = createAccountTestSite.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            Testing_Site__c = createAccountTestSite.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0)
        );
        insert con;

        Appointment_Slot__c createSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createSlot.Testing_Site_type__c = 'Rapid Testing';
        createSlot.Record_type__c = 'Citizen'  ;
        createSlot.Appointment_Count__c = 0;
        insert createSlot;
        Appointment__c  createAppointment = TestDataFactoryAppointment.createAppointment(false);
        createAppointment.Patient__c = con.Id;
        createAppointment.Lab_Center__c = createAccountTestSite.id;
        createAppointment.Appointment_Slot__c = createSlot.id;
        createAppointment.Testing_Site_Type__c = 'rapid Testing';
        createAppointment.Status__c = 'Cancelled';

        insert createAppointment;
        
        Appointment_Slot__c createPCRSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createPCRSlot.Testing_Site_type__c = 'PCR Testing';
        createPCRSlot.Record_type__c = 'Citizen'  ;
        createPCRSlot.Appointment_Count__c = 0;
        insert createPCRSlot;
        Appointment__c  createPCRAppointment = TestDataFactoryAppointment.createAppointment(false);
        createPCRAppointment.Patient__c = con.Id;
        createPCRAppointment.Lab_Center__c = createAccountTestSite.id;
        createPCRAppointment.Appointment_Slot__c = createPCRSlot.id;
        createPCRAppointment.Testing_Site_Type__c = 'PCR Testing';
        createPCRAppointment.Status__c = 'Scheduled';

        insert createPCRAppointment;
        
        Appointment_Slot__c createPCRSlot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createPCRSlot2.Testing_Site_type__c = 'PCR Testing';
        createPCRSlot2.Record_type__c = 'Citizen'  ;
        createPCRSlot2.Appointment_Count__c = 0;
        insert createPCRSlot2;
        
        
        
        Appointment_Slot__c createTemplateSlot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, createAccountTestSite.Id);
        createTemplateSlot.Day_of_the_Week__c = 'Tue';
        createTemplateSlot.Number_of_Available_Slots__c = 2;
        createTemplateSlot.Template__c = true;
        createTemplateSlot.Testing_Site_type__c = 'Rapid Testing';
        insert createTemplateSlot ;
        

    }

    public static testMethod void getSiteAccountsTest(){
        List<Account> accountList = DC_ScheduleAppointment.getSiteAccounts();
        //System.assert(accountList.size() > 0 );
    }
    public static testMethod void retrieveLabTest(){
        List<DC_ScheduleAppointment.OptionWrapper> wrapperList = DC_ScheduleAppointment.retrieveLab();
        System.assert(wrapperList.size() > 0 );
    }
    
    
    public static testMethod void createAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Testing_Site_Account__c, Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];

        String appointmentId = String.valueOf(appList[0].Id);
        okWrapperClass.appointmentDetails appJSON = new okWrapperClass.appointmentDetails();
        appJSON.contactId = contactList[0].Id;
        appJSON.ifSymptomatic = 'Yes';
        appJSON.testingType = 'Rapid Testing';        
        Appointment_Slot__c slot = [SELECt Id from Appointment_Slot__c LIMIT 1];
        Map<String,Object> appMap = DC_ScheduleAppointment.createAppointment('Rapid Testing', JSON.serialize(appJSON), slot.Id, appList[0].Testing_Site_Account__c);
        Map<String,Object> appMapNullAppJSOn = DC_ScheduleAppointment.createAppointment('Rapid Testing',null, slot.Id, appList[0].Testing_Site_Account__c);
    }
    
    public static testMethod void getAppointmentDetailTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];

        String appointmentId = String.valueOf(appList[0].Id);

        Map<String,Object> appMap = DC_ScheduleAppointment.getAppointmentDetail(appointmentId);
        
    }
    public static testMethod void cancelAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];

        Appointment_Slot__c createSlot1 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accountSiteList[0].Id);
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = contactList[0].Id;
        createAppointment1.Lab_Center__c = accountSiteList[0].id;
        createAppointment1.Appointment_Slot__c = createSlot1.id;
        
        insert createAppointment1;
        
        String appointmentId = String.valueOf(createAppointment1.Id);
         List<String> appointmentIds = new List<String>{appointmentId};
        String cancelResult= DC_ScheduleAppointment.cancelAppointment(appointmentIds,'test Reason');
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.stopTest();

    }

    public static testMethod void cancelAppointmentTest_01(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];

        Appointment_Slot__c createSlot1 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accountSiteList[0].Id);
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = contactList[0].Id;
        createAppointment1.Lab_Center__c = accountSiteList[0].id;
        createAppointment1.Appointment_Slot__c = createSlot1.id;
        createAppointment1.Status__c = 'Cancelled';
        insert createAppointment1;
        
        String appointmentId = String.valueOf(createAppointment1.Id);
         List<String> appointmentIds = new List<String>{appointmentId};
        String cancelResult= DC_ScheduleAppointment.cancelAppointment(appointmentIds,'test Reason');
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.stopTest();

    }

    public static testMethod void cancelAppointmentTest_02(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];

        Appointment_Slot__c createSlot1 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accountSiteList[0].Id);
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = contactList[0].Id;
        createAppointment1.Lab_Center__c = accountSiteList[0].id;
        createAppointment1.Appointment_Slot__c = createSlot1.id;
        createAppointment1.Status__c = 'Closed';

        insert createAppointment1;
        
        String appointmentId = String.valueOf(createAppointment1.Id);
         List<String> appointmentIds = new List<String>{appointmentId};
        String cancelResult= DC_ScheduleAppointment.cancelAppointment(appointmentIds,'test Reason');
        Test.startTest();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.stopTest();

    }

    public static testMethod void updateAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];

        List<Account> accountLabList = [Select Id,Name FROM Account WHERE Name = 'testLab'];
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];

        Appointment_Slot__c createSlot1 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accountSiteList[0].Id);
        Appointment_Slot__c createSlot2 = TestDataFactoryAppointmentSlot.createAppointmentSlot(true, accountSiteList[0].Id);
        Appointment_Slot__c createSlot3 = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id);
        createSlot3.Testing_Site_Type__c = 'Rapid Testing';
        insert createSlot3;
        Appointment__c  createAppointment1 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment1.Patient__c = contactList[0].Id;
        createAppointment1.Lab_Center__c = accountSiteList[0].id;
        createAppointment1.Appointment_Slot__c = createSlot1.id;        
        insert createAppointment1;
        
         Appointment__c  createAppointment2 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment2.Patient__c = contactList[0].Id;
        createAppointment2.Lab_Center__c = accountSiteList[0].id;
        createAppointment2.Appointment_Slot__c = createSlot2.id;        
        insert createAppointment2;
        
        Appointment__c  createAppointment3 = TestDataFactoryAppointment.createAppointment(false);
        createAppointment3.Patient__c = contactList[0].Id;
        createAppointment3.Lab_Center__c = accountSiteList[0].id;
        createAppointment3.Appointment_Slot__c = createSlot3.id;        
        insert createAppointment3;

        String appointmentId = String.valueOf(createAppointment1.Id);
        String labId = String.valueOf(accountLabList[0].Id);
        STring TestId = String.valueOf(accountSiteList[0].Id);
        String slotId = String.valueOf(createAppointment1.Appointment_Slot__c);
        
        Map<String, Object>  getAppointment = DC_ScheduleAppointment.getAppointmentData(String.valueOf(createAppointment3.Id), 'Rapid Testing');
        Map<String, Object>  updateCovidAppointment = DC_ScheduleAppointment.updateCovidAppointment(TestId,String.valueOf(createAppointment3.Id),slotId,'Reason','Rapid Testing');
        Map<String, Object>  addAppointmnet= DC_ScheduleAppointment.addNewAppointment(TestId,String.valueOf(createAppointment2.Id),slotId,'Rapid Testing');
        Map<String, Object>  updateResult= DC_ScheduleAppointment.updateAppointment(TestId,appointmentId,slotId,'Reason', 'Rapid Testing');
        
       
        // DC_ScheduleAppointment.integrateWithMulesoft(new List<Id>{createAppointment3.id});
       

    }

    public static testMethod void getAccountSlotsTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
             
        List<Account> accountSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        Appointment_Slot__c slot = TestDataFactoryAppointmentSlot.createAppointmentSlot(false, accountSiteList[0].Id)  ;
        slot.Date__c = Date.today().addDays(3);
        slot.Account__c = accountSiteList[0].Id;
        slot.Start_Time__c = Time.newInstance(14, 0, 0, 0);
        slot.End_Time__c = Time.newInstance(15, 0, 0, 0);
        slot.Unscheduled__c = true;
        slot.Appointment_Count__c = 0; 
        slot.Testing_Site_Type__c = 'Rapid Testing';      
        
        insert slot;
        
        String accountId = String.valueOf(accountSiteList[0].Id);
        Map<String,Object> appMap = DC_ScheduleAppointment.getAccountSlots(accountId, 'Rapid Testing');
        Map<String,Object> appTemplateMap = DC_ScheduleAppointment.getTemplateAppointmentSlots(accountId);

    }


    public static testMethod void completeAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name FROM Appointment__c WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        List<Account> accountList = [Select Id,Name FROM Account WHERE Name = 'testLab'];
        String appointmentId = String.valueOf(appList[0].Id);
        String labId = String.valueOf(accountList[0].Id);
        Antibody_Testing__c testing = TestDataFactory.createTesting('TestInsert',false);
         testing.Results__c = 'Positive';
         testing.Patient__c = contactList[0].Id;
         testing.Appointment__c = appointmentId;
        insert testing;
       
        String result = DC_ScheduleAppointment.completeAppointment(appointmentId, labId, '1', 'Positive', 'TaqPath COVID-19 rRT-PCR Assay', 'Notes');
    }
    
    
    public static testMethod void closeAppointmentTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name, Appointment_slot__c FROM Appointment__c WHERE Testing_Site_Type__c = 'Rapid Testing' AND Patient__c =:contactList[0].Id LIMIT 1 ];
        List<Account> accountList = [Select Id,Name FROM Account WHERE Name = 'testLab'];
        List<Account> accountTestSiteList = [Select Id,Name FROM Account WHERE Name = 'TestSite'];
        List<Appointment_Slot__c> appointmentSlots = [Select Id,Name FROM Appointment_Slot__c WHERE Account__c =:accountTestSiteList[0].Id];

        Antibody_Testing__c antibodyTesting = TestDataFactoryAntiBody.createAppointment(true, accountTestSiteList[0].Id, appList[0].Id, appointmentSlots[0].Id, contactList[0].Id);
        system.debug('test method -- 274 '+ appList[0]);
        
        //Appointment_Slot__c slot = [Select Id,Name, Start_Time__c, End_Time__c, Date__c, Account__c, Testing_Site_Type__c, Record_Type__c, Appointment_Count__c FROM Appointment_Slot__c where Id =: appList[0].Appointment_Slot__C];
        //system.debug('app slot ^^^^ ' + slot);
        //Appointment_Slot__c slot2 = [Select Id,Name, Start_Time__c, End_Time__c, Date__c, Account__c, Testing_Site_Type__c, Record_Type__c, Appointment_Count__c FROM Appointment_Slot__c where Testing_Site_Type__c =: 'PCR Testing'];
        //system.debug('app PCR slot ^^^^ ' + slot2);
        List<String> appointmentIdList = new List<String>();
        appointmentIdList.add(appList[0].Id);
        String labId = String.valueOf(accountList[0].Id);
        DC_ScheduleAppointment.AppointmentWrapper app = new DC_ScheduleAppointment.AppointmentWrapper();
        app.id = appList[0].Id;
        
        app.testingSiteType = 'PCR Testing';
        app.source = 'Swab, nasopharyngeal';
        app.labCenter = labId;
        app.sampleCollectedBy = 'Test';
        app.typeChanged = true;

        DC_ScheduleAppointment.updateAppointmentRec(JSON.serialize(app));
        String result = DC_ScheduleAppointment.closeAppointment(appointmentIdList, labId,'1234');
    }
    
    public static testMethod void completeVitalAppointmentTest(){
         List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];

        String appointmentId = String.valueOf(appList[0].Id);

        String completeVitalAppoResult = DC_ScheduleAppointment.completeVitalAppointment(appointmentId);
        System.assertEquals('success', completeVitalAppoResult);
        
    }
     public static testMethod void appointmentnegativeTest(){
        List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
       
        String appointmentId = String.valueOf(contactList[0].Id);
        string labId='test';
        String slotId='test';
        Map<String, Object>  updateResult= DC_ScheduleAppointment.updateAppointment(labId,appointmentId,slotId, 'Reason', 'Rapid Testing');
        system.assert(!updateResult.isEmpty());
       
        
    }
    public static testMethod void inProgressVitalAppointmentTest(){
         List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];

        String appointmentId = String.valueOf(appList[0].Id);

        String inProgressVitalAppoResult = DC_ScheduleAppointment.inProgressVitalAppointment(appointmentId, 'already showed up');
        System.assertEquals('success', inProgressVitalAppoResult);
        
    }
    public static testMethod void cancelVitalAppointmentTest(){
         List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        
        
        String appointmentId = String.valueOf(appList[0].Id);

        List<String> appIds = new List<String>();
        appIds.add(appointmentId);
        String cancelVitalAppointmentResult = DC_ScheduleAppointment.cancelVitalAppointment(appIds,'Busy schedule');
        
        System.assertEquals('This appointment is already cancelled.', cancelVitalAppointmentResult);
        
    }
    public static testMethod void cancelVitalAppointmentInProgressTest(){
         List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Time__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        
        appList[0].Status__c='In Progress';
        update appList[0];
        String appointmentId = String.valueOf(appList[0].Id);
        List<String> appIds = new List<String>();
        appIds.add(appointmentId);
        String cancelVitalAppointmentResult = DC_ScheduleAppointment.cancelVitalAppointment(appIds,'Busy schedule');
         System.assertEquals('success', cancelVitalAppointmentResult);
         String inProgressVitalAppointmentResult = DC_ScheduleAppointment.inProgressVitalAppointment(appointmentId, 'Test');
        // System.assertEquals('Appointment is already in In Progress status', inProgressVitalAppointmentResult);
        
        
    }
     public static testMethod void cancelVitalAppointmentClosedTest(){
         List<Contact> contactList = [Select Id , Name FROM Contact WHERE lastName = 'portalTestUserv1' LIMIT 1];
        List<Appointment__c> appList = [Select Id , Name , Patient__c,Preferred_Date__c, Appointment_Slot__c,Status__c 
                                         FROM Appointment__c 
                                         WHERE Patient__c =:contactList[0].Id LIMIT 1 ];
        
        appList[0].Status__c='Closed';
        update appList[0];
        String appointmentId = String.valueOf(appList[0].Id);

        List<String> appIds = new List<String>();
        appIds.add(appointmentId);
        String cancelVitalAppointmentResult = DC_ScheduleAppointment.cancelVitalAppointment(appIds,'Busy schedule');
         System.assertEquals('You are not allowed to cancel this appointment.', cancelVitalAppointmentResult);
        
        
    }
    
}