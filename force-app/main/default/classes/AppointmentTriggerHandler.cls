public without sharing class AppointmentTriggerHandler {
    public static void beforeInsert(List<Appointment__c> appointments){
        updateStartDateStartTime(appointments, null);
        
    }
    public static void afterInsert(List<Appointment__c> appointments) {
        sendSMS(appointments, null);
        filterAppointmentSlots(appointments, null);
        createAntibodyTestings(appointments, null);
        updatePendingConfirmationOnContact(appointments,null);
    }
    public static void beforeUpdate(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        updateLabOnAntibodyTesting(appointments, appointmentsMap);
        updateStartDateStartTime(appointments, appointmentsMap);
    }
    
    public static void afterUpdate(List<Appointment__c> appointments,Map<Id,Appointment__c> appointmentsMap) {
        filterAppointmentSlots(appointments, appointmentsMap);
        sendSMS(appointments, appointmentsMap);
        updatePendingConfirmationOnContact(appointments,appointmentsMap);
        createAntibodyTestings(appointments, appointmentsMap);
    }
    public static void afterDelete(List<Appointment__c> appointments) {
        filterAppointmentSlots(appointments, null);
    }
    public static void afterUndelete(List<Appointment__c> appointments) {
        filterAppointmentSlots(appointments, null);
    }
    
    public static void sendSMS(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        Id contactCitizen_COVIDTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen_COVID').getRecordTypeId();
        Id contactCitizen_AntiBodyTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Citizen').getRecordTypeId();
        Set<Id> appointmentIdSet = new Set<Id>();
        Set<Id> canceledAppointmentIdSet = new Set<Id>();
        List<Id> contactIdList = new List<Id>();
        List<Id> canceledContactIdList = new List<Id>();
        Map<Id,Antibody_Testing__c> testingRecordsMap = new Map<Id,Antibody_Testing__c>();
        for(Appointment__c appointment: appointments) {
            //Appoint status Scheduled?? Send Testing site loc
            if(((appointmentsMap != null && appointmentsMap.get(appointment.Id).Status__c!=appointment.Status__c) || appointmentsMap == null) && 
                appointment.Status__c=='Scheduled') {
                   appointmentIdSet.add(appointment.id);
               } else if(appointmentsMap != null && appointmentsMap.get(appointment.Id).Status__c!=appointment.Status__c && appointment.Status__c=='Cancelled') {
                   canceledAppointmentIdSet.add(appointment.id);
               }
        }
        if(appointmentIdSet!=null) {
            for(Appointment__c app : [SELECT Id, Patient__c,Patient__r.RecordTypeId, Patient__r.MobilePhone, Patient__r.Opt_out_for_SMS_Updates__c FROM Appointment__c
                                      WHERE Id IN: appointmentIdSet AND Patient__r.Opt_out_for_SMS_Updates__c = false AND
                                      Patient__r.MobilePhone != null] ) {
                                          contactIdList.add(app.Patient__c);
                                      }
        }
        System.debug('contactIdList just before>>'+contactIdList);
        if(contactIdList.size()>0){
            String query = 'SELECT Id,Account.communication__c, MobilePhone,Testing_Site__c,RecordTypeId, RecordType.DeveloperName, Community_Link__c FROM Contact ';
            SendSMSBatch smsBatch = new SendSMSBatch(query, contactIdList, 'appointment');
             System.debug('contactIdList just after>>'+smsBatch);
            Database.executeBatch(smsBatch, 1);
        }
        
        if(canceledAppointmentIdSet!=null) {
            for(Appointment__c app : [SELECT Id, Patient__c,Patient__r.RecordTypeId, Patient__r.MobilePhone, Patient__r.Opt_out_for_SMS_Updates__c FROM Appointment__c
                                      WHERE Id IN: canceledAppointmentIdSet AND Patient__r.Opt_out_for_SMS_Updates__c = false AND
                                      Patient__r.MobilePhone != null ] ) {
                                          canceledContactIdList.add(app.Patient__c);
                                      }
        }
        System.debug('contactIdList just before>>'+contactIdList);
        if(canceledContactIdList.size()>0){
            System.debug('Sending Testing location YNR for contactCitizen_COVIDTypeId'+canceledContactIdList);
            //Sending Testing location for contactCitizen_COVIDTypeId
            String query = 'SELECT Id,Account.communication__c, RecordTypeId, RecordType.DeveloperName, MobilePhone,Testing_Site__c, Community_Link__c FROM Contact ';
            SendSMSBatch smsBatch = new SendSMSBatch(query, canceledContactIdList, 'cancel');
            Database.executeBatch(smsBatch, 1);
        }
        
        
    }
    
    public static void updateLabOnAntibodyTesting(List<Appointment__c> appointmentList, Map<Id,Appointment__c> oldMap) {
        Map<Id, Id> mapOfAppointmentToLabId = new Map<Id, Id>();
        
        for(Appointment__c appointment : appointmentList) {
           if(appointment.Lab_Center__c !=  oldMap.get(appointment.Id).Lab_Center__c){
              mapOfAppointmentToLabId.put(appointment.Id, appointment.Lab_Center__c);
           }
        }
        
        List<Antibody_Testing__c> antibodyTestingList = [SELECT Laboratory__c , Appointment__c FROM Antibody_Testing__c 
        WHERE Appointment__c IN: mapOfAppointmentToLabId.keySet()];
        
        for(Antibody_Testing__c testing : antibodyTestingList){
           testing.Laboratory__c = mapOfAppointmentToLabId.get(testing.Appointment__c);
        }
        
        if(!antibodyTestingList.isEmpty())
            update antibodyTestingList;
       
    }
    public static void updatePendingConfirmationOnContact(List<Appointment__c> appointmentList,Map<Id,Appointment__c> oldMap){
        Set<Id> contactIds = new Set<Id>();
        List<Contact> listOfContacts = new List<Contact>();
        Map<Id,String> contactIdTestingType = new Map<Id,String>();
        for(Appointment__c appointment : appointmentList) {
            if(oldMap == null || (appointment.Status__c == 'Scheduled' && appointment.Status__c!=oldMap.get(appointment.Id).status__c)) {
                contactIds.add(appointment.Patient__c);
                contactIdTestingType.put(appointment.Patient__c,appointment.Testing_Site_Type__c);
                
            }
        }
        if(contactIds.size()>0){
            for(Contact contactRecord : [select id,pending_confirmation__c,Testing_Type__c from contact where id in: contactIds]){
                contactRecord.pending_confirmation__c = 'Pending'; 
                if(contactIdTestingType.containsKey(contactRecord.Id)){
                   contactRecord.Testing_Type__c = contactIdTestingType.get(contactRecord.Id);
                }
                listOfContacts.add(contactRecord);
            }
        }
        if(!listOfContacts.isEmpty()){
            update listOfContacts;
        }
        
    }
    public static void createAntibodyTestings(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        Set<Id> appointmentSlotIds = new Set<Id>();
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        Id covidRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','COVID_Testing');
        List<Antibody_Testing__c> testingRecordsList = new List<Antibody_Testing__c>();
        for(Appointment__c appointment: [SELECT Id, Status__c,Testing_Site_Type__c,Testing_Site_Account__c, Patient__c FROM Appointment__c WHERE Id IN: appointments AND Id NOT IN (SELECT Appointment__c FROM Antibody_Testing__c) ]) {
            if(((appointmentsMap != null && appointment.Status__c != appointmentsMap.get(appointment.Id).Status__c) || appointmentsMap == null) && appointment.Status__c == 'Scheduled') {
                Antibody_Testing__c antiBodyTesting = new Antibody_Testing__c();
                antiBodyTesting.Appointment__c = appointment.Id;
                //antiBodyTesting.Appointment_Slot__c = appointment.Appointment_Slot__c;
                antiBodyTesting.Patient__c = appointment.Patient__c;
                antiBodyTesting.Testing_Site__c = appointment.Testing_Site_Account__c;
                if(appointment.Testing_Site_Type__c == 'Antibody Testing'){
                    antiBodyTesting.RecordTypeId = antibodyRecordTypeId;
                }else{
                    antiBodyTesting.RecordTypeId = covidRecordTypeId;
                }
                 testingRecordsList.add(antiBodyTesting);
            }
           
        }
        
        if(!testingRecordsList.isEmpty()) {
            insert testingRecordsList;
        }
    }
    
    public static void filterAppointmentSlots(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap) {
        Set<Id> appointmentSlotIds = new Set<Id>();
        List<Id> contactIdList = new List<Id>();
        for(Appointment__c appointment: appointments) {
            if(appointment.Appointment_slot__c != null && appointmentsMap == null) {
                appointmentSlotIds.add(appointment.Appointment_slot__c);
            } else if(appointmentsMap != null && appointment.Appointment_slot__c != appointmentsMap.get(appointment.Id).Appointment_slot__c) {
                if(appointment.Appointment_slot__c != null){
                    appointmentSlotIds.add(appointment.Appointment_slot__c);
                }  
            }
            if(appointmentsMap != null && appointmentsMap.get(appointment.Id).Appointment_slot__c != null){
                appointmentSlotIds.add(appointmentsMap.get(appointment.Id).Appointment_slot__c);
            }
        }
        if(appointmentSlotIds.size() > 0 ) {
            updateCount(appointmentSlotIds);
        }
    }
    
    public static void updateCount(Set<Id> appointmentSlotIds) {
        List<String> status = new List<String>{'Scheduled', 'In Progress', 'Completed'}; 
        List<Appointment_slot__c> appointmentSlots = new List<Appointment_slot__c>();
        for(Appointment_slot__c appointmentSlot : [SELECT Id,Appointment_Count__c,(SELECT Id FROM Appointments__r WHERE Status__c IN: status) FROM Appointment_slot__c WHERE Id IN: appointmentSlotIds]) {
            appointmentSlot.Appointment_Count__c = appointmentSlot.Appointments__r.size();
            appointmentSlots.add(appointmentSlot);
        }
        update appointmentSlots;
    }
    
    public static void updateStartDateStartTime(List<Appointment__c> appointments, Map<Id,Appointment__c> appointmentsMap){
        Set<Id> appointmentSlotIds = new Set<Id>();
        List<Id> contactIdList = new List<Id>();
        List<Appointment__c> appList = new List<Appointment__c>();
        for(Appointment__c appointment: appointments) {
            if((appointmentsMap == null) || (appointmentsMap != null && 
                                             appointmentsMap.get(appointment.Id).Appointment_slot__c != appointment.Appointment_slot__c)){
                                                 appointmentSlotIds.add(appointment.Appointment_slot__c); 
                                                 appList.add(appointment);                           
                                             }
            contactIdList.add(appointment.Patient__c);
        }
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, RecordType.Name FROM Contact WHERE Id IN: contactIdList]);
        Map<Id,Appointment_slot__c> appointmentSlotMap = new Map<Id,Appointment_slot__c>([SELECT Id,Start_Time__c,Date__c FROM Appointment_slot__c WHERE Id IN :appointmentSlotIds]);
        
        for(Appointment__c app : appList){
            if(contactMap.containsKey(app.Patient__c))
                app.Record_Type__c = contactMap.get(app.Patient__c).RecordType.Name;
                
            if(appointmentSlotMap.containsKey(app.Appointment_slot__c) && appointmentSlotMap.get(app.Appointment_slot__c) != null){
                app.Appointment_Start_Date_v1__c = appointmentSlotMap.get(app.Appointment_slot__c).Date__c;
                app.Appointment_Start_Time_v1__c = appointmentSlotMap.get(app.Appointment_slot__c).Start_Time__c;
                DateTime dt = DateTime.newInstance(app.Appointment_Start_Date_v1__c, app.Appointment_Start_Time_v1__c);
                app.Appointment_Start_Date_Time__c = dt;
            }else if(app.Status__c == 'Scheduled' && app.Appointment_slot__c == null){
                app.Appointment_Start_Date_v1__c = System.today();
                app.Appointment_Start_Time_v1__c = system.now().time();
            }else if(app.Status__c == 'To be Scheduled' && app.Appointment_slot__c == null){
                app.Appointment_Start_Date_v1__c = System.today();
                app.Appointment_Start_Time_v1__c = system.now().time();
            }
                 
        }
        
    }
}