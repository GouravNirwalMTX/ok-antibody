@isTest
public class CustomLookupControllerTest {
    
    @TestSetup
    static void makeData(){
     String accRecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByDeveloperName.get('Vital_Records').RecordTypeId;
     String testingSiteId = Schema.SObjectType.Contact.RecordTypeInfosByDeveloperName.get('Vital_Records_Internal_Users').RecordTypeId;
      Account acc1 = TestDataFactoryAccount.createAccount('Vital Site', accRecordTypeId , false);
        acc1.BillingCity = 'testCity';
        acc1.BillingCountry = 'testCountry';
        acc1.BillingState = 'testState';
        acc1.BillingStreet = 'testStreet';
        acc1.BillingPostalCode = '12345';
        acc1.Business_Start_Time__c = Time.newInstance(11, 0, 0, 0);
        acc1.Business_End_Time__c = Time.newInstance(19, 0, 0, 0);
        insert acc1;

        Contact con = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1',
            MiddleName = 'm',
            FirstName = 'testFirst',
            Email = 'testportalTestUserv1@gmail.com',
            Birthdate = Date.newInstance(1994, 5, 5),
            Gender__c = 'Male',
            MobilePhone = '(702)379-44151',
            Phone = '(702)379-4415',
            Street_Address1__c ='testStreet1',
            Street_Address_2__c ='testStreet2',
            City__c = 'TestCity',
            State__c = 'OH',
            ZIP__c = 12311,
            MailingCity = 'Tulsa',
            MailingStreet = '12 N Cheyenne Ave',
            MailingCountry = 'USA',
            MailingState = 'OK',
            MailingPostalCode = '74103',
            Testing_Site__c = acc1.Id,
            RecordTypeId = testingSiteId,
            Preferred_Date__c = Date.today(),
            Preferred_Time__c = Time.newInstance(16, 0, 0, 0),
            Source__c = 'MMS',
            Form_Submitted__c = false
        );
        insert con;
        Account acc2 = TestDataFactoryAccount.createAccount('Vital Site 2', accRecordTypeId , false);
        acc2.BillingCity = 'testCityVital';
        acc2.BillingCountry = 'testCountryVital';
        acc2.BillingState = 'testStateVital';
        acc2.BillingStreet = 'testStreetVital';
        acc2.BillingPostalCode = '12334';
        acc2.Business_Start_Time__c = Time.newInstance(12, 0, 0, 0);
        acc2.Business_End_Time__c = Time.newInstance(13, 0, 0, 0);
        insert acc2;
        /*Account acc3 = TestDataFactoryAccount.createAccount('Vital Site 3', accRecordTypeId , false);
        acc3.BillingCity = 'testCityVitalR';
        acc3.BillingCountry = 'testCountryRecord';
        acc3.BillingState = 'testStateR';
        acc3.BillingStreet = 'testStreetR';
        acc3.BillingPostalCode = '12887';
        acc3.Business_Start_Time__c = Time.newInstance(16, 0, 0, 0);
        acc3.Business_End_Time__c = Time.newInstance(17, 0, 0, 0);
        insert acc3;*/
    }
    
    public static testMethod void retrieveResultTest(){
        Contact c = [SELECT Id, AccountId FROM Contact LIMIT 1];
        Account a = [SELECT Id FROM Account WHERE Id !=: c.AccountId];
        
        List<SObject> accList = CustomLookupController.findRecords('Vital', 'Account', c.Id);
        System.assertEquals(accList.size()>0, true);
        CustomLookupController.createRelationship(c.Id, a.Id);
        
    }
}