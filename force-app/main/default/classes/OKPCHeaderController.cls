public without sharing class OKPCHeaderController {
    static Id userIds = UserInfo.getUserId();
    
    @AuraEnabled(cacheable=true)
    public static User getUserDetails(){
        List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId,Contact.Account.Testing_Site_Type__c
                               FROM User WHERE Id = :userIds];
        return userList[0];
        
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String, Object> getLoggedInTestingUserDetails(){
        Map<String, Object> response = new Map<String, Object>();
        List<User> userList = [SELECT Name, FirstName, LastName, CompanyName, Email, Phone, smallPhotoUrl, ContactId, Contact.AccountId,Contact.Account.Testing_Site_Type__c
                               FROM User WHERE Id = :userIds];
        response.put('user', userList[0]);
        if(!userList.isEmpty() && userList[0].Contact.Account.Testing_Site_Type__c != null){
            List<String> testingType = userList[0].Contact.Account.Testing_Site_Type__c.split(';');
            List<SelectOptionWrapper> testingSiteTypeOptions = new List<SelectOptionWrapper>();
            for(String type : testingType){
                SelectOptionWrapper option = new SelectOptionWrapper(type,type);
                testingSiteTypeOptions.add(option);
            }
            response.put('testingSiteType', testingType);
        }
        
        return response;
        
    }
    
    
    
    @AuraEnabled
    public static List<AntibodyWrapper> getAntibodyList(){
        Id conId;
        List<User> userList = [SELECT Id, contactId From User WHERE Id=: UserInfo.getuserid()];
        if(userList.size()>0){
            conId = userList[0].contactId;
        }
        List<AntibodyWrapper> antibodyTestingWrapperList = new List<AntibodyWrapper>();
        Id antibodyRecordTypeId = OKPCRecordTypeUtility.retrieveRecordTypeId('Antibody_Testing__c','Antibody_Testing');
        for(Antibody_Testing__c antibodyTesting : [SELECT Id, Appointment__c, Appointment__r.Appointment_Slot__r.Name, Patient__r.Name,Patient__r.Patient_Id__c, Status__c, Testing_Site__r.Name FROM Antibody_Testing__c WHERE Patient__c = :conId AND RecordTypeId =: antibodyRecordTypeId]){
            AntibodyWrapper wrapper = new AntibodyWrapper(antibodyTesting); 
            antibodyTestingWrapperList.add(wrapper);
        }
        System.debug('antibodyTestingList-->'+antibodyTestingWrapperList);
        if(antibodyTestingWrapperList.size()>0){
            return antibodyTestingWrapperList;
        }else{
            return null;
        }
        
    }
    
    public class AntibodyWrapper{
    
        @AuraEnabled public String status;
        @AuraEnabled public String slotName;
        @AuraEnabled public String patientName;
        @AuraEnabled public String testingSiteName;
        @AuraEnabled public String patientId;
        @AuraEnabled public String antibodyTestingId;
                

        public AntibodyWrapper(Antibody_Testing__c antibodyTesting){
            this.status = antibodyTesting.Status__c;
            this.slotName = antibodyTesting.Appointment__r.Appointment_Slot__r.Name;
            this.patientName = antibodyTesting.Patient__r.Name;
            this.testingSiteName = antibodyTesting.Testing_Site__r.Name;
            this.patientId = antibodyTesting.Patient__r.Patient_Id__c;
            this.antibodyTestingId = antibodyTesting.Id;
        }
    }
    
    public class SelectOptionWrapper{
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        
        public SelectOptionWrapper(string value, string label){
            this.value = value;
            this.label = label;
        }
    } 
    
}