({
    doInit: function (component, event, helper) {
        var type = component.get("v.portalType");
        console.log('type ___ ' + type);
        var loginlabel = '';

        if(type == 'citizenLobby'){
            loginlabel = $A.get("$Label.c.OKVR_Citizen_Lobby_Login_URL");
            component.set("v.imageRedirectURL", '/VitalRecordsCitizenPortal/s/');
            console.log(component.get("v.imageRedirectURL"));
        }else{
            loginlabel = $A.get("$Label.c.OKVR_Citizen_Consultation_Login_URL");
            component.set("v.imageRedirectURL", '/VitalRecordsConsultationPortal/s/');
            console.log(component.get("v.imageRedirectURL"));
        }
       
        component.set("v.loginURL", loginlabel);
        var registrationlabel = $A.get("$Label.c.OKLC_Registration_URL");
        component.set("v.registrationURL", registrationlabel);
        var supportlabel = $A.get("$Label.c.OKLC_Support_URL");
        component.set("v.supportURL", supportlabel);

        var action = component.get("c.getUserDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var userDetails = response.getReturnValue();
                console.log('userDetails: ', JSON.stringify(userDetails));
                component.set("v.userName", userDetails.Name);
                component.set("v.imageResource", userDetails.SmallPhotoUrl);
            }else{
                console.log('error: ');
            }
        });
        $A.enqueueAction(action);
    },
    redirectToLogin: function (component, event, helper)  {
        var type = component.get("v.portalType");
        
        var logoutRedirectURl = '';

        if(type == 'citizenLobby')
            logoutRedirectURl = $A.get("$Label.c.OKVR_Citizen_Lobby_Login_URL");
        else
            logoutRedirectURl = $A.get("$Label.c.OKVR_Citizen_Consultation_Login_URL");
        
        window.location.replace(logoutRedirectURl);
    }
})