({
    doInit: function (component, event, helper) {
        var loginlabel = $A.get("$Label.c.OKPC_Login_URL");
        component.set("v.loginURL", loginlabel);
        var registrationlabel = $A.get("$Label.c.OKPC_Registration_URL");
        component.set("v.registrationURL", registrationlabel);

        var action = component.get("c.getUserDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var userDetails = response.getReturnValue();
                console.log('userDetails: ', JSON.stringify(userDetails));
                component.set("v.userName", userDetails.Name);
                component.set("v.imageResource", userDetails.SmallPhotoUrl);
            }else{
                console.log('error: ');
            }
        });
        $A.enqueueAction(action);
    },

    redirectToLogin: function (component, event, helper)  {
        var logoutRedirectURl = $A.get("$Label.c.OKPC_Logout_Redirect_URL");
        window.location.replace(logoutRedirectURl);
    }
})