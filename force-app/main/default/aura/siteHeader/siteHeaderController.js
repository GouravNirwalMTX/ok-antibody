({
    doInit: function (component, event, helper) {
        var loginlabel = $A.get("$Label.c.OKLC_Login_URL");
        component.set("v.loginURL", loginlabel);
        var registrationlabel = $A.get("$Label.c.OKLC_Registration_URL");
        component.set("v.registrationURL", registrationlabel);
        var supportlabel = $A.get("$Label.c.OKLC_Support_URL");
        component.set("v.supportURL", supportlabel);

        var action = component.get("c.getUserDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var userDetails = response.getReturnValue();
                console.log('userDetails: ', JSON.stringify(userDetails));
                component.set("v.userName", userDetails.Name);
                component.set("v.imageResource", userDetails.SmallPhotoUrl);
            }else{
                console.log('error: ');
            }
        });
        $A.enqueueAction(action);
    },
    redirectToLogin: function (component, event, helper)  {
        var logoutRedirectURl = $A.get("$Label.c.OKLC_Logout_UR");
        window.location.replace(logoutRedirectURl);
    }
})